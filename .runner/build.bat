@echo off

call %rootRunnerDir%\timer-start.bat %~n0
call %rootRunnerDir%\npm-run.bat "npm run build"
call %rootRunnerDir%\timer-stop.bat %~n0

