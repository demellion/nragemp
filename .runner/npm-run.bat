@echo off

call %rootRunnerDir%\info.bat "trying %~1"

call %~1

IF ERRORLEVEL 1 (
    call %rootRunnerDir%\error.bat "can't %~1"
)

exit /b 0
