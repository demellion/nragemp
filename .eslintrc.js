/* eslint-env node */

module.exports = {
	root: true,
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaVersion: 8,
		sourceType: 'module',
		jsx: true
	},
	extends: ['standard', 'plugin:@typescript-eslint/recommended'],
	env: {
		browser: true,
		node: true,
		jest: true
	},
	plugins: [
		'sort-imports-es6-autofix',
		'react',
		'eslint-plugin-react',
		'typescript',
		'@typescript-eslint'
	],
	rules: {
		'no-empty': ['warn', { allowEmptyCatch: true }],
		'no-multi-spaces': ['error'],
		'space-before-function-paren': [
			'error',
			{
				anonymous: 'always',
				named: 'never',
				asyncArrow: 'always'
			}
		],
		quotes: ['error', 'single', { avoidEscape: true }],
		indent: ['error', 'tab', { SwitchCase: 1 }],
		'no-tabs': 0,
		semi: ['error', 'always', { omitLastInOneLineBlock: true }],
		'object-curly-spacing': ['error', 'always'],
		'no-unused-expressions': 0,
		'no-mixed-operators': 0,
		'generator-star-spacing': ['error', {
			before: false,
			after: true
		}],
		'brace-style': ['error', '1tbs', { allowSingleLine: false }],
		'no-lonely-if': ['error'],
		'object-shorthand': ['error'],
		'typescript/no-unused-vars': ['error'],
		'no-var': 'error',
		'eol-last': 0,
		'no-trailing-spaces': 0,
		'no-extra-boolean-cast': 0,

		'react/jsx-uses-react': 'error',
		'react/jsx-uses-vars': 'error',
		'@typescript-eslint/no-unused-vars': ['error'],
		'@typescript-eslint/ban-types': 0,
		'@typescript-eslint/no-explicit-any': 0,
		'@typescript-eslint/no-var-requires': 0,
		'@typescript-eslint/ban-ts-comment': 0,
		'@typescript-eslint/no-empty-function': 0,

		'max-len': [
			'warn',
			{
				code: 170,
				tabWidth: 4,
				ignoreComments: true,
				ignoreTrailingComments: true,
				ignoreUrls: true,
				ignoreStrings: true,
				ignoreTemplateLiterals: true,
				ignoreRegExpLiterals: true
			}
		]
	}
};
