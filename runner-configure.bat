@echo off

set rootDir=%~dp0
set rootRunnerDir=%rootDir%.runner

set clientDir=%rootDir%packages\client
set serverDir=%rootDir%packages\server
set editorDir=%rootDir%packages\editor
set frontDir=%rootDir%packages\front

exit /b 0
