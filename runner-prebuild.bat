@echo off

call %rootRunnerDir%\npm-run.bat "npm ci"
call %rootRunnerDir%\npm-run.bat "npm run bootstrap"

exit /b 0
