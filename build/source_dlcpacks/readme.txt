About This File
This plugin lets you "fix" RPFs of your dlcpacks on a server start in a completely automated way. Yes, it does support recursive RPFs!

 

Installation

Extract all archive files to the server root folder. No dependencies required, just the latest RAGE Multiplayer server.

 

Usage

Put your dlcpacks into /source_dlcpacks/ folder in the server root folder. Once you start the server it puts the proper dlcpacks into /client_packages/dlcpacks/.

 

Credits

Neodymium - GTA 5 modding research
rootcause - testing