# [NRage Framework]
NRage is a GTAV server framework based on [RAGE:MP] 1.1+ and was designed as superstructure to it to provide extended functionality needed for gameplay-focused development, without need to script core server components from scratch. Project is mostly written by me and contains ready-to-use backend and front-end solutions and wrappers for RAGE Engine in multiplayer. 

# Terms of Usage
NRage framework is licensed under [Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/)

> Portion of the material content do not fall under the license and determined as 3rd-party content that belong to respective authors and is exclusively used to build the target. 

> You are allowed to share and build upon the material but you have to give appropriate credit and not allowed to use this material in commercial reasons without permission granted.

> You can contact me via [email](dismaldem@gmail.com) to get permission to use material for commercial purposes.

# Project profile
1. Language: [TypeScript]  
2. Client: NodeJS  
3. Server: NodeJS  
4. Bundler: Webpack + Lerna  
5. Front: React + Redux + LESS  
6. API Version: 1.1+  
7. Netcode: ragerpc (primarily)  
8. Database: [mongoose]  
9. Localization: [i18next]

# Content short description:
1. Database.
2. Login/registration chains.
3. Multi-account control.
4. Multi-language support.
5. Character creation (WIP).
6. Streaming.
7. Game objects Managers (blips, colshapes, objects, markers, etc).
8. Scheduler.
9. Currencies.
10. Player characteristics.
11. Inventory.
12. Chat.
13. Leveling.
14. Vehicles.
15. Advanced NPCs.
16. HUD and Dialogs.
17. Keybinder. 
18. Admin tools.
19. Anti-cheat.
20. Referral system.
21. Shops.
22. World interaction system.
23. Cinematic camera manager.
24. Debug tools (Built-in IDE + complete CLI).
25. Gameplay areas control (safezones, turfs).
26. Natives wrapper.

# Quick start
1. Clone or download repository.
2. Copy [RAGE:MP] server-files files and paste in `packages/server/`.
3. Open terminal and:
    - `npm ci` install global dependencies
    - `npm run bootstrap` install dependencies for packages
    - `cp packages/server/.env.example packages/server/.env` create .env file
    - Edit .env with your configs.
    - `npm start` build and start `server`, `client` and `front`

# Development
1. Run `npm run dev` in terminal to start packages in dev mode
2. Run `npm run watch` in other terminal to make server restart with every changes
3. Run `npm run test:lint` to lint the code
4. Run `npm run build` to compile `server`, `client` and `front`

# Load order and modules
Server modules are located in [this folder](https://bitbucket.org/nragemp/server/src/master/packages/server/src/modules/) and are loaded with [server module loader](https://bitbucket.org/nragemp/server/src/master/packages/server/src/loaders/module.loader.ts)  
Client-side modules are located in [this folder](https://bitbucket.org/nragemp/server/src/master/packages/client/src/modules/) and are loaded with [client module loader](https://bitbucket.org/nragemp/server/src/master/packages/client/src/loaders/loader.ts)  
To get more details about loading chain you can check out **/loaders** folder in client and server packages

[NRage Framework]: <https://bitbucket.org/nragemp/server>
[RAGE:MP]: <https://rage.mp/>
[TypeScript]: <https://www.typescriptlang.org/>
[mongoose]: <https://www.npmjs.com/package/mongoose>
[i18next]: <https://www.npmjs.com/package/i18next>