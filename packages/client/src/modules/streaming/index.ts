import { setAnimation, setSpeech, setWalkingStyle, distance, isStreamed, fadeAlpha } from '../../core/main';

const movementClipSet = 'move_ped_crouched';
const strafeClipSet = 'move_ped_crouched_strafing';
const clipSetSwitchTime = 0.25;

/*
	Vehicle streaming sequence
*/
export const syncVehicleData = (entity: VehicleMp): void => {
	setTimeout(() => {
		if (!mp.vehicles.exists(entity)) return;
		fadeAlpha(entity, 255, 100);
		const locked = entity.getVariable('lockedState') || false;
		const engineState = entity.getVariable('engineState') || false;
		const mods = entity.getVariable('mods') || {};
		mp.game.streaming.requestCollisionAtCoord(entity.position.x, entity.position.y, entity.position.z);
		entity.setLoadCollisionFlag(true);
		entity.trackVisibility();
		entity.setEngineOn(engineState, true, false);
		locked ? entity.setDoorsLocked(2) : entity.setDoorsLocked(1);

		// Mods
		const {
			tireSmoke
		} = mods;
		if (tireSmoke) {
			entity.toggleMod(20, true);
			entity.setTyreSmokeColor(tireSmoke[0], tireSmoke[1], tireSmoke[2]);
		}
	}, 100);
};

/*
	Player streaming sequence
*/

export const syncPlayerData = (entity: PlayerMp): void => {
	setTimeout(() => {
		if (!mp.players.exists(entity)) return;
		const walkingStyle = entity.getVariable('walkingStyle') || null;
		const isCrouched = !!entity.getVariable('isCrouched');
		setWalkingStyle(entity, walkingStyle);
		if (isCrouched) {
			entity.setMovementClipset(movementClipSet, clipSetSwitchTime);
			entity.setStrafeClipset(strafeClipSet);
		}
	}, 100);
};

/*
	Ped streaming sequence
*/

export const syncPedData = (entity: PedMp): void => {
	setTimeout(() => {
		if (!mp.peds.exists(entity)) return;
		const streamData = entity.getVariable('streamData');
		if (!streamData) return;
		const { heading, animationSet } = streamData;
		if (animationSet) {
			const animDictionary = animationSet[0];
			const animation = animationSet[1];
			const flag = animationSet[2];
			setAnimation(entity, [animDictionary, animation], flag);
		}
		entity.setHeading(heading);
	}, 100);
};

// Streaming mainframe
mp.events.add('entityStreamIn', (entity) => {
	if (entity.type === 'vehicle') {
		entity.setInvincible(true);
		entity.setAlpha(1);
		syncVehicleData(entity);
		return;
	}
	if (entity.type === 'player') {
		syncPlayerData(entity);
		return;
	}
	if (entity.type === 'ped') {
		syncPedData(entity);
	}
});

// Blips distance streaming
setInterval(() => {
	mp.blips.forEachFast((thisBlip) => {
		const drawRange = thisBlip.getVariable('drawDistance') || 0;
		if (drawRange > 0) {
			const dist = distance(mp.players.local, thisBlip.getCoords(), false);
			if (dist > drawRange) return thisBlip.setAlpha(0);
			thisBlip.setAlpha(255);
		}
	});
}, 1000);

// Vehicles and players updates
mp.events.addDataHandler('mods', (entity) => {
	if (entity.type === 'vehicle' && isStreamed(mp.vehicles, entity)) {
		syncVehicleData(entity);
	}
});
mp.events.addDataHandler('customizationData', (entity) => {
	if (entity.type === 'player' && isStreamed(mp.players, entity)) {
		syncPlayerData(entity);
	}
});

// Crouching
mp.events.addDataHandler('isCrouched', (entity, value) => {
	if (entity.type === 'player' && isStreamed(mp.players, entity)) {
		if (value) {
			entity.setMovementClipset(movementClipSet, clipSetSwitchTime);
			entity.setStrafeClipset(strafeClipSet);
		} else {
			entity.resetMovementClipset(clipSetSwitchTime);
			entity.resetStrafeClipset();
		}
	}
});

// Walking style
mp.events.addDataHandler('walkingStyle', (entity, value) => {
	if (entity.type === 'player' && isStreamed(mp.players, entity)) {
		setWalkingStyle(entity, value);
	}
});

// Animation
mp.events.addDataHandler('animationData', (entity, value) => {
	if (entity.type === 'player' && isStreamed(mp.players, entity)) {
		entity.clearTasksImmediately();
		setAnimation(entity, value[0], value[1], value[2], value[3]);
	}
});

// Speech (Taunts)
mp.events.addDataHandler('entitySpeech', (entity, value) => {
	if (entity.type === 'player' && isStreamed(mp.players, entity)) {
		setSpeech(entity, value[0], value[1], value[2]);
	}
});
