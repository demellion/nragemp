import { NREvents } from '../../../../common/enums/nrEvents';
import { nr } from '../../core/wrapper';
import {
	checkLoS,
	distance,
	getCursorObject,
	getPlayerName, getSafezoneName,
	getServerName,
	getServerTime,
	getStreetName,
	getZoneName,
	isAiming, isStreamed,
	notification
} from '../../core/main';
import rpc from 'rage-rpc';
import { HudInfo } from '../../../../common/types/hud';
import { defaultDateFormat } from '../../../../common/constants/date';
import { DebugData } from '../../../../common/types/debug';
import { Routes } from 'common/constants/routes';

interface ToggleCursorProps {
	freezeControls: boolean,
	state: boolean,
}

const _toggleCursor = (freezeControls: boolean, state: boolean): void => {
	mp.gui.cursor.show(freezeControls, state);
};

export const toggleCursor = _toggleCursor;

nr.rpc.register(NREvents.toggleCursor, (props: ToggleCursorProps) => {
	_toggleCursor(props.freezeControls, props.state);
});

export const showCursor = (): void => {
	_toggleCursor(true, true);
};

nr.rpc.register(NREvents.showCursor, showCursor);

export const hideCursor = (): void => {
	_toggleCursor(false, false);
};

nr.rpc.register(NREvents.hideCursor, hideCursor);

export const setRoute = async (route: Routes | string, params?: { [key: string]: any }): Promise<void> => {
	await nr.rpc.callBrowser(global.hud, NREvents.setRoute, { route, params });
};

export const setTimeCycleModifier = (modifierName: string): void => {
	mp.game.graphics.setTimecycleModifier(modifierName);
};

export const displayRadar = (state: boolean): void => {
	mp.game.ui.displayRadar(state);
};

export const showChat = (state: boolean): void => {
	mp.gui.chat.show(state);
};

rpc.register('displayNotification', (args: any) => {
	const title = args.title;
	const message = args.message;
	const icon = args.icon;
	const long = args.long;
	notification(title, message, icon, long);
	return true;
});

rpc.register('receiveDebugData', (): DebugData => {
	if (!global.debugHUD) return null;
	const player = mp.players.local;
	if (!player) return null;
	const pos = player.position;
	const vehicle = player.vehicle;
	const ownedVehicle = player.getVariable('ownedVehicle') || null;
	const rentedVehicle = player.getVariable('rentedVehicle') || null;
	const time = getServerTime().format('HH:mm:ss');
	const cursorObject = getCursorObject(300, 21);
	const npc = global.currentNPC;
	global.cursorObject = cursorObject;

	try {
		return {
			time,
			dimension: player.dimension,
			position: `${pos.x}, ${pos.y}, ${pos.z}`,
			heading: player.getHeading(),
			character: player.getVariable('class'),
			stats: player.getVariable('stats'),
			wallet: {
				cash: player.getVariable('wallet').cash || -1,
				bank: player.getVariable('wallet').bank || -1,
				nrPoints: player.getVariable('points') || -1
			},
			level: player.getVariable('levelData') || null,
			wantedLevel: player.getVariable('wantedLevel') || -1,
			vehicle: vehicle && vehicle.getVariable('class'),
			vehicleRPM: vehicle && vehicle.rpm,
			ownedVehicle: ownedVehicle && `${ownedVehicle.getVariable('key')} (${ownedVehicle.getVariable('class')})`,
			rentedVehicle: rentedVehicle && rentedVehicle.getVariable('class'),
			cursorObject: cursorObject && `${mp.game.invoke('0x9F47B058362C84B5', cursorObject.entity)} (hash)`,
			currentNPC: `${npc ? typeof npc === 'number' ? npc : npc.type : null} (${npc ? typeof npc === 'number' ? '' : npc.remoteId : null})`,
			currentNPCData: `${npc ? typeof npc === 'number' ? '' : npc.getVariable('streamData') ? JSON.stringify(npc.getVariable('streamData').data) : null : null}`,
			currentAction: global.currentAction,
			currentActionName: global.currentActionName,
			isInSafeZone: `${global.isInSafezone} (${global.safezoneLevel})`,
			isFreeAiming: mp.game.player.isFreeAiming(),
			isInMeleeCombat: player.isInMeleeCombat(),
			isInScenario: player.isUsingAnyScenario(),
			isFalling: player.isFalling(),
			street: getStreetName(),
			zone: getZoneName(),
			interiorID: mp.game.interior.getInteriorAtCoords(pos.x, pos.y, pos.z)
		};
	} catch (error) {
		return null;
	}
});
rpc.register('getHUDInfo', (): HudInfo => {
	const player = mp.players.local;
	if (!player) return null;
	const ownedVehicle = player.getVariable('ownedVehicle') || null;
	const mods = ownedVehicle?.getVariable('mods') || {};
	let vehicle: VehicleMp = null;
	if (mp.vehicles.exists(ownedVehicle)) {
		if (mods?.remote) vehicle = ownedVehicle;
	}
	if (mp.vehicles.exists(player.vehicle)) {
		vehicle = player.vehicle;
	}
	return {
		isPlayable: global.isPlayable,
		name: player.name,
		id: player.remoteId,
		online: mp.players.length,
		wallet: {
			cash: player.getVariable('wallet').cash || 0,
			bank: player.getVariable('wallet').bank || 0,
			nrPoints: player.getVariable('points') || 0
		},
		area: getZoneName(),
		safeZoneLevel: global.safezoneLevel,
		safeZoneName: getSafezoneName(global.safezoneLevel),
		street: getStreetName(),
		speaking: !mp.voiceChat.muted,
		time: getServerTime().format(defaultDateFormat),
		servername: getServerName(),
		currentActionName: global.currentActionName || null,
		isVehicle: !!vehicle,
		vehicleSpeed: vehicle ? vehicle.getSpeed() * 3.6 : null,
		vehicleFuel: vehicle ? vehicle.getVariable('fuel') || 0 : null,
		vehicleLocked: vehicle ? vehicle.getVariable('lockedState') || false : null,
		vehicleEngine: vehicle ? vehicle.getVariable('engineState') || false : null
	};
});

/*
	Nametags for peds and players
*/
const maxDistancePlayer = 200; // Not meters, some penguins alike centimeters
const maxDistancePed = 15;
const maxDistanceMarker = 15;
const graphics = mp.game.graphics;
const screenRes = graphics.getScreenResolution(0, 0);

// Scan with interval
let allPeds: PedMp[] = [];
let allMarkers: MarkerMp[] = [];
setInterval(() => {
	const player = mp.players.local;
	allPeds = mp.peds.toArray().filter((thisEntity) => {
		return distance(player, thisEntity) <= maxDistancePed;
	});
	allMarkers = mp.markers.toArray().filter((thisEntity) => {
		return distance(player, thisEntity) <= maxDistanceMarker && thisEntity.remoteId !== 65535;
	});
}, 100);

mp.events.add('render', (nametags) => {
	if (mp.gui.cursor.visible) return;
	const player = mp.players.local;
	if (!player) return;
	if (player.getHealth() === 0) return;

	// Debug only section
	if (!global.hud.active) {
		const { adminLevel } = mp.storage.data.adminLevel || { adminLevel: 0 };
		if (adminLevel === 3) return;
		graphics.drawText('NRage Framework', [0.5, 0.3], {
			font: 4,
			centre: true,
			color: [102, 102, 102, 10],
			scale: [5, 5],
			outline: false
		});
		return;
	}

	if (global.debugHUD) {
		if (isAiming() && player.weapon === 2939590305) {
			const cursorObject = getCursorObject(300, 21);
			if (cursorObject) {
				const pPos = player.getBoneCoords(58867, 0, 0, 0.025);
				const tPos = cursorObject.position;
				if (tPos) mp.game.graphics.drawLine(pPos.x, pPos.y, pPos.z, tPos.x, tPos.y, tPos.z, 255, 255, 255, 255);
			}
		}
		// Draw vehicles info
		mp.vehicles.forEachFast((thisVehicle) => {
			if (!thisVehicle.doesExist()) return;
			const distanceCurrent = distance(player, thisVehicle);
			if (distanceCurrent <= 300) {
				const position3D = thisVehicle.position;
				const position2D = mp.game.graphics.world3dToScreen2d(position3D.x, position3D.y, position3D.z + 0.25);
				if (position2D) {
					const posX = position2D.x;
					let posY = position2D.y;
					let scale = (distanceCurrent / 300);
					if (scale < 0.6) scale = 0.6;
					posY -= scale * (0.005 * (screenRes.y / 1080));
					const owner = thisVehicle.getVariable('owner');
					let text: string;
					if (owner) {
						text = `~b~${thisVehicle.getVariable('class') || 'Car'}\n~s~${owner.name} (${owner.remoteId})\n${owner.getVariable('rgscId') || 'No rgscId?'}~c~\n${thisVehicle.getVariable('fuel').toFixed(2) || 0}L`;
					} else {
						text = '~r~No vehicle owner!';
					}
					graphics.drawText(text, [posX, posY], {
						font: 4,
						centre: true,
						color: [255, 255, 255, 255],
						scale: [0.4, 0.4],
						outline: true
					});
				}
			}
		});
	}

	if (!global.isPlayable && !global.noClipCamera) return;

	nametags.forEach((nametag: any[]) => {
		const [player, x, y, distance] = nametag;
		if (!global.debugHUD && !checkLoS(mp.players.local, player)) return;
		const distanceMax = global.debugHUD ? 18000 : maxDistancePlayer;
		if (distance <= distanceMax) {
			const crouched = !!player.getVariable('isCrouched') && !global.debugHUD;
			graphics.drawText(getPlayerName(player), [x, y], {
				font: 4,
				centre: true,
				color: [255, 255, 255, crouched ? 0 : 255],
				scale: [0.4, 0.4],
				outline: true
			});
		}
	});

	allPeds.forEach((thisPed) => {
		const streamData = thisPed.getVariable('streamData');
		if (!streamData) return;
		const { name } = streamData;
		const position2D = mp.game.graphics.world3dToScreen2d(thisPed.position.x, thisPed.position.y, thisPed.position.z + 1);
		if (position2D) {
			const distanceCurrent = distance(player, thisPed);
			const posX = position2D.x;
			let posY = position2D.y;
			let scale = (distanceCurrent / maxDistancePed);
			if (scale < 0.6) scale = 0.6;
			posY -= scale * (0.02 * (screenRes.y / 1080));
			if (name) {
				graphics.drawText(name, [posX, posY], {
					font: 4,
					centre: true,
					color: [255, 255, 255, 255],
					scale: [0.4, 0.4],
					outline: true
				});
			}
		}
	});
	allMarkers.forEach((thisMarker) => {
		const streamData = thisMarker.getVariable('streamData');
		if (!streamData) return;
		const { name } = streamData;
		const position2D = mp.game.graphics.world3dToScreen2d(thisMarker.position.x, thisMarker.position.y, thisMarker.position.z + 1);
		if (position2D) {
			const distanceCurrent = distance(player, thisMarker);
			const posX = position2D.x;
			let posY = position2D.y;
			let scale = (distanceCurrent / maxDistanceMarker);
			if (scale < 0.6) scale = 0.6;
			posY -= scale * (0.02 * (screenRes.y / 1080));
			if (name) {
				graphics.drawText(name, [posX, posY], {
					font: 4,
					centre: true,
					color: [255, 255, 255, 255],
					scale: [0.4, 0.4],
					outline: true
				});
			}
		}
	});
	if (global.currentNPC && typeof global.currentNPC !== 'number' && !global.debugHUD) {
		if (!isStreamed(mp.players, global.currentNPC) && !isStreamed(mp.vehicles, global.currentNPC)) return;
		if (['vehicle', 'player'].includes(global.currentNPC.type)) {
			const position3D = global.currentNPC.position;
			const position2D = mp.game.graphics.world3dToScreen2d(position3D.x, position3D.y, position3D.z + 0.25);
			if (position2D) {
				graphics.drawText('E', [position2D.x, position2D.y], {
					font: 2,
					centre: true,
					color: [240, 240, 240, 255],
					scale: [0.8, 0.8],
					outline: true
				});
			}
		}
	}
});
