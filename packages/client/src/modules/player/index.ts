// Event called after the player has loaded all client scripts
import {
	changeStat,
	disableFiring,
	getDeathCause,
	getServerTime, isAiming,
	knockOut,
	moveSkyFinished,
	moveSkyFrom,
	moveSkyTo,
	notification,
	playSoundFrontend,
	removeTurfs,
	setActionCooldown,
	setDesiredWantedLevel,
	setWalkingStyle,
	toggleRagdoll
} from '../../core/main';
import rpc from 'rage-rpc';
import { localize } from 'common/index';
import { sleep } from 'common/pipes/sleep';
import { toggleCursor } from '../hud';
import { closeDialog } from '../dialogs';

// Init player
const player = mp.players.local;
player.position = new mp.Vector3(0, 0, 0);
mp.nametags.enabled = false; // Obviously
mp.game.vehicle.defaultEngineBehaviour = false; // Engine cannot be started
mp.game.ui.displayAreaName(false); // Do not display area name
player.setConfigFlag(429, true); // Disable player attempts to run engine causing glitch in the matrix
player.setConfigFlag(35, false); // Disable player using helmet riding a bike
player.setMaxHealth(300); // Set health to 200
removeTurfs();

// Death sequence
mp.events.add(RageEnums.EventKey.PLAYER_DEATH, async (player, reason: number, killer): Promise<void> => {
	const dimension = player.dimension;
	const deathCause = getDeathCause(reason);
	const actualPosition = player.position;
	mp.game.graphics.startScreenEffect('DeathFailNeutralIn', 5000, true);
	const time = getServerTime().toArray();

	switch (true) {
		case (global.isInSafezone && global.safezoneLevel === 0): {
			killer ? mp.console.logWarning(`Knocked out by ${killer.remoteId}: ${deathCause}`) : mp.console.logWarning(`Knocked out: ${deathCause}`);
			await rpc.callServer('playerRespawn', { spawncase: 'safezone' });
			mp.game.graphics.stopScreenEffect('DeathFailNeutralIn');
			break;
		}
		case (!global.isKnocked && dimension === 0): {
			killer ? mp.console.logWarning(`Knocked out by ${killer.remoteId}: ${deathCause}`) : mp.console.logWarning(`Knocked out: ${deathCause}`);
			await knockOut();
			player.position = actualPosition;
			break;
		}

		// Actually die
		default: {
			killer ? mp.console.logWarning(`Killed by ${killer.remoteId}: ${deathCause}`) : mp.console.logWarning(`Died: ${deathCause}`);
			playSoundFrontend(-1, 'MP_Flash', 'WastedSounds');
			global.isPlayable = false;
			await sleep(3500);
			mp.game.graphics.stopScreenEffect('DeathFailNeutralIn');
			moveSkyFrom(player.handle, 1);
			await sleep(1500);
			player.freezePosition(true);
			await rpc.callServer('playerRespawn', { spawncase: 'death' });
			await sleep(3000);
			player.freezePosition(false);
			moveSkyTo(player.handle);
			while (!moveSkyFinished()) await sleep(1);
			await sleep(1000);
			global.isPlayable = true;
			global.isKnocked = false;
		}
	}
	mp.game.time.setClockTime(time[3], time[4], time[5]);
});

/* Damage control

	Developer Note:
		1. Does not trigger upon environmental damage (falling, driving into wall).
		2. Does not trigger upon damaging yourself with a weapon (anything like grenade or RPG).
		3. Does not trigger against any vehicles that has no player in it.

	Examples:

	mp.events.add(RageEnums.EventKey.INCOMING_DAMAGE, (sourceEntity, sourcePlayer, targetEntity, weapon, boneIndex, damage) => {
		mp.console.logInfo(`IncomingDamage: ${sourcePlayer.getVariable('class')} <- ${targetEntity.getVariable('class')}, with ${weapon} to ${boneIndex} = ${damage}`);
	});
	mp.events.add(RageEnums.EventKey.OUTGOING_DAMAGE, (sourceEntity, sourcePlayer, targetEntity, weapon, boneIndex, damage) => {
		mp.console.logInfo(`OutgoingDamage: ${targetEntity.getVariable('class')} -> ${sourcePlayer.getVariable('class')}, with ${weapon} to ${boneIndex} = ${damage}`);
	});
*/
mp.events.add(RageEnums.EventKey.OUTGOING_DAMAGE, (_, sourcePlayer, targetEntity) => {
	if (targetEntity.type === 'vehicle') return true; // Disable sync of damage for vehicles to reduce broadcast.
	if (targetEntity === mp.players.local && sourcePlayer.type === 'player') {
		const thisPlayerRank = sourcePlayer.getVariable('serviceRank') || 0;
		if (thisPlayerRank > 0) {
			setDesiredWantedLevel(3);
			return;
		}
		setDesiredWantedLevel(2);
	}
});

// Specific
mp.events.add('render', () => {
	const player = mp.players.local;
	const vehicle = player.vehicle;

	// Playability switches
	if (!global.isPlayable || global.dialog) {
		if (!mp.game.ui.isPauseMenuActive()) {
			mp.game.controls.disableAllControlActions(32);
		}
		mp.game.ui.displayHud(false);
		mp.game.ui.displayRadar(false);
	} else {
		mp.game.ui.displayRadar(true);
		mp.game.ui.displayHud(true);
	}

	// HUD Elements
	[4, 6, 7, 8, 9].forEach((value) => {
		mp.game.ui.hideHudComponentThisFrame(value);
	});
	if (global.dialog) toggleCursor(true, true);
	if (global.dialog && !global.isPlayable) closeDialog().then();

	// Player characteristics
	mp.game.player.setHealthRechargeMultiplier(0.0); // Remove regeneration

	// Disable controls
	if (vehicle) {
		// Vehicle weapons disabler
		if ([884483972].includes(vehicle.model)) {
			// Disable oppressor things
			mp.game.controls.disableControlAction(32, 68, true); // No fire
			mp.game.controls.disableControlAction(32, 70, true); // No fire
			mp.game.controls.disableControlAction(27, 354, true); // No wings
		}
	}
	mp.game.controls.disableControlAction(32, 36, true); // Remove vanilla crouch.

	// Disable unwanted proofs
	if (global.isInSafezone && global.safezoneLevel === 0) {
		player.setProofs(true, true, true, true, true, true, true, true); // Enable safezones invincibility.
	} else {
		player.setProofs(false, false, false, !player.isFalling(), false, false, false, false); // Enable vehicle proof only.
	}

	// Disable aiming and shooting section
	// @ts-ignore
	const aimTargetHandle = mp.game.player.getEntityIsFreeAimingAt();
	const isInSafezone = global.isInSafezone && global.safezoneLevel < 1;
	switch (true) {
		case (isInSafezone): return disableFiring();
		case ([2939590305].includes(player.weapon)): return disableFiring(); // NRay Gun
		case (typeof aimTargetHandle === 'number' && !!mp.peds.atHandle(aimTargetHandle)): return disableFiring();
		case (!!global.currentAction && !['#vehicle', '#player'].includes(global.currentAction)): return disableFiring();
	}
});

mp.events.add(RageEnums.EventKey.PLAYER_WEAPON_SHOT, (_, targetEntity) => {
	const player = mp.players.local;
	if (targetEntity !== player) return;
	if (!!player.getVariable('isCrouched')) {
		rpc.callServer('toggleCrouch', {}, { noRet: true });
		setActionCooldown(1);
	}

	// Wanted levels
	setDesiredWantedLevel(1);
});

// Stats rotation
const hungerPeriod = 60 * 10;
const thirstPeriod = 60 * 8;
let hungerPeriodCurrent = hungerPeriod;
let thirstPeriodCurrent = thirstPeriod;
setInterval(() => {
	hungerPeriodCurrent--;
	thirstPeriodCurrent--;
	const player = mp.players.local;
	if (!player) return;
	const stats = mp.players.local.getVariable('stats');
	if (!stats) return;
	const { hunger, thirst } = stats;

	// Stamina control
	if (hunger === 0 || thirst === 0 || global.isKnocked) {
		setWalkingStyle(player, 7);
		mp.game.cam.setCamEffect(2); // Have fun being exhausted
		if (player.isSprinting()) {
			notification(`~HUD_COLOUR_RADAR_DAMAGE~${localize.t('GENERIC.EXHAUSTED')}`, localize.t('GENERIC.EXHAUSTEDTEXT'));
			if (Math.random() < 0.5) toggleRagdoll(true, Math.random() * 1000);
		}
		// @ts-ignore
		if (player.isJumping() || player.isClimbing()) {
			notification(`~HUD_COLOUR_RADAR_DAMAGE~${localize.t('GENERIC.EXHAUSTED')}`, localize.t('GENERIC.EXHAUSTEDTEXT'));
			toggleRagdoll(true, Math.random() * 1000);
		}
		return;
	}
	mp.game.cam.setCamEffect(0); // Set camera back to normal
	mp.game.player.restoreStamina(100); // Remove stamina

	if (hungerPeriodCurrent <= 0) {
		hungerPeriodCurrent = hungerPeriod;
		changeStat('hunger', -1);
	}
	if (thirstPeriodCurrent <= 0) {
		thirstPeriodCurrent = thirstPeriod;
		changeStat('thirst', -1);
	}
}, 1000);

// Wanted levels rotation
setInterval(() => {
	const player = mp.players.local;
	if (!player) return;

	// Green zone weapon aiming
	if (global.safezoneLevel === 0 && player.dimension === 0) {
		if (isAiming()) {
			const weapon = player.weapon;
			const safeWeapons = ['weapon_flashlight', 'weapon_unarmed', 'weapon_snowball', 'weapon_ball'].map(value => mp.game.joaat(value));
			if (!safeWeapons.includes(weapon)) {
				setDesiredWantedLevel(1, false);
			}
		}
	}
}, 250);