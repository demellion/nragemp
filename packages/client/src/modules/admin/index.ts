// Admin related
import { nr } from '../../core/wrapper';
import { getCrossProduct, getNormalizedVector } from '../../core/main';

global.adminFrozen = false;
global.adminLastWaypoint = null;

// Admin RPC for client events
nr.rpc.register('adminGateway', (args: any) => {
	const { eventName } = args;
	const player = mp.players.local;
	switch (eventName) {
		case 'freezePosition': {
			if (!global.adminFrozen) mp.console.logWarning('NRage AdminOS: Administator used freeze on you');
			player.freezePosition(!global.adminFrozen);
			global.adminFrozen = !global.adminFrozen;
			return global.adminFrozen;
		}
	}
});

// Teleport to blip handler
mp.events.add('playerCreateWaypoint', (position) => {
	global.adminLastWaypoint = position;
});

// Noclip camera
mp.events.add('render', () => {
	if (!global.noClipCamera) return;
	if (mp.gui.cursor.visible) return;
	global.isKnocked = false;
	global.isPlayable = false;
	const noClipCamera = global.noClipCamera;
	const controlModifier = mp.keys.isDown(17);
	const shiftModifier = mp.keys.isDown(16);
	const rot = noClipCamera.getRot(2);
	const multiplier = shiftModifier ? 3 : controlModifier ? 0.5 : 1;
	const rightAxisX = mp.game.controls.getDisabledControlNormal(0, 220);
	const rightAxisY = mp.game.controls.getDisabledControlNormal(0, 221);
	const leftAxisX = mp.game.controls.getDisabledControlNormal(0, 218);
	const leftAxisY = mp.game.controls.getDisabledControlNormal(0, 219);
	const pos = noClipCamera.getCoord();
	const rr = noClipCamera.getDirection();
	const vector = new mp.Vector3(0, 0, 0);
	vector.x = rr.x * leftAxisY * multiplier * 0.25;
	vector.y = rr.y * leftAxisY * multiplier * 0.25;
	vector.z = rr.z * leftAxisY * multiplier * 0.25;
	const rightVector = getCrossProduct(
		getNormalizedVector(rr),
		getNormalizedVector(new mp.Vector3(0, 0, 1))
	);
	const zMovement = mp.keys.isDown(69) ? 0.05 : mp.keys.isDown(81) ? -0.05 : 0;

	// Set
	rightVector.x *= leftAxisX * (0.25 * multiplier);
	rightVector.y *= leftAxisX * (0.25 * multiplier);
	rightVector.z *= leftAxisX * (0.25 * multiplier);
	mp.players.local.heading = rr.z;
	noClipCamera.setCoord(
		pos.x - vector.x + (rightVector.x * 0.5),
		pos.y - vector.y + (rightVector.y * 0.5),
		pos.z - vector.z + (rightVector.z * 0.5) + zMovement + (zMovement * multiplier)
	);
	noClipCamera.setRot(
		rot.x + rightAxisY * -5,
		0.0,
		rot.z + rightAxisX * -5,
		2
	);
	// Set player pos
	mp.players.local.position = new mp.Vector3(
		pos.x + vector.x,
		pos.y + vector.y,
		pos.z + vector.z
	);
	mp.players.local.setHealth(mp.players.local.getMaxHealth());
});
