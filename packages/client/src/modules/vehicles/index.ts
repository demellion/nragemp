// Fucking re-enable that engine you just turned off.
import { createBlip, playSound3D, setBlipText } from '../../core/main';
import rpc from 'rage-rpc';
import { localize } from 'common/index';

export const keepEngineState = (vehicle: VehicleMp): void => {
	if (!vehicle) return;
	const player = mp.players.local;
	vehicle.setEngineOn(!!vehicle.getVariable('engineState'), true, false);
	if (!!player.getVariable('isCrouched')) {
		rpc.callServer('toggleCrouch', {}, { noRet: true });
	}
};
mp.events.add('playerLeaveVehicle', keepEngineState);
mp.events.add('playerEnterVehicle', keepEngineState);

// Lock sound
const playVehicleLockSound = (vehicle: Vector3Mp, state: boolean): void => {
	if (state) {
		// TODO: Что-нибудь нормальное, ага?
		playSound3D('CONFIRM_BEEP', 'HUD_MINI_GAME_SOUNDSET', vehicle, 1);
	} else {
		playSound3D('CONFIRM_BEEP', 'HUD_MINI_GAME_SOUNDSET', vehicle, 1);
	}
};
mp.events.add('broadcast.lockSound', playVehicleLockSound);

// Share the current waypoint with vehicle passengers
mp.events.add('playerCreateWaypoint', (position) => {
	const vehicle = mp.players.local.vehicle;
	if (vehicle) {
		rpc.callServer('broadcast.shareWaypoint', { position, vehicle });
	}
});
rpc.register('broadcast.createSharedWaypoint', (position) => {
	mp.game.ui.setNewWaypoint(position.x, position.y);
});

// Vehicle GPS
const GPSBlip = createBlip(672, new mp.Vector3(0, 0, 0), 4, false, 1);
mp.events.add('render', () => {
	const player = mp.players.local;
	if (!player) return;
	const vehicle: VehicleMp = player.getVariable('ownedVehicle');
	if (!vehicle) return GPSBlip.setAlpha(0);
	const mods = vehicle.getVariable('mods') || {};
	if (!mods.gps) return GPSBlip.setAlpha(0);
	const position = vehicle.position;
	const rotation = vehicle.getHeading();
	GPSBlip.setPosition(position.x, position.y, position.z);
	GPSBlip.setRotation(rotation);
	GPSBlip.setAlpha(255);
	setBlipText(GPSBlip, localize.t('BLIPS.YOURVEHICLE'));
});