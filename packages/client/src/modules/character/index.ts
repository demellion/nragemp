import { nr } from '../../core/wrapper';
import { toggleCursor } from '../hud';
import { CameraNR, camerasManager } from '../cameras';
import { playSoundFrontend } from '../../core/main';
import { selectCameraPos, selectCameraRotation, skyCameraPos, skyCameraRotation } from '../account';
import { CustomizationData, FaceFeatures, Gender, HeadOverlays } from 'common/types/character/customizationData';
import { sleep } from 'common/pipes/sleep';
import { NREvents } from 'common/enums/nrEvents';
import { defaultCustomizationData, faceFeatures, headOverlays } from 'common/constants/customizationData';

const createCharacterPos = new mp.Vector3(-797.80517578125, 327.2848815917969, 220.43849182128906);
const createCharacterCamraPos = new mp.Vector3(-797.80517578125, 329.2848815917969, 221);
const createCharacterCamraRotation = new mp.Vector3(-5, 0, 180);

let camera: CameraNR;

nr.rpc.register('startCharacterCreation', async () => {
	toggleCursor(true, true);
	const player = mp.players.local;
	mp.game.streaming.requestIpl('apa_v_mp_h_03_a');

	camera = camerasManager.createCamera('auth', 'default', selectCameraPos, selectCameraRotation, 50);
	playSoundFrontend(-1, 'CHECKPOINT_PERFECT', 'HUD_MINI_GAME_SOUNDSET');
	mp.game.graphics.startScreenEffect('CamPushInNeutral', 4000, false);
	camerasManager.setActiveCameraWithInterp(camera, skyCameraPos, skyCameraRotation, 2000, 0, 0);
	await sleep(500);
	mp.game.cam.doScreenFadeOut(1500);
	await sleep(2000);

	player.position = createCharacterPos;
	player.setHeading(0);
	player.clearTasksImmediately();

	camera = camerasManager.createCamera(
		'auth',
		'default',
		skyCameraPos,
		new mp.Vector3(0, 0, 0),
		50
	);
	mp.game.cam.doScreenFadeIn(1000);
	mp.game.graphics.startScreenEffect('SwitchHUDOut', 3000, false);
	playSoundFrontend(-1, 'FocusIn', 'HintCamSounds');

	camera = camerasManager.createCamera(
		'createCharacter',
		'default',
		createCharacterCamraPos,
		createCharacterCamraRotation,
		60
	);

	camerasManager.setActiveCamera(camera, true);

	// mp.game.streaming.removeIpl('apa_v_mp_h_04_a');
});

// const validTorsoIds = [
// 	// male
// 	[0, 0, 2, 14, 14, 5, 14, 14, 8, 0, 14, 15, 12],
// 	// female
// 	[0, 5, 2, 3, 4, 4, 5, 5, 5, 0]
// ];

export class Character {
	private player = mp.players.local;
	gender: Gender;
	father: number;
	mother: number;
	similar: number;
	hair: number;
	hairPrimaryColor: number;
	hairSecondaryColor: number;
	facialHairColor: number;
	eyeColor: number;
	faceFeatures: FaceFeatures;
	headOverlays: HeadOverlays;

	constructor() {
		this.gender = Gender.male;
		this.father = defaultCustomizationData.father;
		this.mother = defaultCustomizationData.mother;
		this.similar = defaultCustomizationData.similar;
		this.hair = defaultCustomizationData.hair;
		this.hairPrimaryColor = defaultCustomizationData.hairPrimaryColor;
		this.hairSecondaryColor = defaultCustomizationData.hairSecondaryColor;
		this.facialHairColor = defaultCustomizationData.facialHairColor;
		this.eyeColor = defaultCustomizationData.eyeColor;
		this.headOverlays = headOverlays;
		this.faceFeatures = faceFeatures;
	}

	public update(data: CustomizationData): void {
		const {
			gender,
			father,
			mother,
			similar,
			hair,
			hairPrimaryColor,
			hairSecondaryColor,
			facialHairColor,
			eyeColor,
			headOverlays,
			faceFeatures
		} = data;

		this.updateGender(gender);
		this.updateParents(father, mother, similar);
		this.updateHeadOverlays(headOverlays);
		this.updateFaceFeatures(faceFeatures);
		this.updateHair(hair);
		this.updateHairColor(hairPrimaryColor, hairSecondaryColor);
		this.updateFacialHairColor(facialHairColor);
		this.updateEyeColor(eyeColor);
	}

	private updateGender(gender: Gender) {
		if (gender === this.gender) {
			return;
		}
		this.gender = gender;
		switch (gender) {
			case Gender.male: {
				this.player.model = mp.game.joaat('mp_m_freemode_01');
				break;
			}
			case Gender.female: {
				this.player.model = mp.game.joaat('mp_f_freemode_01');
				break;
			}
		}
	}

	private updateParents(father: number, mother: number, similar: number) {
		if (father === undefined || mother === undefined || similar === undefined) {
			return;
		}
		if (father === this.father && mother === this.mother && similar === this.similar) {
			return;
		}
		this.father = father;
		this.mother = mother;
		this.similar = similar;

		this.player.setHeadBlendData(
			mother,
			father,
			0,
			mother,
			father,
			0,
			similar,
			similar,
			0.0,
			true
		);
	}

	private updateHeadOverlays(data: HeadOverlays) {
		Object.entries(data).forEach(([key, value], index) => {
			const overlaysKey = key as keyof HeadOverlays;
			if (value !== undefined && value !== this.headOverlays[overlaysKey]) {
				this.headOverlays[overlaysKey] = value;
				this.player.setHeadOverlay(index, value, 100, 0, 0);
			}
		});
	}

	private updateFaceFeatures(data: FaceFeatures) {
		Object.entries(data).forEach(([key, value], index) => {
			const faceKey = key as keyof FaceFeatures;
			if (value !== undefined && value !== this.faceFeatures[faceKey]) {
				this.faceFeatures[faceKey] = value;
				this.player.setFaceFeature(index, value);
			}
		});
	}

	private updateHair(hair: number) {
		if (hair === undefined || hair === this.hair) {
			return;
		}
		this.hair = hair;
		this.player.setComponentVariation(2, hair, 0, 0);
	}

	private updateHairColor(hairPrimaryColor: number, hairSecondaryColor: number) {
		if (hairPrimaryColor === undefined || hairSecondaryColor === undefined) {
			return;
		}
		if (hairPrimaryColor === this.hairPrimaryColor && hairSecondaryColor === this.hairSecondaryColor) {
			return;
		}
		this.hairPrimaryColor = hairPrimaryColor;
		this.hairSecondaryColor = hairSecondaryColor;
		this.player.setHairColor(hairPrimaryColor, hairSecondaryColor);
	}

	private updateFacialHairColor(facialHairColor: number) {
		if (facialHairColor === undefined || facialHairColor === this.facialHairColor) {
			return;
		}
		this.facialHairColor = facialHairColor;
		this.player.setHeadOverlayColor(1, 1, facialHairColor, 100);
	}

	private updateEyeColor(eyeColor: number) {
		if (eyeColor === undefined || eyeColor === this.eyeColor) {
			return;
		}
		this.eyeColor = eyeColor;
		this.player.setEyeColor(eyeColor);
	}
}

const PlayerCharacter = new Character();

const updateCharacter = (charData: CustomizationData): void => {
	PlayerCharacter.update(charData);
};

nr.rpc.register(NREvents.updateCharacter, updateCharacter);

// const updateCharacterHairAndColors = (): void => {
// 	const player = mp.players.local;
// 	const { hair, hairColor, eyeColor } = characterData;
//
// 	// hair
// 	player.setComponentVariation(2, hair, 0, 0);
// 	player.setHairColor(hairColor, 0);
//
// 	// appearance color
// 	player.setHeadOverlayColor(2, 1, hairColor, 100); // eyebrow
// 	player.setHeadOverlayColor(1, 1, hairColor, 100); // beard
// 	player.setHeadOverlayColor(10, 1, hairColor, 100); // chest hair
//
// 	// eye color
// 	player.setEyeColor(eyeColor);
// };
//
// const updateAppearance = (): void => {
// 	const player = mp.players.local;
//
// 	characterData.appearance.forEach((value, index) => {
// 		player.setHeadOverlay(index, value, 100, 0, 0);
// 	});
// };
//
// const updateClothes = (): void => {
// 	const player = mp.players.local;
// 	const { outClothes, pants, shoes, gender } = characterData;
//
// 	player.setComponentVariation(11, outClothes, 1, 0);
// 	player.setComponentVariation(4, pants, 1, 0);
// 	player.setComponentVariation(6, shoes, 1, 0);
// 	player.setComponentVariation(8, 15, 0, 0);
// 	player.setComponentVariation(3, validTorsoIds[gender][outClothes], 0, 0);
// };
//
// nr.rpc.register('onCustomizationChangeGender', async (gender: Gender): Promise<ChangeGenderResult> => {
// 	try {
// 		const player = mp.players.local;
//
// 		switch (gender) {
// 			case Gender.male: {
// 				player.model = mp.game.joaat('mp_m_freemode_01');
//
// 				characterData.outClothes = 1;
// 				characterData.pants = 0;
// 				characterData.shoes = 1;
// 				break;
// 			}
// 			case Gender.female: {
// 				player.model = mp.game.joaat('mp_f_freemode_01');
//
// 				characterData.outClothes = 5;
// 				characterData.pants = 0;
// 				characterData.shoes = 3;
// 				break;
// 			}
// 		}
//
// 		characterData.appearance[1] = 255;
//
// 		updateCharacterParents();
// 		updateAppearance();
// 		updateCharacterHairAndColors();
// 		updateClothes();
// 		characterData.features.forEach((feature, index) => {
// 			player.setFaceFeature(index, feature);
// 		});
// 		return new OperationSuccess(gender);
// 	} catch (error) {
// 		mp.console.logInfo(error.message);
//
// 		return new OperationFailed(
// 			ChangeGenderErrorTypes.UnknownError,
// 			localize.t('ERRORS.UNKNOWN')
// 		);
// 	}
// });
