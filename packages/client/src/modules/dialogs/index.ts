import { setRoute, toggleCursor } from '../hud';
import { Routes } from 'common/constants/routes';
import { nr } from '../../core/wrapper';
import { NREvents } from 'common/enums/nrEvents';
import { sleep } from 'common/pipes/sleep';
import { camerasManager } from '../cameras';
import { playSoundFrontend } from '../../core/main';
import { CamerasInfo } from '../../constants/cameras';

export const createDialogCamera = (alias: string): void => {
	const data = CamerasInfo[alias];
	const position = data?.position;
	const rotation = data?.rotation;
	if (!position || !rotation) return; // No camera here
	mp.game.graphics.startScreenEffect('SwitchHUDOut', 1000, false);
	playSoundFrontend(-1, 'FocusIn', 'HintCamSounds');
	global.camera = camerasManager.createCamera(alias, 'default', position, rotation, data.fov);
	camerasManager.setActiveCamera(global.camera, true);
};

export const createNPCDialog = async (dialog: Routes, entity: EntityMp): Promise<void> => {
	const alias = entity.getVariable('streamData')?.data?.alias;
	if (!alias) return;
	global.vendor = entity;
	await setRoute(dialog, { alias });
	createDialogCamera(alias);
	global.dialog = dialog;
	global.lastDialog = dialog;
	toggleCursor(true, true);
};

export const createDialog = async (dialog: Routes, entity?: EntityMp): Promise<void> => {
	if (entity) {
		await createNPCDialog(dialog, entity);
		return;
	}
	await setRoute(dialog);
	global.dialog = dialog;
	toggleCursor(true, true);
};
nr.rpc.register(NREvents.createLastDialog, async () => {
	await createDialog(global.lastDialog, global.vendor);
});

export const closeDialog = async (): Promise<void> => {
	await sleep(100);
	await setRoute(Routes.hud);
	global.dialog = null;
	if (global.camera) {
		camerasManager.destroyCamera(global.camera);
		mp.game.cam.renderScriptCams(false, false, 0, true, false);
		mp.game.graphics.startScreenEffect('SwitchHUDOut', 1000, false);
		playSoundFrontend(-1, 'FocusIn', 'HintCamSounds');
	}
	await sleep(25);
	toggleCursor(false, false);
};
nr.rpc.register(NREvents.closeDialog, closeDialog);
