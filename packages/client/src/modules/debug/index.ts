/* eslint-disable no-eval, @typescript-eslint/no-unused-vars */
// @ts-nocheck
import { NREvents } from '../../../../common/enums/nrEvents';
import { nr } from '../../core/wrapper';
import { checkPermission } from '../../core/main';
import { AdminPermissions } from '../../../../common/enums/permissions';
import rpc from 'rage-rpc';

const _logServer = (message: string): void => {
	nr.rpc.callServer(NREvents.log, message, { noRet: true });
};
export const logServer = _logServer;

let su = false;
let json = false;
const debug = async (code: string): string => {
	const now = new Date().toLocaleTimeString();
	const player = mp.players.local;
	const vehicle = mp.players.local.vehicle;
	try {
		switch (true) {
			// Command switch
			case (code === 'su'): {
				su = !su;
				return `[C] ${now}: Sever execution mode: ${su}`;
			}
			case (code === 'JSON'): {
				json = !json;
				return `[C] ${now}: JSON Mode: ${json}`;
			}
			case (code === 'pos'): {
				_logServer(`new mp.Vector3(${player.position.x}, ${player.position.y}, ${player.position.z}), ${player.getHeading()}`);
				return (`new mp.Vector3(${player.position.x}, ${player.position.y}, ${player.position.z}), ${player.getHeading()}`);
			}

			// Execution
			case (code.includes('sudo ')): {
				json ? code = `JSON.stringify(${code})` : code;
				return await rpc.callServer(NREvents.debug, code.replace('sudo', ''), { timeout: 2000 });
			}
			case (su): {
				json ? code = `JSON.stringify(${code})` : code;
				return await rpc.callServer(NREvents.debug, code, { timeout: 2000 });
			}
			default: {
				json ? code = `JSON.stringify(${code})` : code;
				const result = eval(code);
				return `[C] ${now}: ${result}`;
			}
		}
	} catch (error) {
		return `[C] Exception: ${error.message}`;
	}
};

nr.rpc.register(NREvents.debug, debug);

mp.events.add('consoleCommand', async (code) => {
	const now = new Date().toLocaleTimeString();
	if (!checkPermission(AdminPermissions.CONSOLE)) return mp.console.logError('ACCESS DENIED');
	const result = await nr.rpc.call(NREvents.debug, code, { timeout: 2000 });
	if (!result) return mp.console.logError(`${now}: Unhandled Exception`);
	mp.console.logInfo(result);
});
//
// mp.events.add('chat:activate', async (toggle: boolean) => {
// 	await nr.rpc.callBrowser(global.hud, 'chat:activate', toggle);
// });
//
// mp.events.add('chat:push', async (message: string) => {
// 	await nr.rpc.callBrowser(global.hud, 'chat:push', message);
// });
//
// mp.events.add('chat:clear', async () => {
// 	await nr.rpc.callBrowser(global.hud, 'chat:clear');
// });
//
// mp.events.add('chat:show', async (toggle: boolean) => {
// 	await nr.rpc.callBrowser(global.hud, 'chat:show', toggle);
// });
