import { nr } from '../../../core/wrapper';
import { NREvents } from 'common/enums/nrEvents';
import { isStreamed } from '../../../core/main';

interface VehicleStats {
	seats: number;
	speed: number;
	acceleration: number;
	handling: number;
	braking: number;
}

const maxSpeed = 62.4;
const maxAcceleration = 0.4;
const maxHandling = 3.7;
const maxBraking = 1.2;

/*
Object.keys(VehiclesPool).forEach((key) => {
	const hash = mp.game.joaat(key);
	const speed = mp.game.vehicle.getVehicleModelMaxSpeed(hash);
	const acceleration = mp.game.vehicle.getVehicleModelAcceleration(hash);
	const handling = mp.game.vehicle.getVehicleModelMaxTraction(hash);
	const braking = mp.game.vehicle.getVehicleModelMaxBraking(hash);
	if (speed > maxSpeed) maxSpeed = speed;
	if (acceleration > maxAcceleration) maxAcceleration = acceleration;
	if (handling > maxHandling) maxHandling = handling;
	if (braking > maxBraking) maxBraking = braking;
});

mp.console.logInfo(`
	${maxSpeed}
	${maxAcceleration}
	${maxHandling}
	${maxBraking}
`);
*/

export const getVehicleStats = (hash: number): VehicleStats => {
	return {
		seats: mp.game.vehicle.getVehicleModelMaxNumberOfPassengers(hash),
		speed: mp.game.vehicle.getVehicleModelMaxSpeed(hash) / maxSpeed,
		acceleration: mp.game.vehicle.getVehicleModelAcceleration(hash) / maxAcceleration,
		handling: mp.game.vehicle.getVehicleModelMaxTraction(hash) / maxHandling,
		braking: mp.game.vehicle.getVehicleModelMaxBraking(hash) / maxBraking
	};
};

nr.rpc.register(NREvents.getVehicleStats, (args) => {
	const { vehicleArray } = args;
	const array: VehicleStats[] = [];
	vehicleArray.forEach((thisVehicleType: string) => {
		const hash = mp.game.joaat(thisVehicleType);
		array.push(getVehicleStats(hash));
	});
	return array;
});

nr.rpc.register(NREvents.testDriveInterval, (args) => {
	const { vehicle } = args;
	const thisInterval = setInterval(() => {
		if (!isStreamed(mp.vehicles, vehicle)) return;
		if (!mp.players.local.vehicle || mp.players.local.vehicle !== vehicle) {
			nr.rpc.callServer(NREvents.cancelTestDrive);
			clearInterval(thisInterval);
		}
	}, 100);
});