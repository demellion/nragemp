import rpc from 'rage-rpc';
import { localize } from 'common/index';
import { notification } from '../../core/main';
import { nr } from '../../core/wrapper';
import { NREvents } from 'common/enums/nrEvents';

export * from './vehicles';

export const doPurchase = async (source: string, amount: number, reason: string): Promise<boolean> => {
	const canAfford = await rpc.callServer(NREvents.purchase, { source, amount, reason });
	canAfford ? notification(localize.t('MONEY.PURCHASED', { amount }), '') : notification(localize.t('MONEY.CANTAFFORD'), '');
	return canAfford;
};

export const getCurrentVendor = (): { type: string, id: number } => {
	return { type: global?.vendor?.type, id: global?.vendor?.remoteId } || null;
};
nr.rpc.register(NREvents.getCurrentVendor, getCurrentVendor);
