import { NREvents } from 'common/enums/nrEvents';
import { nr } from '../../core/wrapper';
import { min, playSoundFrontend } from '../../core/main';
import { localize } from 'common/index';
import { showShardScaleform } from '../../classes/scaleforms';
import { LevelingPreset } from 'common/enums/leveling';

const hudComponentID = 19;
const rankBarColor = 27; // https://wiki.rage.mp/index.php?title=Fonts_and_Colors

const newLevelNotification = (level: number): void => {
	playSoundFrontend(-1, 'RANK_UP', 'HUD_AWARDS');
	showShardScaleform(localize.t('GENERIC.LEVELUP', { level }), localize.t('GENERIC.LEVELUPTEXT'), 27, 2, 5000);
};

// credit: https://illusivetea.me/FiveM/scaleforms.html
export const updateRankBar = (limit: number, nextLimit: number, previousXP: number, levelup = false): void => {
	if (!mp.game.graphics.hasHudScaleformLoaded(hudComponentID)) {
		mp.game.graphics.requestHudScaleform(hudComponentID);
		while (!mp.game.graphics.hasHudScaleformLoaded(hudComponentID)) mp.game.wait(0);
	}
	mp.game.graphics.pushScaleformMovieFunctionFromHudComponent(hudComponentID, 'SET_COLOUR');
	mp.game.graphics.pushScaleformMovieFunctionParameterInt(rankBarColor);
	mp.game.graphics.pushScaleformMovieFunctionParameterInt(116); // Active bar color
	mp.game.graphics.pushScaleformMovieFunctionParameterInt(123); // Background bar color
	mp.game.ui.setHudColour(116, 171, 51, 42, 255); // HUD_COLOUR_FREEMODE - pattern color
	mp.game.ui.setHudColour(140, 0, 0, 0, 255); // HUD_COLOUR_INGAME_BG - circle background color
	mp.game.graphics.pushScaleformMovieFunctionFromHudComponent(19, 'OVERRIDE_ANIMATION_SPEED');
	mp.game.graphics.pushScaleformMovieFunctionParameterInt(2000);
	mp.game.graphics.popScaleformMovieFunctionVoid();

	const { currentXP, currentLevel } = mp.players.local.getVariable('levelData') || { currentXP: 0, currentLevel: 0 };
	mp.game.graphics.pushScaleformMovieFunctionFromHudComponent(hudComponentID, 'SET_RANK_SCORES');
	mp.game.graphics.pushScaleformMovieFunctionParameterInt(limit);
	mp.game.graphics.pushScaleformMovieFunctionParameterInt(nextLimit);
	mp.game.graphics.pushScaleformMovieFunctionParameterInt(previousXP);
	mp.game.graphics.pushScaleformMovieFunctionParameterInt(currentXP);
	mp.game.graphics.pushScaleformMovieFunctionParameterInt(currentLevel);
	mp.game.graphics.popScaleformMovieFunctionVoid();
	if (levelup) newLevelNotification(currentLevel);
};

nr.rpc.register(NREvents.updateRankBar, (args) => {
	const { limit, nextLimit, previousXP, levelup } = args;
	updateRankBar(limit, nextLimit, previousXP, levelup);
});

export const showCurrentLevel = (): void => {
	const value = LevelingPreset.EXPVALUE;
	const cap = LevelingPreset.CAP;
	const { currentXP, currentLevel } = mp.players.local.getVariable('levelData') || { currentXP: 0, currentLevel: 0 };
	const formula = value + (value * (min(currentLevel, cap) * (Math.log(min(currentLevel, cap)))));
	updateRankBar(0, formula, currentXP, false);
};