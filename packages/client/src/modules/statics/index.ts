import { firewalls } from '../../constants/statics';

const firewallClass = mp.game.joaat('prop_ld_garaged_01');

firewalls.forEach((thisElement) => {
	const objectClass = thisElement.type ? mp.game.joaat(thisElement.type) : firewallClass;
	mp.objects.new(objectClass, thisElement.position, {
		rotation: new mp.Vector3(0, thisElement.rotation, thisElement.heading),
		alpha: thisElement.visible ? 255 : 0,
		dimension: thisElement.dimension || 0
	});
});

/*
	IPLs
*/
mp.game.streaming.requestIpl('vw_casino_main'); // new mp.Vector3(1090.0228271484375, 206.0777587890625, -48.99972915649414), 355.3913269042969
mp.game.streaming.requestIpl('TrevorsTrailerTidy'); // ew mp.Vector3(1975.552, 3820.538, 33.44833), 0

// player.position = new mp.Vector3(-2027.946, -1036.695, 6.707587);
mp.game.streaming.requestIpl('hei_yacht_heist');
mp.game.streaming.requestIpl('hei_yacht_heist_enginrm');
mp.game.streaming.requestIpl('hei_yacht_heist_Lounge');
mp.game.streaming.requestIpl('hei_yacht_heist_Bridge');
mp.game.streaming.requestIpl('hei_yacht_heist_Bar');
mp.game.streaming.requestIpl('hei_yacht_heist_Bedrm');
mp.game.streaming.requestIpl('hei_yacht_heist_DistantLights');
mp.game.streaming.requestIpl('hei_yacht_heist_LODLights');

/*
	Specific
*/

// Clipsets preload
[
	'move_ped_crouched',
	'move_ped_crouched_strafing'
].forEach((thisElement) => {
	mp.game.streaming.requestClipSet(thisElement);
	while (!mp.game.streaming.hasClipSetLoaded(thisElement)) mp.game.wait(0);
});

// Notification images dictionary
mp.game.graphics.requestStreamedTextureDict('DIA_GOON', true);
mp.game.graphics.requestStreamedTextureDict('DIA_TARGET', true);
mp.game.graphics.requestStreamedTextureDict('DIA_POLICE', true);
mp.game.graphics.requestStreamedTextureDict('WEB_PREMIUMDELUXEMOTORSPORT', true);