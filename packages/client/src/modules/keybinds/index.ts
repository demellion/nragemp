import {
	createUnderCursor,
	debugHUDToggle,
	deleteUnderCursor,
	doTaunt,
	engineToggle,
	lockToggle,
	makeScreenshot,
	moveUnderCursor,
	savePlayer,
	showInfoOverlay,
	teleportForward,
	teleportHome,
	teleportToBlip,
	teleportUnderCursor,
	toggleCrouch,
	toggleNoClip
} from '../../core/main';
import { doAction } from '../actions';
import { activateChat } from '../chat';

if (!mp.storage.data.keybinds) mp.storage.data.keybinds = {};
const keystorage = mp.storage.data.keybinds;

// Key binder (dynamic key binding from UI)
// All keys DIK references can be found here: https://msdn.microsoft.com/en-us/library/windows/desktop/dd375731
mp.events.add('rebindKey', (tobind: number, tounbind: number, fnname: string): void => {
	try {
		let thisFunction = null;
		switch (fnname) {
			case '#action': {
				thisFunction = doAction;
				break;
			}
			case '#escape': {
				thisFunction = savePlayer;
				break;
			}
			case '#info': {
				thisFunction = showInfoOverlay;
				break;
			}

			// Gameplay
			case 'engineToggle': {
				thisFunction = engineToggle;
				break;
			}
			case 'lockToggle': {
				thisFunction = lockToggle;
				break;
			}
			case 'doTaunt': {
				thisFunction = doTaunt;
				break;
			}

			// Admin tools
			case 'debugHUDToggle': {
				thisFunction = debugHUDToggle;
				break;
			}
			case 'teleportForward': {
				thisFunction = teleportForward;
				break;
			}
			case 'teleportToBlip': {
				thisFunction = teleportToBlip;
				break;
			}
			case 'teleportHome': {
				thisFunction = teleportHome;
				break;
			}
			case 'toggleCrouch': {
				thisFunction = toggleCrouch;
				break;
			}
			case 'toggleNoClip': {
				thisFunction = toggleNoClip;
				break;
			}
			case 'makeScreenshot': {
				thisFunction = makeScreenshot;
				break;
			}

			// NRay Gun
			case 'teleportUnderCursor': {
				thisFunction = teleportUnderCursor;
				break;
			}
			case 'moveUnderCursor': {
				thisFunction = moveUnderCursor;
				break;
			}
			case 'deleteUnderCursor': {
				thisFunction = deleteUnderCursor;
				break;
			}
			case 'createUnderCursor': {
				thisFunction = createUnderCursor;
				break;
			}

			// Chat
			case 'activateChat': {
				thisFunction = activateChat;
				break;
			}
		}
		mp.keys.bind(tobind, true, thisFunction);
		mp.keys.unbind(tounbind, true, thisFunction);
		mp.storage.data.keybinds[fnname] = tobind;
	} catch (error) {
		mp.console.logError(`NRage.modules.keybinds: ${error.message}`);
	}
});

// System keys
mp.events.call('rebindKey', 0x45, 0x00, '#action');
mp.events.call('rebindKey', 0x1B, 0x00, '#escape');
mp.events.call('rebindKey', 0x5A, 0x00, '#info');

// Admin keybinds
mp.events.call('rebindKey', 0x72, 0x00, 'debugHUDToggle');
mp.events.call('rebindKey', 0x35, 0x00, 'teleportForward');
mp.events.call('rebindKey', 0x23, 0x00, 'teleportToBlip');
mp.events.call('rebindKey', 0x24, 0x00, 'teleportHome');
mp.events.call('rebindKey', 0x73, 0x00, 'toggleNoClip');
mp.events.call('rebindKey', 0x78, 0x00, 'makeScreenshot');

// NRay Gun
mp.events.call('rebindKey', 0x51, 0x00, 'teleportUnderCursor');
mp.events.call('rebindKey', 0x45, 0x00, 'moveUnderCursor');
mp.events.call('rebindKey', 0x46, 0x00, 'deleteUnderCursor');
mp.events.call('rebindKey', 0x43, 0x00, 'createUnderCursor');

// Chat
mp.events.call('rebindKey', 0x54, 0x00, 'activateChat');

// Gameplay
mp.events.call('rebindKey', keystorage.engineToggle || 0x47, 0x00, 'engineToggle');
mp.events.call('rebindKey', keystorage.lockToggle || 0x55, 0x00, 'lockToggle');
mp.events.call('rebindKey', keystorage.toggleCrouch || 0x11, 0x00, 'toggleCrouch');
