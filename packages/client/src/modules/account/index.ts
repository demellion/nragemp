import { setRoute, setTimeCycleModifier, showChat, toggleCursor } from '../hud';
import { NREvents } from 'common/enums/nrEvents';
import { nr } from '../../core/wrapper';
import { ACThread } from '../anticheat';
import { CameraNR, camerasManager } from '../cameras';
import { playScenario, playSoundFrontend, selectRandom } from '../../core/main';
import { sendWelcomeMessage } from '../chat';
import { sleep } from 'common/pipes/sleep';
import { Routes } from 'common/constants/routes';

const authCameraPos = new mp.Vector3(-1599.5291748046875, -987.91455078125, 40.09974670410156);
const authCameraRotation = new mp.Vector3(-5, 2, 290);
export const selectCameraPos = new mp.Vector3(-1707.80517578125, -1095.692138671875, 13.766471862792969);
export const selectCameraRotation = new mp.Vector3(-5, 0, 195);
export const skyCameraPos = new mp.Vector3(-1677.2880859375, -1087.302490234375, 29.309967041015625);
export const skyCameraRotation = new mp.Vector3(0, 0, 270);
const selectPlayer = new mp.Vector3(-1707.7742919921875, -1099.919677734375, 13.152441024780273);
const heading = 9.5;
const scenarioRandom = [
	'WORLD_HUMAN_AA_COFFEE',
	'WORLD_HUMAN_TOURIST_MAP',
	'WORLD_HUMAN_TOURIST_MOBILE'
];

const movePlayerToRender = async () => {
	await sleep(100);
	const player = mp.players.local;
	player.position = new mp.Vector3(authCameraPos.x, authCameraPos.y, 0);
};
let camera: CameraNR = camerasManager.createCamera(
	'auth',
	'default',
	authCameraPos,
	authCameraRotation,
	60
);
camerasManager.setActiveCamera(camera, true);
camera.shake('HAND_SHAKE', 0.25);
mp.game.graphics.startScreenEffect('SwitchHUDOut', 1000, false);
playSoundFrontend(-1, 'FocusIn', 'HintCamSounds');
movePlayerToRender().then();
showChat(false);

const syncCall = (args: any) => {
	const { time } = args;

	// Sync time with server
	mp.game.time.setClockTime(time[3], time[4], time[5]);
};

nr.rpc.register(NREvents.syncCall, syncCall);

const authCameraHandler = async () => {
	toggleCursor(true, true);
	await setRoute(Routes.auth);
};

nr.rpc.register(NREvents.authCamera, authCameraHandler);

const selectCharacterCameraHandler = async () => {
	const player = mp.players.local;
	player.position = selectPlayer;
	player.setHeading(heading);
	player.clearTasksImmediately();
	playScenario(selectRandom(scenarioRandom));
	mp.game.graphics.startScreenEffect('SwitchHUDOut', 3000, false);
	playSoundFrontend(-1, 'FocusIn', 'HintCamSounds');
	camerasManager.setActiveCameraWithInterp(camera, selectCameraPos, selectCameraRotation, 0, 0, 0);
	await sleep(750);
	mp.game.graphics.startScreenEffect('SwitchHUDOut', 1000, false);
	playSoundFrontend(-1, 'FocusIn', 'HintCamSounds');
	camera = camerasManager.createCamera('auth', 'default', selectCameraPos, selectCameraRotation, 50);
	camerasManager.setActiveCamera(camera, true);
};

nr.rpc.register('selectCharacterCamera', selectCharacterCameraHandler);

const gameplayCameraHandler = async () => {
	await setRoute('/');
	playSoundFrontend(-1, 'CHECKPOINT_PERFECT', 'HUD_MINI_GAME_SOUNDSET');
	mp.game.graphics.startScreenEffect('CamPushInNeutral', 4000, false);
	camerasManager.setActiveCameraWithInterp(camera, skyCameraPos, skyCameraRotation, 2000, 0, 0);
	await sleep(500);
	mp.game.cam.doScreenFadeOut(1500);
	await sleep(2000);
	camera = camerasManager.createCamera(
		'auth',
		'default',
		skyCameraPos,
		new mp.Vector3(0, 0, 0),
		50
	);
	mp.game.cam.doScreenFadeIn(1000);
	mp.game.graphics.startScreenEffect('SwitchHUDOut', 3000, false);
	playSoundFrontend(-1, 'FocusIn', 'HintCamSounds');
	mp.game.cam.renderScriptCams(false, false, 0, true, false);

	const adminLevel = mp.players.local.getVariable('adminLevel');
	mp.storage.data.adminLevel = { adminLevel };
	ACThread(adminLevel);
	toggleCursor(false, false);
	setTimeCycleModifier('default');
	await setRoute('hud');
	global.hud.markAsChat();
	global.isPlayable = true;
	setTimeout(() => {
		sendWelcomeMessage();
	}, 200);
	mp.players.local.freezePosition(false);

	switch (mp.players.local.getVariable('adminLevel')) {
		case 3: {
			mp.console.logWarning('NRage AdminOS: Logged in as Core Developer ');
			mp.discord.update('NRage Roleplay', 'Core Developer');
			break;
		}
		case 2: {
			mp.console.logWarning('NRage AdminOS: Logged in as Admininstrator');
			mp.discord.update('NRage Roleplay', 'Administrator');
			break;
		}
		case 1: {
			mp.console.logWarning('NRage AdminOS: Logged in as Moderator');
			mp.discord.update('NRage Roleplay', 'Moderator');
			break;
		}
		default: {
			mp.console.logWarning(`NRage Roleplay: Welcome, ${mp.players.local.name}.`);
			mp.discord.update('NRage Roleplay', 'Player');
			break;
		}
	}
};

nr.rpc.register('gameplayCamera', gameplayCameraHandler);

const updateCharacterOnSelection = async (character: any): Promise<void> => {
	mp.console.logInfo(JSON.stringify(character));
};

nr.rpc.register('updateCharacterOnSelection', updateCharacterOnSelection);

const getLastLogin = async (): Promise<string> => {
	const { lastLogin } = mp.storage.data.login || '';
	return lastLogin || '';
};

nr.rpc.register('getLastLogin', getLastLogin);

const saveLastLogin = (lastLogin: string): void => {
	mp.storage.data.login = { lastLogin };
};

nr.rpc.on('saveLastLogin', saveLastLogin);
