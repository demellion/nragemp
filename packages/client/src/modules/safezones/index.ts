import { notification, playSoundFrontend } from '../../core/main';
import { localize } from 'common/index';

global.isInSafezone = false;
global.safezoneLevel = -1;
global.currentColshape = null;

mp.events.add('playerExitColshape', () => {
	global.currentColshape = null;
});
mp.events.add('playerEnterColshape', (shape: ColshapeMp) => {
	setTimeout(() => {
		global.currentColshape = shape;
	}, 25);
});

const disableSafezone = (): void => {
	global.isInSafezone = false;
	global.safezoneLevel = -1;
};

setInterval(() => {
	const rgscId = mp.players.local.getVariable('rgscId') || '0';
	if (mp.players.local.dimension === parseInt(rgscId)) {
		global.isInSafezone = true;
		global.safezoneLevel = 0;
		return;
	}
	if (!global.currentColshape) return disableSafezone();
	const safezoneData = global.currentColshape.getVariable('safezoneData');
	if (!safezoneData) return disableSafezone();
	const { level, name, elevations, icon } = safezoneData;
	const playerZ = mp.players.local.position.z;
	if (playerZ < elevations[0] || playerZ > elevations[1]) return disableSafezone();

	if (global.isInSafezone) return;
	global.safezoneLevel = level;
	switch (level) {
		case 0: {
			notification(`<C>${name}</C>`, localize.t('SAFEZONES.GREEN'), icon || 'CHAR_DEFAULT', true);
			global.isInSafezone = true;
			break;
		}
		case 1: {
			notification(`<C>${name}</C>`, localize.t('SAFEZONES.YELLOW'), icon || 'DIA_GOON');
			global.isInSafezone = false;
			break;
		}
		case 2: {
			notification(`<C>${name}</C>`, localize.t('SAFEZONES.RED'), icon || 'DIA_TARGET');
			global.isInSafezone = false;
			break;
		}
		default: {
			mp.console.logError(`NRage.safezones: Safezone '${name}' has no proper level assigned!`);
		}
	}
	playSoundFrontend(-1, 'CHALLENGE_UNLOCKED', 'HUD_AWARDS');
}, 25);