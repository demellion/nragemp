import { nr } from '../../core/wrapper';
import { toggleCursor } from '../hud';
import { localize } from '../../../../common';
import { ChatMessage } from '../../../../common/types/chat';

let isChatActivated = false;

const openChatHandler = () => {
	toggleCursor(true, true);
	isChatActivated = true;
};

const closeChatHandler = () => {
	toggleCursor(false, false);
	isChatActivated = false;
};

export const activateChat = async (): Promise<void> => {
	if (!global.isPlayable || isChatActivated || mp.gui.cursor.visible) return;
	openChatHandler();
	mp.gui.chat.activate(true);
};

nr.rpc.register('chat:send', async (message: string) => {
	closeChatHandler();
	await nr.rpc.callServer('chat:send', message);
});

nr.rpc.register('chat:close', closeChatHandler);

export const sendWelcomeMessage = async (): Promise<void> => {
	const messages: ChatMessage[] = [];
	messages.push({
		author: {
			name: 'welcome',
			id: null
		},
		text: localize.t('CHAT.WELCOME_MSG1'),
		color: 'rgb(240, 160, 0)'
	});
	for (const message of messages) {
		await nr.rpc.callBrowser(global.hud, 'chat:push', message);
	}
};
