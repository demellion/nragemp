export interface CameraNR extends CameraMp {
	setActiveCamera: (toggle: boolean) => void;
	setActiveCameraWithInterp: (position: Vector3Mp, rotation: Vector3Mp, duration: number, easeLocation: number, easeRotation: number) => void;
}

type EventFunction = (...args: any[]) => void;

interface ICamrasManagerInfo {
	gameplayCamera: CameraMp;
	activeCamera: CameraNR;
	interpCamera: CameraNR;
	interpActive: boolean;
	_events: Map<string, Set<EventFunction>>;
	cameras: Map<string, CameraNR>;
}

const CamerasManagerInfo: ICamrasManagerInfo = {
	gameplayCamera: null,
	activeCamera: null,
	interpCamera: null,
	interpActive: false,
	_events: new Map(),
	cameras: new Map()
};

mp.events.add('render', () => {
	if (CamerasManagerInfo.interpCamera && CamerasManager.doesExist(CamerasManagerInfo.interpCamera) && !CamerasManagerInfo.activeCamera.isInterpolating()) {
		CamerasManager.fireEvent('stopInterp', CamerasManagerInfo.activeCamera);

		CamerasManagerInfo.interpCamera.setActive(false);
		CamerasManagerInfo.interpCamera.destroy();
		CamerasManagerInfo.interpCamera = null;
	}
});

const cameraSerialize = (camera: CameraMp) => {
	const cameraNR = camera as CameraNR;

	cameraNR.setActiveCamera = (toggle) => {
		CamerasManager.setActiveCamera(cameraNR, toggle);
	};

	cameraNR.setActiveCameraWithInterp = (position, rotation, duration, easeLocation, easeRotation) => {
		CamerasManager.setActiveCameraWithInterp(cameraNR, position, rotation, duration, easeLocation, easeRotation);
	};

	return cameraNR;
};

interface ICamerasManager {
	on(eventName: string, eventFunction: EventFunction): void;
	fireEvent(eventName: string, ...args: any[]): void;
	getCamera(name: string): CameraNR;
	setCamera(name: string, camera: CameraNR): void
	hasCamera(name: string): boolean;
	destroyCamera(camera: CameraNR): void;
	createCamera(name: string, type: string, position: Vector3Mp, rotation?: Vector3Mp, fov?: number): CameraNR;
	setActiveCamera(activeCamera: CameraNR, toggle: boolean): void;
	setActiveCameraWithInterp(activeCamera: CameraNR, position: Vector3Mp, rotation: Vector3Mp, duration: number, easeLocation: number, easeRotation: number): void;
	doesExist(camera: CameraNR): boolean;
	activeCamera(): CameraNR;
	gameplayCam(): CameraNR;
}

class CamerasManager {
	static on(eventName: string, eventFunction: EventFunction) {
		if (CamerasManagerInfo._events.has(eventName)) {
			const event = CamerasManagerInfo._events.get(eventName);

			if (!event.has(eventFunction)) {
				event.add(eventFunction);
			}
		} else {
			CamerasManagerInfo._events.set(eventName, new Set([eventFunction]));
		}
	}

	static fireEvent(eventName: string, ...args: any[]) {
		if (CamerasManagerInfo._events.has(eventName)) {
			const event = CamerasManagerInfo._events.get(eventName);

			event.forEach((eventFunction) => {
				eventFunction(...args);
			});
		}
	}

	static getCamera(name: string) {
		const camera = CamerasManagerInfo.cameras.get(name);

		if (typeof camera.setActiveCamera !== 'function') {
			cameraSerialize(camera);
		}

		return camera;
	}

	static setCamera(name: string, camera: CameraNR) {
		CamerasManagerInfo.cameras.set(name, camera);
	}

	static hasCamera(name: string) {
		return CamerasManagerInfo.cameras.has(name);
	}

	static destroyCamera(camera: CameraNR) {
		if (this.doesExist(camera)) {
			if (camera === this.activeCamera) {
				this.activeCamera.setActive(false);
			}
			camera.destroy();
		}
	}

	static createCamera(name: string, type: string, position: Vector3Mp, rotation?: Vector3Mp, fov?: number) {
		const camera = cameraSerialize(mp.cameras.new(type, position, rotation, fov));
		CamerasManagerInfo.cameras.set(name, camera);
		return camera;
	}

	static setActiveCamera(activeCamera: CameraNR, toggle: boolean) {
		if (!toggle) {
			if (this.doesExist(CamerasManagerInfo.activeCamera)) {
				CamerasManagerInfo.activeCamera = null;
				activeCamera.setActive(false);
				mp.game.cam.renderScriptCams(false, false, 0, false, false);
			}

			if (this.doesExist(CamerasManagerInfo.interpCamera)) {
				CamerasManagerInfo.interpCamera.setActive(false);
				CamerasManagerInfo.interpCamera.destroy();
				CamerasManagerInfo.interpCamera = null;
			}
		} else {
			if (this.doesExist(CamerasManagerInfo.activeCamera)) {
				CamerasManagerInfo.activeCamera.setActive(false);
			}
			CamerasManagerInfo.activeCamera = activeCamera;
			activeCamera.setActive(true);
			mp.game.cam.renderScriptCams(true, false, 0, false, false);
		}
	}

	static setActiveCameraWithInterp(activeCamera: CameraNR, position: Vector3Mp, rotation: Vector3Mp, duration: number, easeLocation: number, easeRotation: number) {
		if (this.doesExist(CamerasManagerInfo.activeCamera)) {
			CamerasManagerInfo.activeCamera.setActive(false);
		}

		if (this.doesExist(CamerasManagerInfo.interpCamera)) {
			CamerasManager.fireEvent('stopInterp', CamerasManagerInfo.interpCamera);

			CamerasManagerInfo.interpCamera.setActive(false);
			CamerasManagerInfo.interpCamera.destroy();
			CamerasManagerInfo.interpCamera = null;
		}
		const interpCamera = mp.cameras.new('default', activeCamera.getCoord(), activeCamera.getRot(2), activeCamera.getFov()) as CameraNR;
		activeCamera.setCoord(position.x, position.y, position.z);
		activeCamera.setRot(rotation.x, rotation.y, rotation.z, 2);
		activeCamera.stopPointing();

		CamerasManagerInfo.activeCamera = activeCamera;
		CamerasManagerInfo.interpCamera = interpCamera;
		activeCamera.setActiveWithInterp(interpCamera.handle, duration, easeLocation, easeRotation);
		mp.game.cam.renderScriptCams(true, false, 0, false, false);

		CamerasManager.fireEvent('startInterp', CamerasManagerInfo.interpCamera);
	}

	static doesExist(camera: CameraNR) {
		return mp.cameras.exists(camera) && camera.doesExist();
	}

	static get activeCamera() {
		return CamerasManagerInfo.activeCamera;
	}

	static get gameplayCam() {
		if (!CamerasManagerInfo.gameplayCamera) {
			CamerasManagerInfo.gameplayCamera = mp.cameras.new('gameplay');
		}
		return CamerasManagerInfo.gameplayCamera;
	}
}

const proxyHandler = {
	get: (_: ICamerasManager, name: keyof ICamerasManager) => typeof CamerasManager[name] !== 'undefined'
		? CamerasManager[name]
		: CamerasManagerInfo.cameras.get(name)
};

export const camerasManager = new Proxy({} as ICamerasManager, proxyHandler);

export const getCameraOffset = (pos: Vector3Mp, angle: number, dist: number): Vector3Mp => {
	angle = angle * 0.0174533;

	pos.y = pos.y + dist * Math.sin(angle);
	pos.x = pos.x + dist * Math.cos(angle);

	return pos;
};

export const bodyCamValues = {
	hair: { Angle: 0, Dist: 0.5, Height: 0.7 },
	beard: { Angle: 0, Dist: 0.5, Height: 0.7 },
	eyebrows: { Angle: 0, Dist: 0.5, Height: 0.7 },
	chesthair: { Angle: 0, Dist: 1, Height: 0.2 },
	lenses: { Angle: 0, Dist: 0.5, Height: 0.7 },
	lipstick: { Angle: 0, Dist: 0.5, Height: 0.7 },
	blush: { Angle: 0, Dist: 0.5, Height: 0.7 },
	makeup: { Angle: 0, Dist: 0.5, Height: 0.7 },

	torso: [
		{ Angle: 0, Dist: 1, Height: 0.2 },
		{ Angle: 0, Dist: 1, Height: 0.2 },
		{ Angle: 0, Dist: 1, Height: 0.2 },
		{ Angle: 180, Dist: 1, Height: 0.2 },
		{ Angle: 180, Dist: 1, Height: 0.2 },
		{ Angle: 180, Dist: 1, Height: 0.2 },
		{ Angle: 180, Dist: 1, Height: 0.2 },
		{ Angle: 305, Dist: 1, Height: 0.2 },
		{ Angle: 55, Dist: 1, Height: 0.2 }
	],
	head: [
		{ Angle: 0, Dist: 1, Height: 0.5 },
		{ Angle: 305, Dist: 1, Height: 0.5 },
		{ Angle: 55, Dist: 1, Height: 0.5 },
		{ Angle: 180, Dist: 1, Height: 0.5 },
		{ Angle: 0, Dist: 0.5, Height: 0.5 },
		{ Angle: 0, Dist: 0.5, Height: 0.5 }
	],
	leftArm: [
		{ Angle: 55, Dist: 1, Height: 0.0 },
		{ Angle: 55, Dist: 1, Height: 0.1 },
		{ Angle: 55, Dist: 1, Height: 0.1 }
	],
	rightArm: [
		{ Angle: 305, Dist: 1, Height: 0.0 },
		{ Angle: 305, Dist: 1, Height: 0.1 },
		{ Angle: 305, Dist: 1, Height: 0.1 }
	],
	leftLeg: [
		{ Angle: 55, Dist: 1, Height: -0.6 },
		{ Angle: 55, Dist: 1, Height: -0.6 }
	],
	rightLeg: [
		{ Angle: 305, Dist: 1, Height: -0.6 },
		{ Angle: 305, Dist: 1, Height: -0.6 }
	]
};
