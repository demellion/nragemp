import { changeStat, checkMood, doTaunt, getPropHeading, getPropPosition, notification, playScenario, playSoundFrontend, setSpeech } from '../../../core/main';
import { showShardScaleform } from '../../../classes/scaleforms';
import { localize } from 'common/index';
import { sleep } from 'common/pipes/sleep';
import { nr } from '../../../core/wrapper';
import { createDialog } from '../../dialogs';
import { Routes } from 'common/constants/routes';
import { doPurchase } from '../../shops';

export const action = async (action: string, entity?: EntityMp | number): Promise<void> => {
	// Prop-based actions
	if (typeof entity === 'number') {
		const propHeading = getPropHeading(entity);
		const propPosition = getPropPosition(entity);
		switch (action) {
			// World prop actions
			case 'PROP_HUMAN_SEAT_BENCH': {
				playScenario(global.currentAction, propPosition, propHeading - 180, 0, 0, 0.5);
				break;
			}
			case 'PROP_HUMAN_SEAT_BAR': {
				playScenario(global.currentAction, propPosition, propHeading, 0, 0, 0.78);
				break;
			}
			case 'WORLD_HUMAN_DRINKING_FACILITY': {
				const result = await doPurchase('cash', 25, 'buysoda');
				if (result) {
					playScenario(global.currentAction);
					changeStat('thirst', 30);
				}
				break;
			}
			case 'WORLD_HUMAN_AA_COFFEE': {
				const result = await doPurchase('cash', 75, 'buycoffee');
				if (result) {
					playScenario(global.currentAction);
					changeStat('thirst', 15);
					changeStat('mood', 25, 50);
				}
				break;
			}
			case 'NOSCENE_SNACK': {
				const result = await doPurchase('cash', 50, 'buysnack');
				if (result) {
					changeStat('hunger', 30);
				}
				break;
			}
			case 'FLEX_MIRROR': {
				playScenario('WORLD_HUMAN_HANG_OUT_STREET_CLUBHOUSE');
				break;
			}
			case 'PROP_HUMAN_SEAT_SUNLOUNGER': {
				playScenario(global.currentAction, propPosition, propHeading - 180, -0.05, 0.20, 0.4);
				break;
			}
			case 'PROP_HUMAN_ATM': {
				playScenario(global.currentAction);
				break;
			}
			case 'WORLD_HUMAN_SIT_UPS': {
				playScenario(global.currentAction, propPosition, propHeading - 180, 0, 0, 0.5);
				break;
			}
			case 'PROP_HUMAN_SEAT_CHAIR_DRINK_BEER': {
				playScenario(global.currentAction, propPosition, propHeading - 180, 0, 0, 0.5);
				break;
			}
			case 'PROP_HUMAN_SEAT_STRIP_WATCH': {
				playScenario(global.currentAction, propPosition, propHeading - 180, 0, 0, 0.5);
				break;
			}

			default: {
				mp.console.logError(`NRage.actions.action: No such action defined (${action})`);
				break;
			}
		}
		return;
	}

	// Actions
	switch (action) {
		case 'obj_collectible': {
			entity.setAlpha(0);
			playSoundFrontend(-1, 'Object_Collect_Player', 'GTAO_FM_Events_Soundset');
			const { current, next } = { current: 0, next: 0 }; // TODO: Some actual achievements system.
			showShardScaleform(localize.t('ACHIEVEMENTS.COLLECTIBLES'), localize.t('ACHIEVEMENTS.HOWMUCHCOUNT', { current, next }), 27, 2, 5000);
			break;
		}
		case 'obj_luckywheel': {
			if (!checkMood(0)) return;
			const result = await nr.rpc.callServer('spinWheel', { wheel: entity });
			if (result) notification(localize.t(result), '');
			break;
		}
		case 'ped_casino_roulette': {
			if (!checkMood(0)) return;
			break;
		}
		case 'ped_trevor': {
			playScenario('WORLD_HUMAN_DRINKING');
			break;
		}
		case 'ped_denise': {
			playScenario('WORLD_HUMAN_YOGA');
			break;
		}
		case 'ped_franklin': {
			doTaunt(0);
			await sleep(500);
			setSpeech(entity, 'GENERIC_FUCK_YOU', 'FRANKLIN_NORMAL', 'SPEECH_PARAMS_FORCE');
			break;
		}

		// Shops
		case 'vehshop_elite': {
			await createDialog(Routes.vehShop, entity);
			break;
		}

		default: {
			mp.console.logError(`NRage.actions.action: No such action defined (${action})`);
			break;
		}
	}
};
