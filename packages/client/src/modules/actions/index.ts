import {
	actionInCooldown,
	checkLoS,
	distance,
	getClosestMarker,
	getClosestObject,
	getClosestPed, getClosestPlayer, getClosestVehicle,
	getCursorObject,
	getPropAction, getPropHash, getPropPosition,
	playSound,
	playSoundFrontend,
	setActionCooldown,
	setSpeech
} from '../../core/main';
import { localize } from 'common/index';
import rpc from 'rage-rpc';
import { action } from './action/action';

/*
	Closest ped action
*/
global.currentNPC = null;
global.currentAction = '';
global.currentActionName = '';
let componentShown = false;
let visualQue: MarkerMp = null;
const createVisualQue = (position: Vector3Mp, range = 1.25, zOffset = 0.05): void => {
	if (visualQue) {
		visualQue.destroy();
		visualQue = null;
	}
	const elevation = mp.players.local.position.z - 1;
	visualQue = mp.markers.new(27, new mp.Vector3(position.x, position.y, elevation + zOffset), range, {
		rotation: new mp.Vector3(0, 0, 0),
		color: [172, 23, 32, 200],
		visible: true,
		dimension: mp.players.local.dimension
	});
};
const removeThisFrame = () => {
	if (visualQue) {
		visualQue.destroy();
		visualQue = null;
	}
	componentShown = false;
	global.currentAction = '';
	global.currentNPC = null;
	global.currentActionName = '';
};
const enableThisFrame = (entity: EntityMp | number, inVehicle: boolean) => {
	const vehicle = mp.players.local.vehicle;
	if (vehicle && !inVehicle) return removeThisFrame();
	if (typeof entity === 'number') {
		const propAction = getPropAction(getPropHash(entity));
		global.currentAction = propAction;
		global.currentNPC = entity;
		switch (true) {
			// Scenario based
			case (['PROP_HUMAN_SEAT_BENCH', 'PROP_HUMAN_SEAT_BAR'].includes(propAction)): global.currentActionName = localize.t('ACTIONS.SEAT'); break;
			case (['PROP_HUMAN_ATM'].includes(propAction)): global.currentActionName = localize.t('ACTIONS.USEATM'); break;
			case (['WORLD_HUMAN_DRINKING_FACILITY'].includes(propAction)): global.currentActionName = localize.t('ACTIONS.USESODA'); break;
			case (['PROP_HUMAN_SEAT_SUNLOUNGER'].includes(propAction)): global.currentActionName = localize.t('ACTIONS.SUNLOUNGE'); break;
			case (['WORLD_HUMAN_AA_COFFEE'].includes(propAction)): global.currentActionName = localize.t('ACTIONS.USECOFFEE'); break;
			case (['WORLD_HUMAN_SIT_UPS'].includes(propAction)): global.currentActionName = localize.t('ACTIONS.BODYBUILD'); break;

			// Custom
			case (['NOSCENE_SNACK'].includes(propAction)): global.currentActionName = localize.t('ACTIONS.SNACK'); break;
			case (['FLEX_MIRROR'].includes(propAction)): global.currentActionName = localize.t('ACTIONS.FLEXMIRROR'); break;

			// Stripclub only
			case (['PROP_HUMAN_SEAT_CHAIR_DRINK_BEER'].includes(propAction)): global.currentActionName = localize.t('ACTIONS.DRINKSTRIP'); break;
			case (['PROP_HUMAN_SEAT_STRIP_WATCH'].includes(propAction)): global.currentActionName = localize.t('ACTIONS.WATCHSTRIP'); break;

			// WORLD_HUMAN_STRIP_WATCH_STAND - intreact with girl player.
			default: global.currentActionName = localize.t('ACTIONS.ENDSCENARIO');
		}
	} else {
		const streamData = entity.getVariable('streamData');
		if (!streamData) return removeThisFrame();
		const actionType = streamData.action;
		if (!actionType) return removeThisFrame();
		if (!streamData.data) return removeThisFrame();
		global.currentAction = actionType;
		global.currentNPC = entity;
		switch (actionType) {
			case '#ped': {
				const actionName = streamData.data.actionName || 'ACTIONS.DEFAULT';
				global.currentActionName = localize.t(actionName);
				break;
			}
			case '#object': {
				const actionName = streamData.data.actionName;
				global.currentActionName = localize.t(actionName);
				break;
			}
			case '#transition': {
				const actionName = 'ACTIONS.TRANSITION';
				global.currentActionName = localize.t(actionName);
				break;
			}
			default: {
				const actionName = 'ACTIONS.DEFAULT';
				global.currentActionName = localize.t(actionName);
				break;
			}
		}
		// Speak to me boy
		if (!componentShown) {
			const { speechSet } = streamData;
			if (speechSet) setSpeech(entity, speechSet[0], speechSet[1], speechSet[2]);
		}
	}
	if (!componentShown) playSound('NAV_UP_DOWN', 'HUD_FRONTEND_DEFAULT_SOUNDSET');
	componentShown = true;
};

// Action perform function
export const doAction = (): void => {
	if (mp.gui.cursor.visible) return;
	if (actionInCooldown()) return;
	const player = mp.players.local;
	if (player.isUsingAnyScenario()) {
		player.position = new mp.Vector3(player.position.x, player.position.y, player.position.z);
	}
	if (!global.currentNPC) return;

	// Prop action
	if (typeof global.currentNPC === 'number') {
		player.clearTasks();
		action(global.currentAction, global.currentNPC);
		setActionCooldown(1);
	} else {
		// Players and vehicles
		if (['#vehicle', '#player'].includes(global.currentAction)) {
			action(global.currentAction, global.currentNPC);
			return;
		}
		// Anything else
		const thisActionData = global.currentNPC.getVariable('streamData');
		if (!thisActionData) return;
		const data = thisActionData.data;
		if (!data) return;
		player.clearTasks();
		switch (true) {
			case (!!data.alias): {
				action(data.alias, global.currentNPC);
				break;
			}
			case (!!data.action): {
				action(data.action, global.currentNPC);
				break;
			}
			case (!!data.destination): {
				player.position = data.destination;
				mp.game.graphics.startScreenEffect('SwitchHUDOut', 1000, false);
				playSoundFrontend(-1, 'FocusIn', 'HintCamSounds');
				player.setHeading(data.heading);
				break;
			}
		}
		setActionCooldown(1);
	}
};

// Lookup interval
setInterval(() => {
	const player = mp.players.local;
	if (!player) return removeThisFrame();
	if (!global.isPlayable) return removeThisFrame();

	// External actions
	if (!!player.isInMeleeCombat() && !!player.getVariable('isCrouched')) {
		rpc.callServer('toggleCrouch', {}, { noRet: true });
		setActionCooldown(1);
	}

	// Break interactions
	if (player.isUsingAnyScenario()) {
		if (visualQue) {
			visualQue.destroy();
			visualQue = null;
		}
		enableThisFrame(0, true);
		return;
	}

	const propCast = getCursorObject(10);
	if (propCast && !player.isUsingAnyScenario()) {
		const propHandle: number | EntityMp = propCast.entity;
		if (typeof propHandle === 'number') {
			const propAction = getPropAction(getPropHash(propHandle));
			// @ts-ignore
			const propPosition = getPropPosition(propHandle);
			if (propAction !== '' && distance(player, propPosition) <= 1.5) {
				createVisualQue(propPosition);
				enableThisFrame(propHandle, false);
				return;
			}
		}
	}
	const closestPed = getClosestPed(4);
	if (!!closestPed) {
		const streamData = closestPed.getVariable('streamData');
		if (!streamData) return;
		if (!streamData.ignoreLos && !checkLoS(player, closestPed)) return;
		enableThisFrame(closestPed, false);
		return;
	}
	const closestMarker = getClosestMarker(4);
	if (!!closestMarker && closestMarker.getVariable('streamData')) {
		// @ts-ignore
		const markerScale = closestMarker.scale;
		const actualDistance = distance(player, closestMarker);
		if (actualDistance > (markerScale + 0.5)) return removeThisFrame();
		enableThisFrame(closestMarker, false);
		return;
	}
	const closestObject = getClosestObject(4);
	if (!!closestObject && closestObject.getAlpha() > 0) {
		enableThisFrame(closestObject, false);
		return;
	}

	// Silenced actions
	const closestVehicle = getClosestVehicle(4);
	if (closestVehicle && mp.players.local.vehicle !== closestVehicle) {
		global.currentNPC = closestVehicle;
		global.currentAction = '#vehicle';
		return;
	}
	const closestPlayer = getClosestPlayer(2);
	if (closestPlayer) {
		createVisualQue(closestPlayer.position, 1);
		global.currentNPC = closestPlayer;
		global.currentAction = '#player';
		return;
	}
	removeThisFrame();
}, 100);
