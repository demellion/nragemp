import { notification } from '../../core/main';
import { nr } from '../../core/wrapper';
import { NREvents } from 'common/enums/nrEvents';

// Main thread, for more details look for remote event on server.
nr.rpc.register('ACAction', (args: any) => {
	const level = args.level;
	const codeType = args.codeType;
	const notificationAC = () => {
		let message;
		switch (level) {
			case 0: {
				message = `You have been warned (Code ${codeType})`;
				break;
			}
			case 1: {
				message = `You were kicked from server (Code ${codeType})`;
				break;
			}
			case 2: {
				message = `You were banned from server (Code ${codeType})`;
				break;
			}
			default: {
				message = `Unhandled exception (${codeType}:${level})`;
			}
		}
		notification('NRage Anticheat', message, 'CHAR_BUGSTARS');
		mp.console.logWarning(`NRage Anticheat: ${message}`);
	};
	if (level > 0) mp.players.local.freezePosition(true);
	notificationAC();
	return true;
});

global.ACLastShotCount = 0;
global.ACShotsFlagged = 0;
export const ACThread = (adminLevel: number): void => {
	if (adminLevel > 0) return mp.console.logWarning('NRage Anticheat: Admin privileges detected, disabling anti-cheat.');
	setInterval(() => {
		const player = mp.players.local;
		if (player === null) return;
		if (player.getVariable('eventState')) return;
		const vehicle = player.vehicle;
		const reportCase = (code: number): void => {
			mp.events.callRemote(NREvents.reportAC, code);
		};

		/* Vehicles section */

		if (vehicle !== null) {
			if (vehicle.remoteId === 65535) {
				vehicle.destroy();
				reportCase(0);
				return;
			}
			if (vehicle.getAlpha() < 255) {
				vehicle.resetAlpha();
				reportCase(1);
				return;
			}
			if ((!player.isInAnyHeli()) && (!player.isInAnyPlane()) && (vehicle.getSpeed() * 3.6 > 400)) {
				const vehicleClassID = vehicle.getClass();
				if ([16].includes(vehicleClassID)) return;
				vehicle.setForwardSpeed(0);
				reportCase(6);
				return;
			}
		}

		/* Player section */

		if ((player.getAlpha() < 255) || (!player.isVisible())) {
			player.resetAlpha();
			reportCase(1);
		}
		// @ts-ignore (not implemented in interfaces of d.ts)
		if (!!mp.game.player.getInvincible()) {
			reportCase(2);
		}
		if ((player.getHealth() > 200) || (player.getArmour() > 100)) {
			reportCase(3);
		}
		if ((player.isCollisonDisabled()) && (!player.isVaulting()) && (!player.isClimbing)) {
			reportCase(4);
		}
		if ([0xAF3696A1, 0x476BF155, 0x42BF8A85, 0x6D544C99, 0xB62D1F67, 0x3813FC08].includes(player.weapon)) {
			reportCase(5);
		}
		if (![1885233650, 2627665880].includes(player.model)) {
			reportCase(7);
		}
	}, 100);

	// Event based
	mp.events.add('playerWeaponShot', (_, targetEntity) => {
		const player = mp.players.local;
		if (targetEntity !== player) return;
		if (player.vehicle) return;
		const reportCase = (code: number): void => {
			mp.events.callRemote(NREvents.reportAC, code);
		};
		const nextRoundCounter = player.getAmmoInClip(player.weapon);
		if (nextRoundCounter === 0) return;
		if (nextRoundCounter === global.ACLastShotCount) {
			global.ACShotsFlagged += 1;
		} else {
			global.ACShotsFlagged = 0;
		}
		if (global.ACShotsFlagged >= 5) {
			reportCase(8);
			return;
		}
		global.ACLastShotCount = nextRoundCounter;
	});
};