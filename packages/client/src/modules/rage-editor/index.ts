import rpc from 'rage-rpc';
import { checkPermission } from '../../core/main';
import { AdminPermissions } from '../../../../common/enums/permissions';

const evalInContext = (ctx: any, code: any) => {
	const negate = {};
	// @ts-ignore
	for (const p in this) negate[p] = undefined;
	// eslint-disable-next-line no-new-func
	return (new Function(`with(this){ ${code} }`).call({
		...negate,
		...ctx
	}));
};

const STORAGE_KEY = 'reditorFiles';

let browser: BrowserMp;

// hack to get up-to-date chat activated status. damnit GEROGE :(
const getChatStatus = () => {
	return new Promise((resolve) => {
		const handler = (toggle: any) => {
			resolve(toggle);
			mp.events.remove('reditor:chatActivation', handler);
		};
		mp.events.add('reditor:chatActivation', handler);
		mp.gui.execute('mp.trigger("reditor:chatActivation", chat.active)');
	});
};

const init = (info: any) => {
	// set up browser
	if (!browser) browser = mp.browsers.new(info.url);
	browser.active = false;

	// set up key bind
	mp.keys.bind(info.key, false, onBindPress);
};

let lastChatActivation: Promise<any>;

const onBindPress = () => {
	if (browser) {
		if (browser.active) {
			mp.gui.cursor.visible = false;
			mp.events.call('reditor:hidden');
			lastChatActivation.then(t => mp.gui.chat.activate(t));
			browser.active = false;
		} else {
			const access = getUserAccess();
			if (access) {
				rpc.callBrowser(browser, 'reditor:setAccess', access);
				mp.events.call('reditor:shown');
				lastChatActivation = getChatStatus();
				mp.gui.cursor.visible = true;
				lastChatActivation.then(() => mp.gui.chat.activate(false));
				browser.active = true;
				focusEditor();
			}
		}
	}
};

const getUserAccess = () => {
	if (checkPermission(AdminPermissions.CONSOLE)) {
		return {
			l: true,
			s: true,
			c: true
		};
	}
	return false;
};

const focusEditor = () => {
	if (browser && browser.active) {
		browser.execute('if(reditor && reditor.editor) reditor.editor.focus()');
	}
};

rpc.register('reditor:eval', code => {
	try {
		if (!getUserAccess()) return;
		const player = mp.players.local;
		const vehicle = player.vehicle;
		evalInContext({
			rpc, player, vehicle
		}, code);
	} catch (e) {
		mp.console.logError(`NRage Editor: ${e.message}`);
	}
});

rpc.register('reditor:getFiles', () => {
	const names = Object.keys(mp.storage.data[STORAGE_KEY] || {});
	return names.sort((a, b) => a.toLowerCase().localeCompare(b.toLowerCase()));
});
rpc.register('reditor:getFile', name => (mp.storage.data[STORAGE_KEY] || {})[name]);
rpc.register('reditor:exists', name => typeof (mp.storage.data[STORAGE_KEY] || {})[name] !== 'undefined');
rpc.register('reditor:saveFile', ([name, code]) => {
	if (!mp.storage.data[STORAGE_KEY]) mp.storage.data[STORAGE_KEY] = {};
	mp.storage.data[STORAGE_KEY][name] = code;
	mp.storage.flush();
});
rpc.register('reditor:deleteFile', name => {
	if (mp.storage.data[STORAGE_KEY]) {
		delete mp.storage.data[STORAGE_KEY][name];
		mp.storage.flush();
	}
});

// Init
rpc.callServer('reditor:getInfo').then(info => {
	if (info) init(info);
});
