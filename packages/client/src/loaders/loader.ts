import { localize } from 'common/index';
import en from '../../../common/lang/en';
import ru from '../../../common/lang/ru';
import moment from 'moment';
import { NREvents } from 'common/enums/nrEvents';
import { nr } from '../core/wrapper';

export const load = async (): Promise<void> => {
	try {
		mp.players.local.freezePosition(true); // Freeze them for a while.
		const data = await nr.rpc.callServer(NREvents.clientInit);
		const date = moment().utc().add(data.timezone).toArray();
		mp.console.logWarning('NRage Roleplay: Connected to server.');
		mp.console.logWarning(`NRage Roleplay: rgscID: ${data.rgscId}`);
		mp.console.logWarning(`NRage Roleplay: serial: ${data.serial}`);
		mp.storage.data.timezone = { offset: data.timezone };
		mp.storage.data.servername = { servername: data.serverName };
		mp.storage.data.hostname = { hostname: data.hostname };
		mp.game.time.setClockTime(date[3], date[4], date[5]);
		mp.game.time.setClockDate(date[2], date[1], date[0]);
		global.hud = mp.browsers.new(`http://${data.hostname}:8080/`);
		await localize.init({
			lng: data.lang,
			resources: {
				en,
				ru
			}
		});
		require('../modules');
	} catch (error) {
		mp.console.logFatal(`NRage.loader: ${error.message}`);
	}
};
