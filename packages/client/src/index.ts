import { App } from './app';

(async () => {
	try {
		await App.start();
	} catch (error) {
		mp.console.logFatal(`NRage: ${error.message}`);
	}
})();
