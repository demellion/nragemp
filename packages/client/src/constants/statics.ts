export interface Firewall {
	position: Vector3Mp;
	heading: number
	rotation: number
	type?: string
	visible?: boolean,
	dimension?: number
}

export const firewalls: Firewall[] = [
	// Limbo doors
	{
		position: new mp.Vector3(-595.6866455078125, 2087.077880859375, 130.38916015625),
		heading: 14,
		rotation: 0,
		type: 'prop_tyre_wall_02',
		visible: true,
		dimension: 0
	},
	{
		position: new mp.Vector3(-595.6866455078125, 2087.077880859375, 131.38916015625),
		heading: 14,
		rotation: 0,
		type: 'prop_tyre_wall_02',
		visible: true,
		dimension: 0
	},
	{
		position: new mp.Vector3(-589.4327392578125, 2062.9755859375, 129.94015502929688),
		heading: 6,
		rotation: 0,
		type: 'prop_tyre_wall_02',
		visible: true,
		dimension: 1
	},
	{
		position: new mp.Vector3(-589.4327392578125, 2062.9755859375, 130.94015502929688),
		heading: 6,
		rotation: 0,
		type: 'prop_tyre_wall_04',
		visible: true,
		dimension: 1
	},
	// Ammunation ghetto closed shooting range
	{
		position: new mp.Vector3(814.8893432617188, -2158.0576171875, 30.219014739990234),
		heading: 270,
		rotation: 0,
		type: 'imp_prop_impex_gate_sm_15',
		visible: true
	},

	// Casino Management prop
	{
		position: new mp.Vector3(1122.2630615234375, 264.621337890625, -52.040733337402344),
		heading: 90,
		rotation: 0
	},
	// Casino heist restriction
	{
		position: new mp.Vector3(1102.91552734375, 237.3187713623047, -50.89386520385742),
		heading: 45,
		rotation: 0,
		type: 'imp_prop_impex_gate_sm_15',
		visible: true
	},
	{
		position: new mp.Vector3(1116.3701171875, 257.69036865234375, -51.44072723388672),
		heading: 45,
		rotation: 0,
		type: 'imp_prop_impex_gate_sm_15',
		visible: true
	},
	// Management upper floor door
	{
		position: new mp.Vector3(1111.04443359375, 251.18817138671875, -45.841026306152344),
		heading: 5,
		rotation: 0,
		type: 'imp_prop_impex_gate_sm_15'
	},
	// Closed metro station (end-game content)
	{
		position: new mp.Vector3(-1030.237060546875, -2770.79150390625, 3.639802932739258),
		heading: 321,
		rotation: 0,
		type: 'imp_prop_impex_gate_sm_15',
		visible: true
	},
	// Simeon elite veh shop block NPC
	{
		position: new mp.Vector3(-32.212608337402344, -1113.6773681640625, 27.21487045288086),
		heading: 217,
		rotation: 45
	}
];