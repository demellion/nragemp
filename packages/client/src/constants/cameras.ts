interface CameraInfo {
	position: Vector3Mp;
	rotation: Vector3Mp;
	fov: number;
}

export const CamerasInfo: { [key: string]: CameraInfo } = {
	vehshop_elite: {
		position: new mp.Vector3(-47.84606170654297, -1103.306640625, 28.345821380615234),
		rotation: new mp.Vector3(-20, 0, 300),
		fov: 40
	}
};