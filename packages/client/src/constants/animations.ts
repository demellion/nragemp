export const AnimWalkStyles = [
	{ name: 'Normal', style: null },
	{ name: 'Brave', style: 'move_m@brave' },
	{ name: 'Confident', style: 'move_m@confident' },
	{ name: 'Drunk', style: 'move_m@drunk@verydrunk' },
	{ name: 'Fat', style: 'move_m@fat@a' },
	{ name: 'Gangster', style: 'move_m@shadyped@a' },
	{ name: 'Hurry', style: 'move_m@hurry@a' },
	{ name: 'Injured', style: 'move_m@injured' },
	{ name: 'Intimidated', style: 'move_m@intimidation@1h' },
	{ name: 'Quick', style: 'move_m@quick' },
	{ name: 'Sad', style: 'move_m@sad@a' },
	{ name: 'Tough', style: 'move_m@tool_belt@a' }

	// Mod walking styles here
];