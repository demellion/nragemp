export const seats = [
	1805980844, // "prop_bench_01a",
	2037887057, // "prop_bench_01b",
	3079285877, // "prop_bench_01c",
	3666247552, // "prop_bench_02",
	3232156621, // "prop_bench_03",
	3531108208, // "prop_bench_04",
	2663909392, // "prop_bench_05",
	2977869181, // "prop_bench_06",
	2663909392, // "prop_bench_05",
	3891075673, // "prop_bench_08",
	4195466914, // "prop_bench_09",
	437354449, // "prop_bench_10",
	1290593659, // "prop_bench_11",
	4224340047, // "prop_fib_3b_bench",
	1916676832, // "prop_ld_bench01",
	267626795, // "prop_wait_bench_01",
	3096623373, // "hei_prop_heist_off_chair",
	449297510, // "hei_prop_hei_skid_chair",
	525667351, // "prop_chair_01a",
	764848282, // "prop_chair_01b",
	725259233, // "prop_chair_02",
	1064877149, // "prop_chair_03",
	2064599526, // "prop_chair_04a",
	2353589337, // "prop_chair_04b",
	1545434534, // "prop_chair_05",
	826023884, // "prop_chair_06",
	1545434534, // "prop_chair_05",
	1281480215, // "prop_chair_08",
	1612971419, // "prop_chair_09",
	1691387372, // "prop_chair_10",
	1028260687, // "prop_chateau_chair_01",
	2189585618, // "prop_clown_chair",
	3176547591, // "prop_cs_office_chair",
	900821510, // "prop_direct_chair_01",
	181607490, // "prop_direct_chair_02",
	558578166, // "prop_gc_chair02",
	536071214, // "prop_off_chair_01",
	96868307, // "prop_off_chair_03",
	1268458364, // "prop_off_chair_04",
	1480618483, // "prop_off_chair_04b",
	475561894, // "prop_off_chair_04_s",
	1037469683, // "prop_off_chair_05",
	1103738692, // "prop_old_deck_chair",
	1544350879, // "prop_old_wood_chair",
	854385596, // "prop_rock_chair_01",
	291348133, // "prop_skid_chair_01",
	1071807406, // "prop_skid_chair_02",
	3186063286, // "prop_skid_chair_03",
	2661768647, // "prop_sol_chair",
	1262298127, // "prop_wheelchair_01",
	1737474779, // "prop_wheelchair_01_s",
	2229511919, // "p_armchair_01_s",
	3121651431, // "p_clb_officechair_s",
	3573930076, // "p_dinechair_01_s",
	736919402, // "p_ilev_p_easychair_s",
	1196890646, // "p_soloffchair_s",
	604553643, // "p_yacht_chair_01_s",
	867556671, // "v_club_officechair",
	589738836, // "v_corp_bk_chair3",
	3793032646, // "v_corp_cd_chair",
	4185610837, // "v_corp_offchair",
	3510013129, // "v_ilev_chair02_ped",
	3002973360, // "v_ilev_hd_chair",
	98421364, // "v_ilev_p_easychair",
	3413270752, // "v_ret_gc_chair03",
	2425361652, // "prop_ld_farm_chair01",
	1404176808, // "prop_table_04_chr",
	47332588, // "prop_table_05_chr",
	2278414290, // "prop_table_06_chr",
	3917117880, // "v_ilev_leath_chr",
	2773703096, // "prop_table_01_chr_a",
	3059710928, // "prop_table_01_chr_b",
	2533307946, // "prop_table_02_chr",
	146905321, // "prop_table_03b_chr",
	3553022755, // "prop_table_03_chr",
	1716133836, // "prop_torture_ch_01",
	3005151903, // "v_ilev_fh_dineeamesa",
	3104810479, // "v_ilev_fh_kitchenstool",
	3232943535, // "v_ilev_tort_stool",
	3104810479, // "v_ilev_fh_kitchenstool",
	3104810479, // "v_ilev_fh_kitchenstool",
	3104810479, // "v_ilev_fh_kitchenstool",
	3104810479, // "v_ilev_fh_kitchenstool",
	28672923, // "hei_prop_yah_seat_01",
	4001586487, // "hei_prop_yah_seat_02",
	3703617970, // "hei_prop_yah_seat_03",
	1355718178, // "prop_waiting_seat_01",
	4000468055, // "prop_yacht_seat_01",
	2974667279, // "prop_yacht_seat_02",
	3289347986, // "prop_yacht_seat_03",
	1019962318, // "prop_hobo_seat_01",
	2453471663, // "prop_rub_couch01",
	1894671041, // "miss_rub_couch_01",
	544186037, // "prop_ld_farm_couch01",
	773405192, // "prop_ld_farm_couch02",
	2273307701, // "prop_rub_couch02",
	1975077032, // "prop_rub_couch03",
	3095481907, // "prop_rub_couch04",
	1526269963, // "p_lev_sofa_s",
	3888251049, // "p_res_sofa_l_s",
	1593135630, // "p_v_med_p_sofa_s",
	1532110050, // "p_yacht_sofa_01_s",
	2417508004, // "v_ilev_m_sofa",
	2109741755, // "v_res_tre_sofa_s",
	465467525, // "v_tre_sofa_mess_a_s",
	2568033419, // "v_tre_sofa_mess_b_s",
	417935208, // "v_tre_sofa_mess_c_s",
	1446187959, // "prop_roller_car_01",
	3413442113, // "prop_roller_car_02"
	1888204845, // Bus Stop
	-99500382, // Concrete bench
	-1317098115, // Some park bench
	1681727376, // Bus Stop 2
	2142033519, // Bus Stop 3
	-403891623, // Graveyard bench
	-71417349, // Some wooden bench
	-1788378994, // Sofa (Elite shop),
	-741944541, // Plastic chair
	-1631057904, // Bench with advertisement
	-109356459 // Casino boss sits
];

export const seatbars = [
	-1195678770 // Casino bar seats
];

export const sodamachines = [
	992069095, // "prop_vend_soda_01"
	1114264700, // "prop_vend_soda_02"
	1099892058 // Blue water machine
];
export const atms = [
	-1364697528, // Redish ATM
	-1126237515, // Bluish ATM
	-870868698, // 24/7 ATM
	506770882 // Fleeco ATM
];
export const coffemachines = [
	690372739 // Bean Machine
];
export const smokemachines = [
	73774428
];
export const sunlounging = [
	2017293393, // Pool sunlounge
	-1498352975 // Pool sunlounge 2
];
export const bodybuilding = [
	-1838046182, // Body build bench.
	-1095992177 // Weight lifting bench but who cares
];
export const snacks = [
	1129053052, // Dogs
	-1581502570, // Dogs 2
	-654402915, // Candyvendy
	615030415 // Ice-cream bar

];
export const mirrors = [
	-1
];

// Should be only used in stripclub
export const stripdrink = [
	535835408
];
export const stripwatch = [
	161705229,
	603897027
];