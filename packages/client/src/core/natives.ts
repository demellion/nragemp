// @ts-nocheck
/* eslint-disable */

export type EntityHandle = number;

/*
	RAGE Engine native calls (Last updated 02.10.2020)
*/
export const NATIVES = {
	// Getters
	GET_ENTITY_PHYSICS_HEADING: (entity: EntityHandle): number => mp.game.invokeFloat('0x846BF6291198A71E', entity),
	GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS: (entity: EntityHandle, offsetX: number, offsetY: number, offsetZ: number): Vector3Mp => mp.game.invokeVector3('0x1899F328B0E12848', entity, offsetX, offsetY, offsetZ),
	GET_ENTITY_MODEL: (entity: EntityHandle): number => mp.game.invoke('0x9F47B058362C84B5', entity),
	DOES_BLIP_EXIST: (blip: EntityHandle): boolean => mp.game.invoke('0xA6DB27D19ECBB7DA', blip),
	GET_NEXT_BLIP_INFO_ID: (i: number): number => mp.game.invoke('0x14F96AA50D6FBEA7', i),
	GET_FIRST_BLIP_INFO_ID: (i: number): number => mp.game.invoke('0x1BEDE233E6CD2A1F', i),
	GET_NUMBER_OF_ACTIVE_BLIPS: (): number => mp.game.invoke('0x9A3FF3DE163034E8'),
	IS_PLAYER_SWITCH_IN_PROGRESS: (): boolean => mp.game.invoke('0xD9D2CFFF49FAB35F'),

	// Setter
	SET_PED_CURRENT_WEAPON_VISIBLE: (ped, visible, deselectWeapon, p3, p4): void => mp.game.invoke('0x0725A4CCFDED9A70', ped, visible, deselectWeapon, p3, p4),
	SET_BLIP_SPRITE: (blip, sprite): void => mp.game.invoke('0xDF735600A4696DAF', blip, sprite),
	SET_BLIP_ALPHA: (blip, a): void => mp.game.invoke('0x45FF974EEE1C8734', blip, a),
	SET_BLIP_COLOUR: (blip, c): void => mp.game.invoke('0x03D7FB09E75D6B7E', blip, c),
	SET_BLIP_ROTATION: (blip, r): void => mp.game.invoke('0xF87683CDF73C3F6E', blip, r),
	SET_BLIP_FLASHES: (blip, f): void => mp.game.invoke('0xB14552383D39CE3E', blip, f),
	SET_BLIP_SCALE: (blip, scale): void => mp.game.invoke('0xD38744167B2FA257', blip, scale),
	SET_BLIP_FLASH_TIMER: (blip, t): void => mp.game.invoke('0xD3CD6FD297AE87CC', blip, t),
	SET_BLIP_COORDS: (blip, x, y, z): void => mp.game.invoke('0xAE2AF67E9D9AF65D', blip, x, y, z),
	SET_CURSOR_LOCATION: (x, y): void => mp.game.invoke('0xFC695459D4D0E219', x, y),
	SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT: (toggle): void => mp.game.invoke('0xB98236CAAECEF897', toggle),
	SET_FACIAL_IDLE_ANIM_OVERRIDE: (ped, animName, animDict): void => mp.game.invoke('0xFFC24B988B938B38', ped, animName, animDict),
	_SET_NOTIFICATION_COLOR_NEXT: (textColor): void => mp.game.invoke('0x39BBF623FC803EAC', textColor),
	_SET_NOTIFICATION_BACKGROUND_COLOR: (bgColor): void => mp.game.invoke('0x92F0DA1E27DB96DC', bgColor),

	// Functional
	_SWITCH_IN_PLAYER: (ped): void => mp.game.invoke('0xD8295AF639FD9CB8', ped),
	_SWITCH_OUT_PLAYER: (ped, flags, switchType): void => mp.game.invoke('0xAAB3200ED59016BC', ped, flags, switchType),
	_LINK_STATIC_EMITTER_TO_ENTITY: (emitterName, handle): void => mp.game.invoke('0x651D3228960D08AF', emitterName, handle),
};