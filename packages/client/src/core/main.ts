/*
	Core Functions
*/
import rpc from 'rage-rpc';
import { AnimWalkStyles } from '../constants/animations';
import moment from 'moment';
import { localize } from 'common/index';
import { atms, bodybuilding, coffemachines, mirrors, seatbars, seats, snacks, sodamachines, stripdrink, stripwatch, sunlounging } from '../constants/props';
import { AdminPermissions } from 'common/enums/permissions';
import { sleep } from 'common/pipes/sleep';
import { RadioStations } from 'common/enums/radioStations';
import { EntityHandle, NATIVES } from './natives';
import { NREvents } from 'common/enums/nrEvents';
import { showCurrentLevel } from '../modules/leveling';
import { nr } from './wrapper';

// Core variables
global.actionCooldown = moment();
global.isPlayable = false;
global.noClipCamera = null;

/*
	Core Functions
*/
export const selectRandom = (array: any[]): any => {
	return array[Math.floor(Math.random() * array.length)];
};
export const setActionCooldown = (time: number): void => {
	global.actionCooldown = moment().add(time, 'seconds');
};
export const min = (value: number, to: number): number => {
	return value > to ? to : value;
};
export const max = (value: number, to: number): number => {
	return value < to ? to : value;
};
export const actionInCooldown = (): boolean => {
	const now = moment();
	return now < global.actionCooldown;
};
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const instanceOfVector = (object: any): object is Vector3Mp => {
	return (object.x !== undefined);
};
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const instanceOfEntity = (object: any): object is EntityMp => {
	return (object.model !== undefined);
};
export const checkLoS = (who: EntityMp, target: EntityMp): boolean => {
	if (!who || !target) return false;
	return !!who.hasClearLosTo(target.handle, 17);
};
export const distance = (source: EntityMp | Vector3Mp, destination: EntityMp | Vector3Mp, useZ = true): number => {
	let sourceData;
	let destinationData;
	if (instanceOfEntity(source)) {
		sourceData = source.position;
	} else {
		sourceData = source;
	}

	if (instanceOfEntity(destination)) {
		destinationData = destination.position;
	} else {
		destinationData = destination;
	}
	return mp.game.gameplay.getDistanceBetweenCoords(sourceData.x, sourceData.y, sourceData.z, destinationData.x, destinationData.y, destinationData.z, useZ);
};
export const getStreetName = (): string => {
	const player = mp.players.local;
	const streets = mp.game.pathfind.getStreetNameAtCoord(player.position.x, player.position.y, player.position.z, 0, 0);
	const streetName = mp.game.ui.getStreetNameFromHashKey(streets.streetName); // Return string, if exist
	return (streetName || '');
};
export const getZoneName = (): string => {
	const player = mp.players.local;
	return mp.game.gxt.get(mp.game.zone.getNameOfZone(player.position.x, player.position.y, player.position.z));
};
export const getSafezoneName = (level: number): string => {
	switch (level) {
		case 0: {
			return localize.t('SAFEZONES.GREENNAME');
		}
		case 1: {
			return localize.t('SAFEZONES.YELLOWNAME');
		}
		case 2: {
			return localize.t('SAFEZONES.REDNAME');
		}
		default: {
			return '';
		}
	}
};

export const getServerTime = (): moment.Moment => {
	const { offset } = mp.storage.data.timezone;
	return moment().utc().add(offset, 'hours');
};
export const getPlayerName = (player: PlayerMp): string => {
	const titleData = player.getVariable('title') || {};
	const handle = player.remoteId;
	const rgscId = player.getVariable('rgscId') || 0;
	const friendlist = mp.players.local.getVariable('friendList') || [];
	switch (true) {
		case (global.debugHUD): {
			const { adminLevel } = mp.storage.data.adminLevel;
			const { title } = titleData;
			const targetAdminLevel = player.getVariable('adminLevel');
			const name = player.name.replace('_', ' ');
			if (adminLevel < targetAdminLevel && player.getAlpha() < 255) return '';
			if (adminLevel < targetAdminLevel) return `${handle}\n${global ? title : ''}`;
			return `${handle}\n${name}\n${title}\n\n${Math.round(distance(player, mp.players.local))}m`;
		}
		case (friendlist.includes(rgscId)): {
			const { title } = titleData;
			const name = player.name.replace('_', ' ');
			return `${handle}\n${name}\n${title}`;
		}
		default: {
			const { title, global } = titleData;
			return `${handle}\n${global ? title : ''}`;
		}
	}
};

export const getServerName = (): string => {
	const { servername } = mp.storage.data.servername;
	if (!servername) return '';
	return servername;
};
export const getClosestPed = (range: number): PedMp => {
	const closestPeds = mp.peds.toArray().filter((thisPed) => {
		const actualDistance = distance(mp.players.local, thisPed, true);
		return (isStreamed(mp.peds, thisPed)) && (actualDistance <= range);
	});
	return closestPeds.length > 0 ? closestPeds[0] : null;
};
export const getClosestVehicle = (range: number): VehicleMp => {
	const closestVehicles = mp.vehicles.toArray().filter((thisVehicle) => {
		const actualDistance = distance(mp.players.local, thisVehicle, true);
		return (isStreamed(mp.vehicles, thisVehicle)) && (actualDistance <= range);
	});
	return closestVehicles.length > 0 ? closestVehicles[0] : null;
};
export const getClosestPlayer = (range: number): PlayerMp => {
	const closestPlayers = mp.players.toArray().filter((thisPlayer) => {
		if (thisPlayer === mp.players.local) return false;
		const actualDistance = distance(mp.players.local, thisPlayer, true);
		return (isStreamed(mp.players, thisPlayer)) && (actualDistance <= range);
	});
	return closestPlayers.length > 0 ? closestPlayers[0] : null;
};
export const getVehiclePeds = (vehicle: VehicleMp): PlayerMp[] => {
	const pedsArray = [];
	for (let x = -1; x <= 17; x++) {
		const thisPedHandle = vehicle.getPedInSeat(x);
		if (thisPedHandle !== 0) {
			const thisPed = mp.players.atHandle(thisPedHandle);
			if (thisPed) pedsArray.push(thisPed);
		}
	}
	return pedsArray;
};
export const getClosestMarker = (range: number): MarkerMp => {
	const closestMarkers = mp.markers.toArray().filter((thisMarker) => {
		const actualDistance = distance(mp.players.local, thisMarker, true);
		return (/*isStreamed(mp.markers, thisMarker)) &&*/ (actualDistance <= range));
	});
	return closestMarkers.length > 0 ? closestMarkers[0] : null;
};
export const getClosestObject = (range: number): ObjectMp => {
	const closestObjects = mp.objects.toArray().filter((thisObject) => {
		const actualDistance = distance(mp.players.local, thisObject, true);
		return (isStreamed(mp.objects, thisObject)) && (actualDistance <= range);
	});
	return closestObjects.length > 0 ? closestObjects[0] : null;
};
export const getCursorObject = (range: number, flag = 17): RaycastResult => {
	const camera = mp.cameras.new('gameplay');
	const position = camera.getCoord();
	const direction = camera.getDirection();
	const farAway = new mp.Vector3((direction.x * range) + (position.x), (direction.y * range) + (position.y), (direction.z * range) + (position.z));
	const result = mp.raycasting.testPointToPoint(position, farAway, null, flag); // now test point to point - intersects with map and objects [1 +
	if (!result) return null;
	return result;
};
export const getWorldProp = (hash: number, range: number, position: Vector3Mp): EntityHandle => {
	return mp.game.object.getClosestObjectOfType(position.x, position.y, position.z, range, hash, false, false, false);
};
export const getNormalizedVector = (vector: Vector3Mp): Vector3Mp => {
	const mag = Math.sqrt(
		vector.x * vector.x + vector.y * vector.y + vector.z * vector.z
	);
	vector.x = vector.x / mag;
	vector.y = vector.y / mag;
	vector.z = vector.z / mag;
	return vector;
};
export const getCrossProduct = (v1: Vector3Mp, v2: Vector3Mp): Vector3Mp => {
	const vector = new mp.Vector3(0, 0, 0);
	vector.x = v1.y * v2.z - v1.z * v2.y;
	vector.y = v1.z * v2.x - v1.x * v2.z;
	vector.z = v1.x * v2.y - v1.y * v2.x;
	return vector;
};
export const getDeathCause = (cause: number): string => {
	switch (cause) {
		case 2461879995: {
			return 'weapon_electric_fence';
		}
		case 3425972830: {
			return 'weapon_hit_by_water_cannon';
		}
		case 133987706: {
			return 'weapon_rammed_by_car';
		}
		case 2741846334: {
			return 'weapon_run_over_by_car';
		}
		case 3452007600: {
			return 'weapon_fall';
		}
		case 4194021054: {
			return 'weapon_animal';
		}
		case 324506233: {
			return 'weapon_airstrike_rocket';
		}
		case 2339582971: {
			return 'weapon_bleeding';
		}
		case 2294779575: {
			return 'weapon_briefcase';
		}
		case 28811031: {
			return 'weapon_briefcase_02';
		}
		case 148160082: {
			return 'weapon_cougar';
		}
		case 1223143800: {
			return 'weapon_barbed_wire';
		}
		case 4284007675: {
			return 'weapon_drowning';
		}
		case 1936677264: {
			return 'weapon_drowning_in_vehicle';
		}
		case 539292904: {
			return 'weapon_explosion';
		}
		case 910830060: {
			return 'weapon_exhaustion';
		}
		case 3750660587: {
			return 'weapon_fire';
		}
		case 341774354: {
			return 'weapon_heli_crash';
		}
		case 3204302209: {
			return 'weapon_vehicle_rocket';
		}
		case 2282558706: {
			return 'vehicle_weapon_akula_barrage';
		}
		case 431576697: {
			return 'vehicle_weapon_akula_minigun';
		}
		case 2092838988: {
			return 'vehicle_weapon_akula_missile';
		}
		case 476907586: {
			return 'vehicle_weapon_akula_turret_dual';
		}
		case 3048454573: {
			return 'vehicle_weapon_akula_turret_single';
		}
		case 328167896: {
			return 'vehicle_weapon_apc_cannon';
		}
		case 190244068: {
			return 'vehicle_weapon_apc_mg';
		}
		case 1151689097: {
			return 'vehicle_weapon_apc_missile';
		}
		case 3293463361: {
			return 'vehicle_weapon_ardent_mg';
		}
		case 2556895291: {
			return 'vehicle_weapon_avenger_cannon';
		}
		case 2756453005: {
			return 'vehicle_weapon_barrage_rear_gl';
		}
		case 1200179045: {
			return 'vehicle_weapon_barrage_rear_mg';
		}
		case 525623141: {
			return 'vehicle_weapon_barrage_rear_minigun';
		}
		case 4148791700: {
			return 'vehicle_weapon_barrage_top_mg';
		}
		case 1000258817: {
			return 'vehicle_weapon_barrage_top_minigun';
		}
		case 3628350041: {
			return 'vehicle_weapon_bombushka_cannon';
		}
		case 741027160: {
			return 'vehicle_weapon_bombushka_dualmg';
		}
		case 3959029566: {
			return 'vehicle_weapon_cannon_blazer';
		}
		case 1817275304: {
			return 'vehicle_weapon_caracara_mg';
		}
		case 1338760315: {
			return 'vehicle_weapon_caracara_minigun';
		}
		case 2722615358: {
			return 'vehicle_weapon_cherno_missile';
		}
		case 3936892403: {
			return 'vehicle_weapon_comet_mg';
		}
		case 2600428406: {
			return 'vehicle_weapon_deluxo_mg';
		}
		case 3036244276: {
			return 'vehicle_weapon_deluxo_missile';
		}
		case 1595421922: {
			return 'vehicle_weapon_dogfighter_mg';
		}
		case 3393648765: {
			return 'vehicle_weapon_dogfighter_missile';
		}
		case 2700898573: {
			return 'vehicle_weapon_dune_grenadelauncher';
		}
		case 3507816399: {
			return 'vehicle_weapon_dune_mg';
		}
		case 1416047217: {
			return 'vehicle_weapon_dune_minigun';
		}
		case 1566990507: {
			return 'vehicle_weapon_enemy_laser';
		}
		case 1987049393: {
			return 'vehicle_weapon_hacker_missile';
		}
		case 2011877270: {
			return 'vehicle_weapon_hacker_missile_homing';
		}
		case 1331922171: {
			return 'vehicle_weapon_halftrack_dualmg';
		}
		case 1226518132: {
			return 'vehicle_weapon_halftrack_quadmg';
		}
		case 855547631: {
			return 'vehicle_weapon_havok_minigun';
		}
		case 785467445: {
			return 'vehicle_weapon_hunter_barrage';
		}
		case 704686874: {
			return 'vehicle_weapon_hunter_cannon';
		}
		case 1119518887: {
			return 'vehicle_weapon_hunter_mg';
		}
		case 153396725: {
			return 'vehicle_weapon_hunter_missile';
		}
		case 2861067768: {
			return 'vehicle_weapon_insurgent_minigun';
		}
		case 507170720: {
			return 'vehicle_weapon_khanjali_cannon';
		}
		case 2206953837: {
			return 'vehicle_weapon_khanjali_cannon_heavy';
		}
		case 394659298: {
			return 'vehicle_weapon_khanjali_gl';
		}
		case 711953949: {
			return 'vehicle_weapon_khanjali_mg';
		}
		case 3754621092: {
			return 'vehicle_weapon_menacer_mg';
		}
		case 3303022956: {
			return 'vehicle_weapon_microlight_mg';
		}
		case 3846072740: {
			return 'vehicle_weapon_mobileops_cannon';
		}
		case 3857952303: {
			return 'vehicle_weapon_mogul_dualnose';
		}
		case 3123149825: {
			return 'vehicle_weapon_mogul_dualturret';
		}
		case 4128808778: {
			return 'vehicle_weapon_mogul_nose';
		}
		case 3808236382: {
			return 'vehicle_weapon_mogul_turret';
		}
		case 2220197671: {
			return 'vehicle_weapon_mule4_mg';
		}
		case 1198717003: {
			return 'vehicle_weapon_mule4_missile';
		}
		case 3708963429: {
			return 'vehicle_weapon_mule4_turret_gl';
		}
		case 2786772340: {
			return 'vehicle_weapon_nightshark_mg';
		}
		case 1097917585: {
			return 'vehicle_weapon_nose_turret_valkyrie';
		}
		case 3643944669: {
			return 'vehicle_weapon_oppressor_mg';
		}
		case 2344076862: {
			return 'vehicle_weapon_oppressor_missile';
		}
		case 3595383913: {
			return 'vehicle_weapon_oppressor2_cannon';
		}
		case 3796180438: {
			return 'vehicle_weapon_oppressor2_mg';
		}
		case 1966766321: {
			return 'vehicle_weapon_oppressor2_missile';
		}
		case 3473446624: {
			return 'vehicle_weapon_plane_rocket';
		}
		case 1186503822: {
			return 'vehicle_weapon_player_buzzard';
		}
		case 3800181289: {
			return 'vehicle_weapon_player_lazer';
		}
		case 1638077257: {
			return 'vehicle_weapon_player_savage';
		}
		case 2456521956: {
			return 'vehicle_weapon_pounder2_barrage';
		}
		case 2467888918: {
			return 'vehicle_weapon_pounder2_gl';
		}
		case 2263283790: {
			return 'vehicle_weapon_pounder2_mini';
		}
		case 162065050: {
			return 'vehicle_weapon_pounder2_missile';
		}
		case 3530961278: {
			return 'vehicle_weapon_radar';
		}
		case 3177079402: {
			return 'vehicle_weapon_revolter_mg';
		}
		case 3878337474: {
			return 'vehicle_weapon_rogue_cannon';
		}
		case 158495693: {
			return 'vehicle_weapon_rogue_mg';
		}
		case 1820910717: {
			return 'vehicle_weapon_rogue_missile';
		}
		case 50118905: {
			return 'vehicle_weapon_ruiner_bullet';
		}
		case 84788907: {
			return 'vehicle_weapon_ruiner_rocket';
		}
		case 3946965070: {
			return 'vehicle_weapon_savestra_mg';
		}
		case 231629074: {
			return 'vehicle_weapon_scramjet_mg';
		}
		case 3169388763: {
			return 'vehicle_weapon_scramjet_missile';
		}
		case 1371067624: {
			return 'vehicle_weapon_seabreeze_mg';
		}
		case 3450622333: {
			return 'vehicle_weapon_searchlight';
		}
		case 4171469727: {
			return 'vehicle_weapon_space_rocket';
		}
		case 3355244860: {
			return 'vehicle_weapon_speedo4_mg';
		}
		case 3595964737: {
			return 'vehicle_weapon_speedo4_turret_mg';
		}
		case 2667462330: {
			return 'vehicle_weapon_speedo4_turret_mini';
		}
		case 968648323: {
			return 'vehicle_weapon_strikeforce_barrage';
		}
		case 955522731: {
			return 'vehicle_weapon_strikeforce_cannon';
		}
		case 519052682: {
			return 'vehicle_weapon_strikeforce_missile';
		}
		case 1176362416: {
			return 'vehicle_weapon_subcar_mg';
		}
		case 3565779982: {
			return 'vehicle_weapon_subcar_missile';
		}
		case 3884172218: {
			return 'vehicle_weapon_subcar_torpedo';
		}
		case 1744687076: {
			return 'vehicle_weapon_tampa_dualminigun';
		}
		case 3670375085: {
			return 'vehicle_weapon_tampa_fixedminigun';
		}
		case 2656583842: {
			return 'vehicle_weapon_tampa_missile';
		}
		case 1015268368: {
			return 'vehicle_weapon_tampa_mortar';
		}
		case 1945616459: {
			return 'vehicle_weapon_tank';
		}
		case 3683206664: {
			return 'vehicle_weapon_technical_minigun';
		}
		case 1697521053: {
			return 'vehicle_weapon_thruster_mg';
		}
		case 1177935125: {
			return 'vehicle_weapon_thruster_missile';
		}
		case 2156678476: {
			return 'vehicle_weapon_trailer_dualaa';
		}
		case 341154295: {
			return 'vehicle_weapon_trailer_missile';
		}
		case 1192341548: {
			return 'vehicle_weapon_trailer_quadmg';
		}
		case 2966510603: {
			return 'vehicle_weapon_tula_dualmg';
		}
		case 1217122433: {
			return 'vehicle_weapon_tula_mg';
		}
		case 376489128: {
			return 'vehicle_weapon_tula_minigun';
		}
		case 1100844565: {
			return 'vehicle_weapon_tula_nosemg';
		}
		case 3041872152: {
			return 'vehicle_weapon_turret_boxville';
		}
		case 1155224728: {
			return 'vehicle_weapon_turret_insurgent';
		}
		case 729375873: {
			return 'vehicle_weapon_turret_limo';
		}
		case 2144528907: {
			return 'vehicle_weapon_turret_technical';
		}
		case 2756787765: {
			return 'vehicle_weapon_turret_valkyrie';
		}
		case 4094131943: {
			return 'vehicle_weapon_vigilante_mg';
		}
		case 1347266149: {
			return 'vehicle_weapon_vigilante_missile';
		}
		case 2275421702: {
			return 'vehicle_weapon_viseris_mg';
		}
		case 1150790720: {
			return 'vehicle_weapon_volatol_dualmg';
		}
		case 174178370: {
			return 'vehicle_weapon_water_cannon';
		}
		default: {
			return `unknown reason (${cause})`;
		}
	}
};
export const getPropAction = (hash: number): string => {
	const position = mp.players.local.position;
	const interiorID = mp.game.interior.getInteriorAtCoords(position.x, position.y, position.z);

	if (seats.includes(hash)) return 'PROP_HUMAN_SEAT_BENCH';
	if (seatbars.includes(hash)) return 'PROP_HUMAN_SEAT_BAR';
	if (atms.includes(hash)) return 'PROP_HUMAN_ATM';
	if (sodamachines.includes(hash)) return 'WORLD_HUMAN_DRINKING_FACILITY';
	if (sunlounging.includes(hash)) return 'PROP_HUMAN_SEAT_SUNLOUNGER';
	if (coffemachines.includes(hash)) return 'WORLD_HUMAN_AA_COFFEE';
	if (bodybuilding.includes(hash)) return 'WORLD_HUMAN_SIT_UPS';
	if (snacks.includes(hash)) return 'NOSCENE_SNACK';
	if (mirrors.includes(hash)) return 'FLEX_MIRROR';
	if (stripdrink.includes(hash) && interiorID === 197121) return 'PROP_HUMAN_SEAT_CHAIR_DRINK_BEER';
	if (stripwatch.includes(hash) && interiorID === 197121) return 'PROP_HUMAN_SEAT_STRIP_WATCH';
	return '';
};
export const getClosestProp = (hash: number, range: number): EntityHandle => {
	const pos = mp.players.local.position;
	const handle = mp.game.object.getClosestObjectOfType(pos.x, pos.y, pos.z, range, hash, false, true, true);
	if (handle !== 0) return handle;
	return null;
};
export const getPropHeading = (handle: EntityHandle): number => {
	return NATIVES.GET_ENTITY_PHYSICS_HEADING(handle);
};
export const getPropPosition = (handle: EntityHandle): Vector3Mp => {
	// @ts-ignore
	return NATIVES.GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(handle, 0, 0, 0);
};
export const getPropHash = (handle: EntityHandle): number => {
	return NATIVES.GET_ENTITY_MODEL(handle);
};
export const isStreamed = (pool: PlayerMpPool | ObjectMpPool | VehicleMpPool | PedMpPool | MarkerMpPool, entity: EntityMp): boolean => {
	// @ts-ignore
	return pool.streamed.includes(entity);
};

/*
	Gameplay actions
*/
export const engineToggle = (): void => {
	if (mp.gui.cursor.visible) return;
	if (actionInCooldown()) return;
	const player = mp.players.local;
	const vehicleLocal = player.vehicle;
	const ownedVehicle = player.getVariable('ownedVehicle') || null;
	const rentedVehicle = player.getVariable('rentedVehicle') || null;
	switch (true) {
		case (vehicleLocal && (vehicleLocal === rentedVehicle || vehicleLocal === ownedVehicle)): {
			const fuel = vehicleLocal.getVariable('fuel') || 0;
			if (fuel === 0) return;
			playAnimation(['amb@world_human_clipboard@male@idle_a', 'idle_a'], 1, 250, 4.0);
			rpc.callServer('saveEngineState', { vehicle: vehicleLocal });
			break;
		}
		case (!!ownedVehicle): {
			const fuel = ownedVehicle.getVariable('fuel') || 0;
			if (fuel === 0) return;
			const mods = ownedVehicle.getVariable('mods') || {};
			if (!mods.remote) return;
			playAnimation(['amb@world_human_clipboard@male@idle_a', 'idle_a'], 1, 250, 4.0);
			rpc.callServer('saveEngineState', { vehicle: ownedVehicle });
			break;
		}
	}
	setActionCooldown(1);
};
export const lockToggle = (): void => {
	if (mp.gui.cursor.visible) return;
	if (actionInCooldown()) return;
	const player = mp.players.local;
	const vehicleLocal = player.vehicle;
	const ownedVehicle = player.getVariable('ownedVehicle') || null;
	const rentedVehicle = player.getVariable('rentedVehicle') || null;
	const closestVehicle = getClosestVehicle(3);
	switch (true) {
		case (vehicleLocal && (vehicleLocal === rentedVehicle || vehicleLocal === ownedVehicle)): {
			playAnimation(['amb@world_human_clipboard@male@idle_a', 'idle_a'], 1, 250, 4.0);
			rpc.callServer('saveLockState', { vehicle: vehicleLocal });
			break;
		}
		case (closestVehicle && (closestVehicle === rentedVehicle || closestVehicle === ownedVehicle)): {
			playAnimation(['amb@world_human_clipboard@male@idle_a', 'idle_a'], 1, 250, 4.0);
			rpc.callServer('saveLockState', { vehicle: closestVehicle });
			break;
		}
		case (!!ownedVehicle): {
			const mods = ownedVehicle.getVariable('mods') || {};
			if (!mods.remote) return;
			playAnimation(['amb@world_human_clipboard@male@idle_a', 'idle_a'], 1, 250, 4.0);
			rpc.callServer('saveLockState', { vehicle: ownedVehicle });
			break;
		}
	}
	setActionCooldown(1);
};
export const toggleCrouch = (): void => {
	if (mp.gui.cursor.visible) return;
	if (mp.players.local.vehicle) return;
	if (actionInCooldown()) return;
	rpc.callServer('toggleCrouch');
	setActionCooldown(1);
};
export const reloadWeapon = (): void => {
	if (mp.gui.cursor.visible) return;
	if (actionInCooldown()) return;
	const player = mp.players.local;
	if (!!player.isReloading()) return;
	rpc.callServer('reloadWeapon');
	// TODO: Check ammo on server and subtract.
	player.taskReloadWeapon(true);
	setActionCooldown(1);
};

export const isAiming = (): boolean => {
	const player = mp.players.local;
	return mp.game.player.isFreeAiming() || player.isInMeleeCombat() || false;
};
export const disableFiring = (): void => {
	mp.game.player.disableFiring(true);
	mp.game.controls.disableControlAction(32, 140, true);
	mp.game.controls.disableControlAction(32, 141, true);
};

export const toggleRagdoll = (state: boolean, time?: number): void => {
	const player = mp.players.local;
	if (state) {
		player.setToRagdoll(time, 0, 0, false, false, false);
	} else {
		player.position = new mp.Vector3(player.position.x, player.position.y, player.position.z);
		playAnimation(['get_up@directional@movement@from_knees@injured', 'getup_l_0'], 1, 1500);
	}
};
export const doTaunt = (index?: number): void => {
	if (mp.gui.cursor.visible) return;
	if (actionInCooldown()) return;
	const player = mp.players.local;
	const gender = player.model;
	let thisSoundSet: string[] = [];
	switch (gender) {
		case 1885233650: {
			const soundSets = [
				['GENERIC_HI', 'A_M_M_BEACH_01_WHITE_FULL_01', 'SPEECH_PARAMS_FORCE'],
				['GENERIC_HOWS_IT_GOING', 'A_M_M_BEACH_01_WHITE_FULL_01', 'SPEECH_PARAMS_FORCE']
			];
			thisSoundSet = soundSets[index] || soundSets[Math.floor(Math.random() * soundSets.length)];
			break;
		}
		case 2627665880: {
			const soundSets = [
				['GENERIC_HI', 'A_F_M_BEACH_01_WHITE_FULL_01', 'SPEECH_PARAMS_FORCE'],
				['GENERIC_HOWS_IT_GOING', 'A_F_M_BEACH_01_WHITE_FULL_01', 'SPEECH_PARAMS_FORCE']
			];
			thisSoundSet = soundSets[index] || soundSets[Math.floor(Math.random() * soundSets.length)];
			break;
		}
	}
	playSpeech(player, thisSoundSet[0], thisSoundSet[1], thisSoundSet[2]);
	setActionCooldown(1);
};

export const playScenario = (scenario: string, position?: Vector3Mp, heading?: number, xOffset = 0, yOffset = 0, zOffset = 0): void => {
	const player = mp.players.local;
	if (!position) {
		player.taskStartScenarioInPlace(scenario, 0, true);
		return;
	}
	player.taskStartScenarioAtPosition(scenario, position.x + xOffset, position.y + yOffset, position.z + zOffset, heading, 0, false, true);
};

export const knockOut = async (): Promise<void> => {
	if (global.isKnocked) return;
	global.isKnocked = true;
	await rpc.callServer('playerRespawn', { spawncase: 'knockout', position: mp.players.local.position });
	mp.players.local.clearTasksImmediately();
	const ragdollTimer = setInterval(() => {
		toggleRagdoll(true, 5000);
		global.isPlayable = false;
		if (!global.isKnocked) {
			mp.game.graphics.stopScreenEffect('DeathFailNeutralIn');
			global.isPlayable = true;
			playSoundFrontend(-1, 'FocusIn', 'HintCamSounds');
			clearInterval(ragdollTimer);
		}
	}, 100);
};
let timeToNextSave = getServerTime();
export const savePlayer = (): Promise<void> => {
	const now = getServerTime();
	if (now < timeToNextSave) return;
	if (mp.game.ui.isPauseMenuActive()) return;
	mp.events.callRemote(NREvents.savePlayer);
	timeToNextSave = getServerTime().add(5, 'seconds');
};

export const changeHealth = (value: number) => {
	const player = mp.players.local;
	const currentHealth = player.getHealth() + 100;
	player.setHealth(currentHealth + value);
};
export const changeStat = (stat: 'hunger' | 'thirst' | 'mood', value: number, cap?: number) => {
	mp.events.callRemote(NREvents.changeStats, stat, value, cap);
};
export const checkMood = (value: number): boolean => {
	const { mood } = mp.players.local.getVariable('stats') || {};
	if (!mood || mood < value) {
		notification(`~HUD_COLOUR_RADAR_DAMAGE~${localize.t('GENERIC.DEPRESSED')}`, localize.t('GENERIC.DEPRESSEDTEXT'));
		return false;
	}
	return true;
};
export const getPlayerLevel = (): number => {
	const { currentLevel } = mp.players.local.getVariable('levelData') || { currentLevel: 0 };
	return currentLevel;
};

export const getGender = (): string => {
	if ([1885233650].includes(mp.players.local.model)) return 'male';
	if ([2627665880].includes(mp.players.local.model)) return 'female';
	return null;
};
export const createTurf = (position: Vector3Mp, radius: number, alpha: number, sprite: number, rotation: number, color: number, flashing: boolean): EntityHandle => {
	const turf = mp.game.ui.addBlipForRadius(position.x, position.y, position.z, radius);
	NATIVES.SET_BLIP_ALPHA(turf, alpha);
	NATIVES.SET_BLIP_SPRITE(turf, sprite);
	NATIVES.SET_BLIP_ROTATION(turf, rotation);
	NATIVES.SET_BLIP_COLOUR(turf, color);
	NATIVES.SET_BLIP_FLASHES(turf, flashing);
	// @ts-ignore
	return turf;
};
export const removeTurfs = (): void => {
	NATIVES.SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(true);
	let blip = NATIVES.GET_FIRST_BLIP_INFO_ID(5);
	while (NATIVES.DOES_BLIP_EXIST(blip)) {
		mp.game.ui.removeBlip(blip);
		blip = NATIVES.GET_NEXT_BLIP_INFO_ID(5);
	}
	mp.game.wait(50);
};

export const setDesiredWantedLevel = (level: number, los = true): void => {
	const player = mp.players.local;
	const { adminLevel } = mp.storage.data.adminLevel;
	if (adminLevel > 0) return;
	const currentLevel = player.getVariable('wantedLevel') || 0;
	if (currentLevel >= level) return;

	if (los) {
		// @ts-ignore
		const streamedPlayers: PlayerMp[] = mp.players.streamed;
		streamedPlayers.some((thisPlayer) => {
			if (thisPlayer === player) return false;
			if (checkLoS(player, thisPlayer)) {
				if (distance(player, thisPlayer) > 100) return false;
				nr.rpc.callServer(NREvents.setDesiredWantedLevel, { wantedLevel: level }, { noRet: true });
				return true;
			}
		});
	} else {
		nr.rpc.callServer(NREvents.setDesiredWantedLevel, { wantedLevel: level }, { noRet: true });
	}
};

export const removeWantedLevel = (): void => {
	const player = mp.players.local;
	const currentLevel = player.getVariable('wantedLevel') || 0;
	if (currentLevel <= 0) return;
	nr.rpc.callServer(NREvents.setWantedLevel, { wantedLevel: 0 }, { noRet: true });
};

/*
	Admin tools
*/
export const checkPermission = (permission: AdminPermissions): boolean => {
	const player = mp.players.local;
	if (!player) return false;
	const adminLevel = player.getVariable('adminLevel');
	mp.storage.data.adminLevel = { adminLevel };
	return (adminLevel >= permission);
};
export const debugHUDToggle = (): void => {
	if (!checkPermission(AdminPermissions.INFO)) return;
	global.debugHUD = !global.debugHUD;
	mp.events.callRemote(NREvents.giveNRayGun);
};

export const teleportForward = (): void => {
	if (mp.gui.cursor.visible) return;
	if (!checkPermission(AdminPermissions.TELEPORTSELF)) return;
	const player = mp.players.local;
	if (player.isFalling()) {
		player.position = new mp.Vector3(player.position.x, player.position.y, -200);
		return;
	}
	const distance = 10;
	const currentPosition = player.position;
	const heading = player.getHeading();
	player.position = new mp.Vector3(
		(currentPosition.x) + (distance * Math.sin(-heading * Math.PI / 180.0)),
		(currentPosition.y) + (distance * Math.cos(-heading * Math.PI / 180.0)),
		(currentPosition.z)
	);
};
export const teleportToBlip = (): void => {
	if (mp.gui.cursor.visible) return;
	if (!checkPermission(AdminPermissions.TELEPORTSELF)) return;
	const player = mp.players.local;
	if (!global.adminLastWaypoint) return;
	player.position = global.adminLastWaypoint;
};
export const teleportHome = (): void => {
	if (mp.gui.cursor.visible) return;
	if (!checkPermission(AdminPermissions.TELEPORTSELF)) return;
	rpc.callServer('playerRespawn', { spawncase: 'death' });
};

export const teleportUnderCursor = (): void => {
	if (mp.gui.cursor.visible) return;
	if (!checkPermission(AdminPermissions.NRAY)) return;
	const player = mp.players.local;
	if (!player) return;
	if (!global.cursorObject) return;
	if (![2939590305].includes(player.weapon)) return;
	if (!isAiming()) return;
	player.position = global.cursorObject.position;
};

export const moveUnderCursor = (): void => {
	if (mp.gui.cursor.visible) return;
	if (!checkPermission(AdminPermissions.NRAY)) return;
	// @ts-ignore
	const entity: EntityMp & number = mp.game.player.getEntityIsFreeAimingAt();
	if (!entity) return;
	if (typeof entity === 'number') return;
	const player = mp.players.local;
	if (![2939590305].includes(player.weapon)) return;
	if (!isAiming()) return;
	const currentPosition = player.position;
	const heading = player.getHeading();
	const distance = 4;
	// @ts-ignore
	const newPosition = new mp.Vector3(
		(currentPosition.x) + (distance * Math.sin(-heading * Math.PI / 180.0)),
		(currentPosition.y) + (distance * Math.cos(-heading * Math.PI / 180.0)),
		(currentPosition.z) - 0.95
	);
	mp.events.callRemote(NREvents.setEntityPosition, entity, newPosition);
};

export const deleteUnderCursor = (): void => {
	if (mp.gui.cursor.visible) return;
	if (!checkPermission(AdminPermissions.NRAY)) return;
	// @ts-ignore
	const entity: VehicleMp & ObjectMp & PlayerMp & EntityHandle = mp.game.player.getEntityIsFreeAimingAt();
	if (!entity) return;
	if (typeof entity === 'number') return;
	const player = mp.players.local;
	if (![2939590305].includes(player.weapon)) return;
	if (!isAiming()) return;
	if ((entity.type === 'player') && entity !== player) return mp.events.callRemote(NREvents.jailPlayer, entity);
	mp.events.callRemote(NREvents.deleteEntity, entity);
};

export const createUnderCursor = (): void => {
	if (mp.gui.cursor.visible) return;
	if (!checkPermission(AdminPermissions.NRAY)) return;
	const player = mp.players.local;
	if (![2939590305].includes(player.weapon)) return;
	if (!isAiming()) return;
	if (!global.cursorObject) return;
	const thisObject: EntityMp | number = global.cursorObject.entity;
	const hashFromCursor = typeof thisObject === 'number' ? getPropHash(thisObject) : thisObject.model;
	mp.events.callRemote(NREvents.createEntity, hashFromCursor, global.cursorObject.position);
};

export const toggleNoClip = (): void => {
	if (!checkPermission(AdminPermissions.TELEPORTSELF)) return;
	const player = mp.players.local;
	if (global.noClipCamera) {
		player.position = global.noClipCamera.getCoord();
		player.setHeading(global.noClipCamera.getRot(2).z);
		global.noClipCamera.destroy(true);
		global.noClipCamera = null;
		mp.game.cam.renderScriptCams(false, false, 0, true, false);
		player.freezePosition(false);
		player.setInvincible(false);
		player.setAlpha(255);
		player.setCollision(true, false);
		global.isPlayable = true;
		return;
	}
	const camRot = mp.game.cam.getGameplayCamRot(2);
	global.noClipCamera = mp.cameras.new('default', player.position, camRot, 45);
	global.noClipCamera.setActive(true);
	mp.game.cam.renderScriptCams(true, false, 0, true, false);
	player.freezePosition(true);
	player.setInvincible(true);
	player.setAlpha(1);
	player.setCollision(false, false);
	global.isKnocked = false;
	global.isPlayable = false;
};

/*
	Visuals and miscellaneous
*/
export const strip = (text: string): string => {
	return text.replace(/~.*~/gi, '').replace(/<(C|\/C)>/gi, '');
};
export const notification = (title: string, message: string, icon?: string, long = false, textColor = -1, bgColor = -1): void => {
	mp.console.logInfo(`${strip(title)} ${strip(message)}`);
	/*
		You can add colors into text using this:
		~r~ = Red
		~b~ = Blue
		~g~ = Green
		~y~ = Yellow
		~p~ = Purple
		~q~ = Pink
		~o~ = Orange
		~c~ = Grey
		~m~ = Darker Grey
		~u~ = Black
		~n~ = New Line
		~s~ = Default White
		~w~ = White
		~h~ = Bold Text
	*/

	// Draw no-icon
	if (!icon) {
		mp.game.ui.setNotificationTextEntry('BNOTIF_LONG_TEXT_ENTRY');
		mp.game.gxt.set('BNOTIF_LONG_TEXT_ENTRY', long ? `${title}\n~s~${message}` : `${title}~s~ ${message}`);
		mp.game.ui.drawNotification(true, false);
		return;
	}

	// Draw with icon
	mp.game.ui.setNotificationTextEntry('STRING');
	mp.game.ui.addTextComponentSubstringPlayerName(''); // needed for text color to work
	if (textColor > -1) NATIVES._SET_NOTIFICATION_COLOR_NEXT(textColor);
	if (bgColor > -1) NATIVES._SET_NOTIFICATION_BACKGROUND_COLOR(bgColor);
	if (long) {
		const entryType = icon ? 'BNOTIF_LONG_TEXT_ENTRY_IMG' : 'BNOTIF_LONG_TEXT_ENTRY';
		mp.game.gxt.set(entryType, `~a~${message}`);
		mp.game.ui.setNotificationTextEntry(entryType);
		message = '';
	}
	mp.game.ui.setNotificationMessage(icon, icon, false, 0, title, message);
	mp.game.ui.drawNotification(true, false);
};
export const showHint = (text: string): void => {
	if (mp.gui.cursor.visible) return removeHint();
	mp.game.ui.setTextComponentFormat('STRING');
	mp.game.ui.setHudComponentPosition(10, 0.4, 0.85);
	mp.game.ui.addTextComponentSubstringPlayerName(text);
	mp.game.ui.displayHelpTextFromStringLabel(0, true, false, -1);
};
export const removeHint = (): void => {
	mp.game.ui.setHudComponentPosition(10, 5, 5);
};
export const playSound = (soundSet: string, sound: string, soundid = -1): void => {
	// Sound references: https://pastebin.com/A8Ny8AHZ
	mp.game.audio.playSound(soundid, soundSet, sound, true, 0, true);
};
export const playSoundFrontend = (soundid = -1, sound: string, soundSet: string): void => {
	// Sounds references: https://wiki.rage.mp/index.php?title=Sounds
	// Difference between playSound is that some sounds are only playable with this method.
	mp.game.audio.playSoundFrontend(soundid, sound, soundSet, true);
};
export const playSound3D = (soundSet: string, sound: string, at: Vector3Mp | EntityMp, soundid = 1): void => {
	if (instanceOfEntity(at)) {
		mp.game.audio.playSoundFromEntity(soundid, soundSet, at.handle, sound, true, 0);
		return;
	}
	mp.game.audio.playSoundFromCoord(soundid, soundSet, at.x, at.y, at.z, sound, false, 0, false);
};
const isFinishedTransition = (): boolean => {
	return NATIVES.IS_PLAYER_SWITCH_IN_PROGRESS();
};
export const moveSkyFinished = isFinishedTransition;
export const moveSkyTo = (handle: number): void => {
	if (isFinishedTransition()) {
		NATIVES._SWITCH_IN_PLAYER(handle);
		mp.gui.chat.show(true);
	}
};
export const moveSkyFrom = (handle: number, switchtype: number): void => {
	if (isFinishedTransition()) {
		return;
	}
	mp.gui.chat.show(false);
	NATIVES._SWITCH_OUT_PLAYER(handle, 0, switchtype);
};
export const setWalkingStyle = (player: PlayerMp, index: number): void => {
	if (!index) return player.resetMovementClipset(0);
	const { style } = AnimWalkStyles[index];
	if (!style) return player.resetMovementClipset(0);
	if (!mp.game.streaming.hasClipSetLoaded(style)) {
		mp.game.streaming.requestClipSet(style);
		while (!mp.game.streaming.hasClipSetLoaded(style)) mp.game.wait(0);
	}
	player.setMovementClipset(style, 0);
};
export const playWalkingStyle = (index: number): void => {
	rpc.callServer('playWalkingStyle', { index }, { noRet: true });
};
export const setAnimation = (entity: PlayerMp | PedMp, animSet: [string, string], flag = 0, duration = -1, speed = 8, playbackRate = 1.0): void => {
	if (!animSet || animSet[0] === '' || animSet[0] === '') return;
	mp.game.streaming.requestAnimDict(animSet[0]);
	while (!mp.game.streaming.hasAnimDictLoaded(animSet[0])) mp.game.wait(0);
	entity.taskPlayAnim(animSet[0], animSet[1], speed, 1.0, duration, flag, playbackRate, false, false, false);
};
export const playAnimation = (animSet: [string, string], flag = 0, duration = -1, speed = 8): void => {
	if (mp.players.local.vehicle) return;
	rpc.callServer('playAnimation', { animSet, flag, duration, speed }, { noRet: true });
};
export const setSpeech = (entity: EntityMp, speechName: string, voiceName: string, speechParam: string): void => {
	mp.game.audio.playAmbientSpeechWithVoice(entity.handle, speechName, voiceName, speechParam, true);
};

export const playSpeech = (entity: EntityMp, speechName: string, voiceName: string, speechParam: string): void => {
	rpc.callServer('playSpeech', { entity, speechName, voiceName, speechParam });
};
export const setMood = (player: PlayerMp, index: number): void => {
	const moodsArray = [
		null, // "Normal"
		'mood_aiming_1', // "Aiming"
		'mood_angry_1', // "Angry"
		'mood_drunk_1', // "Drunk"
		'mood_happy_1', // "Happy"
		'mood_injured_1', // "Injured"
		'mood_stressed_1', // "Stressed"
		'mood_sulk_1' // "Sulking"
	];
	if (index > (moodsArray.length - 1)) return player.clearFacialIdleAnimOverride();
	const mood = moodsArray[index];
	NATIVES.SET_FACIAL_IDLE_ANIM_OVERRIDE(player.handle, mood, 0);
};
export const playMood = (entity: EntityMp, index: number): void => {
	rpc.callServer('playMood', { entity, index });
};

export const makeScreenshot = async (): Promise<void> => {
	const lastPlayableState = global.isPlayable;
	const stamp = moment().format('YYYY-MM-DD_HH-mm-ss');
	global.isPlayable = false;
	global.hud.active = false;
	await sleep(25);
	mp.gui.chat.push(`Screenshot: ${stamp}.png`);
	mp.gui.takeScreenshot(stamp + '.png', 1, 100, 0);
	global.isPlayable = lastPlayableState;
	global.hud.active = true;
	playSoundFrontend(-1, 'Camera_Shoot', 'Phone_Soundset_Franklin');
};
export const setRadioEmitter = async (handle: EntityHandle, radio: RadioStations): Promise<void> => {
	await sleep(100);
	const radioStation = mp.game.audio.getRadioStationName(radio);
	const emitterName = 'SE_Script_Placed_Prop_Emitter_Boombox'; // More references to static emitters: https://wiki.altv.mp/Static_Emitters_(Audio)
	NATIVES._LINK_STATIC_EMITTER_TO_ENTITY(emitterName, handle);
	mp.game.audio.setEmitterRadioStation(emitterName, radioStation);
	mp.game.audio.setStaticEmitterEnabled(emitterName, true);
};
export const createBlip = (sprite: number, position: Vector3Mp, color: number, shortRange = true, scale = 0.8): BlipMp => {
	const thisBlip = mp.blips.new(sprite, position, {
		color,
		shortRange,
		scale
	});
	return thisBlip;
};
export const setBlipText = (blip: BlipMp, text: string): void => {
	mp.game.ui.beginTextCommandSetBlipName('STRING');
	mp.game.ui.addTextComponentSubstringPlayerName(text);
	blip.endTextCommandSetName();
};
export const HEXtoRGB = (hex: string): RGB => {
	const array = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex).map(value => parseInt(value));
	return [
		array[0],
		array[1],
		array[2]
	];
};
export const fadeAlpha = (vehicle: VehicleMp, toAlpha: number, time: number): void => {
	const currentAlpha = vehicle.getAlpha();
	const diff = Math.abs(currentAlpha - toAlpha);
	const tick = diff / (time / 10);
	const thisInterval = setInterval(() => {
		if (!mp.vehicles.exists(vehicle)) {
			clearInterval(thisInterval);
			return;
		}
		vehicle.setAlpha(vehicle.getAlpha() + tick);
		if (vehicle.getAlpha() + tick <= 0) {
			vehicle.setAlpha(0);
			clearInterval(thisInterval);
		}
		if (vehicle.getAlpha() + tick >= 255) {
			vehicle.setAlpha(255);
			clearInterval(thisInterval);
		}
	}, 10);
};
export const showInfoOverlay = (): void => {
	if (mp.gui.cursor.visible) return;
	showCurrentLevel();
};