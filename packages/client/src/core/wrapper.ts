import rpc, { Browser, CallOptions, ProcedureListener } from 'rage-rpc';
// import { log } from 'modules/debug';

class NRWrap {
	Vector3 = mp.Vector3;
	joaat = mp.game.joaat;

	public rpc = {
		register: (name: string, cb: ProcedureListener): Function => {
			try {
				const r = rpc.register(name, cb);
				return r;
			} catch (error) {
				mp.console.logError(`NRage.rpc.register: ${error.message}`);
			}
		},
		unregister: (name: string): void => {
			try {
				rpc.unregister(name);
			} catch (error) {
				mp.console.logError(`NRage.rpc.unregister: ${error.message}`);
			}
		},
		call: <T = any>(name: string, args?: any, options?: CallOptions): Promise<T> => {
			try {
				const r = rpc.call(name, args, options);
				return r;
			} catch (error) {
				mp.console.logError(`NRage.rpc.call: ${error.message}`);
			}
		},
		callServer: <T = any>(name: string, args?: any, options?: CallOptions): Promise<T> => {
			try {
				const r = rpc.callServer(name, args, options);
				return r;
			} catch (error) {
				mp.console.logError(`NRage.rpc.callServer: ${error.message}`);
			}
		},
		on: (name: string, cb: ProcedureListener): Function => {
			try {
				const r = rpc.on(name, cb);
				return r;
			} catch (error) {
				mp.console.logError(`NRage.rpc.on: ${error.message}`);
			}
		},
		off: (name: string, cb: ProcedureListener): void => {
			try {
				rpc.off(name, cb);
			} catch (error) {
				mp.console.logError(`NRage.rpc.off: ${error.message}`);
			}
		},
		trigger: (name: string, args?: any): void => {
			try {
				rpc.trigger(name, args);
			} catch (error) {
				mp.console.logError(`NRage.rpc.trigger: ${error.message}`);
			}
		},
		triggerServer: (name: string, args?: any): void => {
			try {
				rpc.triggerServer(name, args);
			} catch (error) {
				mp.console.logError(`NRage.rpc.triggerServer: ${error.message}`);
			}
		},
		// Client-only
		callBrowser: <T = any>(browser: Browser, name: string, args?: any, options?: CallOptions): Promise<T> => {
			try {
				const r = rpc.callBrowser(browser, name, args, options);
				return r;
			} catch (error) {
				mp.console.logError(`NRage.rpc.callBrowser: ${error.message}`);
			}
		},
		triggerBrowser: (browser: Browser, name: string, args?: any): void => {
			try {
				rpc.triggerBrowser(browser, name, args);
			} catch (error) {
				mp.console.logError(`NRage.rpc.triggerBrowser: ${error.message}`);
			}
		}
	}
}

export const nr = new NRWrap();
