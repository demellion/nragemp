import moment from 'moment';

class ScaleformAPI {
	handle: number;

	constructor(scaleformName: string) {
		this.handle = mp.game.graphics.requestScaleformMovie(scaleformName);
		while (!mp.game.graphics.hasScaleformMovieLoaded(this.handle)) mp.game.wait(0);
	}

	// thanks kemperrr
	callFunction(functionName: string, ...args: any[]): void {
		mp.game.graphics.pushScaleformMovieFunction(this.handle, functionName);

		args.forEach((arg: string | boolean | number) => {
			switch (typeof arg) {
				case 'string': {
					mp.game.graphics.pushScaleformMovieFunctionParameterString(arg);
					break;
				}

				case 'boolean': {
					mp.game.graphics.pushScaleformMovieFunctionParameterBool(arg);
					break;
				}

				case 'number': {
					if (Number(arg) === arg && arg % 1 !== 0) {
						mp.game.graphics.pushScaleformMovieFunctionParameterFloat(arg);
					} else {
						mp.game.graphics.pushScaleformMovieFunctionParameterInt(arg);
					}
				}
			}
		});
		mp.game.graphics.popScaleformMovieFunctionVoid();
	}

	renderFullscreen(): void {
		mp.game.graphics.drawScaleformMovieFullscreen(this.handle, 255, 255, 255, 255, false);
	}

	dispose(): void {
		mp.game.graphics.setScaleformMovieAsNoLongerNeeded(this.handle);
	}
}

let scaleform: ScaleformAPI;
let bigMsgInit: moment.Moment;
let bigMsgDuration: number;
let bigMsgAnimatedOut = false;
let bigMsgRender = false;

export const showShardScaleform = (title: string, message: string, titleColor: number, bgColor: number, time = 5000): void => {
	if (!scaleform) scaleform = new ScaleformAPI('mp_big_message_freemode');
	scaleform.callFunction('SHOW_SHARD_CENTERED_MP_MESSAGE', title, message, titleColor, bgColor);
	bigMsgInit = moment();
	bigMsgDuration = time;
	bigMsgAnimatedOut = false;
	if (!bigMsgRender) {
		bigMsgRender = true;
		mp.events.add('render', () => {
			if (scaleform) {
				scaleform.renderFullscreen();
				if (moment().diff(bigMsgInit) > bigMsgDuration) {
					if (!bigMsgAnimatedOut) {
						scaleform.callFunction('TRANSITION_OUT');
						bigMsgAnimatedOut = true;
						bigMsgDuration += 750;
					} else {
						scaleform.dispose();
						bigMsgInit = null;
						scaleform = null;
					}
				}
			}
		});
	}
};