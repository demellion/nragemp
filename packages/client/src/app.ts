import { load } from './loaders/loader';

export class App {
	public static async start(): Promise<void> {
		await load();
	}
}
