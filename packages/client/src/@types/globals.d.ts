type CameraNR = import('../modules/cameras').CameraNR;
type Routes = import('common/constants/routes').Routes;

declare namespace NodeJS {
	interface Global {
		hud: BrowserMp;
		dialog: string;
		lastDialog: Routes;
		vendor: EntityMp;
		camera: CameraNR;
		debugHUD: boolean;
		actionCooldown: object;
		cursorObject: {
			entity: EntityMp | number,
			position: Vector3Mp,
			surfaceNormal: Vector3Mp
		};
		currentNPC: EntityMp | number;
		currentColshape: ColshapeMp;
		noClipCamera: CameraMp;
		currentAction: string;
		currentActionName: string;
		isInSafezone: boolean;
		isKnocked: boolean;
		isPlayable: boolean;
		safezoneLevel: number;
		adminLastWaypoint: Vector3Mp;
		adminFrozen: boolean;
		ACLastShotCount: number;
		ACShotsFlagged: number;
	}
}
