const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const NODE_ENV = process.env.NODE_ENV || 'development';
const IS_DEV = NODE_ENV === 'development';
const OUTPUT_DIR = 'client_packages';
const OUTPUT_PATH = path.resolve(__dirname, '../../', 'build', OUTPUT_DIR);

const webpackConfig = {
	mode: NODE_ENV || 'production',
	watch: IS_DEV,
	stats: 'errors-only',
	context: path.join(__dirname, 'src'),
	entry: {
		index: path.resolve(__dirname, 'src', 'index.ts')
	},
	output: {
		path: OUTPUT_PATH,
		filename: '[name].js'
	},
	target: 'node',
	resolve: {
		extensions: ['.ts', '.js'],
		alias: {
			common: path.resolve(__dirname, '../common')
		}
	},
	plugins: [
		new CleanWebpackPlugin()
	],
	module: {
		rules: [
			{ test: /\.ts$/, loader: 'ts-loader' }
		]
	}
};

module.exports = webpackConfig;
