import { SafeZoneLevels } from '../../enums/saveZoneLevels';
import { Wallet } from '../wallet';

export interface HudInfo {
	isPlayable: boolean;
	name: string;
	id: number;
	online: number;
	wallet: Wallet;
	area: string;
	safeZoneLevel: SafeZoneLevels;
	safeZoneName: string;
	street: string;
	speaking: boolean;
	time: string;
	servername: string
	currentActionName: string;
	isVehicle: boolean;
	vehicleSpeed: number;
	vehicleFuel: number;
	vehicleLocked: boolean;
	vehicleEngine: boolean;
}
