export interface VehicleInventory {
	capacity: number;
	slots: number;
}

export interface VehicleStats {
	speed: number;
	acceleration: number;
	braking: number;
	handling: number;
}

export interface Vehicle {
	name: string;
	class: string;
	price: number;
	fuel: number;
	seats: number;
	inventory: VehicleInventory;
	stats: VehicleStats;
}
