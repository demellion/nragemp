import { Factions } from '../../enums/factions';
import { Position } from '../position';
import { CustomizationData } from './customizationData';

export interface CharacterStats {
	health: number;
	armor: number;
	hunger: number;
	thirst: number;
	mood: number;
}

export interface Character {
	name: string;
	age: number;
	level: number;
	experience: number;
	cash: number;
	bank: number;
	paytime: number;
	faction: Factions; // Faction ID
	organization: number; // Organization ID
	stats: CharacterStats; // Stats, such as health, happiness.
	inventory: string; // JSON Array with inventory
	generic: string; // JSON Array with specific data, such as driver licenses and etc.
	position: Position; // World position in XYZ.
	heading: number;
	friends: string[]; // Array of uniqueIDs character is friended with.
	jailed: boolean;
	lastCarId: string;
	houseId: string;
	warned: boolean;
	cars: string[];
	customizationData: CustomizationData;
	wantedLevel: number;
}

export interface Leveling {
	currentLevel: number;
	currentXP: number;
}
