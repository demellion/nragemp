export enum Gender {
	female,
	male
}

export interface CustomizationDataElement {
	id: number;
	name: string;
}

export interface FaceFeatures {
	noseWidth: number,
	noseHeight: number,
	noseLength: number,
	noseBridge: number,
	noseTip: number,
	noseBridgeShift: number,
	browHeight: number,
	browWidth: number,
	cheekBoneHeight: number,
	cheekBoneWidth: number,
	cheeksWidth: number,
	eyes: number,
	lips: number,
	jawWidth: number,
	jawHeight: number,
	chinLength: number,
	chinPosition: number,
	chinWidth: number,
	chinShape: number,
	neckWidth: number
}

export interface HeadOverlays {
	blemishes: number;
	facialHair: number;
	eyebrows: number;
	ageing: number;
	makeup: number;
	blush: number;
	complexion: number;
	sunDamage: number;
	lipstick: number;
	moles: number;
	chestHair: number;
	bodyBlemishes: number;
	addBodyBlemishes: number;
}

export interface CharCreateData extends HeadOverlays, FaceFeatures {
	gender: Gender;
	father: number;
	mother: number;
	similar: number;
	hair: number;
	hairPrimaryColor: number;
	hairSecondaryColor: number;
	facialHairColor: number;
	eyeColor: number;
}

export interface CustomizationData {
	gender: Gender;
	father: number;
	mother: number;
	similar: number;
	hair: number;
	hairPrimaryColor: number;
	hairSecondaryColor: number;
	facialHairColor: number;
	eyeColor: number;
	headOverlays: HeadOverlays;
	faceFeatures: FaceFeatures;
}
