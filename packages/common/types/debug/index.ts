import { CharacterStats, Leveling } from '../character';
import { Wallet } from '../wallet';

export interface DebugData {
	time: string;
	dimension: number;
	position: string;
	heading: number;
	character: string;
	stats: CharacterStats;
	level: Leveling;
	wantedLevel: number;
	wallet: Wallet;
	vehicle: string;
	vehicleRPM: number;
	ownedVehicle: string;
	rentedVehicle: string;
	cursorObject: string;
	currentNPC: string;
	currentNPCData: string;
	currentAction: string;
	currentActionName: string;
	isInSafeZone: string;
	isFreeAiming: boolean;
	isInMeleeCombat: boolean;
	isInScenario: boolean;
	isFalling: boolean;
	street: string;
	zone: string;
	interiorID: number;
}
