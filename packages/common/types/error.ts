export type Error<TErrorType> = {
	type: TErrorType | Errors;
	message: string;
};

export enum Errors {
	unknown = 'UnknownError'
}
