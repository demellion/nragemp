import { Error } from '../error';
import { OperationResult } from '../operationResult';

export enum BuyVehicleErrorTypes {
	NotEnoughMoney = 'NotEnoughMoney',
}

export type BuyVehicleError = Error<BuyVehicleErrorTypes>;

export type BuyVehicleResult = OperationResult<undefined, BuyVehicleError>;
