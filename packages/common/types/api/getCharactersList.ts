import { Error } from '../error';
import { OperationResult } from '../operationResult';
import { Character } from '../character';

export enum GetCharactersListErrorTypes {}

export type GetCharactersListError = Error<GetCharactersListErrorTypes>;

export type GetCharactersListResult = OperationResult<Character[], GetCharactersListError>;
