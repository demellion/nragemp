import { Error } from '../error';
import { OperationResult } from '../operationResult';
import { Account } from '../account';

export enum GetCurrentAccountErrorTypes {
	NotFound = 'NotFound'
}

export type GetCurrentAccountError = Error<GetCurrentAccountErrorTypes>;

export type GetCurrentAccountResult = OperationResult<Account, GetCurrentAccountError>;
