import { Error } from '../error';
import { OperationResult } from '../operationResult';
import { Gender } from '../character/customizationData';

export enum ChangeGenderErrorTypes {
	UnknownError = 'UnknownError'
}

export type ChangeGenderError = Error<ChangeGenderErrorTypes>;

export type ChangeGenderResult = OperationResult<Gender, ChangeGenderError>;
