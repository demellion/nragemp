import { Error } from '../error';
import { OperationResult } from '../operationResult';
import { Account } from '../account';

export enum LoginErrorTypes {
	WrongUsernameOrPassword = 'WrongUsernameOrPassword',
	InvalidrgscId = 'InvalidrgscId'
}

export type LoginError = Error<LoginErrorTypes>;

export type LoginResult = OperationResult<Account, LoginError>;
