import { Error } from '../error';
import { OperationResult } from '../operationResult';
import { Character } from '../character';

export enum CreateCharacterErrorTypes {
	ToManyCharactersError = 'ToManyCharactersError',
	AlreadyExistsError = 'AlreadyExistsError',
	ParamsIsRequired = 'ParamsIsRequired'
}

export type CreateCharacterError = Error<CreateCharacterErrorTypes>;

export type CreateCharacterResult = OperationResult<Character, CreateCharacterError>;
