import { Error } from '../error';
import { OperationResult } from '../operationResult';

export enum SetActiveVehicleErrorTypes {
}

export type SetActiveVehicleError = Error<SetActiveVehicleErrorTypes>;

export type SetActiveVehicleResult = OperationResult<undefined, SetActiveVehicleError>;
