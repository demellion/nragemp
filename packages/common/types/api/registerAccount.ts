import { Error } from '../error';
import { OperationResult } from '../operationResult';
import { Account } from '../account';

export enum RegisterErrorTypes {
	WrongParams = 'WrongParams',
	LoginAlreadyExists = 'LoginAlreadyExists',
	EmailAlreadyExists = 'EmailAlreadyExists',
	SocialClubExists = 'SocialClubExists',
	TooShortLogin = 'TooWeakLogin',
	TooShortPassword = 'TooShortPassword'
}

export type RegisterError = Error<RegisterErrorTypes>;

export type RegisterResult = OperationResult<Account, RegisterError>;
