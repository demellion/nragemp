import { Error } from '../error';
import { OperationResult } from '../operationResult';
import { Vehicle } from '../vehicle';

export enum GetVehicleListErrorTypes {
}

export type GetVehicleListError = Error<GetVehicleListErrorTypes>;

export type GetVehicleListResult = OperationResult<Vehicle[], GetVehicleListError>;
