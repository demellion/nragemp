import { Error } from '../error';
import { OperationResult } from '../operationResult';
import { Character } from '../character';

export enum LoadCharacterErrorTypes {
	NotFound = 'NotFound'
}

export type LoadCharacterError = Error<LoadCharacterErrorTypes>;

export type LoadCharacterResult = OperationResult<Character, LoadCharacterError>;
