import { IsoString } from '../date';

export interface BanInfo {
	dateFrom: IsoString;
	dateTo: IsoString;
	issuerID: string;
	reason: string;
}

export interface BanAccount {
	socialClub: string;
	banInfo: BanInfo;
}
