export interface Award {
	type: 'money' | 'item' | 'vehicle';
	description: string;
	data: any;
	timestamp?: string;
}