import { IsoString } from '../date';
import { VipLevels } from '../../enums/vipLevels';

export interface Vip {
	level: VipLevels;
	expiryDate: IsoString;
}
