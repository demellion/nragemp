import { AdminLevels } from '../../enums/adminLevels';
import { IPString } from '../ip';
import { PasswordHash } from '../password';
import { IsoString, Timestamp } from '../date';
import { RgscId } from '../rgscId';
import { Vip } from './vip';
import { BanInfo } from './ban';
import { Character } from '../character';
import { Award } from './award';

export interface Account {
	username: string;
	email: string;
	dateOfBirth: IsoString;
	adminLevel: AdminLevels;
	serial: string;
	rgscId: RgscId;
	socialClub: string;
	vip: Vip;
	subscribers: number;
	currency: number;
	blacklisted: boolean;
	lastIP: IPString;
	registeredIP: IPString;
	registeredDate: IsoString;
	lastConnected: IsoString;
	lastDisconnected: IsoString;
	playtime: Timestamp;
	promo: string[];
	awards: Award[];
	currencyHistory: string[];
	banInfo: BanInfo;
	characters: Character[];
}

export interface ServerAccount extends Account {
	password: PasswordHash;
}
