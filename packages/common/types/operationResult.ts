export type IOperationSuccess<TResult> = {
	readonly success: true;
	readonly result: TResult;
}

export type IOperationFailed<TError> = {
	readonly success: false;
	readonly error: TError;
}

export type OperationResult<TResult, TError = Error> = IOperationSuccess<TResult> | IOperationFailed<TError>;
