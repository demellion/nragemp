import { ChatLabels } from '../../enums/chat';

export interface ChatMessageAuthor {
	name: string;
	id: number;
}

export interface ChatMessage {
	author: ChatMessageAuthor;
	text: string;
	label?: ChatLabels;
	color?: string;
}
