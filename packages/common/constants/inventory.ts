interface BackpackConfig {
	slots: [number, number];
	weight: number
}
interface ItemConfig {
	description: string;
	picture: string;
}

export const Backpacks: { [k: string]: BackpackConfig } = {
	base: { slots: [6, 2], weight: 10 },
	small: { slots: [6, 2], weight: 10 },
	medium: { slots: [6, 3], weight: 20 },
	big: { slots: [6, 5], weight: 30 }
};

export const ItemDetails: { [k: string]: ItemConfig } = {
	item: { description: 'example item', picture: 'example.png' }
};