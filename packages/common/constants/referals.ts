export interface Referal {
	rgscId: string;
	name: string;
}

export const referals: { [k: string]: Referal} = {
	WEIRDNEWBIE: { rgscId: '1234567890', name: 'WeirdNewbie' }
};