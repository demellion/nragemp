export interface VehicleConfig {
	type: string;
	price: number;
	fuel: number;
	inventory: number;
	slots: number;
}

export const VehicleMods: { [key: string]: number } = {
	spoiler: 0,
	frontBumper: 1,
	rearBumper: 2,
	sideSkirt: 3,
	exhaust: 4,
	frame: 5,
	grille: 6,
	hood: 7,
	fender: 8,
	rightFender: 9,
	roof: 10,
	engine: 11,
	brakes: 12,
	transmission: 13,
	horns: 14,
	suspension: 15,
	armor: 16,
	turbo: 18,
	xenon: 22,
	frontWheels: 23,
	utilShadowSilver: 20,
	backWheels: 24, // Bikes only
	plateHolders: 25,
	trimDesign: 27,
	ornaments: 28,
	dialDesign: 30,
	steeringWheel: 33,
	shiftLever: 34,
	plaques: 35,
	hydraulics: 38,
	boost: 40,
	windowTint: 55,
	livery: 48,
	plate: 62
};

export type VehicleModsCustom = 'tireSmoke' | 'gps' | 'remote';

export enum VehicleColors {
	MetallicBlack = 0, // #0d1116
	MetallicGraphiteBlack = 1, // #1c1d21
	MetallicBlackSteal = 2, // #32383d
	MetallicDarkSilver = 3, // #454b4f
	MetallicSilver = 4, // #999da0
	MetallicBlueSilver = 5, // #c2c4c6
	MetallicSteelGray = 6, // #979a97
	MetallicShadowSilver = 7, // #637380
	MetallicStoneSilver = 8, // #63625c
	MetallicMidnightSilver = 9, // #3c3f47
	MetallicGunMetal = 10, // #444e54
	MetallicAnthraciteGrey = 11, // #1d2129
	MatteBlack = 12, // #13181f
	MatteGray = 13, // #26282a
	MatteLightGrey = 14, // #515554
	UtilBlack = 15, // #151921
	UtilBlackPoly = 16, // #1e2429
	UtilDarksilver = 17, // #333a3c
	UtilSilver = 18, // #8c9095
	UtilGunMetal = 19, // #39434d
	UtilShadowSilver = 20, // #506272
	WornBlack = 21, // #1e232f
	WornGraphite = 22, // #363a3f
	WornSilverGrey = 23, // #a0a199
	WornSilver = 24, // #d3d3d3
	WornBlueSilver = 25, // #b7bfca
	WornShadowSilver = 26, // #778794
	MetallicRed = 27, // #c00e1a
	MetallicTorinoRed = 28, // #da1918
	MetallicFormulaRed = 29, // #b6111b
	MetallicBlazeRed = 30, // #a51e23
	MetallicGracefulRed = 31, // #7b1a22
	MetallicGarnetRed = 32, // #8e1b1f
	MetallicDesertRed = 33, // #6f1818
	MetallicCabernetRed = 34, // #49111d
	MetallicCandyRed = 35, // #b60f25
	MetallicSunriseOrange = 36, // #d44a17
	MetallicClassicGold = 37, // #c2944f
	MetallicOrange = 38, // #f78616
	MatteRed = 39, // #cf1f21
	MatteDarkRed = 40, // #732021
	MatteOrange = 41, // #f27d20
	MatteYellow = 42, // #ffc91f
	UtilRed = 43, // #9c1016
	UtilBrightRed = 44, // #de0f18
	UtilGarnetRed = 45, // #8f1e17
	WornRed = 46, // #a94744
	WornGoldenRed = 47, // #b16c51
	WornDarkRed = 48, // #371c25
	MetallicDarkGreen = 49, // #132428
	MetallicRacingGreen = 50, // #122e2b
	MetallicSeaGreen = 51, // #12383c
	MetallicOliveGreen = 52, // #31423f
	MetallicGreen = 53, // #155c2d
	MetallicGasolineBlueGreen = 54, // #1b6770
	MatteLimeGreen = 55, // #66b81f
	UtilDarkGreen = 56, // #22383e
	UtilGreen = 57, // #1d5a3f
	WornDarkGreen = 58, // #2d423f
	WornGreen = 59, // #45594b
	WornSeaWash = 60, // #65867f
	MetallicMidnightBlue = 61, // #222e46
	MetallicDarkBlue = 62, // #233155
	MetallicSaxonyBlue = 63, // #304c7e
	MetallicBlue = 64, // #47578f
	MetallicMarinerBlue = 65, // #637ba7
	MetallicHarborBlue = 66, // #394762
	MetallicDiamondBlue = 67, // #d6e7f1
	MetallicSurfBlue = 68, // #76afbe
	MetallicNauticalBlue = 69, // #345e72
	MetallicBrightBlue = 70, // #0b9cf1
	MetallicPurpleBlue = 71, // #2f2d52
	MetallicSpinnakerBlue = 72, // #282c4d
	MetallicUltraBlue = 73, // #2354a1
	MetallicExtraBlue = 74, // #6ea3c6
	UtilDarkBlue = 75, // #112552
	UtilMidnightBlue = 76, // #1b203e
	UtilBlue = 77, // #275190
	UtilSeaFoamBlue = 78, // #608592
	UtilLightningblue = 79, // #2446a8
	UtilMauiBluePoly = 80, // #4271e1
	UtilBrightBlue = 81, // #3b39e0
	MatteDarkBlue = 82, // #1f2852
	MatteBlue = 83, // #253aa7
	MatteMidnightBlue = 84, // #1c3551
	WornDarkblue = 85, // #4c5f81
	WornBlue = 86, // #58688e
	WornLightblue = 87, // #74b5d8
	MetallicTaxiYellow = 88, // #ffcf20
	MetallicRaceYellow = 89, // #fbe212
	MetallicBronze = 90, // #916532
	MetallicYellowBird = 91, // #e0e13d
	MetallicLime = 92, // #98d223
	MetallicChampagne = 93, // #9b8c78
	MetallicPuebloBeige = 94, // #503218
	MetallicDarkIvory = 95, // #473f2b
	MetallicChocoBrown = 96, // #221b19
	MetallicGoldenBrown = 97, // #653f23
	MetallicLightBrown = 98, // #775c3e
	MetallicStrawBeige = 99, // #ac9975
	MetallicMossBrown = 100, // #6c6b4b
	MetallicBistonBrown = 101, // #402e2b
	MetallicBeechwood = 102, // #a4965f
	MetallicDarkBeechwood = 103, // #46231a
	MetallicChocoOrange = 104, // #752b19
	MetallicBeachSand = 105, // #bfae7b
	MetallicSunBleechedSand = 106, // #dfd5b2
	MetallicCream = 107, // #f7edd5
	UtilBrown = 108, // #3a2a1b
	UtilMediumBrown = 109, // #785f33
	UtilLightBrown = 110, // #b5a079
	MetallicWhite = 111, // #fffff6
	MetallicFrostWhite = 112, // #eaeaea
	WornHoneyBeige = 113, // #b0ab94
	WornBrown = 114, // #453831
	WornDarkBrown = 115, // #2a282b
	Wornstrawbeige = 116, // #726c57
	BrushedSteel = 117, // #6a747c
	BrushedBlacksteel = 118, // #354158
	BrushedAluminium = 119, // #9ba0a8
	Chrome = 120, // #5870a1
	WornOffWhite = 121, // #eae6de
	UtilOffWhite = 122, // #dfddd0
	WornOrange = 123, // #f2ad2e
	WornLightOrange = 124, // #f9a458
	MetallicSecuricorGreen = 125, // #83c566
	WornTaxiYellow = 126, // #f1cc40
	policecarblue = 127, // #4cc3da
	MatteGreen = 128, // #4e6443
	MatteBrown = 129, // #bcac8f
	WornOrangeBright = 130, // #f8b658
	MatteWhite = 131, // #fcf9f1
	WornWhite = 132, // #fffffb
	WornOliveArmyGreen = 133, // #81844c
	PureWhite = 134, // #ffffff
	HotPink = 135, // #f21f99
	Salmonpink = 136, // #fdd6cd
	MetallicVermillionPink = 137, // #df5891
	Orange = 138, // #f6ae20
	Green = 139, // #b0ee6e
	Blue = 140, // #08e9fa
	MettalicBlackBlue = 141, // #0a0c17
	MetallicBlackPurple = 142, // #0c0d18
	MetallicBlackRed = 143, // #0e0d14
	huntergreen = 144, // #9f9e8a
	MetallicPurple = 145, // #621276
	MetaillicVDarkBlue = 146, // #0b1421
	MODSHOPBLACK1 = 147, // #11141a
	MattePurple = 148, // #6b1f7b
	MatteDarkPurple = 149, // #1e1d22
	MetallicLavaRed = 150, // #bc1917
	MatteForestGreen = 151, // #2d362a
	MatteOliveDrab = 152, // #696748
	MatteDesertBrown = 153, // #7a6c55
	MatteDesertTan = 154, // #c3b492
	MatteFoilageGreen = 155, // #5a6352
	DEFAULTALLOYCOLOR = 156, // #81827f
	EpsilonBlue = 157, // #afd6e4
	PureGold = 158, // #7a6440
	BrushedGold = 159 // #7f6a48
}

export const VehiclesPool: { [key: string]: VehicleConfig } = {
	// Mod cars

	// Summer DLC
	CLUB: { type: 'Compact', price: 100, fuel: 50, inventory: 10, slots: 10 },
	COQUETTE4: { type: 'Sport', price: 100, fuel: 50, inventory: 10, slots: 10 },
	DUKES3: { type: 'Muscle', price: 100, fuel: 50, inventory: 10, slots: 10 },
	GAUNTLET5: { type: 'Muscle', price: 100, fuel: 50, inventory: 10, slots: 10 },
	GLENDALE2: { type: 'Sedan', price: 100, fuel: 50, inventory: 10, slots: 10 },
	LANDSTALKER2: { type: 'SUVs', price: 100, fuel: 50, inventory: 10, slots: 10 },
	MANANA2: { type: 'Muscle', price: 100, fuel: 50, inventory: 10, slots: 10 },
	OPENWHEEL1: { type: 'Wheel', price: 100, fuel: 50, inventory: 10, slots: 10 },
	OPENWHEEL2: { type: 'Wheel', price: 100, fuel: 50, inventory: 10, slots: 10 },
	PENUMBRA2: { type: 'Sport', price: 100, fuel: 50, inventory: 10, slots: 10 },
	PEYOTE3: { type: 'Classic', price: 100, fuel: 50, inventory: 10, slots: 10 },
	SEMINOLE2: { type: 'SUV', price: 100, fuel: 50, inventory: 10, slots: 10 },
	TIGON: { type: 'Super', price: 100, fuel: 50, inventory: 10, slots: 10 },
	YOSEMITE3: { type: 'Offroad', price: 100, fuel: 50, inventory: 10, slots: 10 },
	YOUGA3: { type: 'Van', price: 100, fuel: 50, inventory: 10, slots: 10 },

	// Casino Heist
	ASBO: { type: 'Compact', price: 408000, fuel: 50, inventory: 10, slots: 10 },
	EVERON: { type: 'Offroad', price: 1475000, fuel: 50, inventory: 10, slots: 10 },
	FORMULA2: { type: 'OpenWheel', price: 3115000, fuel: 50, inventory: 10, slots: 10 },
	FORMULA: { type: 'OpenWheel', price: 3515000, fuel: 50, inventory: 10, slots: 10 },
	FURIA: { type: 'Super', price: 2740000, fuel: 50, inventory: 10, slots: 10 },
	IMORGON: { type: 'Sport', price: 2165000, fuel: 50, inventory: 10, slots: 10 },
	JB7002: { type: 'SportsClassic', price: 1470000, fuel: 50, inventory: 10, slots: 10 },
	KANJO: { type: 'Compact', price: 580000, fuel: 50, inventory: 10, slots: 10 },
	KOMODA: { type: 'Sport', price: 1700000, fuel: 50, inventory: 10, slots: 10 },
	MINITANK: { type: 'Military', price: 2275000, fuel: 50, inventory: 10, slots: 10 },
	OUTLAW: { type: 'Offroad', price: 1268000, fuel: 50, inventory: 10, slots: 10 },
	REBLA: { type: 'SUV', price: 1175000, fuel: 50, inventory: 10, slots: 10 },
	RETINUE2: { type: 'SportsClassic', price: 1620000, fuel: 0, inventory: 10, slots: 10 },
	STRYDER: { type: 'Motorcycle', price: 670000, fuel: 50, inventory: 10, slots: 10 },
	SUGOI: { type: 'Sport', price: 1224000, fuel: 50, inventory: 10, slots: 10 },
	SULTAN2: { type: 'Sport', price: 1718000, fuel: 50, inventory: 10, slots: 10 },
	VAGRANT: { type: 'Offroad', price: 2214000, fuel: 50, inventory: 10, slots: 10 },
	VSTR: { type: 'Sport', price: 1285000, fuel: 50, inventory: 10, slots: 10 },
	YOSEMITE2: { type: 'Muscle', price: 1308000, fuel: 50, inventory: 10, slots: 10 },
	ZHABA: { type: 'Offroad', price: 240000, fuel: 50, inventory: 10, slots: 10 },

	// Other
	ADDER: { type: 'Super', price: 1000000, fuel: 50, inventory: 10, slots: 10 },
	AIRBUS: { type: 'Service', price: 550000, fuel: 50, inventory: 10, slots: 10 },
	AIRTUG: { type: 'Service', price: 0, fuel: 50, inventory: 10, slots: 10 },
	AKULA: { type: 'Helicopter', price: 3704050, fuel: 50, inventory: 10, slots: 10 },
	AKUMA: { type: 'Motorcycle', price: 9000, fuel: 50, inventory: 10, slots: 10 },
	ALPHA: { type: 'Sport', price: 150000, fuel: 50, inventory: 10, slots: 10 },
	ALPHAZ1: { type: 'Plane', price: 2121350, fuel: 50, inventory: 10, slots: 10 },
	AMBULANCE: { type: 'Emergency', price: 0, fuel: 50, inventory: 10, slots: 10 },
	ANNIHILATOR: { type: 'Helicopter', price: 1825000, fuel: 50, inventory: 10, slots: 10 },
	APC: { type: 'Military', price: 2325000, fuel: 50, inventory: 10, slots: 10 },
	ARDENT: { type: 'SportsClassic', price: 1150000, fuel: 50, inventory: 10, slots: 10 },
	ARMYTANKER: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	ARMYTRAILER2: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	ARMYTRAILER: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	ASEA2: { type: 'Sedan', price: 0, fuel: 50, inventory: 10, slots: 10 },
	ASEA: { type: 'Sedan', price: 12000, fuel: 50, inventory: 10, slots: 10 },
	ASTEROPE: { type: 'Sedan', price: 26000, fuel: 50, inventory: 10, slots: 10 },
	AUTARCH: { type: 'Super', price: 1955000, fuel: 50, inventory: 10, slots: 10 },
	AVARUS: { type: 'Motorcycle', price: 116000, fuel: 50, inventory: 10, slots: 10 },
	AVENGER2: { type: 'Plane', price: 3450000, fuel: 50, inventory: 10, slots: 10 },
	AVENGER: { type: 'Plane', price: 3450000, fuel: 50, inventory: 10, slots: 10 },
	BAGGER: { type: 'Motorcycle', price: 16000, fuel: 50, inventory: 10, slots: 10 },
	BALETRAILER: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	BALLER2: { type: 'SUV', price: 90000, fuel: 50, inventory: 10, slots: 10 },
	BALLER3: { type: 'SUV', price: 374000, fuel: 50, inventory: 10, slots: 10 },
	BALLER4: { type: 'SUV', price: 247000, fuel: 50, inventory: 10, slots: 10 },
	BALLER5: { type: 'SUV', price: 374000, fuel: 50, inventory: 10, slots: 10 },
	BALLER6: { type: 'SUV', price: 513000, fuel: 50, inventory: 10, slots: 10 },
	BALLER: { type: 'SUV', price: 90000, fuel: 50, inventory: 10, slots: 10 },
	BANSHEE2: { type: 'Super', price: 670000, fuel: 50, inventory: 10, slots: 10 },
	BANSHEE: { type: 'Sport', price: 105000, fuel: 50, inventory: 10, slots: 10 },
	BARRACKS2: { type: 'Emergency', price: 0, fuel: 50, inventory: 10, slots: 10 },
	BARRACKS3: { type: 'Emergency', price: 0, fuel: 50, inventory: 10, slots: 10 },
	BARRACKS: { type: 'Emergency', price: 450000, fuel: 50, inventory: 10, slots: 10 },
	BARRAGE: { type: 'Military', price: 2121350, fuel: 50, inventory: 10, slots: 10 },
	BATI2: { type: 'Motorcycle', price: 15000, fuel: 50, inventory: 10, slots: 10 },
	BATI: { type: 'Motorcycle', price: 15000, fuel: 50, inventory: 10, slots: 10 },
	BENSON: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	BESRA: { type: 'Plane', price: 1150000, fuel: 50, inventory: 10, slots: 10 },
	BESTIAGTS: { type: 'Sport', price: 610000, fuel: 50, inventory: 10, slots: 10 },
	BF400: { type: 'Motorcycle', price: 95000, fuel: 50, inventory: 10, slots: 10 },
	BFINJECTION: { type: 'Offroad', price: 16000, fuel: 50, inventory: 10, slots: 10 },
	BIFF: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	BIFTA: { type: 'Offroad', price: 75000, fuel: 50, inventory: 10, slots: 10 },
	BISON2: { type: 'Pickup', price: 30000, fuel: 50, inventory: 10, slots: 10 },
	BISON3: { type: 'Pickup', price: 30000, fuel: 50, inventory: 10, slots: 10 },
	BISON: { type: 'Pickup', price: 30000, fuel: 50, inventory: 10, slots: 10 },
	BJXL: { type: 'SUV', price: 27000, fuel: 50, inventory: 10, slots: 10 },
	BLADE: { type: 'Muscle', price: 160000, fuel: 50, inventory: 10, slots: 10 },
	BLAZER2: { type: 'Offroad', price: 62000, fuel: 50, inventory: 10, slots: 10 },
	BLAZER3: { type: 'Offroad', price: 69000, fuel: 50, inventory: 10, slots: 10 },
	BLAZER4: { type: 'Offroad', price: 81000, fuel: 50, inventory: 10, slots: 10 },
	BLAZER5: { type: 'Offroad', price: 1755600, fuel: 50, inventory: 10, slots: 10 },
	BLAZER: { type: 'Offroad', price: 8000, fuel: 50, inventory: 10, slots: 10 },
	BLIMP2: { type: 'Helicopter', price: 0, fuel: 50, inventory: 10, slots: 10 },
	BLIMP3: { type: 'Plane', price: 1190350, fuel: 50, inventory: 10, slots: 10 },
	BLIMP: { type: 'Helicopter', price: 0, fuel: 50, inventory: 10, slots: 10 },
	BLISTA2: { type: 'Sport', price: 42000, fuel: 50, inventory: 10, slots: 10 },
	BLISTA3: { type: 'Sport', price: 0, fuel: 50, inventory: 10, slots: 10 },
	BLISTA: { type: 'Compact', price: 16000, fuel: 50, inventory: 10, slots: 10 },
	BMX: { type: 'Bicycle', price: 0, fuel: 50, inventory: 10, slots: 10 },
	BOATTRAILER: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	BOBCATXL: { type: 'Pickup', price: 23000, fuel: 50, inventory: 10, slots: 10 },
	BODHI2: { type: 'Offroad', price: 25000, fuel: 50, inventory: 10, slots: 10 },
	BOMBUSHKA: { type: 'Plane', price: 5918500, fuel: 50, inventory: 10, slots: 10 },
	BOXVILLE2: { type: 'Truck', price: 25000, fuel: 50, inventory: 10, slots: 10 },
	BOXVILLE3: { type: 'Truck', price: 25000, fuel: 50, inventory: 10, slots: 10 },
	BOXVILLE4: { type: 'Truck', price: 59850, fuel: 50, inventory: 10, slots: 10 },
	BOXVILLE5: { type: 'Truck', price: 2926000, fuel: 50, inventory: 10, slots: 10 },
	BOXVILLE: { type: 'Truck', price: 398000, fuel: 50, inventory: 10, slots: 10 },
	BRAWLER: { type: 'Offroad', price: 715000, fuel: 50, inventory: 10, slots: 10 },
	BRICKADE: { type: 'Service', price: 1110000, fuel: 50, inventory: 10, slots: 10 },
	BRIOSO: { type: 'Compact', price: 155000, fuel: 50, inventory: 10, slots: 10 },
	BRUISER2: { type: 'Offroad', price: 1609000, fuel: 50, inventory: 10, slots: 10 },
	BRUISER3: { type: 'Offroad', price: 1609000, fuel: 50, inventory: 10, slots: 10 },
	BRUISER: { type: 'Offroad', price: 1609000, fuel: 50, inventory: 10, slots: 10 },
	BRUTUS2: { type: 'Offroad', price: 2005000, fuel: 50, inventory: 10, slots: 10 },
	BRUTUS3: { type: 'Offroad', price: 2005000, fuel: 50, inventory: 10, slots: 10 },
	BRUTUS: { type: 'Offroad', price: 2005000, fuel: 50, inventory: 10, slots: 10 },
	BTYPE2: { type: 'Muscle', price: 550000, fuel: 50, inventory: 10, slots: 10 },
	BTYPE3: { type: 'SportsClassic', price: 982000, fuel: 50, inventory: 10, slots: 10 },
	BTYPE: { type: 'SportsClassic', price: 1150000, fuel: 50, inventory: 10, slots: 10 },
	BUCCANEER2: { type: 'Muscle', price: 419000, fuel: 50, inventory: 10, slots: 10 },
	BUCCANEER: { type: 'Muscle', price: 29000, fuel: 50, inventory: 10, slots: 10 },
	BUFFALO2: { type: 'Sport', price: 96000, fuel: 50, inventory: 10, slots: 10 },
	BUFFALO3: { type: 'Sport', price: 1955000, fuel: 50, inventory: 10, slots: 10 },
	BUFFALO: { type: 'Sport', price: 35000, fuel: 50, inventory: 10, slots: 10 },
	BULLDOZER: { type: 'Service', price: 0, fuel: 50, inventory: 10, slots: 10 },
	BULLET: { type: 'Super', price: 3450000, fuel: 50, inventory: 10, slots: 10 },
	BURRITO2: { type: 'Van', price: 598500, fuel: 50, inventory: 10, slots: 10 },
	BURRITO3: { type: 'Van', price: 13000, fuel: 50, inventory: 10, slots: 10 },
	BURRITO4: { type: 'Van', price: 13000, fuel: 50, inventory: 10, slots: 10 },
	BURRITO5: { type: 'Van', price: 13000, fuel: 50, inventory: 10, slots: 10 },
	BURRITO: { type: 'Van', price: 3450000, fuel: 50, inventory: 10, slots: 10 },
	BUS: { type: 'Service', price: 500000, fuel: 50, inventory: 10, slots: 10 },
	BUZZARD2: { type: 'Helicopter', price: 0, fuel: 50, inventory: 10, slots: 10 },
	BUZZARD: { type: 'Helicopter', price: 2000000, fuel: 50, inventory: 10, slots: 10 },
	CABLECAR: { type: 'Service', price: 0, fuel: 50, inventory: 10, slots: 10 },
	CADDY2: { type: 'Service', price: 85000, fuel: 50, inventory: 10, slots: 10 },
	CADDY3: { type: 'Utility', price: 120000, fuel: 50, inventory: 10, slots: 10 },
	CADDY: { type: 'Service', price: 0, fuel: 50, inventory: 10, slots: 10 },
	CAMPER: { type: 'Van', price: 0, fuel: 50, inventory: 10, slots: 10 },
	CARACARA2: { type: 'Offroad', price: 2121350, fuel: 50, inventory: 10, slots: 10 },
	CARACARA: { type: 'Offroad', price: 1775000, fuel: 50, inventory: 10, slots: 10 },
	CARBONIZZARE: { type: 'Sport', price: 195000, fuel: 50, inventory: 10, slots: 10 },
	CARBONRS: { type: 'Motorcycle', price: 40000, fuel: 50, inventory: 10, slots: 10 },
	CARGOBOB2: { type: 'Helicopter', price: 1995000, fuel: 50, inventory: 10, slots: 10 },
	CARGOBOB3: { type: 'Helicopter', price: 0, fuel: 50, inventory: 10, slots: 10 },
	CARGOBOB4: { type: 'Helicopter', price: 0, fuel: 50, inventory: 10, slots: 10 },
	CARGOBOB: { type: 'Helicopter', price: 1790000, fuel: 50, inventory: 10, slots: 10 },
	CARGOPLANE: { type: 'Plane', price: 0, fuel: 50, inventory: 10, slots: 10 },
	CASCO: { type: 'SportsClassic', price: 904400, fuel: 50, inventory: 10, slots: 10 },
	CAVALCADE2: { type: 'SUV', price: 60000, fuel: 50, inventory: 10, slots: 10 },
	CAVALCADE: { type: 'SUV', price: 60000, fuel: 50, inventory: 10, slots: 10 },
	CERBERUS2: { type: 'Commercial', price: 2910000, fuel: 50, inventory: 10, slots: 10 },
	CERBERUS3: { type: 'Commercial', price: 2910000, fuel: 50, inventory: 10, slots: 10 },
	CERBERUS: { type: 'Commercial', price: 2910000, fuel: 50, inventory: 10, slots: 10 },
	CHEBUREK: { type: 'SportsClassic', price: 145000, fuel: 50, inventory: 10, slots: 10 },
	CHEETAH2: { type: 'SportsClassic', price: 865000, fuel: 50, inventory: 10, slots: 10 },
	CHEETAH: { type: 'Super', price: 650000, fuel: 50, inventory: 10, slots: 10 },
	CHERNOBOG: { type: 'Military', price: 3311700, fuel: 50, inventory: 10, slots: 10 },
	CHIMERA: { type: 'Motorcycle', price: 210000, fuel: 50, inventory: 10, slots: 10 },
	CHINO2: { type: 'Muscle', price: 410000, fuel: 50, inventory: 10, slots: 10 },
	CHINO: { type: 'Muscle', price: 225000, fuel: 50, inventory: 10, slots: 10 },
	CLIFFHANGER: { type: 'Motorcycle', price: 225000, fuel: 50, inventory: 10, slots: 10 },
	CLIQUE: { type: 'Muscle', price: 909000, fuel: 50, inventory: 10, slots: 10 },
	COACH: { type: 'Service', price: 525000, fuel: 50, inventory: 10, slots: 10 },
	COG552: { type: 'Sedan', price: 396000, fuel: 50, inventory: 10, slots: 10 },
	COG55: { type: 'Sedan', price: 154000, fuel: 50, inventory: 10, slots: 10 },
	COGCABRIO: { type: 'Coupe', price: 185000, fuel: 50, inventory: 10, slots: 10 },
	COGNOSCENTI2: { type: 'Sedan', price: 558000, fuel: 50, inventory: 10, slots: 10 },
	COGNOSCENTI: { type: 'Sedan', price: 254000, fuel: 50, inventory: 10, slots: 10 },
	COMET2: { type: 'Sport', price: 100000, fuel: 50, inventory: 10, slots: 10 },
	COMET3: { type: 'Sport', price: 745000, fuel: 50, inventory: 10, slots: 10 },
	COMET4: { type: 'Sport', price: 710000, fuel: 50, inventory: 10, slots: 10 },
	COMET5: { type: 'Sport', price: 1145000, fuel: 50, inventory: 10, slots: 10 },
	CONTENDER: { type: 'Pickup', price: 250000, fuel: 50, inventory: 10, slots: 10 },
	COQUETTE2: { type: 'SportsClassic', price: 665000, fuel: 50, inventory: 10, slots: 10 },
	COQUETTE3: { type: 'SportsClassic', price: 695000, fuel: 50, inventory: 10, slots: 10 },
	COQUETTE: { type: 'Sport', price: 138000, fuel: 50, inventory: 10, slots: 10 },
	CRUISER: { type: 'Bicycle', price: 0, fuel: 50, inventory: 10, slots: 10 },
	CRUSADER: { type: 'Emergency', price: 225000, fuel: 50, inventory: 10, slots: 10 },
	CUBAN800: { type: 'Plane', price: 240000, fuel: 50, inventory: 10, slots: 10 },
	CUTTER: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	CYCLONE: { type: 'Super', price: 1890000, fuel: 50, inventory: 10, slots: 10 },
	DAEMON2: { type: 'Motorcycle', price: 145000, fuel: 50, inventory: 10, slots: 10 },
	DAEMON: { type: 'Motorcycle', price: 20000, fuel: 50, inventory: 10, slots: 10 },
	DEATHBIKE2: { type: 'Motorcycle', price: 1269000, fuel: 50, inventory: 10, slots: 10 },
	DEATHBIKE3: { type: 'Motorcycle', price: 1269000, fuel: 50, inventory: 10, slots: 10 },
	DEATHBIKE: { type: 'Motorcycle', price: 1269000, fuel: 50, inventory: 10, slots: 10 },
	DEFILER: { type: 'Motorcycle', price: 412000, fuel: 50, inventory: 10, slots: 10 },
	DELUXO: { type: 'SportsClassic', price: 4721500, fuel: 50, inventory: 10, slots: 10 },
	DEVESTE: { type: 'Super', price: 1795000, fuel: 50, inventory: 10, slots: 10 },
	DEVIANT: { type: 'Muscle', price: 512000, fuel: 50, inventory: 10, slots: 10 },
	DIABLOUS2: { type: 'Motorcycle', price: 414000, fuel: 50, inventory: 10, slots: 10 },
	DIABLOUS: { type: 'Motorcycle', price: 169000, fuel: 50, inventory: 10, slots: 10 },
	DILETTANTE2: { type: 'Compact', price: 25000, fuel: 50, inventory: 10, slots: 10 },
	DILETTANTE: { type: 'Compact', price: 25000, fuel: 50, inventory: 10, slots: 10 },
	DINGHY2: { type: 'Boat', price: 0, fuel: 50, inventory: 10, slots: 10 },
	DINGHY3: { type: 'Boat', price: 166250, fuel: 50, inventory: 10, slots: 10 },
	DINGHY4: { type: 'Boat', price: 0, fuel: 50, inventory: 10, slots: 10 },
	DINGHY: { type: 'Boat', price: 0, fuel: 50, inventory: 10, slots: 10 },
	DLOADER: { type: 'Offroad', price: 15000, fuel: 50, inventory: 10, slots: 10 },
	DOCKTRAILER: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	DOCKTUG: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	DODO: { type: 'Plane', price: 500000, fuel: 50, inventory: 10, slots: 10 },
	DOMINATOR2: { type: 'Muscle', price: 315000, fuel: 50, inventory: 10, slots: 10 },
	DOMINATOR3: { type: 'Muscle', price: 725000, fuel: 50, inventory: 10, slots: 10 },
	DOMINATOR4: { type: 'Muscle', price: 1132000, fuel: 50, inventory: 10, slots: 10 },
	DOMINATOR5: { type: 'Muscle', price: 1132000, fuel: 50, inventory: 10, slots: 10 },
	DOMINATOR6: { type: 'Muscle', price: 1132000, fuel: 50, inventory: 10, slots: 10 },
	DOMINATOR: { type: 'Muscle', price: 35000, fuel: 50, inventory: 10, slots: 10 },
	DOUBLE: { type: 'Motorcycle', price: 12000, fuel: 50, inventory: 10, slots: 10 },
	DRAFTER: { type: 'Sport', price: 718000, fuel: 50, inventory: 10, slots: 10 },
	DUBSTA2: { type: 'SUV', price: 145000, fuel: 50, inventory: 10, slots: 10 },
	DUBSTA3: { type: 'Offroad', price: 249000, fuel: 50, inventory: 10, slots: 10 },
	DUBSTA: { type: 'SUV', price: 70000, fuel: 50, inventory: 10, slots: 10 },
	DUKES2: { type: 'Muscle', price: 3311700, fuel: 50, inventory: 10, slots: 10 },
	DUKES: { type: 'Muscle', price: 62000, fuel: 50, inventory: 10, slots: 10 },
	DUMP: { type: 'Truck', price: 1000000, fuel: 50, inventory: 10, slots: 10 },
	DUNE2: { type: 'Offroad', price: 1000000, fuel: 50, inventory: 10, slots: 10 },
	DUNE3: { type: 'Offroad', price: 850000, fuel: 50, inventory: 10, slots: 10 },
	DUNE4: { type: 'Offroad', price: 0, fuel: 50, inventory: 10, slots: 10 },
	DUNE5: { type: 'Offroad', price: 3192000, fuel: 50, inventory: 10, slots: 10 },
	DUNE: { type: 'Offroad', price: 20000, fuel: 50, inventory: 10, slots: 10 },
	DUSTER: { type: 'Plane', price: 275000, fuel: 50, inventory: 10, slots: 10 },
	DYNASTY: { type: 'SportsClassic', price: 450000, fuel: 50, inventory: 10, slots: 10 },
	ELEGY2: { type: 'Sport', price: 95000, fuel: 50, inventory: 10, slots: 10 },
	ELEGY: { type: 'Sport', price: 904000, fuel: 50, inventory: 10, slots: 10 },
	ELLIE: { type: 'Muscle', price: 565000, fuel: 50, inventory: 10, slots: 10 },
	EMERUS: { type: 'Super', price: 2750000, fuel: 50, inventory: 10, slots: 10 },
	EMPEROR2: { type: 'Sedan', price: 1145000, fuel: 50, inventory: 10, slots: 10 },
	EMPEROR3: { type: 'Sedan', price: 5000, fuel: 50, inventory: 10, slots: 10 },
	EMPEROR: { type: 'Sedan', price: 710000, fuel: 50, inventory: 10, slots: 10 },
	ENDURO: { type: 'Motorcycle', price: 48000, fuel: 50, inventory: 10, slots: 10 },
	ENTITY2: { type: 'Super', price: 2305000, fuel: 50, inventory: 10, slots: 10 },
	ENTITYXF: { type: 'Super', price: 795000, fuel: 50, inventory: 10, slots: 10 },
	ESSKEY: { type: 'Motorcycle', price: 264000, fuel: 50, inventory: 10, slots: 10 },
	EXEMPLAR: { type: 'Coupe', price: 205000, fuel: 50, inventory: 10, slots: 10 },
	F620: { type: 'Coupe', price: 80000, fuel: 50, inventory: 10, slots: 10 },
	FACTION2: { type: 'Coupe', price: 371000, fuel: 50, inventory: 10, slots: 10 },
	FACTION3: { type: 'Muscle', price: 731000, fuel: 50, inventory: 10, slots: 10 },
	FACTION: { type: 'Coupe', price: 36000, fuel: 50, inventory: 10, slots: 10 },
	FAGALOA: { type: 'SportsClassic', price: 335000, fuel: 50, inventory: 10, slots: 10 },
	FAGGIO2: { type: 'Motorcycle', price: 4721500, fuel: 50, inventory: 10, slots: 10 },
	FAGGIO3: { type: 'Motorcycle', price: 55000, fuel: 50, inventory: 10, slots: 10 },
	FAGGIO: { type: 'Motorcycle', price: 47500, fuel: 50, inventory: 10, slots: 10 },
	FAGGION: { type: 'Motorcycle', price: 0, fuel: 50, inventory: 10, slots: 10 },
	FBI2: { type: 'Emergency', price: 0, fuel: 50, inventory: 10, slots: 10 },
	FBI: { type: 'Emergency', price: 0, fuel: 50, inventory: 10, slots: 10 },
	FCR2: { type: 'Motorcycle', price: 331000, fuel: 50, inventory: 10, slots: 10 },
	FCR: { type: 'Motorcycle', price: 135000, fuel: 50, inventory: 10, slots: 10 },
	FELON2: { type: 'Coupe', price: 95000, fuel: 50, inventory: 10, slots: 10 },
	FELON: { type: 'Coupe', price: 90000, fuel: 50, inventory: 10, slots: 10 },
	FELTZER2: { type: 'Sport', price: 145000, fuel: 50, inventory: 10, slots: 10 },
	FELTZER3: { type: 'SportsClassic', price: 975000, fuel: 50, inventory: 10, slots: 10 },
	FIRETRUK: { type: 'Emergency', price: 3295000, fuel: 50, inventory: 10, slots: 10 },
	FIXTER: { type: 'Bicycle', price: 0, fuel: 50, inventory: 10, slots: 10 },
	FLASHGT: { type: 'Sport', price: 1675000, fuel: 50, inventory: 10, slots: 10 },
	FLATBED: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	FMJ: { type: 'Super', price: 725000, fuel: 50, inventory: 10, slots: 10 },
	FORKLIFT: { type: 'Service', price: 0, fuel: 50, inventory: 10, slots: 10 },
	FQ2: { type: 'SUV', price: 50000, fuel: 50, inventory: 10, slots: 10 },
	FREECRAWLER: { type: 'Offroad', price: 597000, fuel: 50, inventory: 10, slots: 10 },
	FREIGHT: { type: 'Train', price: 0, fuel: 50, inventory: 10, slots: 10 },
	FREIGHTCAR: { type: 'Train', price: 0, fuel: 50, inventory: 10, slots: 10 },
	FREIGHTCONT1: { type: 'Train', price: 0, fuel: 50, inventory: 10, slots: 10 },
	FREIGHTCONT2: { type: 'Train', price: 0, fuel: 50, inventory: 10, slots: 10 },
	FREIGHTGRAIN: { type: 'Train', price: 0, fuel: 50, inventory: 10, slots: 10 },
	FREIGHTTRAILER: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	FROGGER2: { type: 'Helicopter', price: 0, fuel: 50, inventory: 10, slots: 10 },
	FROGGER: { type: 'Helicopter', price: 1300000, fuel: 50, inventory: 10, slots: 10 },
	FUGITIVE: { type: 'Sedan', price: 24000, fuel: 50, inventory: 10, slots: 10 },
	FUROREGT: { type: 'Sport', price: 448000, fuel: 50, inventory: 10, slots: 10 },
	FUSILADE: { type: 'Sport', price: 36000, fuel: 50, inventory: 10, slots: 10 },
	FUTO: { type: 'Sport', price: 9000, fuel: 50, inventory: 10, slots: 10 },
	GARGOYLE: { type: 'Motorcycle', price: 120000, fuel: 50, inventory: 10, slots: 10 },
	GAUNTLET2: { type: 'Muscle', price: 230000, fuel: 50, inventory: 10, slots: 10 },
	GAUNTLET3: { type: 'Muscle', price: 615000, fuel: 50, inventory: 10, slots: 10 },
	GAUNTLET4: { type: 'Muscle', price: 745000, fuel: 50, inventory: 10, slots: 10 },
	GAUNTLET: { type: 'Muscle', price: 565000, fuel: 50, inventory: 10, slots: 10 },
	GB200: { type: 'Sport', price: 940000, fuel: 50, inventory: 10, slots: 10 },
	GBURRITO2: { type: 'Van', price: 86450, fuel: 50, inventory: 10, slots: 10 },
	GBURRITO: { type: 'Van', price: 2305000, fuel: 50, inventory: 10, slots: 10 },
	GLENDALE: { type: 'Sedan', price: 200000, fuel: 50, inventory: 10, slots: 10 },
	GP1: { type: 'Super', price: 1260000, fuel: 50, inventory: 10, slots: 10 },
	GRAINTRAILER: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	GRANGER: { type: 'SUV', price: 35000, fuel: 50, inventory: 10, slots: 10 },
	GRESLEY: { type: 'SUV', price: 29000, fuel: 50, inventory: 10, slots: 10 },
	GT500: { type: 'SportsClassic', price: 785000, fuel: 50, inventory: 10, slots: 10 },
	GUARDIAN: { type: 'Offroad', price: 335000, fuel: 50, inventory: 10, slots: 10 },
	HABANERO: { type: 'SUV', price: 42000, fuel: 50, inventory: 10, slots: 10 },
	HAKUCHOU2: { type: 'Motorcycle', price: 976000, fuel: 50, inventory: 10, slots: 10 },
	HAKUCHOU: { type: 'Motorcycle', price: 82000, fuel: 50, inventory: 10, slots: 10 },
	HALFTRACK: { type: 'Military', price: 1695000, fuel: 50, inventory: 10, slots: 10 },
	HANDLER: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	HAULER2: { type: 'Commercial', price: 1400000, fuel: 50, inventory: 10, slots: 10 },
	HAULER: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	HAVOK: { type: 'Helicopter', price: 2300900, fuel: 50, inventory: 10, slots: 10 },
	HELLION: { type: 'Offroad', price: 835000, fuel: 50, inventory: 10, slots: 10 },
	HERMES: { type: 'Muscle', price: 535000, fuel: 50, inventory: 10, slots: 10 },
	HEXER: { type: 'Motorcycle', price: 15000, fuel: 50, inventory: 10, slots: 10 },
	HOTKNIFE: { type: 'Muscle', price: 90000, fuel: 50, inventory: 10, slots: 10 },
	HOTRING: { type: 'Sport', price: 830000, fuel: 50, inventory: 10, slots: 10 },
	HOWARD: { type: 'Plane', price: 1296750, fuel: 50, inventory: 10, slots: 10 },
	HUNTER: { type: 'Helicopter', price: 4123000, fuel: 50, inventory: 10, slots: 10 },
	HUNTLEY: { type: 'SUV', price: 195000, fuel: 50, inventory: 10, slots: 10 },
	HUSTLER: { type: 'Muscle', price: 625000, fuel: 50, inventory: 10, slots: 10 },
	HYDRA: { type: 'Plane', price: 3990000, fuel: 50, inventory: 10, slots: 10 },
	IMPALER2: { type: 'Muscle', price: 1209500, fuel: 50, inventory: 10, slots: 10 },
	IMPALER3: { type: 'Muscle', price: 1209500, fuel: 50, inventory: 10, slots: 10 },
	IMPALER4: { type: 'Muscle', price: 1209500, fuel: 50, inventory: 10, slots: 10 },
	IMPALER: { type: 'Muscle', price: 1209500, fuel: 50, inventory: 10, slots: 10 },
	IMPERATOR2: { type: 'Muscle', price: 1718000, fuel: 50, inventory: 10, slots: 10 },
	IMPERATOR3: { type: 'Muscle', price: 1718000, fuel: 50, inventory: 10, slots: 10 },
	IMPERATOR: { type: 'Muscle', price: 1718000, fuel: 50, inventory: 10, slots: 10 },
	INFERNUS2: { type: 'Sport', price: 915000, fuel: 50, inventory: 10, slots: 10 },
	INFERNUS: { type: 'Super', price: 440000, fuel: 50, inventory: 10, slots: 10 },
	INGOT: { type: 'Sedan', price: 9000, fuel: 50, inventory: 10, slots: 10 },
	INNOVATION: { type: 'Motorcycle', price: 92500, fuel: 50, inventory: 10, slots: 10 },
	INSURGENT2: { type: 'Offroad', price: 897750, fuel: 50, inventory: 10, slots: 10 },
	INSURGENT3: { type: 'Offroad', price: 1998000, fuel: 50, inventory: 10, slots: 10 },
	INSURGENT: { type: 'Offroad', price: 1795500, fuel: 50, inventory: 10, slots: 10 },
	INTRUDER: { type: 'Sedan', price: 16000, fuel: 50, inventory: 10, slots: 10 },
	ISSI2: { type: 'Compact', price: 18000, fuel: 50, inventory: 10, slots: 10 },
	ISSI3: { type: 'Compact', price: 940000, fuel: 50, inventory: 10, slots: 10 },
	ISSI4: { type: 'Compact', price: 1089000, fuel: 50, inventory: 10, slots: 10 },
	ISSI5: { type: 'Compact', price: 1089000, fuel: 50, inventory: 10, slots: 10 },
	ISSI6: { type: 'Compact', price: 1089000, fuel: 50, inventory: 10, slots: 10 },
	ISSI7: { type: 'Sport', price: 897000, fuel: 50, inventory: 10, slots: 10 },
	ITALIGTB2: { type: 'Super', price: 1684000, fuel: 50, inventory: 10, slots: 10 },
	ITALIGTB: { type: 'Super', price: 1189000, fuel: 50, inventory: 10, slots: 10 },
	ITALIGTO: { type: 'Sport', price: 1965000, fuel: 50, inventory: 10, slots: 10 },
	JACKAL: { type: 'Coupe', price: 785000, fuel: 50, inventory: 10, slots: 10 },
	JB700: { type: 'SportsClassic', price: 350000, fuel: 50, inventory: 10, slots: 10 },
	JESTER2: { type: 'Sport', price: 350000, fuel: 50, inventory: 10, slots: 10 },
	JESTER3: { type: 'SportsClassic', price: 790000, fuel: 50, inventory: 10, slots: 10 },
	JESTER: { type: 'Sport', price: 240000, fuel: 50, inventory: 10, slots: 10 },
	JET: { type: 'Plane', price: 0, fuel: 50, inventory: 10, slots: 10 },
	JETMAX: { type: 'Boat', price: 299000, fuel: 50, inventory: 10, slots: 10 },
	JOURNEY: { type: 'Van', price: 15000, fuel: 50, inventory: 10, slots: 10 },
	JUGULAR: { type: 'Sport', price: 1225000, fuel: 50, inventory: 10, slots: 10 },
	KALAHARI: { type: 'Offroad', price: 40000, fuel: 50, inventory: 10, slots: 10 },
	KAMACHO: { type: 'Offroad', price: 535000, fuel: 50, inventory: 10, slots: 10 },
	KHAMELION: { type: 'Sport', price: 100000, fuel: 50, inventory: 10, slots: 10 },
	KHANJALI: { type: 'Military', price: 3850350, fuel: 50, inventory: 10, slots: 10 },
	KRIEGER: { type: 'Super', price: 830000, fuel: 50, inventory: 10, slots: 10 },
	KURUMA2: { type: 'Sport', price: 698250, fuel: 50, inventory: 10, slots: 10 },
	KURUMA: { type: 'Sport', price: 126350, fuel: 50, inventory: 10, slots: 10 },
	LANDSTALKER: { type: 'SUV', price: 58000, fuel: 50, inventory: 10, slots: 10 },
	LAZER: { type: 'Plane', price: 625000, fuel: 50, inventory: 10, slots: 10 },
	LE7B: { type: 'Super', price: 2475000, fuel: 50, inventory: 10, slots: 10 },
	LECTRO: { type: 'Motorcycle', price: 997500, fuel: 50, inventory: 10, slots: 10 },
	LGUARD: { type: 'Emergency', price: 865000, fuel: 50, inventory: 10, slots: 10 },
	LIMO2: { type: 'Sedan', price: 1650000, fuel: 50, inventory: 10, slots: 10 },
	LOCUST: { type: 'Sport', price: 1625000, fuel: 50, inventory: 10, slots: 10 },
	LURCHER: { type: 'SportsClassic', price: 650000, fuel: 50, inventory: 10, slots: 10 },
	LUXOR2: { type: 'Plane', price: 10000000, fuel: 50, inventory: 10, slots: 10 },
	LUXOR: { type: 'Plane', price: 1625000, fuel: 50, inventory: 10, slots: 10 },
	LYNX: { type: 'Sport', price: 1735000, fuel: 50, inventory: 10, slots: 10 },
	MAMBA: { type: 'Sport', price: 995000, fuel: 50, inventory: 10, slots: 10 },
	MAMMATUS: { type: 'Plane', price: 360000, fuel: 50, inventory: 10, slots: 10 },
	MANANA: { type: 'SportsClassic', price: 8000, fuel: 50, inventory: 10, slots: 10 },
	MANCHEZ: { type: 'Motorcycle', price: 67000, fuel: 50, inventory: 10, slots: 10 },
	MARQUIS: { type: 'Boat', price: 413990, fuel: 50, inventory: 10, slots: 10 },
	MARSHALL: { type: 'Offroad', price: 500000, fuel: 50, inventory: 10, slots: 10 },
	MASSACRO2: { type: 'Sport', price: 385000, fuel: 50, inventory: 10, slots: 10 },
	MASSACRO: { type: 'Sport', price: 275000, fuel: 50, inventory: 10, slots: 10 },
	MAVERICK: { type: 'Helicopter', price: 790000, fuel: 50, inventory: 10, slots: 10 },
	MENACER: { type: 'Offroad', price: 1775000, fuel: 50, inventory: 10, slots: 10 },
	MESA2: { type: 'SUV', price: 0, fuel: 50, inventory: 10, slots: 10 },
	MESA3: { type: 'Offroad', price: 87000, fuel: 50, inventory: 10, slots: 10 },
	MESA: { type: 'SUV', price: 30000, fuel: 50, inventory: 10, slots: 10 },
	METROTRAIN: { type: 'Train', price: 0, fuel: 50, inventory: 10, slots: 10 },
	MICHELLI: { type: 'SportsClassic', price: 345000, fuel: 50, inventory: 10, slots: 10 },
	MICROLIGHT: { type: 'Plane', price: 665000, fuel: 50, inventory: 10, slots: 10 },
	MILJET: { type: 'Plane', price: 3850350, fuel: 50, inventory: 10, slots: 10 },
	MINIVAN2: { type: 'Muscle', price: 360000, fuel: 50, inventory: 10, slots: 10 },
	MINIVAN: { type: 'Van', price: 30000, fuel: 50, inventory: 10, slots: 10 },
	MIXER2: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	MIXER: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	MOGUL: { type: 'Plane', price: 3125500, fuel: 50, inventory: 10, slots: 10 },
	MOLOTOK: { type: 'Plane', price: 4788000, fuel: 50, inventory: 10, slots: 10 },
	MONROE: { type: 'SportsClassic', price: 490000, fuel: 50, inventory: 10, slots: 10 },
	MONSTER3: { type: 'Offroad', price: 1530875, fuel: 50, inventory: 10, slots: 10 },
	MONSTER4: { type: 'Offroad', price: 1530875, fuel: 50, inventory: 10, slots: 10 },
	MONSTER5: { type: 'Offroad', price: 1530875, fuel: 50, inventory: 10, slots: 10 },
	MONSTER: { type: 'Offroad', price: 742014, fuel: 50, inventory: 10, slots: 10 },
	MOONBEAM2: { type: 'Van', price: 402500, fuel: 50, inventory: 10, slots: 10 },
	MOONBEAM: { type: 'Van', price: 32500, fuel: 50, inventory: 10, slots: 10 },
	MOWER: { type: 'Service', price: 0, fuel: 50, inventory: 10, slots: 10 },
	MULE2: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	MULE3: { type: 'Truck', price: 43225, fuel: 50, inventory: 10, slots: 10 },
	MULE4: { type: 'Commercial', price: 95700, fuel: 50, inventory: 10, slots: 10 },
	MULE: { type: 'Truck', price: 27000, fuel: 50, inventory: 10, slots: 10 },
	NEBULA: { type: 'SportsClassic', price: 797000, fuel: 50, inventory: 10, slots: 10 },
	NEMESIS: { type: 'Motorcycle', price: 12000, fuel: 50, inventory: 10, slots: 10 },
	NEO: { type: 'Sport', price: 1875000, fuel: 50, inventory: 10, slots: 10 },
	NEON: { type: 'Sport', price: 1500000, fuel: 50, inventory: 10, slots: 10 },
	NERO2: { type: 'Super', price: 2045000, fuel: 50, inventory: 10, slots: 10 },
	NERO: { type: 'Super', price: 1440000, fuel: 50, inventory: 10, slots: 10 },
	NIGHTBLADE: { type: 'Motorcycle', price: 100000, fuel: 50, inventory: 10, slots: 10 },
	NIGHTSHADE: { type: 'Muscle', price: 585000, fuel: 50, inventory: 10, slots: 10 },
	NIGHTSHARK: { type: 'Offroad', price: 1225000, fuel: 50, inventory: 10, slots: 10 },
	NIMBUS: { type: 'Plane', price: 1900000, fuel: 50, inventory: 10, slots: 10 },
	NINEF2: { type: 'Sport', price: 130000, fuel: 50, inventory: 10, slots: 10 },
	NINEF: { type: 'Sport', price: 120000, fuel: 50, inventory: 10, slots: 10 },
	NOKOTA: { type: 'Plane', price: 2653350, fuel: 50, inventory: 10, slots: 10 },
	NOVAK: { type: 'SUV', price: 608000, fuel: 50, inventory: 10, slots: 10 },
	OMNIS: { type: 'Sport', price: 701000, fuel: 50, inventory: 10, slots: 10 },
	OPPRESSOR2: { type: 'Motorcycle', price: 3890250, fuel: 50, inventory: 10, slots: 10 },
	OPPRESSOR: { type: 'Motorcycle', price: 2650000, fuel: 50, inventory: 10, slots: 10 },
	ORACLE2: { type: 'Sedan', price: 80000, fuel: 50, inventory: 10, slots: 10 },
	ORACLE: { type: 'Sedan', price: 82000, fuel: 50, inventory: 10, slots: 10 },
	OSIRIS: { type: 'Super', price: 1950000, fuel: 50, inventory: 10, slots: 10 },
	PACKER: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	PANTO: { type: 'Compact', price: 85000, fuel: 50, inventory: 10, slots: 10 },
	PARADISE: { type: 'Van', price: 50000, fuel: 50, inventory: 10, slots: 10 },
	PARAGON2: { type: 'Sport', price: 0, fuel: 50, inventory: 10, slots: 10 },
	PARAGON: { type: 'Sport', price: 905000, fuel: 50, inventory: 10, slots: 10 },
	PARIAH: { type: 'Sport', price: 1420000, fuel: 50, inventory: 10, slots: 10 },
	PATRIOT2: { type: 'SUV', price: 611800, fuel: 50, inventory: 10, slots: 10 },
	PATRIOT: { type: 'SUV', price: 50000, fuel: 50, inventory: 10, slots: 10 },
	PBUS2: { type: 'Service', price: 1385000, fuel: 50, inventory: 10, slots: 10 },
	PBUS: { type: 'Emergency', price: 731500, fuel: 50, inventory: 10, slots: 10 },
	PCJ: { type: 'Motorcycle', price: 9000, fuel: 50, inventory: 10, slots: 10 },
	PENETRATOR: { type: 'Super', price: 880000, fuel: 50, inventory: 10, slots: 10 },
	PENUMBRA: { type: 'Sport', price: 24000, fuel: 50, inventory: 10, slots: 10 },
	PEYOTE2: { type: 'Muscle', price: 805000, fuel: 50, inventory: 10, slots: 10 },
	PEYOTE: { type: 'SportsClassic', price: 38000, fuel: 50, inventory: 10, slots: 10 },
	PFISTER811: { type: 'Super', price: 1135000, fuel: 50, inventory: 10, slots: 10 },
	PHANTOM2: { type: 'Truck', price: 2553600, fuel: 50, inventory: 10, slots: 10 },
	PHANTOM3: { type: 'Commercial', price: 1225000, fuel: 50, inventory: 10, slots: 10 },
	PHANTOM: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	PHOENIX: { type: 'Muscle', price: 20000, fuel: 50, inventory: 10, slots: 10 },
	PICADOR: { type: 'Muscle', price: 9000, fuel: 50, inventory: 10, slots: 10 },
	PIGALLE: { type: 'SportsClassic', price: 400000, fuel: 50, inventory: 10, slots: 10 },
	POLICE2: { type: 'Emergency', price: 0, fuel: 50, inventory: 10, slots: 10 },
	POLICE3: { type: 'Emergency', price: 0, fuel: 50, inventory: 10, slots: 10 },
	POLICE4: { type: 'Emergency', price: 0, fuel: 50, inventory: 10, slots: 10 },
	POLICE: { type: 'Emergency', price: 0, fuel: 50, inventory: 10, slots: 10 },
	POLICEB: { type: 'Emergency', price: 0, fuel: 50, inventory: 10, slots: 10 },
	POLICEOLD1: { type: 'Emergency', price: 0, fuel: 50, inventory: 10, slots: 10 },
	POLICEOLD2: { type: 'Emergency', price: 0, fuel: 50, inventory: 10, slots: 10 },
	POLICET: { type: 'Emergency', price: 0, fuel: 50, inventory: 10, slots: 10 },
	POLMAV: { type: 'Helicopter', price: 0, fuel: 50, inventory: 10, slots: 10 },
	PONY2: { type: 'Van', price: 0, fuel: 50, inventory: 10, slots: 10 },
	PONY: { type: 'Van', price: 0, fuel: 50, inventory: 10, slots: 10 },
	POUNDER2: { type: 'Commercial', price: 820550, fuel: 50, inventory: 10, slots: 10 },
	POUNDER: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	PRAIRIE: { type: 'Compact', price: 25000, fuel: 50, inventory: 10, slots: 10 },
	PRANGER: { type: 'Emergency', price: 35000, fuel: 50, inventory: 10, slots: 10 },
	PREDATOR: { type: 'Boat', price: 0, fuel: 50, inventory: 10, slots: 10 },
	PREMIER: { type: 'Sedan', price: 10000, fuel: 50, inventory: 10, slots: 10 },
	PRIMO2: { type: 'Sedan', price: 409000, fuel: 50, inventory: 10, slots: 10 },
	PRIMO: { type: 'Sedan', price: 9000, fuel: 50, inventory: 10, slots: 10 },
	PROPTRAILER: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	PROTOTIPO: { type: 'Super', price: 2700000, fuel: 50, inventory: 10, slots: 10 },
	PYRO: { type: 'Plane', price: 4455500, fuel: 50, inventory: 10, slots: 10 },
	RADI: { type: 'SUV', price: 32000, fuel: 50, inventory: 10, slots: 10 },
	RAIDEN: { type: 'Sport', price: 1375000, fuel: 50, inventory: 10, slots: 10 },
	RAKETRAILER: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	RALLYTRUCK: { type: 'Truck', price: 1385000, fuel: 50, inventory: 10, slots: 10 },
	RANCHERXL2: { type: 'Offroad', price: 9000, fuel: 50, inventory: 10, slots: 10 },
	RANCHERXL: { type: 'Offroad', price: 9000, fuel: 50, inventory: 10, slots: 10 },
	RAPIDGT2: { type: 'Sport', price: 140000, fuel: 50, inventory: 10, slots: 10 },
	RAPIDGT3: { type: 'SportsClassic', price: 885000, fuel: 50, inventory: 10, slots: 10 },
	RAPIDGT: { type: 'Sport', price: 132000, fuel: 50, inventory: 10, slots: 10 },
	RAPTOR: { type: 'Sport', price: 648000, fuel: 50, inventory: 10, slots: 10 },
	RATBIKE: { type: 'Motorcycle', price: 48000, fuel: 50, inventory: 10, slots: 10 },
	RATLOADER2: { type: 'Pickup', price: 37500, fuel: 50, inventory: 10, slots: 10 },
	RATLOADER: { type: 'Pickup', price: 6000, fuel: 50, inventory: 10, slots: 10 },
	RCBANDITO: { type: 'Offroad', price: 1590000, fuel: 50, inventory: 10, slots: 10 },
	REAPER: { type: 'Super', price: 1595000, fuel: 50, inventory: 10, slots: 10 },
	REBEL2: { type: 'Offroad', price: 22000, fuel: 50, inventory: 10, slots: 10 },
	REBEL: { type: 'Offroad', price: 3000, fuel: 50, inventory: 10, slots: 10 },
	REGINA: { type: 'Sedan', price: 8000, fuel: 50, inventory: 10, slots: 10 },
	RENTALBUS: { type: 'Service', price: 30000, fuel: 50, inventory: 10, slots: 10 },
	RETINUE: { type: 'SportsClassic', price: 615000, fuel: 50, inventory: 10, slots: 10 },
	REVOLTER: { type: 'Sport', price: 1375000, fuel: 50, inventory: 10, slots: 10 },
	RHAPSODY: { type: 'Compact', price: 140000, fuel: 50, inventory: 10, slots: 10 },
	RHINO: { type: 'Emergency', price: 1500000, fuel: 50, inventory: 10, slots: 10 },
	RIATA: { type: 'Offroad', price: 380000, fuel: 50, inventory: 10, slots: 10 },
	RIOT2: { type: 'Emergency', price: 3125500, fuel: 50, inventory: 10, slots: 10 },
	RIOT: { type: 'Emergency', price: 0, fuel: 50, inventory: 10, slots: 10 },
	RIPLEY: { type: 'Service', price: 0, fuel: 50, inventory: 10, slots: 10 },
	ROCOTO: { type: 'SUV', price: 85000, fuel: 50, inventory: 10, slots: 10 },
	ROGUE: { type: 'Plane', price: 1596000, fuel: 50, inventory: 10, slots: 10 },
	ROMERO: { type: 'Sedan', price: 45000, fuel: 50, inventory: 10, slots: 10 },
	RROCKET: { type: 'Motorcycle', price: 925000, fuel: 50, inventory: 10, slots: 10 },
	RUBBLE: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	RUFFIAN: { type: 'Motorcycle', price: 10000, fuel: 50, inventory: 10, slots: 10 },
	RUINER2: { type: 'Sport', price: 5745600, fuel: 50, inventory: 10, slots: 10 },
	RUINER3: { type: 'Sport', price: 0, fuel: 50, inventory: 10, slots: 10 },
	RUINER: { type: 'Muscle', price: 10000, fuel: 50, inventory: 10, slots: 10 },
	RUMPO2: { type: 'Van', price: 0, fuel: 50, inventory: 10, slots: 10 },
	RUMPO3: { type: 'Van', price: 1610000, fuel: 50, inventory: 10, slots: 10 },
	RUMPO: { type: 'Van', price: 13000, fuel: 50, inventory: 10, slots: 10 },
	RUSTON: { type: 'Sport', price: 430000, fuel: 50, inventory: 10, slots: 10 },
	S80: { type: 'Super', price: 2575000, fuel: 50, inventory: 10, slots: 10 },
	SABREGT2: { type: 'Muscle', price: 480500, fuel: 50, inventory: 10, slots: 10 },
	SABREGT: { type: 'Muscle', price: 380000, fuel: 50, inventory: 10, slots: 10 },
	SADLER2: { type: 'Pickup', price: 0, fuel: 50, inventory: 10, slots: 10 },
	SADLER: { type: 'Pickup', price: 3125500, fuel: 50, inventory: 10, slots: 10 },
	SANCHEZ2: { type: 'Motorcycle', price: 8000, fuel: 50, inventory: 10, slots: 10 },
	SANCHEZ: { type: 'Motorcycle', price: 7000, fuel: 50, inventory: 10, slots: 10 },
	SANCTUS: { type: 'Motorcycle', price: 1995000, fuel: 50, inventory: 10, slots: 10 },
	SANDKING2: { type: 'Offroad', price: 38000, fuel: 50, inventory: 10, slots: 10 },
	SANDKING: { type: 'Offroad', price: 450000, fuel: 50, inventory: 10, slots: 10 },
	SAVAGE: { type: 'Helicopter', price: 2593500, fuel: 50, inventory: 10, slots: 10 },
	SAVESTRA: { type: 'SportsClassic', price: 990000, fuel: 50, inventory: 10, slots: 10 },
	SC1: { type: 'Super', price: 1603000, fuel: 50, inventory: 10, slots: 10 },
	SCARAB2: { type: 'Military', price: 2313000, fuel: 50, inventory: 10, slots: 10 },
	SCARAB3: { type: 'Military', price: 2313000, fuel: 50, inventory: 10, slots: 10 },
	SCARAB: { type: 'Military', price: 2313000, fuel: 50, inventory: 10, slots: 10 },
	SCHAFTER2: { type: 'Sedan', price: 65000, fuel: 50, inventory: 10, slots: 10 },
	SCHAFTER3: { type: 'Sedan', price: 116000, fuel: 50, inventory: 10, slots: 10 },
	SCHAFTER4: { type: 'Sedan', price: 208000, fuel: 50, inventory: 10, slots: 10 },
	SCHAFTER5: { type: 'Sedan', price: 325000, fuel: 50, inventory: 10, slots: 10 },
	SCHAFTER6: { type: 'Sedan', price: 438000, fuel: 50, inventory: 10, slots: 10 },
	SCHLAGEN: { type: 'Sport', price: 1300000, fuel: 50, inventory: 10, slots: 10 },
	SCHWARZER: { type: 'Sport', price: 80000, fuel: 50, inventory: 10, slots: 10 },
	SCORCHER: { type: 'Bicycle', price: 2000, fuel: 50, inventory: 10, slots: 10 },
	SCRAMJET: { type: 'Super', price: 4628400, fuel: 50, inventory: 10, slots: 10 },
	SCRAP: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	SEABREEZE: { type: 'Plane', price: 1130500, fuel: 50, inventory: 10, slots: 10 },
	SEASHARK2: { type: 'Boat', price: 0, fuel: 50, inventory: 10, slots: 10 },
	SEASHARK3: { type: 'Boat', price: 0, fuel: 50, inventory: 10, slots: 10 },
	SEASHARK: { type: 'Boat', price: 16899, fuel: 50, inventory: 10, slots: 10 },
	SEASPARROW: { type: 'Helicopter', price: 1815000, fuel: 50, inventory: 10, slots: 10 },
	SEMINOLE: { type: 'SUV', price: 30000, fuel: 50, inventory: 10, slots: 10 },
	SENTINEL2: { type: 'Coupe', price: 95000, fuel: 50, inventory: 10, slots: 10 },
	SENTINEL3: { type: 'Sport', price: 650000, fuel: 50, inventory: 10, slots: 10 },
	SENTINEL: { type: 'Coupe', price: 60000, fuel: 50, inventory: 10, slots: 10 },
	SERRANO: { type: 'SUV', price: 60000, fuel: 50, inventory: 10, slots: 10 },
	SEVEN70: { type: 'Sport', price: 695000, fuel: 50, inventory: 10, slots: 10 },
	SHAMAL: { type: 'Plane', price: 1150000, fuel: 50, inventory: 10, slots: 10 },
	SHEAVA: { type: 'Super', price: 1995000, fuel: 50, inventory: 10, slots: 10 },
	SHERIFF2: { type: 'Emergency', price: 0, fuel: 50, inventory: 10, slots: 10 },
	SHERIFF: { type: 'Emergency', price: 0, fuel: 50, inventory: 10, slots: 10 },
	SHOTARO: { type: 'Motorcycle', price: 2225000, fuel: 50, inventory: 10, slots: 10 },
	SKYLIFT: { type: 'Helicopter', price: 0, fuel: 50, inventory: 10, slots: 10 },
	SLAMVAN2: { type: 'Muscle', price: 0, fuel: 50, inventory: 10, slots: 10 },
	SLAMVAN3: { type: 'Muscle', price: 464500, fuel: 50, inventory: 10, slots: 10 },
	SLAMVAN4: { type: 'Muscle', price: 650000, fuel: 50, inventory: 10, slots: 10 },
	SLAMVAN5: { type: 'Muscle', price: 1321875, fuel: 50, inventory: 10, slots: 10 },
	SLAMVAN6: { type: 'Muscle', price: 1321875, fuel: 50, inventory: 10, slots: 10 },
	SLAMVAN: { type: 'Muscle', price: 49500, fuel: 50, inventory: 10, slots: 10 },
	SOVEREIGN: { type: 'Motorcycle', price: 120000, fuel: 50, inventory: 10, slots: 10 },
	SPECTER2: { type: 'Sport', price: 851000, fuel: 50, inventory: 10, slots: 10 },
	SPECTER: { type: 'Sport', price: 599000, fuel: 50, inventory: 10, slots: 10 },
	SPEEDER2: { type: 'Boat', price: 0, fuel: 50, inventory: 10, slots: 10 },
	SPEEDER: { type: 'Boat', price: 325000, fuel: 50, inventory: 10, slots: 10 },
	SPEEDO2: { type: 'Van', price: 15000, fuel: 50, inventory: 10, slots: 10 },
	SPEEDO4: { type: 'Van', price: 0, fuel: 50, inventory: 10, slots: 10 },
	SPEEDO: { type: 'Van', price: 15000, fuel: 50, inventory: 10, slots: 10 },
	SQUALO: { type: 'Boat', price: 196621, fuel: 50, inventory: 10, slots: 10 },
	STAFFORD: { type: 'Sedan', price: 1272000, fuel: 50, inventory: 10, slots: 10 },
	STALION2: { type: 'Muscle', price: 277000, fuel: 50, inventory: 10, slots: 10 },
	STALION: { type: 'Muscle', price: 71000, fuel: 50, inventory: 10, slots: 10 },
	STANIER: { type: 'Sedan', price: 10000, fuel: 50, inventory: 10, slots: 10 },
	STARLING: { type: 'Plane', price: 3657500, fuel: 50, inventory: 10, slots: 10 },
	STINGER: { type: 'SportsClassic', price: 850000, fuel: 50, inventory: 10, slots: 10 },
	STINGERGT: { type: 'SportsClassic', price: 875000, fuel: 50, inventory: 10, slots: 10 },
	STOCKADE3: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	STOCKADE: { type: 'Truck', price: 2240000, fuel: 50, inventory: 10, slots: 10 },
	STRATUM: { type: 'Sedan', price: 10000, fuel: 50, inventory: 10, slots: 10 },
	STREITER: { type: 'Sport', price: 500000, fuel: 50, inventory: 10, slots: 10 },
	STRETCH: { type: 'Sedan', price: 30000, fuel: 50, inventory: 10, slots: 10 },
	STRIKEFORCE: { type: 'Plane', price: 3800000, fuel: 50, inventory: 10, slots: 10 },
	STROMBERG: { type: 'SportsClassic', price: 3185350, fuel: 50, inventory: 10, slots: 10 },
	STUNT: { type: 'Plane', price: 250000, fuel: 50, inventory: 10, slots: 10 },
	SUBMERSIBLE2: { type: 'Boat', price: 1325000, fuel: 50, inventory: 10, slots: 10 },
	SUBMERSIBLE: { type: 'Boat', price: 0, fuel: 50, inventory: 10, slots: 10 },
	SULTAN: { type: 'Sport', price: 12000, fuel: 50, inventory: 10, slots: 10 },
	SULTANRS: { type: 'Super', price: 807000, fuel: 50, inventory: 10, slots: 10 },
	SUNTRAP: { type: 'Boat', price: 25160, fuel: 50, inventory: 10, slots: 10 },
	SUPERD: { type: 'Sedan', price: 500000, fuel: 50, inventory: 10, slots: 10 },
	SUPERVOLITO2: { type: 'Helicopter', price: 3185350, fuel: 50, inventory: 10, slots: 10 },
	SUPERVOLITO: { type: 'Helicopter', price: 2113000, fuel: 50, inventory: 10, slots: 10 },
	SURANO: { type: 'Sport', price: 110000, fuel: 50, inventory: 10, slots: 10 },
	SURFER2: { type: 'Van', price: 5000, fuel: 50, inventory: 10, slots: 10 },
	SURFER: { type: 'Van', price: 11000, fuel: 50, inventory: 10, slots: 10 },
	SURGE: { type: 'Sedan', price: 38000, fuel: 50, inventory: 10, slots: 10 },
	SWIFT2: { type: 'Helicopter', price: 5150000, fuel: 50, inventory: 10, slots: 10 },
	SWIFT: { type: 'Helicopter', price: 1600000, fuel: 50, inventory: 10, slots: 10 },
	SWINGER: { type: 'SportsClassic', price: 909000, fuel: 50, inventory: 10, slots: 10 },
	T20: { type: 'Super', price: 2200000, fuel: 50, inventory: 10, slots: 10 },
	TACO: { type: 'Van', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TAILGATER: { type: 'Sedan', price: 55000, fuel: 50, inventory: 10, slots: 10 },
	TAIPAN: { type: 'Super', price: 1980000, fuel: 50, inventory: 10, slots: 10 },
	TAMPA2: { type: 'Muscle', price: 995000, fuel: 50, inventory: 10, slots: 10 },
	TAMPA3: { type: 'Muscle', price: 1585000, fuel: 50, inventory: 10, slots: 10 },
	TAMPA: { type: 'Muscle', price: 375000, fuel: 50, inventory: 10, slots: 10 },
	TANKER2: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TANKER: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TANKERCAR: { type: 'Train', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TAXI: { type: 'Sedan', price: 13000, fuel: 50, inventory: 10, slots: 10 },
	TECHNICAL2: { type: 'Offroad', price: 1489600, fuel: 50, inventory: 10, slots: 10 },
	TECHNICAL3: { type: 'Offroad', price: 1406000, fuel: 50, inventory: 10, slots: 10 },
	TECHNICAL: { type: 'Offroad', price: 1263500, fuel: 50, inventory: 10, slots: 10 },
	TEMPESTA: { type: 'Super', price: 1329000, fuel: 50, inventory: 10, slots: 10 },
	TERBYTE: { type: 'Commercial', price: 3459500, fuel: 50, inventory: 10, slots: 10 },
	TEZERACT: { type: 'Super', price: 2825000, fuel: 50, inventory: 10, slots: 10 },
	THRAX: { type: 'Super', price: 2325000, fuel: 50, inventory: 10, slots: 10 },
	THRUST: { type: 'Motorcycle', price: 75000, fuel: 50, inventory: 10, slots: 10 },
	THRUSTER: { type: 'Military', price: 3657500, fuel: 50, inventory: 10, slots: 10 },
	TIPTRUCK2: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TIPTRUCK: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TITAN: { type: 'Plane', price: 5000000, fuel: 50, inventory: 10, slots: 10 },
	TORERO: { type: 'SportsClassic', price: 998000, fuel: 50, inventory: 10, slots: 10 },
	TORNADO2: { type: 'SportsClassic', price: 2825000, fuel: 50, inventory: 10, slots: 10 },
	TORNADO3: { type: 'SportsClassic', price: 30000, fuel: 50, inventory: 10, slots: 10 },
	TORNADO4: { type: 'SportsClassic', price: 3657500, fuel: 50, inventory: 10, slots: 10 },
	TORNADO5: { type: 'Muscle', price: 405000, fuel: 50, inventory: 10, slots: 10 },
	TORNADO6: { type: 'Muscle', price: 378000, fuel: 50, inventory: 10, slots: 10 },
	TORNADO: { type: 'SportsClassic', price: 30000, fuel: 50, inventory: 10, slots: 10 },
	TORO2: { type: 'Boat', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TORO: { type: 'Boat', price: 1750000, fuel: 50, inventory: 10, slots: 10 },
	TOROS: { type: 'SUV', price: 498000, fuel: 50, inventory: 10, slots: 10 },
	TOURBUS: { type: 'Service', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TOWTRUCK2: { type: 'Truck', price: 32000, fuel: 50, inventory: 10, slots: 10 },
	TOWTRUCK: { type: 'Truck', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TR2: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TR3: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TR4: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TRACTOR2: { type: 'Service', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TRACTOR3: { type: 'Service', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TRACTOR: { type: 'Service', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TRAILERLARGE: { type: 'Utility', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TRAILERLOGS: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TRAILERS2: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TRAILERS3: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TRAILERS4: { type: 'Utility', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TRAILERS: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TRAILERSMALL2: { type: 'Military', price: 1400000, fuel: 50, inventory: 10, slots: 10 },
	TRAILERSMALL: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TRASH2: { type: 'Service', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TRASH: { type: 'Service', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TRFLAT: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TRIBIKE2: { type: 'Bicycle', price: 10000, fuel: 50, inventory: 10, slots: 10 },
	TRIBIKE3: { type: 'Bicycle', price: 10000, fuel: 50, inventory: 10, slots: 10 },
	TRIBIKE: { type: 'Bicycle', price: 10000, fuel: 50, inventory: 10, slots: 10 },
	TROPHYTRUCK2: { type: 'Offroad', price: 695000, fuel: 50, inventory: 10, slots: 10 },
	TROPHYTRUCK: { type: 'Offroad', price: 550000, fuel: 50, inventory: 10, slots: 10 },
	TROPIC2: { type: 'Boat', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TROPIC: { type: 'Boat', price: 22000, fuel: 50, inventory: 10, slots: 10 },
	TROPOS: { type: 'Sport', price: 816000, fuel: 50, inventory: 10, slots: 10 },
	TUG: { type: 'Boat', price: 1250000, fuel: 50, inventory: 10, slots: 10 },
	TULA: { type: 'Plane', price: 5173700, fuel: 50, inventory: 10, slots: 10 },
	TULIP: { type: 'Muscle', price: 718000, fuel: 50, inventory: 10, slots: 10 },
	TURISMO2: { type: 'Super', price: 705000, fuel: 50, inventory: 10, slots: 10 },
	TURISMOR: { type: 'Super', price: 500000, fuel: 50, inventory: 10, slots: 10 },
	TVTRAILER: { type: 'Trailer', price: 0, fuel: 50, inventory: 10, slots: 10 },
	TYRANT: { type: 'Super', price: 2515000, fuel: 50, inventory: 10, slots: 10 },
	TYRUS: { type: 'Super', price: 2550000, fuel: 50, inventory: 10, slots: 10 },
	UTILLITRUCK2: { type: 'Service', price: 0, fuel: 50, inventory: 10, slots: 10 },
	UTILLITRUCK3: { type: 'Service', price: 0, fuel: 50, inventory: 10, slots: 10 },
	UTILLITRUCK: { type: 'Service', price: 0, fuel: 50, inventory: 10, slots: 10 },
	VACCA: { type: 'Super', price: 240000, fuel: 50, inventory: 10, slots: 10 },
	VADER: { type: 'Motorcycle', price: 2515000, fuel: 50, inventory: 10, slots: 10 },
	VAGNER: { type: 'Super', price: 1535000, fuel: 50, inventory: 10, slots: 10 },
	VALKYRIE2: { type: 'Helicopter', price: 0, fuel: 50, inventory: 10, slots: 10 },
	VALKYRIE: { type: 'Helicopter', price: 3790500, fuel: 50, inventory: 10, slots: 10 },
	VAMOS: { type: 'Muscle', price: 596000, fuel: 50, inventory: 10, slots: 10 },
	VELUM2: { type: 'Plane', price: 1323350, fuel: 50, inventory: 10, slots: 10 },
	VELUM: { type: 'Plane', price: 450000, fuel: 50, inventory: 10, slots: 10 },
	VERLIERER2: { type: 'Sport', price: 695000, fuel: 50, inventory: 10, slots: 10 },
	VESTRA: { type: 'Plane', price: 950000, fuel: 50, inventory: 10, slots: 10 },
	VIGERO: { type: 'Muscle', price: 21000, fuel: 50, inventory: 10, slots: 10 },
	VIGILANTE: { type: 'Military', price: 3750000, fuel: 50, inventory: 10, slots: 10 },
	VINDICATOR: { type: 'Motorcycle', price: 630000, fuel: 50, inventory: 10, slots: 10 },
	VIRGO2: { type: 'Muscle', price: 405000, fuel: 50, inventory: 10, slots: 10 },
	VIRGO3: { type: 'Muscle', price: 165000, fuel: 50, inventory: 10, slots: 10 },
	VIRGO: { type: 'Muscle', price: 195000, fuel: 50, inventory: 10, slots: 10 },
	VISERIS: { type: 'SportsClassic', price: 875000, fuel: 50, inventory: 10, slots: 10 },
	VISIONE: { type: 'Super', price: 2250000, fuel: 50, inventory: 10, slots: 10 },
	VOLATOL: { type: 'Plane', price: 3724000, fuel: 50, inventory: 10, slots: 10 },
	VOLATUS: { type: 'Helicopter', price: 2295000, fuel: 50, inventory: 10, slots: 10 },
	VOLTIC2: { type: 'Super', price: 3830400, fuel: 50, inventory: 10, slots: 10 },
	VOLTIC: { type: 'Super', price: 150000, fuel: 50, inventory: 10, slots: 10 },
	VOODOO2: { type: 'Muscle', price: 3724000, fuel: 50, inventory: 10, slots: 10 },
	VOODOO: { type: 'Muscle', price: 425500, fuel: 50, inventory: 10, slots: 10 },
	VORTEX: { type: 'Motorcycle', price: 356000, fuel: 50, inventory: 10, slots: 10 },
	WARRENER: { type: 'Sedan', price: 125000, fuel: 50, inventory: 10, slots: 10 },
	WASHINGTON: { type: 'Sedan', price: 15000, fuel: 50, inventory: 10, slots: 10 },
	WASTELANDER: { type: 'Super', price: 658350, fuel: 50, inventory: 10, slots: 10 },
	WINDSOR2: { type: 'Coupe', price: 900000, fuel: 50, inventory: 10, slots: 10 },
	WINDSOR: { type: 'Coupe', price: 845000, fuel: 50, inventory: 10, slots: 10 },
	WOLFSBANE: { type: 'Motorcycle', price: 95000, fuel: 50, inventory: 10, slots: 10 },
	XA21: { type: 'Super', price: 2375000, fuel: 50, inventory: 10, slots: 10 },
	XLS2: { type: 'SUV', price: 522000, fuel: 50, inventory: 10, slots: 10 },
	XLS: { type: 'SUV', price: 253000, fuel: 50, inventory: 10, slots: 10 },
	YOSEMITE: { type: 'Muscle', price: 485000, fuel: 50, inventory: 10, slots: 10 },
	YOUGA2: { type: 'Van', price: 195000, fuel: 50, inventory: 10, slots: 10 },
	YOUGA: { type: 'Van', price: 16000, fuel: 50, inventory: 10, slots: 10 },
	Z190: { type: 'SportsClassic', price: 900000, fuel: 50, inventory: 10, slots: 10 },
	ZENTORNO: { type: 'Super', price: 725000, fuel: 50, inventory: 10, slots: 10 },
	ZION2: { type: 'Coupe', price: 65000, fuel: 50, inventory: 10, slots: 10 },
	ZION3: { type: 'SportsClassic', price: 812000, fuel: 50, inventory: 10, slots: 10 },
	ZION: { type: 'Coupe', price: 485000, fuel: 50, inventory: 10, slots: 10 },
	ZOMBIEA: { type: 'Motorcycle', price: 900000, fuel: 50, inventory: 10, slots: 10 },
	ZOMBIEB: { type: 'Motorcycle', price: 122000, fuel: 50, inventory: 10, slots: 10 },
	ZORRUSSO: { type: 'Super', price: 1925000, fuel: 50, inventory: 10, slots: 10 },
	ZR3802: { type: 'Sport', price: 1608000, fuel: 50, inventory: 10, slots: 10 },
	ZR3803: { type: 'Sport', price: 1608000, fuel: 50, inventory: 10, slots: 10 },
	ZR380: { type: 'Sport', price: 1608000, fuel: 50, inventory: 10, slots: 10 },
	ZTYPE: { type: 'SportsClassic', price: 950000, fuel: 50, inventory: 10, slots: 10 }
};
