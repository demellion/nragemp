import { localize } from '../index';

interface Shop {
	name: string;
	description: string;
	data: any;
}

export const Shops: { [key: string]: Shop } = {
	vehshop_elite: {
		name: localize.t('BLIPS.VEHICLESHOPELITE'),
		description: 'Example description',
		data: [
			'TIGON',
			'FURIA',
			'ADDER',
			'AUTARCH',
			'BANSHEE2',
			'BULLET',
			'CHEETAH',
			'CYCLONE',
			'DEVESTE',
			'EMERUS',
			'ENTITY2',
			'ENTITYXF',
			'FMJ',
			'GP1',
			'INFERNUS',
			'ITALIGTB2',
			'ITALIGTB',
			'KRIEGER',
			'LE7B',
			'NERO2',
			'NERO',
			'OSIRIS',
			'PENETRATOR',
			'PFISTER811',
			'PROTOTIPO',
			'REAPER',
			'S80',
			'SC1',
			'SCRAMJET',
			'SHEAVA',
			'SULTANRS',
			'SUPERD',
			'SUPERVOLITO2',
			'SUPERVOLITO',
			'T20',
			'TAIPAN',
			'TEMPESTA',
			'TEZERACT',
			'THRAX',
			'TURISMO2',
			'TURISMOR',
			'TYRANT',
			'TYRUS',
			'VACCA',
			'VAGNER',
			'VISIONE',
			'VOLTIC2',
			'VOLTIC',
			'WASTELANDER',
			'XA21',
			'ZENTORNO',
			'ZORRUSSO'
		]
	}
};