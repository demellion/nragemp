export enum Routes {
	empty = '/',
	auth = '/auth',
	characters = '/characters',
	createCharacter = '/create_character',
	console = '/console',
	hud = '/hud',
	vehShop = '/veh_shop/:alias'
}

export const GuidTemplate = '[0-9A-Fa-f]{8}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{12}';
