import { CharCreateData, CustomizationData } from '../types/character/customizationData';

export const splitCustomizationData = (data: CharCreateData): CustomizationData => {
	const headOverlays = {
		blemishes: data.blemishes,
		facialHair: data.facialHair,
		eyebrows: data.eyebrows,
		ageing: data.ageing,
		makeup: data.makeup,
		blush: data.blush,
		complexion: data.complexion,
		sunDamage: data.sunDamage,
		lipstick: data.lipstick,
		moles: data.moles,
		chestHair: data.chestHair,
		bodyBlemishes: data.bodyBlemishes,
		addBodyBlemishes: data.addBodyBlemishes
	};
	const faceFeatures = {
		noseWidth: data.noseWidth,
		noseHeight: data.noseHeight,
		noseLength: data.noseLength,
		noseBridge: data.noseBridge,
		noseTip: data.noseTip,
		noseBridgeShift: data.noseBridgeShift,
		browHeight: data.browHeight,
		browWidth: data.browWidth,
		cheekBoneHeight: data.cheekBoneHeight,
		cheekBoneWidth: data.cheekBoneWidth,
		cheeksWidth: data.cheeksWidth,
		eyes: data.eyes,
		lips: data.lips,
		jawWidth: data.jawWidth,
		jawHeight: data.jawHeight,
		chinLength: data.chinLength,
		chinPosition: data.chinPosition,
		chinWidth: data.cheeksWidth,
		chinShape: data.chinShape,
		neckWidth: data.neckWidth
	};
	return {
		gender: data.gender,
		father: data.father,
		mother: data.mother,
		similar: data.similar,
		hair: data.hair,
		hairPrimaryColor: data.hairPrimaryColor,
		hairSecondaryColor: data.hairSecondaryColor,
		facialHairColor: data.facialHairColor,
		eyeColor: data.eyeColor,
		headOverlays,
		faceFeatures
	};
};
