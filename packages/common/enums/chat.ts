export enum ChatLabels {
	me = 'me',
	do = 'do',
	try = 'try',
	todo = 'todo',
	b = 'b',
	s = 's',
	w = 'w',
	f = 'f',
	fb = 'fb',
	r = 'r',
	rb = 'rb',
	fam = 'fam'
}
