export enum AdminLevels {
	DEFAULT,
	MODERATOR,
	ADMIN,
	SUPERADMIN
}
