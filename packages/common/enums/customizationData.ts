export enum headOverlay {
	blemishes, // недостатки
	facialHair, // волосы на лице
	eyebrows, // брови
	ageing, // старение
	makeup, // макияж
	blush, // румянец
	complexion, // топ лица
	sunDamage, // ожоги
	lipstick, // помада
	moles, // родинки и веснушки
	chestHair, // волосы на груди
	bodyBlemishes,
	addBodyBlemishes
}

export enum faceFeature {
	noseWidth, // ширина носа
	noseHeight, // высота носа
	noseLength, // длина кончика носа
	noseBridge, // глубина моста ноа
	noseTip, // высота кончика носа
	noseBridgeShift, // поломанноть носа

	browHeight,
	browWidth,

	cheekBoneHeight,
	cheekBoneWidth,
	cheeksWidth,

	eyes,

	lips,

	jawWidth,
	jawHeight,

	chinLength,
	chinPosition,
	chinWidth,
	chinShape,

	neckWidth
}
