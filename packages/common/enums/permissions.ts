export enum AdminPermissions {
	// Moderator level
	MUTE = 1,
	KICK = 1,
	TELEPORTSELF = 1,
	SPECTATE = 1,
	INFO = 1,

	// Admin level
	BAN = 2,
	WARN = 2,
	KILL = 2,
	FREEZE = 2,
	INVISIBLE = 2,
	CREATE = 2,
	DELETE = 2,
	EVENTS = 2,
	JAIL = 2,
	TELEPORT = 2,
	DIMENSION = 2,
	NRAY = 2,

	// Superadmin level
	CONSOLE = 3
}
