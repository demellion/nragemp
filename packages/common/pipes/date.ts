import { IsoString, Timestamp } from '../types/date';
import * as moment from 'moment';

export const getIsoString = (date: moment.Moment): IsoString => {
	if (!date) return '';

	return date.toISOString();
};

export const getTimestamp = (duration: moment.Duration): Timestamp => {
	if (!duration) return 0;

	return duration.as('ms');
};
