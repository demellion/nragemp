import { Easing, easingsFunctions } from '../constants/easingsFunctions';

export const animate = (
	value: number,
	maxValue: number,
	duration: number, // in milliseconds
	easing: Easing,
	callback: (v: number) => void
): void => {
	const start = Date.now();
	let stop = false;

	const anim = (now: number) => {
		if (stop) return;
		if (now - start >= duration) stop = true;

		const progress = (now - start) / duration;
		const easingValue = easingsFunctions[easing](progress);
		const calculated = value + (maxValue - value) * easingValue;

		callback(calculated);

		setTimeout(() => anim(Date.now()), easingValue);
	};

	anim(start);
};

export const animateCef = (
	start: number,
	value: number,
	maxValue: number,
	duration: number, // in milliseconds
	easing: Easing,
	callback: (v: number) => void
): void => {
	let stop = false;

	const drawFrame = (now: number) => {
		if (stop) return;
		if (now - start >= duration) stop = true;

		const progress = (now - start) / duration;
		const easingValue = easingsFunctions[easing](progress);
		const calculated = value + (maxValue - value) * easingValue;

		callback(calculated);

		requestAnimationFrame(drawFrame);
	};

	drawFrame(start);
};
