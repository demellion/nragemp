export const sleep = async (ms: number): Promise<void> => (
	new Promise<void>((resolve: any) => {
		setTimeout(() => {
			resolve();
		}, ms);
	})
);
