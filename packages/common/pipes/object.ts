const objEqual = require('fast-deep-equal');

export function patchObject<S>(current: S, update: Partial<S>): S {
	return Object.assign({}, current, update);
}

export const compareObjects = (obj1: object, obj2: object, filter?: string[]): boolean => {
	if (obj1 === obj2) {
		return true;
	}
	if (obj1 !== Object(obj1) || obj2 !== Object(obj2)) {
		return false;
	}
	if (!filter) {
		return objEqual(obj1, obj2);
	}
	const keys1 = Object.keys(obj1).filter(key => filter.indexOf(key) === -1);
	const keys2 = Object.keys(obj2).filter(key => filter.indexOf(key) === -1);
	const newObj1 = {};
	const newObj2 = {};
	keys1.forEach(key => {
		newObj1[key] = obj1[key];
	});
	keys2.forEach(key => {
		newObj2[key] = obj2[key];
	});
	return objEqual(newObj1, newObj2);
};
