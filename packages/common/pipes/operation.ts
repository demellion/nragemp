import { Errors } from '../types/error';
import { localize } from '../index';
import { IOperationSuccess } from '../types/operationResult';

export class Error<TError> {
	readonly type: TError | Errors;
	readonly message: string;

	constructor(type: TError | Errors, message: string) {
		this.type = type;
		this.message = message;
	}
}

export class OperationFailed<TError = Errors.unknown> {
	readonly success = false;
	readonly error: Error<TError | Errors>;

	constructor(type?: TError, message?: string) {
		this.error = new Error<TError>(type || Errors.unknown, message || localize.t('ERRORS.UNKNOWN'));
	}
}

export class OperationSuccess<TResult> implements IOperationSuccess<TResult> {
	readonly success = true;
	readonly result: TResult;

	constructor(result?: TResult) {
		this.result = result;
	}
}
