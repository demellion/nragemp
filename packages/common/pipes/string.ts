export const maxLength = (s: string, count: number): string => {
	if (!s || s.length <= count) {
		return s;
	}

	return `${s.substr(0, count - 3)}...`;
};

export const firstLetterUpperCase = (text: string): string => {
	if (!text) {
		return '';
	}
	return text[0].toUpperCase() + text.substr(1);
};

export const firstLetterLowerCase = (text: string): string => {
	if (!text) {
		return '';
	}
	return text[0].toLowerCase() + text.substr(1);
};

export const replaceSpaceToThin = (text: string): string => {
	if (!text) {
		return '';
	}
	return text.replace(/\s/g, ' ');
};

export const moneyString = (value: number | string): string => {
	const money = +value;
	if (Number.isNaN(money)) {
		return '';
	}
	return (
		'$' + replaceSpaceToThin(money.toLocaleString('ru').replace(/-/g, '−'))
	);
};

export const shortMoneyString = (value: number | string): string => {
	const money = +value;
	if (Number.isNaN(money)) {
		return '';
	}
	return moneyString(parseFloat((money / 1000).toFixed(1))) + ' K';
};

export const speedString = (value: number | string): string => {
	const speed = +value;
	if (Number.isNaN(speed)) {
		return '';
	}
	return `${Math.floor(speed)} КМ/Ч`;
};

export const fuelString = (value: number | string): string => {
	const fuel = +value;
	if (Number.isNaN(fuel)) {
		return '';
	}
	return `${Math.floor(fuel)}л`;
};
