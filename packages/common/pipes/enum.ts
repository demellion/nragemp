export const getKeysFromEnum = <T>(item: T): string[] => {
	if (!item) return null;
	// eslint-disable-next-line no-self-compare
	return Object.keys(item).filter(key => parseInt(key) !== parseInt(key));
};
