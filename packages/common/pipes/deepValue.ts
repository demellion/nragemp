export const getDeepValue = (obj: any, path: string): any => {
	if (!obj || !path) {
		return null;
	}

	let result = obj;
	path.split('.').forEach((fieldName) => {
		if (result !== undefined) {
			result = result[fieldName];
		}
	});
	return result;
};

export const setDeepValue = (obj: any, path: string, value: any): void => {
	if (!obj || !path) {
		return null;
	}

	let node = obj;
	path.split('.').forEach((fieldName, index, array) => {
		const isLastItem = (index === (array.length - 1));
		if (node[fieldName] === undefined || isLastItem) {
			node[fieldName] = isLastItem ? value : {};
		}
		node = node[fieldName];
	});
};
