export default {
	translation: {
		ACHIEVEMENTS: {
			COLLECTIBLES: '~HUD_COLOUR_RADAR_DAMAGE~Collectible found',
			HOWMUCHCOUNT: '{{current}} | {{next}}'
		},
		GLOBAL: {
			CASH: 'Cash',
			BANK: 'Bank',
			POINTS: 'NR Points'
		},
		CHAT: {
			SAY: '{{name}} says: {{message}}',
			BAN: '{{name}} was banned for {{time}} days. Reason: {{text}}',
			KICK: '{{name}} was kicked for: {{text}}',
			WARN: '{{name}} was warned for: {{text}}',
			WELCOME_MSG1: 'Welcome on NRage server!'
		},
		PLAYER: {
			BAN: 'You were banned until: {{time}}. Reason: {{text}}',
			KICK: 'You were kicked for: {{text}}',
			WARN: 'You were warned for: {{text}}'
		},
		BLIPS: {
			GASSTATION: 'Gas Station',
			SHOOTINGRANGE: 'Shooting Range',
			VEHICLEIMPOUND: 'Vehicle Impound',
			SUPERMARKET: '24/7',
			AIRCARRIER: 'Airlines',
			PLAYERHOUSE: 'Home',
			TAXI: 'Taxi',
			LSPD: 'LSPD',
			EMS: 'EMS',
			COLLECTOR: 'Collector',
			BARBER: 'Barber',
			CARTUNING: 'Los Santos Customs',
			CLOTHINGSHOP: 'Clothing Shop',
			TATTOO: 'Tattoo',
			FIB: 'FIB',
			GHETTOSKULL: 'Ghetto',
			MASKSSHOP1: 'Masks Shop',
			MASKSSHOP2: 'Masks Shop',
			MASKSSHOP3: 'Masks Shop',
			BANK: 'Bank',
			AMMUNATION: 'Ammunation',
			AMMUNATIONPLUS: 'Shooting Range (Ammunation)',
			HUNTING: 'Hunting',
			CANNABIS: 'Cannabis',
			GPS: 'GPS',
			WEAZELNEWS: 'News',
			PRISON: 'Jail',
			VEHICLESHOP: 'Vehicle Shop',
			VEHICLESHOPELITE: 'VehicleShop (Elite)',
			BIKECLUB: 'Bikers Club',
			PETSSHOP: 'Zoo Shop',
			BOATSHOP: 'Boats Shop',
			REALESTATE: 'Real Estate',
			HOUSESMALL: 'Motel',
			HOUSEMEDIUM: 'Apartments',
			HOUSEBIG: 'Mansion',
			CASINO1: 'Casino',
			CASINO2: 'Casino',
			DIVING: 'Diving',
			MECHANICS: 'Repair Shop',
			CITYHALL: 'City Hall',
			ARMY: 'Army',
			JOBTRUCKER: 'Truckers Park',
			JOBBUS: 'Bus Park',
			PARKING: 'Parking',
			YOURVEHICLE: 'Your personal vehicle'
		},
		PEDS: {
			CASINOCASH: 'Cashier',
			BARMEN: 'Bartender',
			ROULETTE: 'Roulette',
			TREVOR: 'Trevor',
			DENISE: 'Denise Clinton',
			MICHAEL: 'Michael',
			FRANKLIN: 'Franklin',
			LAMAR: 'Lamar',
			AGATHA: 'Agatha Baker',
			SECURITY: 'Security'
		},
		ACTIONS: {
			DEFAULT: 'Press ~INPUT_CONTEXT~ to Interact',
			ENDSCENARIO: 'Press ~INPUT_CONTEXT~ to stop action',
			TRANSITION: 'Press ~INPUT_CONTEXT~ to go through',
			LUCKYWHEEL: 'Press ~INPUT_CONTEXT~ to spin wheel',
			ROULETTE: 'Press ~INPUT_CONTEXT~ to play roulette',
			SEAT: 'Press ~INPUT_CONTEXT~ to sit down',
			USESODA: 'Press ~INPUT_CONTEXT~ to buy soda (25$)',
			SNACK: 'Press ~INPUT_CONTEXT~ to buy snack (50$)',
			USECOFFEE: 'Press ~INPUT_CONTEXT~ to buy coffee (75$)',
			SUNLOUNGE: 'Press ~INPUT_CONTEXT~ to sunbath',
			BODYBUILD: 'Press ~INPUT_CONTEXT~ to workout',
			USEATM: 'Press ~INPUT_CONTEXT~ to use ATM',
			FLEXMIRROR: 'Press ~INPUT_CONTEXT~ to poze',
			DRINKSTRIP: 'Press ~INPUT_CONTEXT~ to get drink (100$)',
			WATCHSTRIP: 'Press ~INPUT_CONTEXT~ to watch strip'
		},
		SAFEZONES: {
			GREEN: 'You have entered a ~g~safe zone',
			YELLOW: 'You have entered a ~y~high-risk zone',
			RED: 'You have entered a ~r~restricted zone',
			GREENNAME: 'Safe zone',
			YELLOWNAME: 'High-risk zone',
			REDNAME: 'Restricted zone'
		},
		MONEY: {
			PURCHASED: '~r~-{{amount}}$',
			CANTAFFORD: '~r~Insufficient money'
		},
		GENERIC: {
			ISSPINNING: 'Lucky wheel is already spinning, please wait.',
			ALREADYSPINNED: 'You already tried your luck today, please come back tomorrow.',
			SPINFINISHED: 'Lucky wheel yield: {{reward}}',
			EXHAUSTED: 'You are exhausted',
			EXHAUSTEDTEXT: 'and struggle to make any moves.',
			DEPRESSED: 'You are depressed',
			DEPRESSEDTEXT: 'and cannot perform that action.',
			LEVELUP: '~HUD_COLOUR_RADAR_DAMAGE~Level {{level}}',
			LEVELUPTEXT: 'New level achieved.',
			REFERAL: '~g~Referal system:',
			REFERALNEW: 'you have a new subscriber',
			WANTED: '~HUD_COLOUR_RADAR_DAMAGE~Offense',
			WANTEDTEXT: 'You have been marked as offender and must be punished for your crimes - please turn in to closest police department.'
		},
		CEF: {
			AUTH: {
				TABS: {
					LOGIN: 'Login',
					REGISTER: 'Register'
				},
				BUTTONS: {
					LOGIN: 'Play',
					REGISTER: 'Register'
				},
				FIELDS: {
					USERNAME: 'Login',
					EMAIL: 'Email',
					PASSWORD: 'Password',
					CONFIRM: 'Repeat password'
				}
			},
			EXIT: 'Exit',
			READMORE: 'Read more'
		},
		VEH_SHOP: {
			NAME: 'Vehicles list',
			VEHICLE: {
				CLASS: 'Class',
				PRICE: 'Price',
				FUEL: 'Fuel',
				INVENTORY: 'Inventory size',
				SEATS: 'Seats',
				PRIMARY_COLOR: 'Primary color',
				SECONDARY_COLOR: 'Secondary color',
				STATS: {
					SPEED: 'Speed',
					ACCELERATION: 'Acceleration',
					HANDLING: 'Handling',
					BRAKING: 'Braking'
				}
			},
			TEST_DRIVE: 'Test Drive'
		},
		WALLET: {
			CASH: 'Cash',
			BANK: 'Card'
		},
		CHAR: {
			NEW: 'Create character',
			GENDERSELECT: 'Choose gender',
			PERSONAL_DATA: 'Personal information',
			INPUT: {
				FIRSTNAME: 'Character name',
				LASTNAME: 'Character surname',
				AGE: 'Character age (18+)'
			},
			FATHER: 'Father',
			MOTHER: 'Mother',
			SIMILAR: 'Similarity',
			RANDOM: 'Randomize parameters',
			CREATE: 'Confirm and create',
			SKIN_COLOR: 'Skin color',
			COMPLEXION: 'Complexions',
			BLEMISHES: 'Blemishes',
			AGEING: 'Ageing',
			SUN_DAMAGE: 'Sun burns',
			MOLES: 'Moles and freckles',
			NOSE_WIDTH: 'Nose width',
			NOSE_HEIGHT: 'Nose height',
			NOSE_LENGTH: 'Nose length',
			NOSE_BRIDGE: 'Nose bridge',
			NOSE_TIP: 'Nose tip',
			NOSE_BRIDGE_SHIFT: 'Nose bridge shift',
			EYES_COLOR: 'Eyes color',
			BROW_HEIGHT: 'Eyebrows height',
			BROW_WIDTH: 'Eyebrows width',
			EYES: 'Eyes type',
			CHIN_LENGTH: 'Chin length',
			CHIN_POSITION: 'Chin position',
			CHIN_WIDTH: 'Chin width',
			CHIN_SHAPE: 'Форма подбородка',
			NECK_WIDTH: 'Neck width',
			CHEEK_BONE_HEIGHT: 'Cheek bone height',
			CHEEK_BONE_WIDTH: 'Cheek bone width',
			CHEEKS_WIDTH: 'Cheeks width',
			JAW_WIDTH: 'Jaw width',
			JAW_HEIGHT: 'Jaw height',
			LIPS: 'Lips type',
			HAIR_COLOR_PRIMARY: 'Primary hair color',
			HAIR_COLOR_SECONDARY: 'Secondary hair color',
			BEARD_COLOR: 'Beard color'
		},
		ERRORS: {
			// TODO: localize
			UNKNOWN: 'UNKNOWN ERROR',
			CREATE_CHARACTER: {
				TOO_MANY_CHARACTERS: 'Characters amount exceeded',
				ALREADY_EXISTS: 'This character already exists',
				FIRSTNAME_IS_REQUIRED: 'Firstname is required',
				LASTNAME_IS_REQUIRED: 'Lastname is required',
				AGE_IS_REQUIRED: 'Age is required',
				INVALID_AGE: 'Age must be between 18 and 75',
				PARAMS_IS_REQUIRED: 'Firstname, lastname and age are required'
			},
			LOAD_CHARACTER: {
				NOT_FOUND: 'Character was not found'
			},
			GET_CURRENT_ACCOUNT: {
				NOT_FOUND: 'Account was not found'
			},
			LOGIN: {
				WRONG_USERNAME_OR_PASSWORD: 'Wrong username or password',
				WRONG_RGSCID: 'Social Club do not match'
			},
			REGISTER: {
				NAME_IS_REQUIRED: 'Name is required',
				EMAIL_IS_REQUIRED: 'Email is required',
				PASSWORD_IS_REQUIRED: 'Password is required',
				CONFIRM_IS_REQUIRED: 'Please repeat password',
				INCORRECT_EMAIL: 'Email format is invalid',
				PASSWORDS_DO_NOT_MATCH: 'Passwords do not match',
				WRONG_PARAMS: 'Invalid parameters',
				ALREADY_EXISTS: 'Account already exists',
				TOO_SHORT_LOGIN: 'Login is too short (3 сhars or more)',
				TOO_SHORT_PASSWORD: 'Password is too short (6 chars or more)',
				LOGIN_ALREADY_EXISTS: 'Login already exists',
				EMAIL_ALREADY_EXISTS: 'Email already exists',
				SOCIALCLUB_ALREADY_EXISTS: 'Social Club already exists'
			},
			VEH_SHOP: {
				NOT_ENOUGH_MONEY: 'Not enough money'
			}
		},
		FATHERS: {
			Benjamin: 'Benjamin',
			Daniel: 'Daniel',
			Joshua: 'Joshua',
			Noah: 'Noah',
			Andrew: 'Andrew',
			Juan: 'Juan',
			Alex: 'Alex',
			Isaac: 'Isaac',
			Ivan: 'Ivan',
			Ethan: 'Ethan',
			Vincent: 'Vincent',
			Angel: 'Angel',
			Diego: 'Diego',
			Adrian: 'Adrian',
			Gabriel: 'Gabriel',
			Michael: 'Michael',
			Santiago: 'Santiago',
			Kevin: 'Kevin',
			Luis: 'Luis',
			Samuel: 'Samuel',
			Anthony: 'Anthony',
			Claude: 'Claude',
			Niko: 'Niko',
			John: 'John'
		},
		MOTHERS: {
			Hanna: 'Hanna',
			Audrey: 'Audrey',
			Jasmine: 'Jasmine',
			Giselle: 'Giselle',
			Amelia: 'Amelia',
			Isabel: 'Isabel',
			Zoya: 'Zoya',
			Ava: 'Ava',
			Camilla: 'Camilla',
			Violet: 'Violet',
			Sophia: 'Sophia',
			Evelina: 'Evelina',
			Nicole: 'Nicole',
			Ashley: 'Ashley',
			Gracie: 'Gracie',
			Brianna: 'Brianna',
			Natalie: 'Natalie',
			Olivia: 'Olivia',
			Elizabeth: 'Elizabeth',
			Charlotte: 'Charlotte',
			Emma: 'Emma',
			Misty: 'Misty'
		}
	}
};
