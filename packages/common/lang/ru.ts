export default {
	translation: {
		ACHIEVEMENTS: {
			COLLECTIBLES: '~HUD_COLOUR_RADAR_DAMAGE~Найдена поклажа',
			HOWMUCHCOUNT: '{{current}} | {{next}}'
		},
		GLOBAL: {
			CASH: 'Наличные',
			BANK: 'Счет',
			POINTS: 'NR Поинты'
		},
		CHAT: {
			SAY: '{{name}} сказал: {{message}}',
			BAN: '{{name}} был забанен на {{time}} дней. Причина: {{text}}',
			KICK: '{{name}} был кикинут по причине: {{text}}',
			WARN: '{{name}} получил варн по причине: {{text}}',
			WELCOME_MSG1: 'Добро пожаловать на сервер NRage!'
		},
		PLAYER: {
			BAN: 'Вы были забанены до: {{time}}. Причина: {{text}}',
			KICK: 'Вас изгнали из игры по причине: {{text}}',
			WARN: 'Вы получили предупреждение: {{text}}'
		},
		BLIPS: {
			GASSTATION: 'Заправка',
			SHOOTINGRANGE: 'Тир',
			VEHICLEIMPOUND: 'Штрафстоянка',
			SUPERMARKET: 'Магазин',
			AIRCARRIER: 'Авиакомпания',
			PLAYERHOUSE: 'Дом',
			TAXI: 'Такси',
			LSPD: 'LSPD',
			EMS: 'EMS',
			COLLECTOR: 'Инкассатор',
			BARBER: 'Парикмахерская',
			CARTUNING: 'Автомастерская',
			CLOTHINGSHOP: 'Магазин одежды',
			TATTOO: 'Тату салон',
			FIB: 'FIB',
			GHETTOSKULL: 'Гетто',
			MASKSSHOP1: 'Магазин масок',
			MASKSSHOP2: 'Магазин масок',
			MASKSSHOP3: 'Магазин масок',
			BANK: 'Банк',
			AMMUNATION: 'Оружейный магазин',
			AMMUNATIONPLUS: 'Тир (Оружейный Магазин)',
			HUNTING: 'Охота',
			CANNABIS: 'Канабис',
			GPS: 'Навигация',
			WEAZELNEWS: 'Агенство новостей',
			PRISON: 'Тюрьма',
			VEHICLESHOP: 'Автосалон',
			VEHICLESHOPELITE: 'Автосалон (Элитный)',
			BIKECLUB: 'Байкерский клуб',
			PETSSHOP: 'Зоомагазин',
			BOATSHOP: 'Магазин лодок',
			REALESTATE: 'Недвижимость',
			HOUSESMALL: 'Бюджетный дом',
			HOUSEMEDIUM: 'Дом среднего класса',
			HOUSEBIG: 'Элитный дом',
			CASINO1: 'Казино',
			CASINO2: 'Казино',
			DIVING: 'Дайвинг',
			MECHANICS: 'Мастерская',
			CITYHALL: 'Мэрия',
			ARMY: 'Армия',
			JOBTRUCKER: 'Дальнобойщики',
			JOBBUS: 'Водитель автобуса',
			PARKING: 'Парковка',
			YOURVEHICLE: 'Ваша персональная техника'
		},
		PEDS: {
			CASINOCASH: 'Касса',
			BARMEN: 'Бармен',
			ROULETTE: 'Рулетка',
			TREVOR: 'Тревор',
			DENISE: 'Дениз Клинтон',
			MICHAEL: 'Майкл',
			FRANKLIN: 'Франклин',
			LAMAR: 'Ламар',
			AGATHA: 'Агата Бэйкер',
			SECURITY: 'Охрана'
		},
		ACTIONS: {
			DEFAULT: 'Нажмите ~INPUT_CONTEXT~ для взаимодействия',
			ENDSCENARIO: 'Нажмите ~INPUT_CONTEXT~ чтобы прекратить действие',
			TRANSITION: 'Нажмите ~INPUT_CONTEXT~ чтобы пройти',
			LUCKYWHEEL: 'Нажмите ~INPUT_CONTEXT~ чтобы покрутить',
			ROULETTE: 'Нажмите ~INPUT_CONTEXT~ чтобы сыграть в рулетку',
			SEAT: 'Нажмите ~INPUT_CONTEXT~ чтобы сесть',
			USESODA: 'Нажмите ~INPUT_CONTEXT~ чтобы купить воду (25$)',
			SNACK: 'Нажмите ~INPUT_CONTEXT~ чтобы купить перекус (50$)',
			USECOFFEE: 'Нажмите ~INPUT_CONTEXT~ чтобы купить кофе (75$)',
			SUNLOUNGE: 'Нажмите ~INPUT_CONTEXT~ чтобы позагорать',
			BODYBUILD: 'Нажмите ~INPUT_CONTEXT~ чтобы качать пресс',
			USEATM: 'Нажмите ~INPUT_CONTEXT~ для взаимодействия с банкоматом',
			FLEXMIRROR: 'Нажмите ~INPUT_CONTEXT~ чтобы попозировать',
			DRINKSTRIP: 'Нажмите ~INPUT_CONTEXT~ чтобы выпить (100$)',
			WATCHSTRIP: 'Нажмите ~INPUT_CONTEXT~ чтобы смотреть стриптиз'

		},
		SAFEZONES: {
			GREEN: 'Вы вошли в ~g~безопасную зону',
			YELLOW: 'Вы вошли в зону ~y~повышенного риска',
			RED: 'Вы вошли в ~r~запретную зону',
			GREENNAME: 'Безопасная зона',
			YELLOWNAME: 'Зона повышенного риска',
			REDNAME: 'Запретная зона'
		},
		MONEY: {
			PURCHASED: '~r~-{{amount}}$',
			CANTAFFORD: '~r~Недостаточно средств'
		},
		GENERIC: {
			ISSPINNING: 'Колесо удачи уже крутится, подождите немного.',
			ALREADYSPINNED: 'Вы уже испытали свою удачу сегодня, приходите завтра.',
			SPINFINISHED: 'Колесо удачи принесло вам: {{reward}}',
			EXHAUSTED: 'Вы слишком истощены',
			EXHAUSTEDTEXT: 'и испытываете трудности в передвижении.',
			DEPRESSED: 'Вы в депрессии',
			DEPRESSEDTEXT: 'и не можете выполнять это действие.',
			LEVELUP: '~HUD_COLOUR_RADAR_DAMAGE~Уровень {{level}}',
			LEVELUPTEXT: 'Получен новый уровень',
			REFERAL: '~g~Реферальная система:',
			REFERALNEW: 'у вас новый подписчик',
			WANTED: '~HUD_COLOUR_RADAR_DAMAGE~Правонарушение',
			WANTEDTEXT: 'Вы были отмечены как правонарушитель и должны понести наказание - обратитесь в ближайший полицейский участок.'
		},
		VEH_SHOP: {
			NAME: 'Список техники',
			VEHICLE: {
				CLASS: 'Класс',
				PRICE: 'Стоимость',
				FUEL: 'Бак',
				INVENTORY: 'Багажник',
				SEATS: 'Мест',
				PRIMARY_COLOR: 'Основной цвет',
				SECONDARY_COLOR: 'Дополнительный цвет',
				STATS: {
					SPEED: 'Скорость',
					ACCELERATION: 'Ускорение',
					HANDLING: 'Управление',
					BRAKING: 'Торможение'
				}
			},
			TEST_DRIVE: 'Тест драйв'
		},
		WALLET: {
			CASH: 'Наличными',
			BANK: 'Картой'
		},
		CEF: {
			AUTH: {
				TABS: {
					LOGIN: 'Вход',
					REGISTER: 'Регистрация'
				},
				BUTTONS: {
					LOGIN: 'Играть',
					REGISTER: 'Зарегистрироваться'
				},
				FIELDS: {
					USERNAME: 'Логин',
					EMAIL: 'Email',
					PASSWORD: 'Пароль',
					CONFIRM: 'Подтвердите пароль'
				}
			},
			EXIT: 'Выйти',
			READMORE: 'Читать больше'
		},
		CHAR: {
			NEW: 'Создание персонажа',
			GENDERSELECT: 'Выберите пол',
			PERSONAL_DATA: 'Личная инфромация',
			INPUT: {
				FIRSTNAME: 'Имя персонажа',
				LASTNAME: 'Фамилия персонажа',
				AGE: 'Возраст персонажа (18+)'
			},
			FATHER: 'Отец',
			MOTHER: 'Мать',
			SIMILAR: 'Схожесть предков',
			RANDOM: 'Выбрать случайно',
			CREATE: 'Подтвердить и создать',
			SKIN_COLOR: 'Цвет кожи',
			COMPLEXION: 'Особенности',
			BLEMISHES: 'Недостатки',
			AGEING: 'Старение',
			SUN_DAMAGE: 'Ожоги',
			MOLES: 'Родинки и веснушки',
			NOSE_WIDTH: 'Ширина носа',
			NOSE_HEIGHT: 'Высота носа',
			NOSE_LENGTH: 'Длинна носа',
			NOSE_BRIDGE: 'Горбинка носа',
			NOSE_TIP: 'Форма кончика носа',
			NOSE_BRIDGE_SHIFT: 'Смещение носа',
			EYES_COLOR: 'Цвет глаз',
			BROW_HEIGHT: 'Высота бровей',
			BROW_WIDTH: 'Ширина бровей',
			EYES: 'Тип глаз',
			CHIN_LENGTH: 'Длинна подбородка',
			CHIN_POSITION: 'Положение подбородка',
			CHIN_WIDTH: 'Ширина подбородка',
			CHIN_SHAPE: 'Форма подбородка',
			NECK_WIDTH: 'Ширина шеи',
			CHEEK_BONE_HEIGHT: 'Высота скул',
			CHEEK_BONE_WIDTH: 'Ширина скул',
			CHEEKS_WIDTH: 'Ширина щек',
			JAW_WIDTH: 'Ширина челюсти',
			JAW_HEIGHT: 'Высота челюсти',
			LIPS: 'Тип губ',
			HAIR_COLOR_PRIMARY: 'Основной цвет волос',
			HAIR_COLOR_SECONDARY: 'Вторичный цвет волос',
			BEARD_COLOR: 'Цвет бороды'
		},
		ERRORS: {
			UNKNOWN: 'Неизвестная ошибка',
			CREATE_CHARACTER: {
				TOO_MANY_CHARACTERS: 'Слишком много персонажей',
				ALREADY_EXISTS: 'Имя уже занято',
				FIRSTNAME_IS_REQUIRED: 'Введите имя',
				LASTNAME_IS_REQUIRED: 'Введите фамилию',
				AGE_IS_REQUIRED: 'Укажите возраст',
				INVALID_AGE: 'Возраст должен быть от 18 до 75',
				PARAMS_IS_REQUIRED: 'Необходимо указать имя, фамилию и возраст'
			},
			LOAD_CHARACTER: {
				NOT_FOUND: 'Персонаж не найден'
			},
			GET_CURRENT_ACCOUNT: {
				NOT_FOUND: 'Аккаунт не найден'
			},
			LOGIN: {
				WRONG_USERNAME_OR_PASSWORD: 'Неправильный логин или пароль',
				// TODO: hz
				WRONG_RGSCID: 'Не подходит Social club'
			},
			REGISTER: {
				NAME_IS_REQUIRED: 'Введите логин',
				EMAIL_IS_REQUIRED: 'Введите почту',
				PASSWORD_IS_REQUIRED: 'Введите пароль',
				CONFIRM_IS_REQUIRED: 'Повторите пароль',
				INCORRECT_EMAIL: 'Введите корректную почту',
				PASSWORDS_DO_NOT_MATCH: 'Пароли не совпадают',
				WRONG_PARAMS: 'Некорректные параметры',
				LOGIN_ALREADY_EXISTS: 'Логин уже занят',
				EMAIL_ALREADY_EXISTS: 'Email уже занят',
				SOCIALCLUB_ALREADY_EXISTS: 'Social Club уже занят',
				TOO_SHORT_LOGIN: 'Слишком короткий логин (3 символа или более)',
				TOO_SHORT_PASSWORD: 'Слишком короткий пароль (6 символов или более)',
				ALREADY_EXISTS: 'Логин уже существует'
			},
			VEH_SHOP: {
				NOT_ENOUGH_MONEY: 'Не достаточно средств'
			}
		},
		FATHERS: {
			Benjamin: 'Бенджамин',
			Daniel: 'Дэниэл',
			Joshua: 'Джошуа',
			Noah: 'Ноа',
			Andrew: 'Эндрю',
			Juan: 'Хуан',
			Alex: 'Алекс',
			Isaac: 'Айзек',
			Ivan: 'Иван',
			Ethan: 'Итан',
			Vincent: 'Винсент',
			Angel: 'Анджел',
			Diego: 'Диего',
			Adrian: 'Адриан',
			Gabriel: 'Габриэль',
			Michael: 'Майкл',
			Santiago: 'Сантьяго',
			Kevin: 'Кевин',
			Luis: 'Льюис',
			Samuel: 'Сэмюэл',
			Anthony: 'Энтони',
			Claude: 'Клод',
			Niko: 'Никл',
			John: 'Джон'
		},
		MOTHERS: {
			Hanna: 'Ханна',
			Audrey: 'Одри',
			Jasmine: 'Жасмин',
			Giselle: 'Жизель',
			Amelia: 'Амелия',
			Isabel: 'Изабель',
			Zoya: 'Зоя',
			Ava: 'Ава',
			Camilla: 'Камилла',
			Violet: 'Вайолет',
			Sophia: 'София',
			Evelina: 'Эвелина',
			Nicole: 'Николь',
			Ashley: 'Эшли',
			Gracie: 'Грейси',
			Brianna: 'Брианна',
			Natalie: 'Натали',
			Olivia: 'Оливия',
			Elizabeth: 'Элзабет',
			Charlotte: 'Шарлотта',
			Emma: 'Эмма',
			Misty: 'Мисти'
		}
	}
};
