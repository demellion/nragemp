import React from 'react';
import { render } from 'react-dom';
import rpc from 'rage-rpc';
import { NREvents } from 'common/enums/nrEvents';
import { localize } from '../../common';
import en from '../../common/lang/en';
import ru from '../../common/lang/ru';
import { App } from './app';

(async () => {
	const lng = await rpc.callServer(NREvents.getServerLang);
	await localize.init({
		lng,
		resources: {
			en,
			ru
		}
	});

	const rootElement = document.getElementById('root');

	render(<App />, rootElement);

	rpc.triggerServer(NREvents.cefReady);
})();
