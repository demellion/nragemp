import { ActionType, BaseAction } from '../../actions';
import { Account } from '../../../../../common/types/account';
import { patchObject } from '../../../../../common/pipes/object';
import { Error } from '../../../../../common/types/error';

export interface AccountState {
	active: Account;
	error: Error<unknown>;
}

const initialState: AccountState = {
	active: null,
	error: null
};

export const account = (state: AccountState = initialState, action: BaseAction): AccountState => {
	switch (action.type) {
		case ActionType.ACCOUNT_SET_ACTIVE:
			return patchObject(state, {
				active: action.payload
			});
		case ActionType.ACCOUNT_SET_AUTH_ERROR:
			return patchObject(state, {
				error: action.payload
			});
		default:
			return state;
	}
};
