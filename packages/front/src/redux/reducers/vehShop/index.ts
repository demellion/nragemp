import { ActionType, BaseAction } from '../../actions';
import { Vehicle } from 'common/types/vehicle';
import { patchObject } from 'common/pipes/object';

export type VehShopState = {
	active: Vehicle;
	list: Vehicle[];
};

const initialState: VehShopState = {
	active: null,
	list: null
};

export const vehShop = (state: VehShopState = initialState, action: BaseAction): VehShopState => {
	switch (action.type) {
		case ActionType.VEH_SHOP_SET_ACTIVE:
			return patchObject(state, {
				active: action.payload
			});
		case ActionType.VEH_SHOP_SET_LIST:
			return patchObject(state, {
				list: action.payload
			});
		default:
			return state;
	}
};
