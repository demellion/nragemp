import { ActionType, BaseAction } from '../../actions';
import { DebugData } from 'common/types/debug';
import { patchObject } from 'common/pipes/object';

export type DebugState = {
	data: DebugData
};

const initialState: DebugState = {
	data: null
};

export const debug = (state: DebugState = initialState, action: BaseAction): DebugState => {
	switch (action.type) {
		case ActionType.SET_DEBUG_DATA:
			return patchObject(state, {
				data: action.payload
			});
		default:
			return state;
	}
};
