import { ActionType, BaseAction } from '../../actions';
import { localStore } from '../../../app/shared/pipes';
import { ChatMessage } from '../../../../../common/types/chat';
import { patchObject } from '../../../../../common/pipes/object';

export type ChatState = {
	active: boolean;
	show: boolean;
	labelPosition: number;
	historyPosition: number;
	historyMessages: string[];
	messages: ChatMessage[];
	value: string;
};

const initialState: ChatState = {
	active: false,
	show: false,
	labelPosition: -1,
	historyPosition: -1,
	historyMessages: localStore.get<string[]>('historyMessages') || [],
	messages: localStore.get<ChatMessage[]>('messages') || [],
	value: ''
};

export const chat = (state: ChatState = initialState, action: BaseAction): ChatState => {
	switch (action.type) {
		case ActionType.CHAT_ACTIVATE:
			return patchObject(state, {
				active: action.payload,
				historyPosition: -1
			});
		case ActionType.CHAT_SHOW:
			return patchObject(state, {
				show: action.payload
			});
		case ActionType.CHAT_LABEL_POSITION:
			return patchObject(state, {
				labelPosition: action.payload
			});
		case ActionType.CHAT_HISTORY_POSITION:
			return patchObject(state, {
				historyPosition: action.payload
			});
		case ActionType.CHAT_HISTORY_MESSAGES: {
			const storageMessages = localStore.get<string[]>('historyMessages') || [];

			storageMessages.push(action.payload);
			storageMessages.slice(-50);

			localStore.set<string[]>('historyMessages', storageMessages);

			return patchObject(state, {
				historyMessages: localStore.get('historyMessages')
			});
		}
		case ActionType.CHAT_MESSAGES: {
			const storageMessages = localStore.get<ChatMessage[]>('messages') || [];

			storageMessages.push(action.payload);
			storageMessages.slice(-50);

			localStore.set<ChatMessage[]>('messages', storageMessages);

			return patchObject(state, {
				messages: storageMessages
			});
		}
		case ActionType.CHAT_VALUE:
			return patchObject(state, {
				value: action.payload
			});
		case ActionType.CHAT_MESSAGES_CLEAR: {
			return patchObject(state, {
				messages: []
			});
		}
		default:
			return state;
	}
};
