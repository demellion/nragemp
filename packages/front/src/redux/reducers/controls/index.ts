import { BaseAction } from '../../actions';
import { FormAction } from 'redux-form';
import { combineReducers } from 'redux';
import { patchObject } from 'common/pipes/object';
import { charEditor, CharEditorState } from './charEditor';

export interface CommonState {
	formCustom: { [formName: string]: { isFirstErrorFocused: boolean } };
}

const initialState: CommonState = {
	formCustom: {}
};

const common = (state: CommonState = initialState, action: BaseAction | FormAction): CommonState => {
	switch (action.type) {
		case '@@redux-form/SET_SUBMIT_FAILED':
		case '@@redux-form/STOP_SUBMIT': {
			const formAction = action as FormAction;
			return patchObject(state, {
				formCustom: {
					...state.formCustom,
					[formAction.meta.form]: {
						isFirstErrorFocused: false
					}
				}
			});
		}
		default:
			return state;
	}
};

export interface ControlsState {
	common: CommonState;
	charEditor: CharEditorState;
}

export const controls = combineReducers<ControlsState>({
	common,
	charEditor
});
