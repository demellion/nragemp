import { ActionType, BaseAction } from '../../../actions';

export enum MainTab {
	appearance = 'appearance',
	hairs = 'hairs',
}

export enum AppearanceTab {
	skin = 'skin',
	nose = 'nose',
	eyes = 'eyes',
	chin = 'chin',
	jaw = 'jaw',
}

export enum HairsTab {
	hair = 'hair',
	beard = 'beard'
}

export interface CharEditorState {
	mainTab: MainTab;
	appearanceTab: AppearanceTab;
	hairTab: HairsTab;
}

const initialState: CharEditorState = {
	mainTab: MainTab.appearance,
	appearanceTab: AppearanceTab.skin,
	hairTab: HairsTab.hair
};

export const charEditor = (state: CharEditorState = initialState, action: BaseAction): CharEditorState => {
	switch (action.type) {
		case ActionType.CHAR_EDITOR_SET_MAIN_TAB: {
			return {
				...state,
				mainTab: action.payload
			};
		}
		case ActionType.CHAR_EDITOR_SET_APPEARANCE_TAB: {
			return {
				...state,
				appearanceTab: action.payload
			};
		}
		case ActionType.CHAR_EDITOR_SET_HAIR_TAB: {
			return {
				...state,
				hairTab: action.payload
			};
		}
		case ActionType.CHAR_EDITOR_CHANGE_GENDER:
		case ActionType.CHAR_EDITOR_CREATE: {
			return initialState;
		}
		default:
			return state;
	}
};
