import { ActionType, BaseAction } from '../../actions';
import { Character } from 'common/types/character';
import { Error } from 'common/types/error';
import { patchObject } from 'common/pipes/object';

export interface CharactersState {
	active: Character;
	list: Character[];
	error: Error<unknown>;
}

const initialState: CharactersState = {
	active: null,
	list: null,
	error: null
};

export const characters = (state: CharactersState = initialState, action: BaseAction): CharactersState => {
	switch (action.type) {
		case ActionType.CHARACTER_SET_ACTIVE:
			return patchObject(state, {
				active: action.payload
			});
		case ActionType.CHARACTER_SET_LIST:
			return patchObject(state, {
				list: action.payload
			});
		case ActionType.CHARACTER_SET_ERROR:
			return patchObject(state, {
				error: action.payload
			});
		case ActionType.CUSTOMIZATION_SET_GENDER: {
			if (!state.active || !state.active.customizationData) break;

			const newCustomizationData = {
				...state.active.customizationData,
				gender: action.payload
			};

			return patchObject(state, {
				active: {
					...state.active,
					customizationData: newCustomizationData
				}
			});
		}
		case ActionType.CUSTOMIZATION_SET_PARAM: {
			if (!state.active || !state.active.customizationData) break;

			const newCustomizationData = { ...state.active.customizationData };
			newCustomizationData[action.payload.param] = action.payload.value;

			return patchObject(state, {
				active: {
					...state.active,
					customizationData: newCustomizationData
				}
			});
		}
		default:
			return state;
	}
};
