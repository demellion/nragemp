import { History } from 'history';
import { CombinedState, combineReducers, Reducer } from 'redux';
import { FormStateMap, reducer as formReducer } from 'redux-form';
import { connectRouter, RouterState } from 'connected-react-router';
import { debug, DebugState } from './debug';
import { characters, CharactersState } from './characters';
import { account, AccountState } from './account';
import { hud, HudState } from './hud';
import { chat, ChatState } from './chat';
import { controls, ControlsState } from './controls';
import { vehShop, VehShopState } from './vehShop';

export interface StorageState {
	router: RouterState;
	form: FormStateMap;

	debug: DebugState;
	account: AccountState;
	characters: CharactersState;
	hud: HudState;
	chat: ChatState;
	controls: ControlsState;
	vehShop: VehShopState;
}

export const createRootReducer = (history: History): Reducer<CombinedState<StorageState>> => combineReducers<StorageState>({
	router: connectRouter(history),
	form: formReducer,

	debug,
	account,
	characters,
	hud,
	chat,
	controls,
	vehShop
});
