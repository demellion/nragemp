import { ActionType, BaseAction } from '../../actions';
import { HudInfo } from '../../../../../common/types/hud';
import { patchObject } from '../../../../../common/pipes/object';

export type HudState = HudInfo;

const initialState: HudState = {
	isPlayable: null,
	name: null,
	id: null,
	online: null,
	wallet: null,
	area: null,
	safeZoneLevel: null,
	safeZoneName: null,
	street: null,
	speaking: null,
	time: null,
	servername: null,
	currentActionName: null,
	isVehicle: null,
	vehicleSpeed: null,
	vehicleFuel: null,
	vehicleLocked: null,
	vehicleEngine: null
};

export const hud = (state: HudState = initialState, action: BaseAction): HudState => {
	switch (action.type) {
		case ActionType.SET_HUD_DATA:
			return patchObject(state, {
				...action.payload
			});
		default:
			return state;
	}
};
