import rpc from 'rage-rpc';
import { ActionType, BaseAction, BaseThunkAction } from '../index';
import { ChatMessage } from '../../../../../common/types/chat';

export const clearChat = (): BaseAction => ({
	type: ActionType.CHAT_MESSAGES_CLEAR
});

export const showChat = (toggle: boolean): BaseAction => ({
	type: ActionType.CHAT_SHOW,
	payload: toggle
});

export const pushMessage = (message: ChatMessage): BaseThunkAction => async dispatch => {
	dispatch({
		type: ActionType.CHAT_MESSAGES,
		payload: message
	});
};

export const activateChat = (toggle: boolean): BaseThunkAction => async (dispatch, getState) => {
	const active = getState().chat.active;

	dispatch({
		type: ActionType.CHAT_ACTIVATE,
		payload: toggle
	});

	if (!toggle && active) {
		await rpc.callClient('chat:close');
	}
};

export const addHistoryMessage = (message: string): BaseThunkAction => async dispatch => {
	await rpc.callClient('chat:send', message);

	dispatch({
		type: ActionType.CHAT_HISTORY_MESSAGES,
		payload: message
	});
};

export const setChatValue = (value: string): BaseAction => ({
	type: ActionType.CHAT_VALUE,
	payload: value
});

export const setChatLabelPosition = (position: number): BaseAction => ({
	type: ActionType.CHAT_LABEL_POSITION,
	payload: position
});

export const setChatHistoryPosition = (position: number): BaseAction => ({
	type: ActionType.CHAT_HISTORY_POSITION,
	payload: position
});
