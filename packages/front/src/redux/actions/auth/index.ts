import { ActionType, BaseAction, BaseThunkAction } from '../index';
import rpc from 'rage-rpc';
import { Routes } from 'common/constants/routes';
import { getCharactersList } from '../characters';
import { push } from 'connected-react-router';
import { getReactRoute } from '../../../app/shared/pipes';
import { GetCurrentAccountResult } from 'common/types/api/getCurrentAccount';
import { Account } from 'common/types/account';
import { RegisterResult } from 'common/types/api/registerAccount';
import { LoginResult } from 'common/types/api/loginAccount';
import { NREvents } from 'common/enums/nrEvents';
import { Error } from 'common/types/error';
import { LoginFormData } from '../../../app/content/auth/components/loginTab/view';
import { RegisterFormData } from '../../../app/content/auth/components/registerTab/view';

export interface RegisterData {
	username: string,
	password: string,
	email: string,
	dateOfBirth: Date,
}

export const setActiveAccount = (account: Account): BaseAction => ({
	type: ActionType.ACCOUNT_SET_ACTIVE,
	payload: account
});

export const setAuthError = (error: Error<unknown>): BaseAction => ({
	type: ActionType.ACCOUNT_SET_AUTH_ERROR,
	payload: error
});

export const loadAccount = (account: Account): BaseThunkAction => async dispatch => {
	if (!account) {
		return;
	}

	dispatch(setActiveAccount(account));

	dispatch(push(getReactRoute(Routes.empty)));
	await dispatch(getCharactersList());
	await rpc.callClient('selectCharacterCamera');
	dispatch(push(getReactRoute(Routes.characters)));
};

export const getCurrentAccount = (): BaseThunkAction => async dispatch => {
	const getResult: GetCurrentAccountResult = await rpc.callServer('getCurrentAccount');

	if (getResult.success === false) {
		return;
	}

	dispatch(setActiveAccount(getResult.result));
};

export const registerAccount = (registerData: RegisterFormData): BaseThunkAction => async (dispatch, getState) => {
	const registerSendData = {
		username: registerData.username,
		email: registerData.email,
		password: registerData.password
	};

	const registerResult: RegisterResult = await rpc.callServer(NREvents.registration, registerSendData);

	if (registerResult.success === false) {
		dispatch(setAuthError(registerResult.error));
		return Promise.reject(registerResult.error);
	}

	getState().account.error && dispatch(setAuthError(null));

	const account = registerResult.result;
	await dispatch(loadAccount(account));
};

export const logInAccount = (loginData: LoginFormData): BaseThunkAction => async (dispatch) => {
	const loginResult: LoginResult = await rpc.callServer(NREvents.login, loginData);

	if (loginResult.success === false) {
		dispatch(setAuthError(loginResult.error));
		return Promise.reject(loginResult.error);
	}

	const account = loginResult.result;
	await dispatch(loadAccount(account));
};
