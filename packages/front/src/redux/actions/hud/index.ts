import { NREvents } from 'common/enums/nrEvents';
import { Routes } from 'common/constants/routes';
import { getReactRoute, QueryStringParams } from '../../../app/shared/pipes';
import { ActionType, BaseAction, BaseThunkAction } from '../index';
import { push } from 'connected-react-router';
import { HudState } from '../../reducers/hud';
import rpc from 'rage-rpc';

export const setRoute = (route: Routes, params?: QueryStringParams, search: string = null): BaseThunkAction => async (dispatch) => {
	dispatch(push(getReactRoute(route, params, search)));
};

export const showCursor = (): BaseThunkAction => () => {
	rpc.callClient(NREvents.showCursor, null, { noRet: true });
};

export const hideCursor = (): BaseThunkAction => () => {
	rpc.callClient(NREvents.hideCursor, null, { noRet: true });
};

export const setHudData = (data: HudState): BaseAction => ({
	type: ActionType.SET_HUD_DATA,
	payload: data
});
