import { ActionType, BaseAction, BaseThunkAction } from '../index';
import { Routes } from 'common/constants/routes';
import rpc from 'rage-rpc';
import { push } from 'connected-react-router';
import { getReactRoute } from '../../../app/shared/pipes';
import { Error } from 'common/types/error';
import { LoadCharacterResult } from 'common/types/api/loadCharacter';
import { Character } from 'common/types/character';
import { GetCharactersListResult } from 'common/types/api/getCharactersList';
import { CreateCharacterResult } from 'common/types/api/createCharacter';
import { CharCreateData, CustomizationData, Gender } from 'common/types/character/customizationData';
import { ChangeGenderResult } from 'common/types/api/changeGender';
import { NREvents } from 'common/enums/nrEvents';
import { defaultCharacter } from '../../../../../server/src/constants/character';
import { getCurrentAccount, loadAccount } from '../auth';
import { CharEditorFormData } from '../../../app/content/charEditor/model';
import { charEditorInit } from '../../../app/shared/constants/charEditorInit';
import { splitCustomizationData } from 'common/helpers/character';

export const setCharactersList = (characters: Character[]): BaseThunkAction => async dispatch => {
	dispatch({
		type: ActionType.CHARACTER_SET_LIST,
		payload: [...characters]
	});
};

export const getCharactersList = (): BaseThunkAction => async (dispatch, getState) => {
	const getResult: GetCharactersListResult = await rpc.callServer(NREvents.getCharacters);

	if (getResult.success === false) {
		dispatch(setCharacterError(getResult.error));
		return;
	}

	getState().characters.error && dispatch(setCharacterError(null));

	const characters = getResult.result;

	if (!characters) {
		return;
	}

	dispatch(setCharactersList(characters));

	if (!getState().characters.active && characters.length) {
		await dispatch(setActiveCharacterByName(characters[0].name));
	}
};

const setActiveCharacter = (character: Character): BaseAction => ({
	type: ActionType.CHARACTER_SET_ACTIVE,
	payload: character
});

export const updateCharacter = (customizationData: CustomizationData): BaseThunkAction => async () => {
	await rpc.callClient(NREvents.updateCharacter, customizationData);
};

export const setActiveCharacterByName = (name: string): BaseThunkAction => async (dispatch, getState) => {
	const characters = getState().characters.list || [];
	const character = characters.find(x => x.name === name);

	await dispatch(updateCharacter(character.customizationData));

	if (!character) {
		return;
	}

	dispatch(setActiveCharacter(character));
};

export const loadCharacter = (name: string): BaseThunkAction => async (dispatch, getState) => {
	const loadResult: LoadCharacterResult = await rpc.callServer(NREvents.loadCharacter, name);

	if (loadResult.success === false) {
		dispatch(setCharacterError(loadResult.error));
		return;
	}

	getState().characters.error && dispatch(setCharacterError(null));
};

export const setCharacterError = (error: Error<unknown>): BaseAction => ({
	type: ActionType.CHARACTER_SET_ERROR,
	payload: error
});

export const getDefaultCharacter = (): BaseThunkAction => async (dispatch, getState) => {
	!getState().characters.active && dispatch(setActiveCharacter(defaultCharacter));
};

export const startCharacterCreation = (): BaseThunkAction => async dispatch => {
	await dispatch(push(Routes.empty));
	await rpc.callClient('startCharacterCreation');
	await dispatch(push(getReactRoute(Routes.createCharacter)));
};

export const setCustomizationGender = (gender: Gender): BaseThunkAction => async (dispatch, getState) => {
	const setResult: ChangeGenderResult = await rpc.callClient('onCustomizationChangeGender', gender);

	if (setResult.success === false) {
		dispatch(setCharacterError(setResult.error));
		return;
	}

	dispatch({
		type: ActionType.CUSTOMIZATION_SET_GENDER,
		payload: gender
	});

	getState().characters.error && dispatch(setCharacterError(null));
};

export const setCustomizationData = (param: string, value: number): BaseThunkAction => async dispatch => {
	dispatch({
		type: ActionType.CUSTOMIZATION_SET_PARAM,
		payload: { param, value }
	});
	await rpc.callClient('onCustomizationChange', { param, value });
};

export const exitCharEditor = (): BaseThunkAction => async (dispatch, getState) => {
	let account = getState().account.active;
	if (!account) await dispatch(getCurrentAccount());
	account = getState().account.active;
	dispatch(loadAccount(account));
	const character = getState().characters.active;
	dispatch(updateCharacter(character.customizationData));
};

export const updateCharInEditor = (values: Partial<CharEditorFormData>): BaseThunkAction => async () => {
	const charData: CharCreateData = {
		...charEditorInit,
		...values
	};

	const customizationData = splitCustomizationData(charData);

	await rpc.callClient(NREvents.updateCharacter, customizationData);
};

export const createCharacter = (formData: CharEditorFormData): BaseThunkAction => async (dispatch, getState) => {
	const { firstname, lastname, age } = formData;

	const createResult: CreateCharacterResult = await rpc.callServer(NREvents.createCharacter, { firstname, lastname, age, formData });

	if (createResult.success === false) {
		dispatch(setCharacterError(createResult.error));
		return;
	}

	await dispatch(getCharactersList());
	await dispatch(loadCharacter(`${firstname} ${lastname}`));

	getState().characters.error && dispatch(setCharacterError(null));
};
