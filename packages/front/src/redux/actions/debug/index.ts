import { ActionType, BaseAction } from '../index';

export const setDebugData = (data: { [key: string]: unknown }): BaseAction => ({
	type: ActionType.SET_DEBUG_DATA,
	payload: data
});
