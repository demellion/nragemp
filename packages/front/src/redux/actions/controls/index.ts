import { ActionType, BaseAction } from '../index';

export const setFormErrorFocused = (formName: string, isFocused: boolean): BaseAction => ({
	type: ActionType.CONTROLS_SET_FORM_ERROR_FOCUSED,
	payload: {
		formName,
		isFocused
	}
});
