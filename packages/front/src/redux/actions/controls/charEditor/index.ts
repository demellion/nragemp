import { AppearanceTab, HairsTab, MainTab } from '../../../reducers/controls/charEditor';
import { ActionType, BaseAction } from '../../index';

export const setCharEditorMainTab = (tab: MainTab): BaseAction => ({
	type: ActionType.CHAR_EDITOR_SET_MAIN_TAB,
	payload: tab
});

export const setCharEditorAppearanceTab = (tab: AppearanceTab): BaseAction => ({
	type: ActionType.CHAR_EDITOR_SET_APPEARANCE_TAB,
	payload: tab
});

export const setCharEditorHairTab = (tab: HairsTab): BaseAction => ({
	type: ActionType.CHAR_EDITOR_SET_HAIR_TAB,
	payload: tab
});
