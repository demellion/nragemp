import rpc from 'rage-rpc';
import { ActionType, BaseAction, BaseThunkAction } from '../index';
import { NREvents } from 'common/enums/nrEvents';
import { BuyVehicleResult } from 'common/types/api/buyVehicle';
import { SetActiveVehicleResult } from 'common/types/api/setActiveVehicle';
import { GetVehicleListResult } from 'common/types/api/getVehicleList';
import { Vehicle } from 'common/types/vehicle';

export const closeVehicleShop = (): BaseThunkAction => async dispatch => {
	await rpc.callClient(NREvents.closeDialog);
	await rpc.callServer(NREvents.resetVehicleShop);
	dispatch(setVehicleList(null));
	dispatch(setActiveVehicle(null));
};

export const setVehicleList = (list: Vehicle[]): BaseAction => ({
	type: ActionType.VEH_SHOP_SET_LIST,
	payload: list
});

export const setActiveVehicle = (vehicle: Vehicle): BaseAction => ({
	type: ActionType.VEH_SHOP_SET_ACTIVE,
	payload: vehicle
});

export const getVehicleList = (alias: string): BaseThunkAction => async dispatch => {
	const getListResult: GetVehicleListResult = await rpc.callServer(NREvents.getVehicleList, alias); // TODO: aliases

	if (getListResult.success === false) {
		return;
	}

	dispatch(setVehicleList(getListResult.result));
};

export const setActiveVehicleByName = (name: string): BaseThunkAction => async (dispatch, getState) => {
	if (!name) return;

	const setActiveResult: SetActiveVehicleResult = await rpc.callServer(NREvents.setActiveVehicleInVehShop, name);

	if (setActiveResult.success === false) {
		return;
	}

	const vehicle = getState().vehShop.list.find(x => x.name === name);

	if (!vehicle) return;

	dispatch(setActiveVehicle(vehicle));
};

export const changeVehicleColor = (primary: string, secondary: string): BaseThunkAction => async () => {
	if (!primary || !secondary) return;

	await rpc.callServer(NREvents.changeVehicleColorInVehShop, { primary, secondary });
};

export const buyVehicle = (name: string, moneySource: 'cash' | 'bank'): BaseThunkAction => async dispatch => {
	if (!name || !moneySource) return;

	const buyStatus: BuyVehicleResult = await rpc.callServer(NREvents.buyVehicleInVehShop, { name, moneySource });

	if (buyStatus.success === false) {
		return;
	}

	await dispatch(closeVehicleShop());
};

export const doTestDrive = (): BaseThunkAction => async () => {
	await rpc.callServer(NREvents.doTestDrive);
};
