import { StorageState } from '../../reducers';
import { CharEditorState } from '../../reducers/controls/charEditor';

export const charEditorControlsSelector = (state: StorageState): CharEditorState => state.controls.charEditor;
