import { StorageState } from '../../reducers';
import { DebugData } from 'common/types/debug';

export const debugDataSelector = (state: StorageState): DebugData => state.debug.data;
