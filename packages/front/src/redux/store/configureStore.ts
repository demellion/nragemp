import { routerMiddleware } from 'connected-react-router';
import { applyMiddleware, compose, createStore, Store } from 'redux';
import { createBrowserHistory } from 'history';
import thunk from 'redux-thunk';
import { createRootReducer, StorageState } from '../reducers';
import { createLogger } from 'redux-logger';
import { ActionType } from '../actions';

export const history = createBrowserHistory();
const router = routerMiddleware(history);

const logger = createLogger({
	predicate: (getState, action) => {
		switch (action.type) {
			case ActionType.SET_HUD_DATA:
			case ActionType.SET_DEBUG_DATA:
				return false;
			default:
				return true;
		}
	}
});

const middleware = [thunk, router, logger].filter(Boolean);

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancer = composeEnhancers(applyMiddleware(...middleware));

export const configureStore = (): Store => createStore<StorageState, any, any, any>(createRootReducer(history), enhancer);
