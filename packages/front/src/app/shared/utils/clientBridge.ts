import { store } from '../../index';
import { Routes } from 'common/constants/routes';
import { NREvents } from 'common/enums/nrEvents';
import { QueryStringParams } from '../pipes';
import { setRoute } from '../../../redux/actions/hud';
import rpc from 'rage-rpc';

interface setRouteProps {
	route: Routes;
	params?: QueryStringParams;
	search?: string;
}

rpc.register(NREvents.setRoute, (props: setRouteProps) => {
	const { route, params, search } = props;
	store.dispatch(setRoute(route, params, search));
});
