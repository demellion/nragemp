import React from 'react';

export interface GappedProps {
	gap: number;
	verticalAlign: 'top' | 'middle' | 'baseline' | 'bottom';
	vertical: boolean;
	wrap: boolean;
	children: React.ReactNode;
}

export class Gapped extends React.Component<GappedProps> {
	public static defaultProps = {
		gap: 8,
		wrap: false,
		vertical: false,
		verticalAlign: 'baseline'
	};

	public render(): React.ReactElement {
		if (this.props.vertical) {
			return this.renderVertical();
		}
		return this.renderHorizontal();
	}

	private renderVertical() {
		const subsequentItemStyle: React.CSSProperties = {
			paddingTop: this.props.gap
		};
		let isFirst = true;
		const children = React.Children.map(this.props.children, child => {
			if (!child) {
				return child;
			}
			const style = isFirst ? undefined : subsequentItemStyle;

			isFirst = false;

			return <div style={style}>{child}</div>;
		});

		return <div>{children}</div>;
	}

	private renderHorizontal() {
		const { gap, children, verticalAlign, wrap } = this.props;
		const itemStyle: React.CSSProperties = {
			display: 'inline-block',
			verticalAlign,
			...(wrap ? { marginLeft: gap, marginTop: gap } : {})
		};
		const rootStyle: React.CSSProperties = wrap ? { paddingTop: 1 } : {};
		const contStyle: React.CSSProperties = wrap ? { marginTop: -gap - 1, marginLeft: -gap } : { whiteSpace: 'nowrap' };

		return (
			<div style={rootStyle}>
				<div style={contStyle}>
					{React.Children.toArray(children).map((child, index) => {
						const marginLeft = index === 0 ? undefined : gap;
						return (
							<span key={index} style={{ marginLeft, ...itemStyle }}>
								{child}
							</span>
						);
					})}
				</div>
			</div>
		);
	}
}
