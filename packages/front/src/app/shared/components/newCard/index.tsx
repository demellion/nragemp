import React, { ReactNode } from 'react';
import './styles.less';

interface CardHeaderProps {
	children?: ReactNode;
}

const isHeader = (child: React.ReactNode): child is React.ReactElement<CardHeaderProps> => {
	return (
		React.isValidElement<CardHeaderProps>(child) && Object.prototype.hasOwnProperty.call(child.type, '__CARD_HEADER__')
	);
};

function CardHeader({ children }: CardHeaderProps): React.ReactElement {
	return (
		<div className='card-header'>
			{children}
		</div>
	);
}

interface CardFooterProps {
	children?: ReactNode;
}

const isFooter = (child: React.ReactNode): child is React.ReactElement<CardFooterProps> => {
	return (
		React.isValidElement<CardFooterProps>(child) && Object.prototype.hasOwnProperty.call(child.type, '__CARD_FOOTER__')
	);
};

function CardFooter({ children }: CardFooterProps): React.ReactElement {
	return (
		<div className='card-footer'>
			{children}
		</div>
	);
}

interface CardBlockProps {
	children?: ReactNode;
}

function CardBlock({ children }: CardBlockProps): React.ReactElement {
	return (
		<div className='card-block'>
			{children}
		</div>
	);
}

class CardBody extends React.Component {
	render(): React.ReactElement {
		return <div className='card-body'>
			<div className='card-body-container'>
				{this.props.children}
			</div>
		</div>;
	}
}

interface NewCardProps {
	width?: number | string;
}

export class NewCard extends React.Component<NewCardProps> {
	public static Header = CardHeader;
	public static Body = CardBody;
	public static Footer = CardFooter;
	public static Block = CardBlock;

	render(): React.ReactElement {
		let hasHeader = false;
		let hasFooter = false;

		React.Children.toArray(this.props.children).forEach(child => {
			if (isHeader(child)) {
				// eslint-disable-next-line @typescript-eslint/no-unused-vars
				hasHeader = true;
			}
			if (isFooter(child)) {
				// eslint-disable-next-line @typescript-eslint/no-unused-vars
				hasFooter = true;
			}
		});

		return <div className='card' style={{ width: this.props.width }}>
			{this.props.children}
		</div>;
	}
}
