import React, { ReactNode } from 'react';
import Slider, { SliderProps } from 'react-rangeslider';
import cn from 'classnames';
import './styles.less';

type RangeSliderProps = SliderProps & {
	label?: ReactNode;
	default255?: boolean;
	orientation: 'horizontal' | 'vertical'
};

export const RangeSlider = (props: RangeSliderProps): React.ReactElement => {
	const { label, tooltip, default255 } = props;
	let { value, max } = props;

	if (default255) {
		max = max + 1;
		value = value === 255 ? 0 : value + 1;
	}

	const handleChange = (newValue: number): void => {
		if (default255) {
			newValue = newValue === 0 ? 255 : newValue - 1;
		}
		props.onChange(Math.round(newValue * 100) / 100);
	};

	return <div className='rangeslider-container'>
		{(label || tooltip) && <div className={cn('rangeslider-tooltip', [props.orientation])}>
			<div className='label'>{label && label}</div>
			<div className='value'>{tooltip && <>{value}/{max}</>}</div>
		</div>}
		<Slider
			{...props}
			value={value}
			max={max}
			tooltip={false}
			onChange={handleChange}
		/>
	</div>;
};
