import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import './styles.less';
import { Button } from '../button/view';
import { localize } from 'common/index';
import cn from 'classnames';

interface ExitButtonProps {
	onClick: () => void;
	pos?: 'left' | 'right';
}

export const ExitButton = (props: ExitButtonProps): React.ReactElement => {
	const { pos = 'left', onClick } = props;
	return <div className={cn('exit-button', [pos])}>
		<Button color='default' size='large' flat onClick={onClick}>
			<FontAwesomeIcon icon={faSignOutAlt} size={'lg'}/> {`  ${localize.t('CEF.EXIT')} (ESC)`}
		</Button>
	</div>;
};
