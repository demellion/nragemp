import React from 'react';
import { BaseField } from '../baseField';
import { BaseFieldModel } from '../baseField/model';
import { Field } from 'redux-form';

type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
type GenericBaseFieldModel<T> = Omit<BaseFieldModel, 'name' | 'component'> & { name: keyof T, component?: undefined };

export const createFormField = <T extends object>(defaultProps: Partial<GenericBaseFieldModel<T>> = {}) =>
	(props: GenericBaseFieldModel<T>): React.ReactElement => {
		const fieldProps = {
			...defaultProps,
			...props,
			component: BaseField,
			name: props.name as string
		};

		return <Field {...fieldProps} />;
	};
