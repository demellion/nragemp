import React from 'react';
import cn from 'classnames';

import './styles.scss';

type cardSize = 'small' | 'medium' | 'big';
type linePos = 'left' | 'right' | 'both' | 'bottom' | 'top' | 'none';
type widthMod = 'full' | 'auto';

interface IStateProps {
	className?: string;
	size?: cardSize;
	linePos?: linePos;
	widthMod?: widthMod;
}

export const Card = (props: React.PropsWithChildren<IStateProps>): React.ReactElement => {
	const { size = 'big', linePos = 'left', className, widthMod } = props;
	return <div className={cn('card', [size, linePos, widthMod, className])}>
		<div className='card-content'>
			{props.children}
		</div>
	</div>;
};
