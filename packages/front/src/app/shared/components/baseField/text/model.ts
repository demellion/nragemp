import { WrappedFieldProps } from 'redux-form';
import { ChangeEvent, KeyboardEvent } from 'react';
import { InputProps } from 'antd/lib/input';

export interface CommonModel extends WrappedFieldProps, InputProps {
	onMyBlur?: (event: any, formName: string, fieldName: string, newValue?: any) => void;
	onFocus?: () => void;
	onChangeInput?: (event: ChangeEvent<HTMLInputElement>, value: string) => void;
	onKeyPress?: (event: KeyboardEvent) => void;
	onKeyDown?: (event: KeyboardEvent) => void;
}

export interface InputModel extends CommonModel {
	password?: boolean;
}

export type TextareaModel = CommonModel;

export type Model = InputModel & TextareaModel & {
	type: 'textarea' | 'text';
};
