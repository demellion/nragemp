import React from 'react';
import { Model } from './model';
import { InputField } from './views/input';

export class View extends React.Component<Model> {
	renderInput(): React.ReactNode {
		const {
			meta, input, type, onMyBlur, onFocus, onChangeInput, onKeyPress, onKeyDown, addonAfter, addonBefore,
			defaultValue, id, maxLength, prefix, size, suffix, value, onPressEnter, allowClear, password
		} = this.props;
		switch (type) {
			case 'textarea':
				return <>Textarea</>;
			default:
				return <InputField
					meta={meta}
					input={input}

					password={password}
					addonAfter={addonAfter}
					addonBefore={addonBefore}
					defaultValue={defaultValue}
					id={id}
					maxLength={maxLength}
					prefix={prefix}
					size={size}
					suffix={suffix}
					value={value}
					onPressEnter={onPressEnter}
					allowClear={allowClear}

					onMyBlur={onMyBlur}
					onFocus={onFocus}
					onChangeInput={onChangeInput}
					onKeyPress={onKeyPress}
					onKeyDown={onKeyDown}
				/>;
		}
	}

	render(): React.ReactElement {
		const { meta } = this.props;
		const showError = !!meta.error && meta.touched;
		const errorMessageRender = () => showError
			? <div className='error-message-label'>{meta.error.error || meta.error}</div>
			: null;

		return <>
			{this.renderInput()}
			{errorMessageRender()}
		</>;
	}
}
