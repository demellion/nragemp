import * as React from 'react';
import { InputModel } from '../model';
import { Input } from 'antd';

export class InputField extends React.Component<InputModel> {
	render(): React.ReactElement {
		const {
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
			meta, input, onMyBlur, onFocus, onChangeInput, onKeyPress, onKeyDown, addonAfter, addonBefore,
			defaultValue, id, maxLength, prefix, size, suffix, onPressEnter, allowClear, password
		} = this.props;

		return <Input
			{...input}

			autoFocus={meta.active}
			type={password && 'password'}
			addonAfter={addonAfter}
			addonBefore={addonBefore}
			defaultValue={defaultValue}
			id={id}
			maxLength={maxLength}
			prefix={prefix}
			size={size}
			suffix={suffix}
			onPressEnter={onPressEnter}
			allowClear={allowClear}

			onBlur={e => onMyBlur(e, meta.form, input.name, e.target.value)}
		/>;
	}
}
