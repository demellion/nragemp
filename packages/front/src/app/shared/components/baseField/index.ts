import { Actions, Model, OwnProps, StateProps } from './model';
import { StorageState } from '../../../../redux/reducers';
import { View } from './view';
import { WrappedFieldProps } from 'redux-form';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { setFormErrorFocused } from '../../../../redux/actions/controls';

const redux = connect<StateProps, Actions, OwnProps & WrappedFieldProps, StorageState>(
	(state, ownProps) => {
		const formCustom = state.controls.common.formCustom[ownProps.meta.form];
		return <StateProps>{
			...ownProps,
			isFirstErrorFocused: formCustom && formCustom.isFirstErrorFocused
		};
	},
	(dispatch: any, ownProps) => ({
		onChangeInput: (event: any, v: string) => {
			if (!!v && ownProps.pattern && ownProps.pattern.test && !ownProps.pattern.test(v)) {
				return;
			}
			if (ownProps.input && ownProps.input.onChange) {
				ownProps.input.onChange(event);
			}
		},
		onFirstErrorFocused: () => {
			dispatch(setFormErrorFocused(ownProps.meta.form, true));
		}
	})
);

export const BaseField = compose<Model, WrappedFieldProps>(
	redux
)(View);
