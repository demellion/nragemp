import React from 'react';
import cn from 'classnames';
import { Model, State } from './model';
import { Checkbox } from './checkbox';
import { TextField } from './text';

export class View extends React.Component<Model, State> {
	constructor(props: Model) {
		super(props);

		this.state = {
			focused: false
		};
	}

	blurHandler(event: FocusEvent, formName: string, fieldName: string, newValue?: unknown): void {
		const { onMyBlur, meta, input } = this.props;

		this.setState({ focused: false });

		onMyBlur && onMyBlur(event, formName || meta.form, fieldName || input.name, newValue);
	}

	focusHandler(): void {
		this.setState({ focused: true });
	}

	renderField(): React.ReactNode {
		const {
			type, meta, input, label, disabled, addonAfter, addonBefore, password,
			defaultValue, id, maxLength, prefix, size, suffix, value, onPressEnter, allowClear
		} = this.props;
		const error: boolean = !!this.props.error || (meta.touched && !!meta.error);

		switch (type) {
			case 'checkbox':
				return <Checkbox
					{...input}
					checked={input.checked}
					disabled={disabled}
					error={error}
					label={label}
					meta={meta}
					onMyBlur={this.blurHandler.bind(this)}
				/>;
			default:
				return <TextField
					type={type}
					meta={meta}
					input={input}

					password={password}
					addonAfter={addonAfter}
					addonBefore={addonBefore}
					defaultValue={defaultValue}
					id={id}
					maxLength={maxLength}
					prefix={prefix}
					size={size}
					suffix={suffix}
					value={value}
					onPressEnter={onPressEnter}
					allowClear={allowClear}

					onFocus={this.props.onFocus}
					onChangeInput={this.props.onChangeInput}
					onKeyPress={this.props.onKeyPress}
					onKeyDown={this.props.onKeyDown}
					onMyBlur={this.blurHandler.bind(this)}
				/>;
		}
	}

	render(): React.ReactElement {
		const { label, type, meta } = this.props;
		const { focused } = this.state;

		const error: boolean = !!this.props.error || (meta.touched && !!meta.error);

		return <span className={cn('base-field', { error, focused })}>
			{
				label && type !== 'checkbox' && <div>
					{label}
				</div>
			}
			{this.renderField()}
		</span>;
	}
}
