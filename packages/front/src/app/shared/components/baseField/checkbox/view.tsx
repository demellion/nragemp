import React from 'react';
import { Model } from './model';
import { Checkbox } from 'antd';

export class View extends React.Component<Model> {
	render(): React.ReactElement {
		const { label, checked, disabled, onMyBlur, meta, name } = this.props;
		return <Checkbox
			checked={checked}
			disabled={disabled}
			onChange={(e => onMyBlur(e, meta.form, name, e.target.checked))}
		>
			{label}
		</Checkbox>;
	}
}
