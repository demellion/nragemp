import { Actions, InputModel, Model } from './model';
import { StorageState } from '../../../../../redux/reducers';
import { View } from './view';
import { compose } from 'recompose';
import { connect } from 'react-redux';

const redux = connect<InputModel, Actions, InputModel, StorageState>(
	(state, ownProps) => ({
		...ownProps
	}),
	(dispatch, ownProps) => ({
		onChangeInternal: (event: React.ChangeEvent<HTMLInputElement>, v: boolean) => {
			ownProps.onMyBlur && ownProps.onMyBlur(undefined, ownProps.meta.form, ownProps.name, v);
			ownProps.onChange && ownProps.onChange(event, v, !v, ownProps.name);
		}
	})
);

export const Checkbox = compose<Model, InputModel>(
	redux
)(View);
