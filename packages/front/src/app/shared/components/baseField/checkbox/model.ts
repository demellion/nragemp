import { CommonFieldProps, WrappedFieldMetaProps } from 'redux-form';

export interface InputModel extends CommonFieldProps {
	checked: boolean;
	disabled?: boolean;
	error?: boolean;
	label?: string;
	meta: WrappedFieldMetaProps;
	onMyBlur: (event: any, formName: string, fieldName: string, newValue?: unknown) => void;
}

export interface Actions {
	onChangeInternal: (event: any, v: boolean) => void;
}

export type Model = InputModel & Actions;
