import { BaseFieldProps, WrappedFieldProps } from 'redux-form';
import { ChangeEvent, KeyboardEvent, KeyboardEventHandler, ReactNode } from 'react';

export interface OwnProps {
	label?: string;
	type: 'text' | 'checkbox';
	pattern?: RegExp;
	onMyBlur?: (event: any, formName: string, fieldName: string, newValue?: any) => void;
	onFocus?: () => void;
	onChangeInput?: (event: ChangeEvent<HTMLInputElement>, value: string) => void;
	onKeyPress?: (event: KeyboardEvent) => void;
	onKeyDown?: (event: KeyboardEvent) => void;
	error?: string | boolean;
	disabled?: boolean;
	bordered?: boolean;

	// Input
	password?: boolean;
	addonAfter?: string | ReactNode;
	addonBefore?: string | ReactNode;
	defaultValue?: string;
	id?: string;
	maxLength?: number;
	prefix?: string | ReactNode;
	size?: 'large' | 'middle' | 'small';
	suffix?: string | ReactNode;
	value?: string;
	onPressEnter?: KeyboardEventHandler<HTMLInputElement>;
	allowClear?: boolean;
}

export interface StateProps extends OwnProps {
	isFirstErrorFocused?: boolean;
}

export interface Actions {
	onChangeInput: (event: any, v: string) => void;
	onFirstErrorFocused: () => void;
}

export interface State {
	focused: boolean;
}

export type BaseFieldModel = OwnProps & BaseFieldProps;

export type Model = StateProps & Actions & WrappedFieldProps;
