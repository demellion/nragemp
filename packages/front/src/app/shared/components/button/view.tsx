import React, { ReactNode } from 'react';
import './styles.less';
import cn from 'classnames';
import { tabListener } from '../../../lib/events/tabListener';

export type ButtonType = 'button' | 'submit' | 'reset';
export type ButtonColor = 'default' | 'primary';

interface ButtonProps {
	autoFocus?: boolean;
	disabled?: boolean;
	width?: number | string;
	type?: ButtonType;
	color?: ButtonColor;
	flat?: boolean;
	children?: ReactNode;
	size?: 'small' | 'medium' | 'large';
	onBlur?: React.FocusEventHandler<HTMLButtonElement>;
	onClick?: React.MouseEventHandler<HTMLButtonElement>;
	onFocus?: React.FocusEventHandler<HTMLButtonElement>;
	onKeyDown?: React.KeyboardEventHandler<HTMLButtonElement>;
	onMouseEnter?: React.MouseEventHandler<HTMLButtonElement>;
	onMouseLeave?: React.MouseEventHandler<HTMLButtonElement>;
	onMouseOver?: React.MouseEventHandler<HTMLButtonElement>;

	/** @ignore */
	disableFocus?: boolean;
}

export class Button extends React.Component<ButtonProps> {
	private node: HTMLButtonElement | null = null;

	public componentDidMount(): void {
		if (this.props.autoFocus) {
			tabListener.isTabPressed = true;
			this.focus();
		}
	}

	public focus(): void {
		this.node?.focus();
	}

	public blur(): void {
		this.node?.blur();
	}

	render(): React.ReactElement {
		const rootProps = {
			type: this.props.type,
			className: cn(
				'button',
				[this.props.color || 'default'],
				[this.props.size || 'small'],
				[this.props.flat],
				{
					disabled: this.props.disabled
				}),

			disabled: this.props.disabled,
			onClick: this.props.onClick,
			onFocus: this.handleFocus,
			onBlur: this.handleBlur,
			onKeyDown: this.props.onKeyDown,
			onMouseEnter: this.props.onMouseEnter,
			onMouseLeave: this.props.onMouseLeave,
			onMouseOver: this.props.onMouseOver
		};

		const wrapProps = {
			style: {
				width: this.props.width
			}
		};

		return <span {...wrapProps}>
			<button ref={this._ref} {...rootProps}>
				{this.props.children}
			</button>
		</span>;
	}

	private handleFocus = (e: React.FocusEvent<HTMLButtonElement>) => {
		if (!this.props.disabled && !this.props.disableFocus) {
			// focus event fires before keyDown eventlistener
			// so we should check tabPressed in async way
			process.nextTick(() => {
				if (tabListener.isTabPressed) {
					this.setState({ focusedByTab: true });
				}
			});
			this.props.onFocus?.(e);
		}
	};

	private handleBlur = (e: React.FocusEvent<HTMLButtonElement>) => {
		this.setState({ focusedByTab: false });
		if (!this.props.disabled && !this.props.disableFocus) {
			this.props.onBlur?.(e);
		}
	};

	private _ref = (node: HTMLButtonElement | null) => {
		this.node = node;
	};
}
