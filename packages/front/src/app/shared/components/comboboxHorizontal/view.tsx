import React from 'react';
import './styles.less';

interface ComboboxHorizontalProps {
	items: string[];
	value?: number;
	overlap?: boolean;
	onChange: (value: number) => void;
}

export const ComboboxHorizontal = (props: ComboboxHorizontalProps): React.ReactElement => {
	const { value, items, onChange } = props;

	const handleChange = (mod: number): void => {
		let newValue = value + mod;
		if (newValue > items.length - 1) newValue = 0;
		else if (newValue < 0) newValue = items.length - 1;
		onChange(newValue);
	};

	return <div className='combobox-horizontal'>
		<div className='combobox-button combobox-left-button' onClick={() => handleChange(-1)}>
			<svg width="6" height="11" viewBox="0 0 6 11" fill="none">
				<path d="M5.5 1L1 5.5L5.5 10"/>
			</svg>
		</div>
		<div className='combobox-body'>
			{items[value]}
		</div>
		<div className='combobox-button combobox-right-button' onClick={() => handleChange(+1)}>
			<svg width="6" height="11" viewBox="0 0 6 11" fill="none">
				<path d="M0.5 1L5 5.5L0.5 10"/>
			</svg>
		</div>
	</div>;
};
