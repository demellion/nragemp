import React from 'react';
import cn from 'classnames';
import './styles.less';

interface IconSvgProps {
	name: string;
	size?: number;
	width?: number;
	height?: number;
	className?: string;
}

export const IconSvg = (props: IconSvgProps): React.ReactElement => {
	const { name, width, height, size, className } = props;
	const src = require(`../../../../assets/icons/ic-${name}.svg`);
	return <img
		src={src}
		className={cn('svg-icon', [className])}
		style={{
			width: width || size,
			height: height || size
		}}
		alt={name}
	/>;
};
