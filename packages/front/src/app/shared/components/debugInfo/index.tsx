import React from 'react';

import './styles.scss';
import { useSelector } from 'react-redux';
import { debugDataSelector } from '../../../../redux/selectors/debug';
import { DebugData } from '../../../../../../common/types/debug';

export const DebugInfoComponent = (): React.ReactElement => {
	const debugData = useSelector(debugDataSelector);

	if (!debugData) return null;

	const entries: [string, Partial<DebugData>][] = Object.entries(debugData);

	return <div className='debug-info-component'>
		{entries.map((item, index) => <div key={index}>
			{item[0]}: {typeof item[1] === 'string' ? item[1] : JSON.stringify(item[1])}
		</div>)}
	</div>;
};
