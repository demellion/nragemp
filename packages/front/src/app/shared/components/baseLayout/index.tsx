import React, { ReactElement } from 'react';
import { renderRoutes, RouteConfigComponentProps } from 'react-router-config';
import './styles.scss';

export const BaseLayout = (props: RouteConfigComponentProps<{}>): ReactElement => {
	const route = props.route || {};

	return <div className='baseLayout'>
		{renderRoutes(route.routes)}
	</div>;
};