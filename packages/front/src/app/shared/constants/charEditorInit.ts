import { CharCreateData } from 'common/types/character/customizationData';
import { defaultCustomizationData, faceFeatures, headOverlays } from 'common/constants/customizationData';

export const charEditorInit: CharCreateData = {
	gender: defaultCustomizationData.gender,
	father: defaultCustomizationData.father,
	mother: defaultCustomizationData.mother,
	similar: defaultCustomizationData.similar,
	hair: defaultCustomizationData.hair,
	hairPrimaryColor: defaultCustomizationData.hairPrimaryColor,
	hairSecondaryColor: defaultCustomizationData.hairSecondaryColor,
	facialHairColor: defaultCustomizationData.facialHairColor,
	eyeColor: defaultCustomizationData.eyeColor,
	...faceFeatures,
	...headOverlays
};
