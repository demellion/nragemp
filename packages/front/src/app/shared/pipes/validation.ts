import { getDeepValue, setDeepValue } from '../../../../../common/pipes/deepValue';

export interface ValidationItem {
	names: string;
	error: string;
	type: 'required' | 'pattern';
	pattern?: RegExp;
}

export class FormValidationError {
	public readonly error: string;
	public readonly isFirstField: boolean;

	constructor(error: string, isFirstField: boolean) {
		this.error = error;
		this.isFirstField = isFirstField;
	}
}

interface FieldWValue {
	field: string;
	value: any;
}

const arrayWildcard = '[]';
const getValuesToValidate = (fieldPrefix: string, field: string, data: any): FieldWValue[] => {
	const arrayWildcardIndex = field.indexOf(arrayWildcard);

	if (arrayWildcardIndex === -1) {
		return [{
			field: fieldPrefix + field,
			value: getDeepValue(data, field)
		}];
	}

	const leftSide = field.substring(0, arrayWildcardIndex);
	const unnormalizedRightSide = field.substring(arrayWildcardIndex + arrayWildcard.length);
	const rightSide = unnormalizedRightSide[0] === '.'
		? unnormalizedRightSide.substring(1)
		: unnormalizedRightSide;
	const array = getDeepValue(data, leftSide) as Array<any>;

	if (!array || !array.length) {
		return [];
	}

	if (rightSide) {
		return array
			.map((value: any, index: number) => getValuesToValidate(
				fieldPrefix + leftSide + `.${index}.`,
				rightSide,
				value))
			.reduce((result: any[], current: any) => current instanceof Array
				? result.concat(current)
				: result.concat([current]), []);
	}

	return array.map((value: any, index: number) => ({
		field: fieldPrefix + leftSide + `.${index}`,
		value
	}));
};
const validateFieldFor = (validation: ValidationItem) => (fieldWValue: FieldWValue): boolean => {
	switch (validation.type) {
		case 'required':
			return !!fieldWValue.value && fieldWValue.value.toString().replace(/(\r\n\t|\n|\r\t|\s)/gm, '').length > 0;
		case 'pattern':
			return fieldWValue.value === null || fieldWValue.value === undefined || fieldWValue.value === '' ||
				!validation.pattern || validation.pattern.test(fieldWValue.value);
		default:
			return true;
	}
};

export const validationPipe = (formData: unknown, fieldsWithValidation: ValidationItem[]) => {
	const clonedFormData = (formData) ? JSON.parse(JSON.stringify(formData)) : {};
	if (!fieldsWithValidation || !fieldsWithValidation.length) {
		return null;
	}

	const result: any = {};
	// let firstErrorField: string = null;
	fieldsWithValidation.forEach((validationField) => {
		const fieldArray: string[] = validationField.names.split(',');
		const valuesArrays = fieldArray.map(field => getValuesToValidate('', field, clonedFormData));
		const validation = validateFieldFor(validationField);
		const validationResults = valuesArrays.map(valuesToValidate =>
			valuesToValidate.filter(fieldWValue => !validation(fieldWValue)));
		const finded = validationResults.some(validationResult => !validationResult.length);

		if (!finded) {
			validationResults.forEach(validationResult => validationResult.forEach((fieldWValue) => {
				setDeepValue(result,
					fieldWValue.field,
					validationField.error
					// new FormValidationError(validationField.error, !firstErrorField)
				);
				// if (!firstErrorField) {
				// 	firstErrorField = fieldWValue.field;
				// }
			}));
		}
	});

	// if (!firstErrorField) {
	// 	return null;
	// }

	return result;
};
