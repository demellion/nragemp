class LocalStorage {
	readonly _namespace: string;
	readonly _store: Storage;
	readonly _sep: string;

	constructor(namespace: string) {
		this._namespace = namespace;
		this._store = window.localStorage;
		this._sep = '\x00';
	}

	static clear() {
		window.localStorage.clear();
	}

	get<T = unknown>(key: string): T {
		const k = [this._namespace, key].join(this._sep);
		if (!this._store[k]) return this.notFound();

		try {
			return JSON.parse(this._store[k]);
		} catch (err) {
			console.error(err);
			return undefined;
		}
	}

	set<T = unknown>(key: string, value: T): T {
		if (typeof value === 'undefined') {
			console.error(new Error(`Invalid parameters to set, ('${key}', undefined)`));
			return undefined;
		}

		try {
			const k = [this._namespace, key].join(this._sep);
			this._store[k] = JSON.stringify(value);
			return this._store[k];
		} catch (err) {
			console.error(err);
			return undefined;
		}
	}

	has(key: string): boolean {
		const k = [this._namespace, key].join(this._sep);
		return this._store[k];
	}

	delete(key: string): boolean {
		if (key) {
			const k = [this._namespace, key].join(this._sep);
			if (!this._store[k]) {
				this.notFound();
				return false;
			}

			delete this._store[k];
			return true;
		}

		Object.keys(window.localStorage).forEach(k => {
			const ns = k.split(this._sep)[0];
			if (ns === this._namespace) {
				delete this._store[k];
			}
		});
	}

	search<T = { [key: string]: any }>(pattern: RegExp): T {
		if (!pattern) {
			throw new Error('A pattern is required');
		}

		const matchKeys = (key: string) => {
			const [, _key] = key.split(this._sep);

			if (!_key) return;
			if (!pattern.test(_key)) return;

			return key;
		};

		const makePairs = (key: string) => ({
			key: key.split(this._sep)[1],
			value: this._store[key]
		});

		return Object.keys(this._store).filter(matchKeys).map(makePairs) as any as T;
	}

	private notFound(): undefined {
		return undefined;
	}
}

export const localStore = new LocalStorage('localStore');
