import * as pathToRegexp from 'path-to-regexp';
import { Routes } from 'common/constants/routes';

export interface QueryStringParams {
	date?: string;
	test?: string;
	alias?: string;

	[key: string]: any;
}

export type StringifedQueryStringParams = {
	[K in keyof QueryStringParams]: string;
};

export const getReactRoute = (baseUrl: Routes | string, params?: QueryStringParams, search: string = null): string => {
	let result: string = baseUrl;
	if (!!params) {
		const toPath = pathToRegexp.compile(baseUrl);

		result = toPath(params);
	}
	if (search) {
		result += search;
	}
	return result;
};
