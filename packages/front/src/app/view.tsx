import React, { ReactElement } from 'react';
import { Provider } from 'react-redux';
import { store } from './index';
import { ConnectedRouter } from 'connected-react-router';
import { history } from '../redux/store/configureStore';
import { Content } from './routing';
// import 'antd/lib/style/themes/default.less';
// import 'antd/dist/antd.less';
import './styles.less';

export const View = (): ReactElement => {
	return (
		<Provider store={store as any}>
			<ConnectedRouter history={history}>
				<div>
					<span className='iframe'><iframe name="global_frame">frame</iframe></span>
					{Content}
				</div>
			</ConnectedRouter>
		</Provider>
	);
};
