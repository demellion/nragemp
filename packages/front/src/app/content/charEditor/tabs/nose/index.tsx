import React from 'react';
import { NewCard } from '../../../../shared/components/newCard';
import { localize } from 'common/index';
import { RangeSlider } from '../../../../shared/components/rangeSlider/view';
import { Field } from 'redux-form';

const renderField = ({ input, label, min, max, step }: any) => {
	const { value, onChange } = input;
	return <RangeSlider
		min={min}
		max={max}
		value={+value}
		step={step}
		orientation='horizontal'
		label={label}
		tooltip
		onChange={onChange}
	/>;
};

export const NoseTab = (): React.ReactElement => {
	return <>
		<NewCard.Block>
			<Field
				name='noseWidth'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.NOSE_WIDTH')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='noseHeight'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.NOSE_HEIGHT')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='noseLength'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.NOSE_LENGTH')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='noseBridge'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.NOSE_BRIDGE')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='noseTip'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.NOSE_TIP')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='noseBridgeShift'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.NOSE_BRIDGE_SHIFT')}
			/>
		</NewCard.Block>
	</>;
};
