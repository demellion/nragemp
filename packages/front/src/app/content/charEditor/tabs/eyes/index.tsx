import React from 'react';
import { NewCard } from '../../../../shared/components/newCard';
import { localize } from 'common/index';
import { RangeSlider } from '../../../../shared/components/rangeSlider/view';
import { change, Field } from 'redux-form';
import { Forms } from '../../../../shared/constants/forms';
import { useDispatch } from 'react-redux';

// REAL ID COLOR DOES NOT MATCH CODE
// TODO: get real color codes
const eyesColor = [
	{ id: 0, code: '#BCA6A0' },
	{ id: 1, code: '#B0BBA0' },
	{ id: 2, code: '#A7935A' },
	{ id: 3, code: '#716A95' },
	{ id: 4, code: '#75A8A6' },
	{ id: 5, code: '#5A6846' },
	{ id: 6, code: '#B8C0CB' },
	{ id: 7, code: '#6A7B9F' },
	{ id: 8, code: '#381610' },
	{ id: 9, code: '#060403' },
	{ id: 10, code: '#8FB0AF' },
	{ id: 11, code: '#777B6B' },
	{ id: 12, code: '#798AA7' },
	{ id: 13, code: '#4D5A7A' },
	{ id: 14, code: '#675A50' },
	{ id: 15, code: '#777985' },
	{ id: 16, code: '#5C2A21' },
	{ id: 17, code: '#6F5E5B' },
	{ id: 18, code: '#6F5E5B' }
];

const renderField = ({ input, label, min, max, step }: any) => {
	const { value, onChange } = input;
	return <RangeSlider
		min={min}
		max={max}
		value={+value}
		step={step}
		orientation='horizontal'
		label={label}
		tooltip
		onChange={onChange}
	/>;
};

export const EyesTab = (): React.ReactElement => {
	const dispatch = useDispatch();

	const onChangeColorHandler = (value: number): void => {
		dispatch(change(Forms.charEditor, 'eyeColor', value));
	};
	return <>
		<NewCard.Block>
			<div className='char-editor-colors-container'>
				<div className='char-editor-colors-label'>{localize.t('CHAR.EYES_COLOR')}</div>
				<div className='char-editor-colors'>
					{eyesColor.map((color, index) => (
						<div
							key={index}
							className='char-editor-color'
							style={{ backgroundColor: color.code }}
							onClick={() => onChangeColorHandler(color.id)}
						/>
					))}
				</div>
			</div>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='browHeight'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.BROW_HEIGHT')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='browWidth'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.BROW_WIDTH')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='eyes'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.EYES')}
			/>
		</NewCard.Block>
	</>;
};
