import React from 'react';
import { NewCard } from '../../../../shared/components/newCard';
import { localize } from 'common/index';
import { change } from 'redux-form';
import { Forms } from '../../../../shared/constants/forms';
import { useDispatch } from 'react-redux';
import { hairColors } from '../hair';

export const BeardTab = (): React.ReactElement => {
	const dispatch = useDispatch();

	const onBeardColorPrimaryChangeHandler = (value: number): void => {
		dispatch(change(Forms.charEditor, 'facialHairColor', value));
	};

	const onBeardChangeHandler = (value: number): void => {
		dispatch(change(Forms.charEditor, 'facialHair', value));
	};

	const beards = Array.from(Array(29).keys());
	beards.unshift(255);

	return <>
		<NewCard.Block>
			<div className='char-editor-colors-container'>
				<div className='char-editor-colors-label'>{localize.t('CHAR.BEARD_COLOR')}</div>
				<div className='char-editor-colors'>
					{hairColors.map((color, index) => (
						<div
							key={index}
							className='char-editor-color'
							style={{ backgroundColor: color.code }}
							onClick={() => onBeardColorPrimaryChangeHandler(color.id)}
						/>
					))}
				</div>
			</div>
		</NewCard.Block>
		<NewCard.Block>
			<div className='char-editor-scroll-container'>
				{beards.map((style, index) => <div key={index} onClick={() => onBeardChangeHandler(style)} />)}
			</div>
		</NewCard.Block>
	</>;
};
