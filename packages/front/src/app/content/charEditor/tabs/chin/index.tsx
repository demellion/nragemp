import React from 'react';
import { NewCard } from '../../../../shared/components/newCard';
import { localize } from 'common/index';
import { RangeSlider } from '../../../../shared/components/rangeSlider/view';
import { Field } from 'redux-form';

const renderField = ({ input, label, min, max, step }: any) => {
	const { value, onChange } = input;
	return <RangeSlider
		min={min}
		max={max}
		value={+value}
		step={step}
		orientation='horizontal'
		label={label}
		tooltip
		onChange={onChange}
	/>;
};

export const ChinTab = (): React.ReactElement => {
	return <>
		<NewCard.Block>
			<Field
				name='chinLength'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.CHIN_LENGTH')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='chinPosition'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.CHIN_POSITION')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='chinWidth'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.CHIN_WIDTH')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='chinShape'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.CHIN_SHAPE')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='neckWidth'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.NECK_WIDTH')}
			/>
		</NewCard.Block>
	</>;
};
