import React from 'react';
import { NewCard } from '../../../../shared/components/newCard';
import { localize } from 'common/index';
import { RangeSlider } from '../../../../shared/components/rangeSlider/view';
import { Field } from 'redux-form';

const renderField = ({ input, label, min, max, step }: any) => {
	const { value, onChange } = input;
	return <RangeSlider
		min={min}
		max={max}
		value={+value}
		step={step}
		orientation='horizontal'
		label={label}
		tooltip
		onChange={onChange}
	/>;
};

export const JawTab = (): React.ReactElement => {
	return <>
		<NewCard.Block>
			<Field
				name='cheekBoneHeight'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.CHEEK_BONE_HEIGHT')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='cheekBoneWidth'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.CHEEK_BONE_WIDTH')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='cheeksWidth'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.CHEEKS_WIDTH')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='jawWidth'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.JAW_WIDTH')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='jawHeight'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.JAW_HEIGHT')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='lips'
				component={renderField}
				min={-1}
				max={1}
				step={0.1}
				label={localize.t('CHAR.LIPS')}
			/>
		</NewCard.Block>
	</>;
};
