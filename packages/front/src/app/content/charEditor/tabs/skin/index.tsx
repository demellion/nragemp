import React from 'react';
import { NewCard } from '../../../../shared/components/newCard';
import { localize } from 'common/index';
import { Field } from 'redux-form';
import { renderField } from '../../view';

export const SkinTab = (): React.ReactElement => {
	return <>
		<NewCard.Block>
			<Field
				name='complexion'
				component={renderField}
				min={0}
				max={11}
				step={1}
				default255
				label={localize.t('CHAR.COMPLEXION')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='blemishes'
				component={renderField}
				min={0}
				max={23}
				step={1}
				default255
				label={localize.t('CHAR.BLEMISHES')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='ageing'
				component={renderField}
				min={0}
				max={14}
				step={1}
				default255
				label={localize.t('CHAR.AGEING')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='sunDamage'
				component={renderField}
				min={0}
				max={10}
				step={1}
				default255
				label={localize.t('CHAR.SUN_DAMAGE')}
			/>
		</NewCard.Block>
		<NewCard.Block>
			<Field
				name='moles'
				component={renderField}
				min={0}
				max={17}
				step={1}
				default255
				label={localize.t('CHAR.MOLES')}
			/>
		</NewCard.Block>
	</>;
};
