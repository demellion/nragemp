import React from 'react';
import { NewCard } from '../../../../shared/components/newCard';
import { localize } from 'common/index';
import { useDispatch } from 'react-redux';
import { change } from 'redux-form';
import { Forms } from '../../../../shared/constants/forms';
import { Gender } from 'common/types/character/customizationData';
import cn from 'classnames';

export const hairColors = [
	{ id: 18, code: '#9D3824' },
	{ id: 17, code: '#A75B3D' },
	{ id: 16, code: '#B88559' },
	{ id: 15, code: '#F6D99E' },
	{ id: 14, code: '#DBAC6C' },
	{ id: 13, code: '#DEB269' },
	{ id: 12, code: '#CDA970' },
	{ id: 11, code: '#DFBB82' },
	{ id: 10, code: '#BE905F' },
	{ id: 9, code: '#C09062' },
	{ id: 8, code: '#A06840' },
	{ id: 7, code: '#A76843' },
	{ id: 6, code: '#9F512F' },
	{ id: 5, code: '#8C3C1F' },
	{ id: 4, code: '#462017' },
	{ id: 3, code: '#502A1C' },
	{ id: 2, code: '#48352A' },
	{ id: 1, code: '#282828' },
	{ id: 0, code: '#1e1e1e' }
];

interface HairTabProps {
	hair: number;
	gender: Gender;
}

export const HairTab = (props: HairTabProps): React.ReactElement => {
	const { hair, gender } = props;

	const hairStyles = gender === Gender.male
		? Array.from(Array(75).keys()).filter(item => ![23].includes(item))
		: Array.from(Array(79).keys()).filter(item => ![24].includes(item));

	const dispatch = useDispatch();

	const onHairColorPrimaryChangeHandler = (value: number): void => {
		dispatch(change(Forms.charEditor, 'hairPrimaryColor', value));
	};

	const onHairColorSecondaryChangeHandler = (value: number): void => {
		dispatch(change(Forms.charEditor, 'hairSecondaryColor', value));
	};

	const onHairStyleChangeHandler = (value: number): void => {
		dispatch(change(Forms.charEditor, 'hair', value));
	};

	return <>
		<NewCard.Block>
			<div className='char-editor-colors-container'>
				<div className='char-editor-colors-label'>{localize.t('CHAR.HAIR_COLOR_PRIMARY')}</div>
				<div className='char-editor-colors'>
					{hairColors.map((color, index) => (
						<div
							key={index}
							className='char-editor-color'
							style={{ backgroundColor: color.code }}
							onClick={() => onHairColorPrimaryChangeHandler(color.id)}
						/>
					))}
				</div>
			</div>
		</NewCard.Block>
		<NewCard.Block>
			<div className='char-editor-colors-container'>
				<div className='char-editor-colors-label'>{localize.t('CHAR.HAIR_COLOR_SECONDARY')}</div>
				<div className='char-editor-colors'>
					{hairColors.map((color, index) => (
						<div
							key={index}
							className='char-editor-color'
							style={{ backgroundColor: color.code }}
							onClick={() => onHairColorSecondaryChangeHandler(color.id)}
						/>
					))}
				</div>
			</div>
		</NewCard.Block>
		<NewCard.Block>
			<div className='char-editor-scroll-container'>
				{hairStyles.map((style, index) => {
					const folder = gender === Gender.male ? 'male' : 'female';
					const imgName = gender === Gender.male ? `120px-Clothing_M_2_${style}.jpg` : `120px-Clothing_F_2_${style}.jpg`;
					const img = require(`../../../../../assets/hairs/${folder}/${imgName}`);
					return <div
						key={index}
						onClick={() => onHairStyleChangeHandler(style)}
						className={cn({ active: hair === style })}
						style={{ backgroundImage: `url('${img}')` }}
					/>;
				})}
			</div>
		</NewCard.Block>
	</>;
};
