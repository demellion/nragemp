import { View } from './view';
import { compose, lifecycle } from 'recompose';
import { RouteComponentProps } from 'react-router';
import { change, getFormValues, initialize, reduxForm, reset, SubmissionError } from 'redux-form';
import { Forms } from '../../shared/constants/forms';
import { connect } from 'react-redux';
import { ActionProps, CharEditorFormData, Model, StateProps } from './model';
import { StorageState } from '../../../redux/reducers';
import { validationPipe } from '../../shared/pipes/validation';
import { charEditorControlsSelector } from '../../../redux/selectors/controls';
import { setCharEditorAppearanceTab, setCharEditorHairTab, setCharEditorMainTab } from '../../../redux/actions/controls/charEditor';
import { ActionType, BaseThunkAction } from '../../../redux/actions';
import { store } from '../../index';
import { Gender } from 'common/types/character/customizationData';
import { createCharacter, exitCharEditor, updateCharInEditor } from '../../../redux/actions/characters';
import { charEditorInit } from '../../shared/constants/charEditorInit';
import { localize } from 'common/index';
import { Regexps } from 'common/constants/regexps';

let storageState: StorageState;

const getFormErrors = (formData: CharEditorFormData) => validationPipe(formData, [
	{
		names: 'firstname',
		error: localize.t('ERRORS.CREATE_CHARACTER.FIRSTNAME_IS_REQUIRED'),
		type: 'required'
	},
	{
		names: 'lastname',
		error: localize.t('ERRORS.CREATE_CHARACTER.LASTNAME_IS_REQUIRED'),
		type: 'required'
	},
	{
		names: 'age',
		error: localize.t('ERRORS.CREATE_CHARACTER.AGE_IS_REQUIRED'),
		type: 'required'
	},
	{
		names: 'age',
		error: localize.t('ERRORS.CREATE_CHARACTER.INVALID_AGE'),
		type: 'pattern',
		pattern: Regexps.age
	}
]);

const onInit = (): BaseThunkAction => async dispatch => {
	dispatch(initialize(Forms.charEditor, charEditorInit));
};

const lifecycleHooks = lifecycle<Model, undefined>({
	componentDidMount() {
		store.dispatch(onInit());
	}
});

const redux = connect<StateProps, ActionProps, {}, StorageState>(
	state => {
		storageState = state;
		const formData = getFormValues(Forms.charEditor)(storageState) as CharEditorFormData;
		const charEditorControls = charEditorControlsSelector(state);
		const { mainTab, appearanceTab, hairTab } = charEditorControls;
		return {
			mainTab,
			appearanceTab,
			hairTab,
			gender: formData?.gender,
			hair: formData?.hair,
			isFormValid: () => !getFormErrors(formData)
		};
	},
	(dispatch: any) => ({
		onChangeGender(gender: Gender) {
			dispatch(reset(Forms.charEditor));
			dispatch(change(Forms.charEditor, 'gender', gender));
			dispatch({ type: ActionType.CHAR_EDITOR_CHANGE_GENDER });
		},
		onChangeMainTab: tab => dispatch(setCharEditorMainTab(tab)),
		onChangeAppearanceTab: tab => dispatch(setCharEditorAppearanceTab(tab)),
		onChangeHairTab: tab => dispatch(setCharEditorHairTab(tab)),
		onExit: () => dispatch(exitCharEditor())
	})
);

const onSubmit = async (formData: CharEditorFormData, dispatch: any): Promise<void> => {
	let errors = getFormErrors(formData);
	if (!errors) errors = {};

	if (!!errors && Object.keys(errors).length > 0) {
		throw new SubmissionError(errors);
	}

	dispatch(createCharacter(formData));
};

const form = reduxForm<any>({
	form: Forms.charEditor,
	destroyOnUnmount: true,
	onSubmit,
	onChange(values: Partial<CharEditorFormData>, dispatch: any) {
		dispatch(updateCharInEditor(values));
	}
});

export const CharEditor = compose<Model, RouteComponentProps>(
	redux,
	form,
	lifecycleHooks,
	redux
)(View);
