import React from 'react';
import cn from 'classnames';
import { NewCard } from '../../shared/components/newCard';
import './styles.less';
import { Button } from '../../shared/components/button/view';
import { RangeSlider } from '../../shared/components/rangeSlider/view';
import { ComboboxHorizontal } from '../../shared/components/comboboxHorizontal/view';
import { customizationDictionary } from 'common/constants/customizationData';
import { Gapped } from '../../shared/components/gapped';
import { Model } from './model';
import { Gender } from 'common/types/character/customizationData';
import { Field } from 'redux-form';
import { AppearanceTab, HairsTab, MainTab } from '../../../redux/reducers/controls/charEditor';
import { IconSvg } from '../../shared/components/icon';
import { SkinTab } from './tabs/skin';
import { localize } from 'common/index';
import { NoseTab } from './tabs/nose';
import { EyesTab } from './tabs/eyes';
import { ChinTab } from './tabs/chin';
import { JawTab } from './tabs/jaw';
import { HairTab } from './tabs/hair';
import { BeardTab } from './tabs/beard';
import { ExitButton } from '../../shared/components/exitButton';

const renderInputField = ({ input, placeholder, type, meta: { asyncValidating, touched, error, active } }: any) => {
	return <div className='char-editor-input-field'>
		<div className={asyncValidating ? 'async-validating' : ''}>
			<input
				{...input}
				type={type}
				autoComplete='off'
				spellCheck={false}
				placeholder={placeholder}
				ref={(element) => active && element?.focus()}
			/>
			<span className='auth-form-field-error'>{touched && error}</span>
		</div>
	</div>;
};

export const renderField = ({ input, label, min, max, step, default255 }: any): React.ReactNode => {
	const { value, onChange } = input;
	return <RangeSlider
		min={min}
		max={max}
		value={+value}
		step={step}
		orientation='horizontal'
		label={label}
		tooltip
		default255={default255}
		onChange={onChange}
	/>;
};

const renderSliderField = ({ input, label, min, max, step, tooltip = true }: any) => {
	const { value, onChange } = input;
	return <RangeSlider
		min={min}
		max={max}
		value={+value}
		step={step}
		orientation='horizontal'
		label={label}
		tooltip={tooltip}
		onChange={onChange}
	/>;
};

const renderComboboxField = ({ input, items }: any) => {
	const { value, onChange } = input;
	return <ComboboxHorizontal
		value={value}
		items={items}
		onChange={onChange}
	/>;
};

export class View extends React.Component<Model> {
	private renderModsTab = (): React.ReactNode => {
		const { mainTab, appearanceTab, hairTab, hair, gender } = this.props;

		switch (mainTab) {
			case MainTab.appearance: {
				switch (appearanceTab) {
					case AppearanceTab.skin: {
						return <SkinTab />;
					}
					case AppearanceTab.nose: {
						return <NoseTab />;
					}
					case AppearanceTab.eyes: {
						return <EyesTab />;
					}
					case AppearanceTab.chin: {
						return <ChinTab />;
					}
					case AppearanceTab.jaw: {
						return <JawTab />;
					}
				}
				break;
			}
			case MainTab.hairs: {
				switch (hairTab) {
					case HairsTab.hair: {
						return <HairTab hair={hair} gender={gender} />;
					}
					case HairsTab.beard: {
						return gender === Gender.male && <BeardTab />;
					}
				}
			}
		}

		return null;
	};

	private getIconName = (tab: MainTab | AppearanceTab | HairsTab): string => {
		if (!tab) return null;
		switch (tab) {
			case MainTab.appearance:
				return 'head';
			case AppearanceTab.eyes:
				return 'eye';
			case HairsTab.hair:
				return 'hair-color';
			default:
				return tab;
		}
	}

	private onEscapePressed = (event: KeyboardEvent): void => {
		if (event.keyCode !== 27) return;
		this.props.onExit();
	}

	componentDidMount(): void {
		document.addEventListener('keydown', this.onEscapePressed);
	}

	componentWillUnmount(): void {
		document.removeEventListener('keydown', this.onEscapePressed);
	}

	render(): React.ReactElement {
		const {
			handleSubmit, gender, onChangeGender, mainTab, appearanceTab, hairTab,
			onChangeMainTab, onChangeAppearanceTab, onChangeHairTab, onExit
		} = this.props;

		return (
			<form className='char-editor-page' onSubmit={handleSubmit}>
				<div className='char-editor-left'>
					<NewCard width={298}>
						<NewCard.Header>
							{localize.t('CHAR.NEW')}
						</NewCard.Header>
						<NewCard.Body>
							<NewCard.Block>
								<div className='flex center char-editor-gender-block'>
									<Gapped vertical>
										<div className='flex center bold'>{localize.t('CHAR.GENDERSELECT')}</div>
										<div className='flex char-editor-gender-block-buttons'>
											<div
												className={cn('gender-male flex center', { active: gender === Gender.male })}
												onClick={() => onChangeGender(Gender.male)}
											>
												<IconSvg name='gender-male' size={39} />
											</div>
											<div
												className={cn('gender-female flex center', { active: gender === Gender.female })}
												onClick={() => onChangeGender(Gender.female)}
											>
												<IconSvg name='gender-female' size={32} />
											</div>
										</div>
									</Gapped>
								</div>
							</NewCard.Block>
							<NewCard.Block>
								<Gapped vertical>
									<b>{localize.t('CHAR.PERSONAL_DATA')}</b>
									<Field
										name='firstname'
										type='text'
										component={renderInputField}
										autoComplete='off'
										placeholder={localize.t('CHAR.INPUT.FIRSTNAME')}
									/>
									<Field
										name='lastname'
										type='text'
										component={renderInputField}
										autoComplete='off'
										placeholder={localize.t('CHAR.INPUT.LASTNAME')}
									/>
									<Field
										name='age'
										type='number'
										component={renderInputField}
										autoComplete='off'
										placeholder={localize.t('CHAR.INPUT.AGE')}
									/>
								</Gapped>
							</NewCard.Block>
							<NewCard.Block>
								<Gapped vertical>
									<b>{localize.t('CHAR.FATHER')}</b>
									<Field
										name='father'
										component={renderComboboxField}
										items={customizationDictionary.father.map(x => localize.t(`FATHERS.${x.name}`))}
									/>
									<b>{localize.t('CHAR.MOTHER')}</b>
									<Field
										name='mother'
										component={renderComboboxField}
										items={customizationDictionary.mother.map(x => localize.t(`MOTHERS.${x.name}`))}
									/>
									<div className='char-editor-similarity-slider'>
										<Field
											name='similar'
											component={renderSliderField}
											min={0}
											max={1}
											step={0.1}
											tooltip={false}
											label={localize.t('CHAR.SIMILAR')}
										/>
									</div>
									<Button color='primary'>{localize.t('CHAR.RANDOM')}</Button>
								</Gapped>
							</NewCard.Block>
						</NewCard.Body>
					</NewCard>
				</div>
				<div className='char-editor-center'>
					<Button
						color='primary'
						size='medium'
						flat
						type='submit'
					>
						{localize.t('CHAR.CREATE')}
					</Button>
				</div>
				<div className='char-editor-right'>
					<div className='char-editor-right-container'>
						<Gapped verticalAlign='middle'>
							<NewCard>
								<NewCard.Block>
									<Gapped vertical>
										{Object.keys(MainTab).map((tab, index) => (
											<div
												key={index}
												className={cn('char-editor-tab-button', { active: mainTab === MainTab[tab] })}
												onClick={() => onChangeMainTab(MainTab[tab])}
											>
												<IconSvg name={this.getIconName(MainTab[tab])} size={32} />
											</div>
										))}
									</Gapped>
								</NewCard.Block>
							</NewCard>
							<div className='char-editor-second-tab-container'>
								<NewCard>
									<NewCard.Block>
										<Gapped vertical>
											{mainTab === MainTab.appearance && Object.keys(AppearanceTab).map((tab, index) => (
												<div
													key={index}
													className={cn('char-editor-tab-button', { active: appearanceTab === AppearanceTab[tab] })}
													onClick={() => onChangeAppearanceTab(AppearanceTab[tab])}
												>
													<IconSvg name={this.getIconName(AppearanceTab[tab])} size={32} />
												</div>
											))}
											{mainTab === MainTab.hairs && Object.keys(HairsTab).map((tab, index) => {
												if (gender === Gender.female && HairsTab[tab] === HairsTab.beard) return null;
												return <div
													key={index}
													className={cn('char-editor-tab-button', { active: hairTab === HairsTab[tab] })}
													onClick={() => onChangeHairTab(HairsTab[tab])}
												>
													<IconSvg name={this.getIconName(HairsTab[tab])} size={32} />
												</div>;
											})}
										</Gapped>
									</NewCard.Block>
								</NewCard>
							</div>
							<div className='char-editor-mods-container'>
								<NewCard>
									<NewCard.Body>
										{this.renderModsTab()}
									</NewCard.Body>
								</NewCard>
							</div>
						</Gapped>
					</div>
				</div>
				<ExitButton onClick={onExit} pos={'left'} />
			</form>
		);
	}
}
