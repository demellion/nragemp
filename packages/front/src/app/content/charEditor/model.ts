import { AppearanceTab, HairsTab, MainTab } from '../../../redux/reducers/controls/charEditor';
import { Gender } from 'common/types/character/customizationData';

export interface StateProps {
	hair: number;
	gender: Gender;
	mainTab: MainTab;
	appearanceTab: AppearanceTab;
	hairTab: HairsTab;
	isFormValid: () => boolean;
}

export interface ActionProps {
	onChangeGender: (gender: Gender) => void;
	onChangeMainTab: (tab: MainTab) => void;
	onChangeAppearanceTab: (tab: AppearanceTab) => void;
	onChangeHairTab: (tab: HairsTab) => void;
	onExit: () => void;
	handleSubmit?: () => void;
}

export interface CharEditorFormData {
	gender: Gender;
	firstname: string;
	lastname: string;
	age: number;
	father: number;
	mother: number;
	similar: number;

	complexion: number;
	blemishes: number;
	ageing: number;
	sunDamage: number;
	moles: number;

	noseWidth: number;
	noseHeight: number;
	noseLength: number;
	noseBridge: number;
	noseTip: number;
	noseBridgeShift: number;

	browHeight: number;
	browWidth: number;

	eyes: number;

	chinLength: number;
	chinPosition: number;
	chinWidth: number;
	chinShape: number;
	neckWidth: number;

	cheekBoneHeight: number;
	cheekBoneWidth: number;
	cheeksWidth: number;
	jawWidth: number;
	jawHeight: number;
	lips: number;

	hair: number;
	hairPrimaryColor: number;
	hairSecondaryColor: number;
	facialHair: number;
	facialHairPrimaryColor: number;
	facialHairSecondaryColor: number;
}

export type Model = StateProps & ActionProps;
