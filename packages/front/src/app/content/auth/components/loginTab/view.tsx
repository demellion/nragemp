import React from 'react';
import { Field } from 'redux-form';
import { localize } from 'common/index';

export interface LoginFormData {
	username: string;
	password: string;
}

export interface IStateProps {
	isFormValid: () => boolean;
}

export interface IActionProps {
	handleSubmit?: any;
}

export type IModel = IStateProps & IActionProps;

const renderField = ({ input, label, type, meta: { asyncValidating, touched, error, active } }: any) => {
	return <div className='auth-form-field'>
		<label className='auth-form-field-label'>{label}</label>
		<div className={asyncValidating ? 'async-validating' : ''}>
			<input
				{...input}
				type={type}
				autoComplete='off'
				spellCheck={false}
				ref={(element) => active && element?.focus()}
			/>
			<span className='auth-form-field-error'>{touched && error}</span>
		</div>
	</div>;
};

export class View extends React.Component<IModel> {
	render(): React.ReactElement {
		const { handleSubmit } = this.props;
		return <form onSubmit={handleSubmit}>
			<div className='auth-form-body'>
				<Field
					name='username'
					type='text'
					component={renderField}
					label={localize.t('CEF.AUTH.FIELDS.USERNAME')}
					autoComplete='off'
				/>
				<Field
					name='password'
					type='password'
					component={renderField}
					label={localize.t('CEF.AUTH.FIELDS.PASSWORD')}
				/>
			</div>
			<div className='auth-form-footer'>
				<button type='submit'>
					{localize.t('CEF.AUTH.BUTTONS.LOGIN')}
				</button>
			</div>
		</form>;
	}
}
