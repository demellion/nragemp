import rpc from 'rage-rpc';
import { BaseThunkAction } from '../../../../../redux/actions';
import { Forms } from '../../../../shared/constants/forms';
import { IModel, IStateProps, LoginFormData, View } from './view';
import { LoginError, LoginErrorTypes } from 'common/types/api/loginAccount';
import { StorageState } from '../../../../../redux/reducers';
import { compose, lifecycle } from 'recompose';
import { connect } from 'react-redux';
import { focus, getFormValues, initialize, reduxForm, SubmissionError } from 'redux-form';
import { localize } from 'common/index';
import { logInAccount } from '../../../../../redux/actions/auth';
import { store } from '../../../../index';
import { validationPipe } from '../../../../shared/pipes/validation';

let storageState: StorageState = null;

const getFormErrors = (formData: LoginFormData) => validationPipe(formData, [
	{
		names: 'username',
		error: localize.t('ERRORS.REGISTER.NAME_IS_REQUIRED'),
		type: 'required'
	},
	{
		names: 'password',
		error: localize.t('ERRORS.REGISTER.PASSWORD_IS_REQUIRED'),
		type: 'required'
	}
]);

const onInit = (): BaseThunkAction => async dispatch => {
	const lastLogin = await rpc.callClient('getLastLogin');

	if (!lastLogin) return;

	dispatch(initialize(Forms.login, { username: lastLogin }));
	dispatch(focus(Forms.login, lastLogin ? 'password' : 'username'));
};

const lifecycleHooks = lifecycle<IModel, undefined>({
	componentDidMount() {
		store.dispatch(onInit());
	}
});

const redux = connect<IStateProps, null, {}, StorageState>(
	(state) => {
		storageState = state;
		const formData = getFormValues(Forms.login)(storageState) as LoginFormData;
		return {
			isFormValid: () => !getFormErrors(formData)
		};
	},
	null
);

const onSubmit = async (formData: LoginFormData, dispatch: any): Promise<void> => {
	let errors = getFormErrors(formData);
	if (!errors) errors = {};

	await dispatch(logInAccount(formData)).catch((error: LoginError) => {
		switch (error.type) {
			case LoginErrorTypes.WrongUsernameOrPassword: {
				errors.password = localize.t('ERRORS.LOGIN.WRONG_USERNAME_OR_PASSWORD');
				return;
			}
			case LoginErrorTypes.InvalidrgscId: {
				errors.username = localize.t('ERRORS.LOGIN.WRONG_RGSCID');
			}
		}
	});

	if (!!errors && Object.keys(errors).length > 0) {
		throw new SubmissionError(errors);
	}
};

const form = reduxForm<LoginFormData>({
	form: Forms.login,
	destroyOnUnmount: false,
	onSubmit
});

export const LoginTab = compose<IModel, {}>(
	redux,
	form,
	lifecycleHooks,
	redux
)(View);
