import { Forms } from '../../../../shared/constants/forms';
import { IModel, IStateProps, RegisterFormData, View } from './view';
import { Regexps } from 'common/constants/regexps';
import { RegisterError, RegisterErrorTypes } from 'common/types/api/registerAccount';
import { StorageState } from '../../../../../redux/reducers';
import { compose, lifecycle } from 'recompose';
import { connect } from 'react-redux';
import { focus, getFormValues, reduxForm, SubmissionError } from 'redux-form';
import { localize } from 'common/index';
import { registerAccount } from '../../../../../redux/actions/auth';
import { store } from '../../../../index';
import { validationPipe } from '../../../../shared/pipes/validation';

const getFormErrors = (formData: RegisterFormData) => validationPipe(formData, [
	{
		names: 'username',
		error: localize.t('ERRORS.REGISTER.NAME_IS_REQUIRED'),
		type: 'required'
	},
	{
		names: 'email',
		error: localize.t('ERRORS.REGISTER.EMAIL_IS_REQUIRED'),
		type: 'required'
	},
	{
		names: 'email',
		error: localize.t('ERRORS.REGISTER.INCORRECT_EMAIL'),
		type: 'pattern',
		pattern: Regexps.emailValidation
	},
	{
		names: 'password',
		error: localize.t('ERRORS.REGISTER.PASSWORD_IS_REQUIRED'),
		type: 'required'
	},
	{
		names: 'confirm',
		error: localize.t('ERRORS.REGISTER.CONFIRM_IS_REQUIRED'),
		type: 'required'
	}
]);

const lifecycleHooks = lifecycle<IModel, undefined>({
	componentDidMount() {
		store.dispatch(focus(Forms.register, 'username'));
	}
});

const redux = connect<IStateProps, null, {}, StorageState>(
	state => {
		const formData = getFormValues(Forms.register)(state) as RegisterFormData;
		return {
			isFormValid: () => !getFormErrors(formData)
		};
	},
	null
);

const onSubmit = async (formData: RegisterFormData, dispatch: any): Promise<void> => {
	let errors = getFormErrors(formData);
	if (!errors) errors = {};

	if (formData.password?.length < 6) {
		errors.password = localize.t('ERRORS.REGISTER.TOO_SHORT_PASSWORD');
	}

	if (formData.password !== formData.confirm) {
		errors.confirm = localize.t('ERRORS.REGISTER.PASSWORDS_DO_NOT_MATCH');
	}

	if (!!errors && Object.keys(errors).length > 0) {
		throw new SubmissionError(errors);
	}

	await dispatch(registerAccount(formData)).catch((error: RegisterError) => {
		switch (error.type) {
			case RegisterErrorTypes.TooShortLogin:
			case RegisterErrorTypes.LoginAlreadyExists: {
				errors.username = errors.username || error.message;
				return;
			}
			case RegisterErrorTypes.EmailAlreadyExists: {
				errors.email = errors.email || error.message;
				return;
			}
			case RegisterErrorTypes.SocialClubExists: {
				errors.socialClub = errors.socialClub || error.message;
				return;
			}
			case RegisterErrorTypes.TooShortPassword: {
				errors.password = errors.password || error.message;
			}
		}
	});

	if (!!errors && Object.keys(errors).length > 0) {
		throw new SubmissionError(errors);
	}
};

const form = reduxForm<RegisterFormData>({
	form: Forms.register,
	destroyOnUnmount: false,
	onSubmit
});

export const RegisterTab = compose<IModel, {}>(
	redux,
	form,
	lifecycleHooks,
	redux
)(View);
