import React from 'react';
import { NewsItem, NewsItemComponent } from './newsItemComponent';
import { localize } from 'common/index';

export const NewsPanel = (): React.ReactElement => {
	const lang = localize.language;
	const news: NewsItem[] = [
		{
			header: 'DEVBLOG #0001',
			date: '2/9/2020',
			author: 'Demellion',
			text: lang === 'en'
				? 'We made our first steps! Visit our official Patreon and Twitter page to get more details about our project'
				: 'Мы сделали первые шаги! Посетите нашу официальную страницу Patreon и Твиттер, чтобы узнать больше о нашем проекте',
			imageUrl: 'https://i.gyazo.com/d7e91f26331d37bfa6a88fa1044b6f5b.png',
			link: 'https://twitter.com/NRageMP'
		}
	];

	return <div className='auth-news-container'>
		{news.map((item, index) => <NewsItemComponent key={index} item={item} />)}
	</div>;
};
