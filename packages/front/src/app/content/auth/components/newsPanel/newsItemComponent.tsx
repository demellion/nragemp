import React from 'react';
import { Card } from '../../../../shared/components/card';
import { localize } from 'common/index';

export interface NewsItem {
	header: string;
	date: string;
	author: string;
	text: string;
	imageUrl: string;
	link: string;
}

export interface IStateProps {
	item: NewsItem;
}

export const NewsItemComponent = ({ item }: IStateProps): React.ReactElement => {
	return <div className='news-item'>
		<Card size='medium' linePos='top'>
			<div className='news-item-header'>
				<div className='news-item-header-text'>{item.header}</div>
				<div className='news-item-header-meta'>Posted on {item.date} by {item.author}</div>
			</div>
			<div className='news-item-body'>
				<a href={item.link} target='global_frame'><img className='news-item-image' alt={item.header} src={item.imageUrl}/></a>
				{item.text} <br /><a href={item.link} target='global_frame'>{localize.t('CEF.READMORE')}</a>
			</div>
		</Card>
	</div>;
};
