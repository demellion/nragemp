import React, { ReactElement, useState } from 'react';
import cn from 'classnames';
import { LoginTab } from './components/loginTab';
import { RegisterTab } from './components/registerTab';
import { NewsPanel } from './components/newsPanel';

import './styles.scss';
import { localize } from 'common/index';

enum AuthTabs {
	login,
	register
}

export const AuthPage = (): ReactElement => {
	const [tab, setTab] = useState(AuthTabs.login);

	const changeTab = (tab: AuthTabs): void => {
		setTab(tab);
	};

	return <div className='auth-page'>
		<div className='auth-left-container' />
		<div className='auth-form-container'>
			<img alt='auth-form-logo' src={require('../../../assets/logo.png')} />
			<div className='auth-form'>
				<div className='auth-form-header'>
					<div className={cn('auth-form-tab', { active: tab === AuthTabs.login })}>
						<button onClick={() => changeTab(AuthTabs.login)}>
							{localize.t('CEF.AUTH.TABS.LOGIN')}
						</button>
					</div>
					<div className={cn('auth-form-tab', { active: tab === AuthTabs.register })}>
						<button onClick={() => changeTab(AuthTabs.register)}>
							{localize.t('CEF.AUTH.TABS.REGISTER')}
						</button>
					</div>
				</div>
				{
					tab === AuthTabs.login && <LoginTab />
				}
				{
					tab === AuthTabs.register && <RegisterTab />
				}
			</div>
		</div>
		<NewsPanel />
	</div>;
};
