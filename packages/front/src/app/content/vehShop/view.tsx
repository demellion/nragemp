import React from 'react';
import { Model } from './model';
import { animateCef } from 'common/pipes/animate';
import './styles.less';
import { moneyString, shortMoneyString } from 'common/pipes/string';
import cn from 'classnames';
import { localize } from 'common/index';

interface IProgressBarProps {
	maxFill: number;
}

const ProgressBar = ({ maxFill }: IProgressBarProps): React.ReactElement => {
	const [fill, setFill] = React.useState(0);

	if (!maxFill) maxFill = 0;
	if (maxFill > 100) maxFill = 100;

	let mounted: boolean;
	let rAF: number;

	const onAnimationTick = (newFill: number) => {
		if (!mounted) return;
		setFill(newFill);
	};

	const startAnimation: FrameRequestCallback = (time: number) => {
		animateCef(time, fill, maxFill, 500, 'outCube', onAnimationTick);
	};

	const onComponentMount = (): void => {
		mounted = true;
		rAF = requestAnimationFrame(startAnimation);
	};

	const onComponentUnmount = (): void => {
		mounted = false;
		if (rAF) cancelAnimationFrame(rAF);
	};

	React.useEffect(() => {
		onComponentMount();
		return () => onComponentUnmount();
	}, [maxFill]);

	return <div className='stats-bar-container'>
		<div className='stats-bar' style={{ width: `${fill}%` }}/>
		<div className='stats-value'>{Math.round(maxFill)}%</div>
	</div>;
};

const colors = [
	'#009933',
	'#003366',
	'#663399',
	'#CC0033',
	'#FF6699',
	'#FFFF33',
	'#330033',
	'#00CCCC',
	'#FFFFFF',
	'#990033',
	'#000000',
	'#9966CC',
	'#F86395'
];

export const View = (props: Model): React.ReactElement => {
	const [primaryColor, setPrimaryColor] = React.useState(colors[8]);
	const [secondaryColor, setSecondaryColor] = React.useState(colors[8]);

	const { active, list, closeShop, getList, setActive, onBuyVehicle, onTestDrive, onChangeColor, alias } = props;

	const changePrimaryColorHandler = (color: string): void => {
		setPrimaryColor(color);
		onChangeColor(color, secondaryColor);
	};

	const changeSecondaryColorHandler = (color: string): void => {
		setSecondaryColor(color);
		onChangeColor(primaryColor, color);
	};

	const onEscapePressed = (event: KeyboardEvent): void => {
		if (event.keyCode !== 27) return;
		closeShop();
	};

	React.useEffect(() => {
		if (list.length > 0) return;
		getList(alias);
	}, [list]);

	React.useEffect(() => {
		document.addEventListener('keydown', onEscapePressed);
		return () => document.removeEventListener('keydown', onEscapePressed);
	}, []);

	if (!list || !list.length) return null;

	return <div className='veh-shop'>
		<div className='veh-shop-left'>
			<div className='veh-shop-list-container'>
				<div className='veh-shop-list-header'>
					{localize.t('VEH_SHOP.NAME')}
				</div>
				<div className='veh-shop-list-body'>
					<div className='veh-shop-list-body-scroll-container'>
						{list.map((vehicle, index) => (
							<div
								key={index}
								className={cn('veh-shop-list-item', { active: active?.name === vehicle.name })}
								onClick={() => setActive(vehicle.name)}
							>
								<span>{vehicle.name}</span>
								<span>{shortMoneyString(vehicle.price)}</span>
							</div>
						))}
					</div>
				</div>
			</div>
		</div>
		<div className='veh-shop-right'>
			{active && <div className='veh-shop-mods-container'>
				<div className='veh-shop-mods-label'>
					<span>{localize.t('VEH_SHOP.VEHICLE.CLASS')}</span>
					<span>{active.class}</span>
				</div>
				<div className='veh-shop-mods-stats'>
					<div className='veh-shop-mods-stats-item'>
						<div>{localize.t('VEH_SHOP.VEHICLE.STATS.SPEED')}</div>
						<div><ProgressBar maxFill={active.stats.speed * 100}/></div>
					</div>
					<div className='veh-shop-mods-stats-item'>
						<div>{localize.t('VEH_SHOP.VEHICLE.STATS.ACCELERATION')}</div>
						<div><ProgressBar maxFill={active.stats.acceleration * 100}/></div>
					</div>
					<div className='veh-shop-mods-stats-item'>
						<div>{localize.t('VEH_SHOP.VEHICLE.STATS.HANDLING')}</div>
						<div><ProgressBar maxFill={active.stats.handling * 100}/></div>
					</div>
					<div className='veh-shop-mods-stats-item'>
						<div>{localize.t('VEH_SHOP.VEHICLE.STATS.BRAKING')}</div>
						<div><ProgressBar maxFill={active.stats.braking * 100}/></div>
					</div>
				</div>
				<div className='veh-shop-mods-label'>
					<span>{localize.t('VEH_SHOP.VEHICLE.FUEL')}</span>
					<span>{active.fuel} L</span>
				</div>
				<div className='veh-shop-mods-label'>
					<span>{localize.t('VEH_SHOP.VEHICLE.INVENTORY')}</span>
					<span>{active.inventory.capacity} KG ({active.inventory.slots})</span>
				</div>
				<div className='veh-shop-mods-label'>
					<span>{localize.t('VEH_SHOP.VEHICLE.SEATS')}</span>
					<span>{active.seats}</span>
				</div>
				<div className='veh-shop-mods-colors'>
					<div className='veh-shop-mods-colors-item'>
						<div>{localize.t('VEH_SHOP.VEHICLE.PRIMARY_COLOR')}</div>
						<div>
							{colors.map(color => <span
								className='veh-shop-colors-color'
								key={color}
								style={{ backgroundColor: color }}
								onClick={() => changePrimaryColorHandler(color)}
							/>)}
						</div>
					</div>
					<div className='veh-shop-mods-colors-item'>
						<div>{localize.t('VEH_SHOP.VEHICLE.SECONDARY_COLOR')}</div>
						<div>
							{colors.map(color => <span
								className='veh-shop-colors-color'
								key={color}
								style={{ backgroundColor: color }}
								onClick={() => changeSecondaryColorHandler(color)}
							/>)}
						</div>
					</div>
				</div>
				<div className='veh-shop-mods-label'>
					<span>{localize.t('VEH_SHOP.VEHICLE.PRICE')}</span>
					<span className='price green'>{moneyString(active.price)}</span>
				</div>
				<div className='veh-shop-mods-buttons'>
					<div>
						<button onClick={() => onBuyVehicle(active.name, 'cash')}>
							{localize.t('WALLET.CASH')}
						</button>
						<button onClick={() => onBuyVehicle(active.name, 'bank')}>
							{localize.t('WALLET.BANK')}
						</button>
					</div>
					<div>
						<button onClick={onTestDrive}>
							{localize.t('VEH_SHOP.TEST_DRIVE')}
						</button>
					</div>
				</div>
			</div>}
		</div>
	</div>;
};
