import { Vehicle } from 'common/types/vehicle';

export interface StateProps {
	alias: string;
	active: Vehicle;
	list: Vehicle[];
}

export interface ActionProps {
	closeShop: () => void;
	getList: (alias: string) => void;
	setActive: (name: string) => void;
	onChangeColor: (primary: string, secondary: string) => void;
	onBuyVehicle: (name: string, moneySource: 'cash' | 'bank') => void;
	onTestDrive: () => void;
}

export type Model = StateProps & ActionProps;
