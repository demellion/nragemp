import { ActionProps, Model, StateProps } from './model';
import { StorageState } from '../../../redux/reducers';
import { View } from './view';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { buyVehicle, changeVehicleColor, closeVehicleShop, doTestDrive, getVehicleList, setActiveVehicleByName } from '../../../redux/actions/vehShop';
import { RouteComponentProps } from 'react-router';
import { StringifedQueryStringParams } from '../../shared/pipes';

const redux = connect<StateProps, ActionProps, RouteComponentProps<StringifedQueryStringParams>, StorageState>(
	(state, ownProps) => ({
		alias: ownProps.match.params.alias,
		active: state.vehShop.active,
		list: state.vehShop.list || []
	}),
	(dispatch: any) => ({
		closeShop: () => {
			dispatch(closeVehicleShop());
		},
		getList: (alias: string) => {
			dispatch(getVehicleList(alias));
		},
		setActive: (name: string) => {
			dispatch(setActiveVehicleByName(name));
		},
		onChangeColor: (primary, secondary) => {
			dispatch(changeVehicleColor(primary, secondary));
		},
		onBuyVehicle: (name, moneySource) => {
			dispatch(buyVehicle(name, moneySource));
		},
		onTestDrive: () => {
			dispatch(doTestDrive());
		}
	})
);

export const VehShop = compose<Model, {}>(
	redux
)(View);
