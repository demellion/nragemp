import React from 'react';
import { DebugInfoComponent } from '../../shared/components/debugInfo';
import { HudState } from '../../../redux/reducers/hud';
import rpc from 'rage-rpc';
import { store } from '../../index';
import { setHudData } from '../../../redux/actions/hud';
import { useSelector } from 'react-redux';
import { StorageState } from '../../../redux/reducers';
import { Card } from '../../shared/components/card';
import { Chat } from './components/chat';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFireAlt, faGasPump, faLock, faMicrophone, faUser, faWallet } from '@fortawesome/free-solid-svg-icons';
import './styles.less';
import { fuelString, moneyString, speedString } from 'common/pipes/string';
import { sleep } from 'common/pipes/sleep';
import { SafeZoneLevels } from 'common/enums/saveZoneLevels';
import cn from 'classnames';
import { Wallet } from 'common/types/wallet';
import { inputDictionary } from 'common/constants/inputDictionary';

const moment = require('moment');

const receiveHudData = async () => {
	const hudData: HudState = await rpc.callClient('getHUDInfo');
	store.dispatch(setHudData(hudData));
	await sleep(100);
	receiveHudData();
};

const getCash = (wallet: Wallet): string => {
	if (!wallet || wallet.cash <= 0) return moneyString(0);
	return moneyString(wallet.cash);
};

const getVehicleSpeed = (speed: number): string => {
	if (speed === null || speed === undefined) return speedString(0);
	return speedString(speed);
};

const getVehicleFuel = (fuel: number): string => {
	if (fuel === null || fuel === undefined) return fuelString(0);
	return fuelString(fuel);
};

interface IPlayerInfoProps {
	wallet: Wallet;
	speaking: boolean;
	isVehicle: boolean;
	vehicleLocked: boolean;
	vehicleSpeed: number;
	vehicleFuel: number;
	vehicleEngine: boolean;
}

const PlayerInfo = (props: IPlayerInfoProps): React.ReactElement => {
	const {
		wallet,
		speaking,
		isVehicle,
		vehicleLocked,
		vehicleSpeed,
		vehicleFuel,
		vehicleEngine
	} = props;

	return <div className='hud-player-info'>
		{
			isVehicle && <Card size='small' linePos='right' widthMod='auto' className='mb-8'>
				<div className='vehicle-info-left'>
					<FontAwesomeIcon icon={faFireAlt} color={vehicleEngine ? '#72CC72' : '#E03232'}/>
					<FontAwesomeIcon icon={vehicleLocked ? faLock : faLock} color={vehicleLocked ? '#72CC72' : '#E03232'}/>
				</div>
				<span className='vehicle-speed'>{getVehicleSpeed(vehicleSpeed)}</span>
				<div className='vehicle-info-right'>
					<FontAwesomeIcon icon={faGasPump}/>
					<span>{getVehicleFuel(vehicleFuel)}</span>
				</div>
			</Card>
		}
		<Card size='small' linePos='right' widthMod='auto'>
			<FontAwesomeIcon icon={faMicrophone} color={speaking ? '#72CC72' : '#E03232'}/>
			<span className='player-cash'>{getCash(wallet)}</span>
			<FontAwesomeIcon icon={faWallet}/>
		</Card>
	</div>;
};

interface IPositionInfoProps {
	area: string;
	street: string;
	safeZoneLevel: SafeZoneLevels;
	safeZoneName: string;
}

const getSafeZoneClassName = (level: SafeZoneLevels): string => {
	switch (level) {
		case SafeZoneLevels.GREEN:
			return 'green';
		case SafeZoneLevels.YELLOW:
			return 'yellow';
		case SafeZoneLevels.RED:
			return 'red';
		default:
			return null;
	}
};

const PositionInfo = (props: IPositionInfoProps): React.ReactElement => {
	const { area, street, safeZoneLevel, safeZoneName } = props;

	const safeZoneClassName = getSafeZoneClassName(safeZoneLevel);

	return <div className='hud-position-info'>
		<Card size='small' widthMod='auto'>
			<div className='area'>
				{area}
			</div>
			<div className='street'>
				{street}
			</div>
			{
				getSafeZoneClassName(safeZoneLevel) && <div className={cn('zone', [safeZoneClassName])}>
					{safeZoneName}
				</div>
			}
		</Card>
	</div>;
};

interface IServerInfoProps {
	servername: string;
	online: number;
	time: string;
}

const renderLogoImg = (serverName: string): React.ReactNode => {
	if (!serverName) return null;
	return <img alt='server-logo' src={require(`../../../assets/${serverName}.png`)}/>;
};

const ServerInfo = (props: IServerInfoProps): React.ReactElement => {
	const { servername, online, time } = props;

	if (!servername) return null;

	return <div className='hud-server-info'>
		<div className='server-logo'>{renderLogoImg(servername)}</div>
		<div className='server-info'>
			<FontAwesomeIcon icon={faUser} color='#E51E2B'/> {online} | {moment(time).format('DD.MM.YYYY hh:mm')}
		</div>
	</div>;
};

interface IActionInfoProps {
	currentActionName: string;
}

const inputString = (value: string, className: string): React.ReactNode => {
	if (value === null || value === undefined || typeof value !== 'string') {
		return '';
	}
	const inputKeys = value.match(/~(.*?)~/gi);
	const array = value.split(/~(.*?)~/gi);

	return <div className='hud-action-input'>{array.map((item, index) => {
		return inputKeys?.includes(`~${item}~`)
			? inputDictionary[item] && <div key={index} className={className}>{inputDictionary[item]}</div>
			: <div key={index}>{item}</div>;
	})}</div>;
};

const ActionInfo = (props: IActionInfoProps): React.ReactElement => {
	const { currentActionName } = props;

	if (!currentActionName) return null;

	return <div className='hud-action-info'>
		<div className='hud-action-text'>{inputString(currentActionName, 'hud-action-input-label')}</div>
	</div>;
};

export const Hud = (): React.ReactElement => {
	const hudData = useSelector((state: StorageState) => state.hud);
	const {
		isPlayable,
		wallet,
		speaking,
		isVehicle,
		vehicleLocked,
		vehicleSpeed,
		vehicleFuel,
		vehicleEngine,
		area,
		street,
		safeZoneLevel,
		safeZoneName,
		servername,
		online,
		time,
		currentActionName
	} = hudData;

	React.useEffect(() => {
		receiveHudData();
	}, []);

	if (!hudData) return null;

	return <div className='hud'>
		<DebugInfoComponent/>
		<ServerInfo
			servername={servername}
			online={online}
			time={time}
		/>
		<Chat/>
		{
			isPlayable && <>
				<PositionInfo
					area={area}
					street={street}
					safeZoneLevel={safeZoneLevel}
					safeZoneName={safeZoneName}
				/>
				<ActionInfo currentActionName={currentActionName}/>
				<PlayerInfo
					wallet={wallet}
					speaking={speaking}
					isVehicle={isVehicle}
					vehicleLocked={vehicleLocked}
					vehicleSpeed={vehicleSpeed}
					vehicleFuel={vehicleFuel}
					vehicleEngine={vehicleEngine}
				/>
			</>
		}
	</div>;
};
