import React, { ChangeEvent } from 'react';
import cn from 'classnames';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { Card } from '../../../../shared/components/card';
import { IChatAPI } from './index';

import './styles.scss';
import { ChatMessage } from '../../../../../../../common/types/chat';
import { ChatLabels } from '../../../../../../../common/enums/chat';

declare let window: Window & {
	chatAPI: IChatAPI
};

export interface IActions {
	clearChat: () => void;
	showChat: (toggle: boolean) => void;
	pushMessage: (message: ChatMessage) => void;
	activateInput: (toggle: boolean) => void;
	addHistoryMessage: (message: string) => void;
	valueInput: (value: string) => void;
	upLabelPosition: (value: number) => void;
	updatePosition: (value: number) => void;
}

export interface IProps {
	active: boolean;
	labelPosition: number;
	historyPosition: number;
	historyMessages: string[];
	messages: ChatMessage[];
	value: string;
}

export type IModel = IProps & IActions;

export class View extends React.Component<IModel> {
	label: string[];
	inputChat: HTMLInputElement;
	messageList: HTMLDivElement;

	constructor(props: IModel) {
		super(props);

		window.chatAPI = {
			push: this.pushMessage.bind(this),
			send: this.sendMessage.bind(this),
			clear: this.props.clearChat,
			show: this.props.showChat,
			activate: this.props.activateInput,
			value: this.props.valueInput
		};

		if (mp.events) {
			const api = {
				'chat:push': window.chatAPI.push,
				'chat:clear': window.chatAPI.clear,
				'chat:activate': window.chatAPI.activate,
				'chat:show': window.chatAPI.show
			};

			for (const fn in api) {
				mp.events.add(fn, api[fn]);
			}
		}

		this.label = Object.values(ChatLabels).map(label => `/${label} `);
	}

	componentDidMount(): void {
		document.addEventListener('keyup', this.handleKeyUp.bind(this));
		document.addEventListener('keydown', this.handleKeyDown.bind(this));
		this.scrollToBottom();
	}

	componentWillUnmount(): void {
		document.addEventListener('keyup', this.handleKeyUp.bind(this));
		document.addEventListener('keydown', this.handleKeyDown.bind(this));
	}

	scrollToBottom(): void {
		const scrollHeight = this.messageList.scrollHeight;
		const height = this.messageList.clientHeight;
		const maxScrollTop = scrollHeight - height;
		this.messageList.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
	}

	pushMessage(message: ChatMessage): void {
		this.props.pushMessage(message);
		this.scrollToBottom();
	}

	sendMessage(message: string): void {
		if (message && message.length !== 0 && !this.label.includes(message)) {
			this.props.addHistoryMessage(message);
			this.props.valueInput('');
		}

		this.props.activateInput(false);
	}

	handleKeyDown(event: KeyboardEvent): void {
		if (!this.props.active) return;
		if (event.keyCode === 38) {
			event.preventDefault();
			return;
		}
		if (event.keyCode !== 9) return;

		event.preventDefault();

		if (this.props.labelPosition === this.label.length - 1) {
			this.props.upLabelPosition(-1);
			this.props.valueInput('');
		} else {
			this.props.upLabelPosition(parseInt(this.props.labelPosition + '', 10) + 1);
			this.props.valueInput(this.label[this.props.labelPosition]);
		}
	}

	handleKeyUp(event: KeyboardEvent): void {
		if (!this.props.active) return;

		switch (event.keyCode) {
			case 13: {
				event.preventDefault();
				this.sendMessage(this.inputChat.value);
				return;
			}
			case 27: {
				event.preventDefault();
				this.props.activateInput(false);
				return;
			}
			case 38: {
				if (this.props.historyPosition >= (this.props.historyMessages.length - 1)) return;
				this.props.valueInput(this.props.historyMessages[this.props.historyPosition + 1]);
				this.props.updatePosition(this.props.historyPosition + 1);
				return;
			}
			case 40: {
				if (this.props.historyPosition <= -1) return;
				if (this.props.historyPosition === 0) {
					this.props.updatePosition(-1);
					this.props.valueInput('');
					return;
				}
				this.props.valueInput(this.props.historyMessages[this.props.historyPosition - 1]);
				this.props.updatePosition(this.props.historyPosition - 1);
			}
		}
	}

	render(): React.ReactElement {
		return (
			<div id='chat' className='chat'>
				<div
					className={cn('chat-messages', { 'scroll-block': this.props.active })}
					ref={(div) => {
						this.messageList = div;
					}}
				>
					<ul>
						<TransitionGroup className="chat-list">
							{
								this.props.messages.map((message: ChatMessage, index: number) => (
									<CSSTransition
										classNames='item'
										appear
										enter
										key={index}
										timeout={500}
									>
										<ChatMessage message={message}/>
									</CSSTransition>
								))
							}
						</TransitionGroup>
					</ul>
				</div>
				<div className='chat-input-container'>
					{
						this.props.active && <Card size='medium' widthMod='full'>
							<input
								type='text'
								ref={(input) => {
									if (input) {
										this.inputChat = input;
										input.focus();
									}
								}}
								value={this.props.value}
								onChange={(event: ChangeEvent<HTMLInputElement>): void => {
									this.props.valueInput(event.target.value);
								}}
							/>
						</Card>
					}
				</div>
			</div>
		);
	}
}

const ChatMessage = ({ message }: { message: ChatMessage }): React.ReactElement => {
	if (typeof message === 'string') {
		return <li className='chat-message-unknown'>{message}</li>;
	}
	if (message.author && message.author.name === 'welcome') {
		return <li style={message.color ? { color: message.color } : null}>{message.text}</li>;
	}
	return <li>
		<span className='chat-message-sender'>{message.author.name}[{message.author.id}]: </span>
		<span className='chat-message-text'>{message.text}</span>
	</li>;
};
