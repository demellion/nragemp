import { connect } from 'react-redux';
import { compose } from 'recompose';
import rpc from 'rage-rpc';
import { IActions, IModel, IProps, View } from './view';
import { StorageState } from '../../../../../redux/reducers';
import {
	activateChat,
	addHistoryMessage,
	pushMessage,
	clearChat,
	setChatHistoryPosition,
	setChatLabelPosition,
	setChatValue,
	showChat
} from '../../../../../redux/actions/chat';
import { ChatMessage } from '../../../../../../../common/types/chat';

export interface IChatAPI {
	push: (message: ChatMessage) => void;
	clear: () => void;
	show: (toggle: boolean) => void;
	activate: (toggle: boolean) => void;
	send: (message: string) => void;
	value: (value: string) => void;
}

declare let window: Window & {
	chatAPI: IChatAPI
};

window.chatAPI = {
	push: () => {
	},
	clear: () => {
	},
	activate: () => {
	},
	show: () => {
	},
	send: () => {
	},
	value: () => {
	}
};

rpc.register('chat:activate', (toggle: boolean) => {
	window.chatAPI.activate(toggle);
});

rpc.register('chat:push', (message: ChatMessage) => {
	window.chatAPI.push(message);
});

const redux = connect<IProps, IActions, {}, StorageState>(
	(state: StorageState): IProps => ({
		active: state.chat.active,
		labelPosition: state.chat.labelPosition,
		historyPosition: state.chat.historyPosition,
		historyMessages: state.chat.historyMessages,
		messages: state.chat.messages,
		value: state.chat.value
	}),
	(dispatch: any) => ({
		clearChat: () => {
			dispatch(clearChat);
		},
		showChat: (toggle: boolean) => {
			dispatch(showChat(toggle));
		},
		pushMessage: (message: ChatMessage) => {
			dispatch(pushMessage(message));
		},
		activateInput: (toggle: boolean) => {
			dispatch(activateChat(toggle));
		},
		addHistoryMessage: (message: string) => {
			dispatch(addHistoryMessage(message));
		},
		valueInput: (value: string) => {
			dispatch(setChatValue(value));
		},
		upLabelPosition: (position: number) => {
			dispatch(setChatLabelPosition(position));
		},
		updatePosition: (position: number) => {
			dispatch(setChatHistoryPosition(position));
		}
	})
);

export const Chat = compose<IModel, {}>(
	redux
)(View);
