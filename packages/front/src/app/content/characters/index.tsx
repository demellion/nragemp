import React from 'react';

import './styles.scss';
import { useDispatch, useSelector } from 'react-redux';
import { getCharactersList, loadCharacter, setActiveCharacterByName, startCharacterCreation } from '../../../redux/actions/characters';
import { StorageState } from '../../../redux/reducers';
import { Card } from '../../shared/components/card';

export const CharactersPage = (): React.ReactElement => {
	const characters = useSelector((state: StorageState) => state.characters.list) || [];
	const character = useSelector((state: StorageState) => state.characters.active);

	const [isBusy, setIsBusy] = React.useState(false);

	const dispatch = useDispatch();

	const changeActiveCharacter = async (mod: number): Promise<void> => {
		if (isBusy) return;
		if (!character) return;
		const activeCharacterIndex = characters.findIndex(x => x.name === character.name);
		let index = activeCharacterIndex + mod;
		if (index < 0) index = characters.length - 1;
		if (index > (characters.length - 1)) index = 0;
		dispatch(setActiveCharacterByName(characters[index].name));
		setIsBusy(true);
		setTimeout(() => {
			setIsBusy(false);
		}, 500);
	};

	const enterFunction = (event: KeyboardEvent) => {
		if (event.key !== 'Enter') {
			return;
		}
		character && character.name && selectHandler();
	};

	React.useEffect(() => {
		document.addEventListener('keydown', enterFunction);
		dispatch(getCharactersList());
		return () => document.removeEventListener('keydown', enterFunction);
	}, []);

	const createHandler = () => dispatch(startCharacterCreation());

	const selectHandler = () => dispatch(loadCharacter(character.name));

	return <div className='characters-page'>
		<div className='left-panel'>
			{
				characters.length && <>
					<Card>
						<div className='header'>
							Панель информации
						</div>
						{
							character && <div className='list'>
								<div className='list-item'>
									<div className='list-item-left'>Имя</div>
									<div className='list-item-right'>{character.name}</div>
								</div>
								<div className='list-item'>
									<div className='list-item-left'>Уровень</div>
									<div className='list-item-right'>{character.level}</div>
								</div>
								<div className='list-item'>
									<div className='list-item-left'>Времени сыграно</div>
									<div className='list-item-right'>1927 часов</div>
								</div>
								<div className='list-item'>
									<div className='list-item-left'>Деньги</div>
									<div className='list-item-right'>$ {character.cash.toLocaleString('de')}</div>
								</div>
								<div className='list-item'>
									<div className='list-item-left'>Банк</div>
									<div className='list-item-right'>$ {character.bank.toLocaleString('de')}</div>
								</div>
								<div className='list-item'>
									<div className='list-item-left'>DP</div>
									<div className='list-item-right'>513</div>
								</div>
							</div>
						}
					</Card>
					<div className='panel-footer'>
						<button className='play-btn' onClick={selectHandler}>Играть</button>
					</div>
				</>
			}
		</div>
		<div className='right-panel'>
			<div className='panel-body'>
				<button className='left-btn btn' disabled={isBusy} onClick={() => changeActiveCharacter(-1)}>{'<'}</button>
				<button className='create-btn btn' onClick={createHandler}>Create</button>
				<button className='left-btn btn' disabled={isBusy} onClick={() => changeActiveCharacter(1)}>{'>'}</button>
			</div>
		</div>
	</div>;
};
