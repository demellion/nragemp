import { Routes } from 'common/constants/routes';
import { renderRoutes } from 'react-router-config';
import { BaseLayout } from '../shared/components/baseLayout';
import { AuthPage } from '../content/auth';
import { Hud } from '../content/hud';
import { CharactersPage } from '../content/characters';
import { VehShop } from '../content/vehShop';
import { CharEditor } from '../content/charEditor';

const routesConfig = [
	{
		path: Routes.auth,
		component: AuthPage
	},
	{
		path: Routes.characters,
		component: CharactersPage
	},
	{
		path: Routes.createCharacter,
		component: CharEditor
	},
	{
		path: Routes.vehShop,
		component: VehShop
	},
	{
		path: Routes.empty,
		component: BaseLayout,
		routes: [
			{
				path: Routes.hud,
				component: Hud
			}
		]
	}
];

export const Content = renderRoutes(routesConfig);
