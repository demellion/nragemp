import { StorageState } from '../redux/reducers';
import { configureStore } from '../redux/store/configureStore';
import { compose, lifecycle } from 'recompose';
import { RouteComponentProps } from 'react-router';
import { View } from './view';
import { ActionType } from '../redux/actions';
import { StringifedQueryStringParams } from './shared/pipes';
import './shared/utils/clientBridge';
import { setDebugData } from '../redux/actions/debug';
import { NREvents } from '../../../common/enums/nrEvents';
import rpc from 'rage-rpc';
import { sleep } from '../../../common/pipes/sleep';

export interface Store {
	dispatch: any;
	getState: () => StorageState;
}

export const store: Store = configureStore();

const escFunction = (event: any) => {
	if (event.keycode === 27) {
		store.dispatch({
			type: ActionType.ESCAPE_PRESSED
		});
	}
};

const receiveDebugData = async () => {
	const debugData: { [key: string]: unknown } = await rpc.callClient(NREvents.receiveDebugData);
	store.dispatch(setDebugData(debugData));
	await sleep(500);
	receiveDebugData();
};

const lifecycleHooks = lifecycle<any & RouteComponentProps<StringifedQueryStringParams>, {}, {}>({
	componentDidMount() {
		document.addEventListener('keydown', escFunction, false);
		receiveDebugData();
	},
	componentWillUnmount() {
		document.removeEventListener('keydown', escFunction, false);
	}
});

export const App = compose(lifecycleHooks)(View);
