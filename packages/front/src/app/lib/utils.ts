const platform = ((navigator && navigator.platform) || '').toLowerCase();

export const isMac = platform.includes('mac');
