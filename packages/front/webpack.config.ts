import path from 'path';
import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import StyleLintPlugin from 'stylelint-webpack-plugin';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';

type Env = 'development' | 'production' | 'none';

const NODE_MODULES_DIR = /node_modules/;
const NODE_ENV = process.env.NODE_ENV as Env || 'development';

const isDev = NODE_ENV === 'development';

const outputPath = path.resolve(__dirname, '../../', 'build', 'dist');
const sourcePath = path.join(__dirname, 'src');

const webpackConfig: webpack.Configuration = {
	mode: NODE_ENV || 'production',
	devtool: isDev ? 'inline-source-map' : undefined,
	stats: 'errors-only',
	context: sourcePath,
	entry: {
		main: path.join(sourcePath, 'index.tsx')
	},
	output: {
		publicPath: isDev ? '/' : './',
		path: outputPath,
		filename: '[name].js',
		sourceMapFilename: '[name].map',
		chunkFilename: 'vendors.js'
	},
	devServer: {
		historyApiFallback: true,
		contentBase: outputPath,
		compress: true,
		host: '0.0.0.0',
		hot: true,
		port: 8080
	},
	plugins: [
		new HtmlWebpackPlugin({
			filename: path.join(outputPath, 'index.html'),
			template: path.join(__dirname, 'templates', 'page-template.ejs')
		}),
		new StyleLintPlugin({
			files: '**/*.scss',
			configFile: path.resolve(__dirname, '.stylelintrc')
		}),
		new CleanWebpackPlugin()
	],
	module: {
		rules: [
			{
				test: /\.(js|jsx|tsx|ts)/,
				loader: 'ts-loader',
				exclude: NODE_MODULES_DIR,
				options: {
					configFile: 'tsconfig.json'
				}
			},
			{
				test: /(\.css|\.scss|\.sass)$/,
				use: [
					'style-loader',
					{
						loader: 'css-loader',
						options: {
							sourceMap: true
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							plugins: () => [require('autoprefixer')],
							sourceMap: true
						}
					},
					{
						loader: 'sass-loader',
						options: {
							sassOptions: {
								includePaths: [path.resolve(__dirname, 'src')]
							},
							sourceMap: true
						}
					}
				]
			},
			{
				test: /\.less$/,
				use: [
					{
						loader: 'style-loader'
					},
					{
						loader: 'css-loader'
					},
					{
						loader: 'less-loader',
						options: {
							lessOptions: {
								javascriptEnabled: true
							}
						}
					}
				]
			},
			{
				test: /\.(jpe?g|gif|png)$/i,
				use: [
					{
						loader: 'url-loader',
						options: {
							limit: 10000,
							esModule: false
						}
					}
				]
			},
			{
				test: /\.svg$/,
				use: [
					{
						loader: 'svg-url-loader',
						options: {
							limit: 10000
						}
					}
				]
			}
		]
	},
	resolve: {
		extensions: ['.js', '.jsx', '.ts', '.tsx', '.css', '.scss'],
		alias: {
			common: path.resolve(__dirname, '../common')
		}
	},
	optimization: {
		splitChunks: {
			chunks: 'all'
		}
	}
};

export default webpackConfig;
