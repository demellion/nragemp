import log4js from 'log4js';

export const LoggerLevels = {
	trace: 'trace',
	debug: 'debug',
	info: 'info',
	warn: 'warn',
	error: 'error',
	fatal: 'fatal'
};

export const getLogger = (level = LoggerLevels.debug) => {
	const logger = log4js.getLogger('editor');
	logger.level = level;
	return logger;
};
