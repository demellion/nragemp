import { getLogger, Loggers } from '../utils/logger';
import { nr, PlayerNR } from './wrapper';
import accountController from './controllers/account/account.controller';
import moment, { DurationInputArg2, Moment } from 'moment';
import { Location } from '../constants/locations';
import { timezone } from '../index';
import { wrapVehicle } from '../modules/vehicles';
import { getIsoString } from 'common/pipes/date';
import { localize } from 'common/index';
import { Player } from 'rage-rpc';
import { NREvents } from 'common/enums/nrEvents';
import { levelAPI } from '../classes/leveling';
import { currencyAPI } from '../classes/currency';
import { VipLevels } from 'common/enums/vipLevels';
import { Account, ServerAccount } from 'common/types/account';
import fs from 'fs';
import path from 'path';

const log = getLogger(Loggers.server);

// Core functions
export const random = (min: number, mid: number, max: number): number => {
	const leftArbitrary = Math.random() * (mid - min) + min;
	const rightArbitrary = Math.random() * (max - mid) + mid;
	return ((leftArbitrary + rightArbitrary) / 2);
};
export const selectRandom = (array: any[]): any => {
	return array[Math.floor(Math.random() * array.length)];
};
export const min = (value: number, to: number): number => {
	return value > to ? to : value;
};
export const max = (value: number, to: number): number => {
	return value < to ? to : value;
};

export const createHash = (length = 16): string => {
	const vocabulary = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'A', 'B', 'C', 'D', 'E', 'F'];
	let result = '';
	for (let x = 0; x < length; x++) {
		result += selectRandom(vocabulary);
	}
	log.debug(`Hash created: ${result}`);
	return result;
};

export const HEXtoRGB = (hex: string): RGB => {
	if (hex[0] !== '#') return [0, 0, 0];
	const h = hex.replace('#', '');
	let r = '0';
	let g = '0';
	let b = '0';
	if (h.length === 3) {
		r = '0x' + h[0] + h[0];
		g = '0x' + h[1] + h[1];
		b = '0x' + h[2] + h[2];
	}
	if (h.length === 6) {
		r = '0x' + h[0] + h[1];
		g = '0x' + h[2] + h[3];
		b = '0x' + h[4] + h[5];
	}
	return [+r, +g, +b];
};

export const getClosestVector = (arrayOfVectors: Vector3Mp[], object: EntityMp): Vector3Mp => {
	let closestDist: number;
	return arrayOfVectors.reduce((nearestVector, currentVector, i) => {
		if (i === 0) {
			closestDist = object.dist(currentVector);
			return currentVector;
		}

		const dist = object.dist(currentVector);

		if (dist < closestDist) {
			closestDist = dist;
			return currentVector;
		}
		return nearestVector;
	}, null);
};

export const getGender = (player: PlayerMp | Player): string => {
	if ([1885233650].includes(player.model)) return 'male';
	if ([2627665880].includes(player.model)) return 'female';
	return null;
};

export const addMomentTimezone = (value: number, type: DurationInputArg2, time = moment()): Moment => {
	return time.add(timezone, 'hours').add(value, type);
};

export const getClosestLocation = (arrayOfLocations: Location[], object: EntityMp): Location => {
	let closestDistance: number;
	return arrayOfLocations.reduce((nearestLocation, currentLocation, i) => {
		if (i === 0) {
			closestDistance = object.dist(currentLocation.position);
			return currentLocation;
		}

		const dist = object.dist(currentLocation.position);

		if (dist < closestDistance) {
			closestDistance = dist;
			return currentLocation;
		}
		return nearestLocation;
	}, null);
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const instanceOfVector = (object: any): object is Vector3Mp => {
	return (object.x !== undefined);
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const instanceOfEntity = (object: any): object is EntityMp => {
	return (object.model !== undefined);
};

export const playAnimation = (player: PlayerMp | Player, animationDict: string, animation: string, flag = 1, duration = 0, speed = 8.0): void => {
	player.setVariable('animationData', [
		[animationDict, animation],
		flag,
		duration,
		speed
	]);
};
export const getEntityFromId = (entityType: string, id: number): EntityMp => {
	if (entityType === 'player') return (mp.players.at(id));
	// @ts-ignore
	if (entityType === 'ped') return (mp.peds.at(id));
	if (entityType === 'vehicle') return (mp.vehicles.at(id));
	if (entityType === 'object') return (mp.objects.at(id));
	if (entityType === 'pickup') return (mp.pickups.at(id));
	if (entityType === 'blip') return (mp.blips.at(id));
	if (entityType === 'checkpoint') return (mp.checkpoints.at(id));
	if (entityType === 'marker') return (mp.markers.at(id));
	if (entityType === 'colshape') return (mp.colshapes.at(id));
	return null;
};

export const getCurrentVendorFromClient = async (player: Player | PlayerNR): Promise<EntityMp> => {
	const { type, id } = await nr.rpc.callClient(player, NREvents.getCurrentVendor);
	return getEntityFromId(type, id);
};
export const getVehicleSpeed = (velocity: Vector3Mp): number => {
	return Math.sqrt(Math.pow(velocity.x, 2) + Math.pow(velocity.y, 2) + Math.pow(velocity.z, 2));
};

export const doPayday = (player: PlayerNR | Player): void => {
	if (!player._characterLoaded) return;
	player._payTime++;
	player._playTime++;

	if (player._payTime >= 60) {
		player._payTime = 0;
		levelAPI.addXP(player, 100);
		currencyAPI.addCurrency(player, 'bank', 1000, 'payday');
	}
};

export const setVIPLevel = async (player: PlayerNR | Player, level: VipLevels, time: moment.Moment): Promise<void> => {
	const currentStamp = player._vipMoment || moment();
	if (currentStamp > time) return;
	const expiryDate = getIsoString(time);
	await accountController.saveAccount({
		username: player._userName,
		vip: {
			level,
			expiryDate
		}
	});
	player.setVariable('vipLevel', level);
	player._vipMoment = time;
};

export const findPlayerByID = (rgscId: string): PlayerMp => {
	mp.players.forEach((thisPlayer) => {
		if (thisPlayer.rgscId === rgscId) return thisPlayer;
	});
	return null;
};

export const setDesiredWantedLevel = (player: PlayerNR | Player, wantedLevel: number): boolean => {
	const currentWantedLevel = player.getVariable('wantedLevel') || 0;
	if (wantedLevel <= currentWantedLevel) return false;
	setWantedLevel(player, wantedLevel);
	return true;
};

export const setWantedLevel = (player: PlayerNR | Player, wantedLevel: number): void => {
	player.setVariable('wantedLevel', wantedLevel);
	nr.rpc.callClient(player, 'displayNotification', {
		title: localize.t('GENERIC.WANTED'),
		message: localize.t('GENERIC.WANTEDTEXT'),
		icon: 'DIA_POLICE',
		long: true
	}, { noRet: true });
};

// Service functions (server only)
export const banPlayerServer = async (violator: PlayerMp, reason: string, time: number, silent = false): Promise<void> => {
	const violatorID = violator.socialClub;
	const dateFrom = getIsoString(moment());
	const banDate = getIsoString(moment().add(time, 'days'));

	try {
		await accountController.banAccount({
			socialClub: violatorID,
			banInfo: {
				dateFrom,
				dateTo: banDate,
				issuerID: 'SERVER',
				reason
			}
		});
		log.debug(`NRage.banPlayerServer: ${violator.name}(${violatorID}) was banned for ${time} minutes, reason: ${reason}`);
		if (silent) return; // Do we broadcast this?

		const timeFormatted = moment(time, 'd').format('DD');
		// @ts-ignore
		violator.kickSilent('');
		// violator.kick(localize.t('PLAYER.BAN', { time: banDate.toLocaleString(), text: reason }));
		nr.players.broadcast(localize.t('CHAT.BAN', { name: violator.name, time: timeFormatted, text: reason }));
	} catch (error) {
		log.error(`NRage.banPlayerServer: ${error.message}`);
	}
};

export const kickPlayerServer = (violator: PlayerMp, reason: string, silent = false): void => {
	const violatorID = violator.socialClub;
	try {
		// @ts-ignore
		violator.kickSilent('');
		// violator.kick(localize.t('PLAYER.KICK', { text: reason }));
		log.debug(`NRage.kickPlayerServer: ${violator.name}(${violatorID}) was kicked. Reason: ${reason}`);
		if (silent) return; // Do we broadcast this?

		nr.players.broadcast(localize.t('CHAT.KICK', { name: violator.name, text: reason }));
	} catch (error) {
		log.error(`NRage.kickPlayerServer: ${error.message}`);
	}
};
export const createAdminVeh = (vehclass: string, issuer: PlayerMp, options?: {
	alpha?: number,
	color?: [Array2d, Array2d] | [RGB, RGB],
	dimension?: number,
	engine?: boolean,
	heading?: number;
	locked?: boolean,
	numberPlate?: string
}): void => {
	const vehicle = mp.vehicles.new(mp.joaat(vehclass), issuer.position, options);
	wrapVehicle(vehicle, vehclass, issuer);
	vehicle.numberPlate = 'NRAGE';
	issuer.putIntoVehicle(vehicle, RageEnums.VehicleSeat.DRIVER);
};
export const checkDuplicates = (player: PlayerNR | Player): void => {
	mp.players.forEach((thisPlayer) => {
		if (player === thisPlayer) return;
		if (player.rgscId === thisPlayer.rgscId) thisPlayer.kickSilent();
	});
};
export const getAccountFromServerAccount = (serverAccount: ServerAccount): Account => {
	if (!serverAccount) return null;

	const account = serverAccount;
	account.password = undefined;

	return account;
};
export const config = (): { [k: string]: any } => {
	try {
		const file = path.resolve(process.cwd(), '', 'conf.json');
		const data = fs.readFileSync(file, 'utf-8');
		return JSON.parse(data);
	} catch (error) {
		log.fatal('NRage.config: Unable to load server conf.json');
		return {};
	}
};