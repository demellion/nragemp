// Static body
import { Schema, Document, model } from 'mongoose';

/*
	Interfaces
*/

// Main collection
export interface IVehicle {
	key: string,
	modelHash: string,
	rgscId: string,
	position?: [number, number, number],
	heading?: number,
	mods?: IVehicleMods,
	fuel?: number,
	impounded?: boolean,
	inventory?: string,
	numberplate?: string
}

export interface IVehicleMods {
	neonColor: RGB;
	colorPearlescent: number;
	colorPrimary: RGB;
	colorSecondary: RGB;
	tireSmoke: RGB;
	gps: boolean;
	remote: boolean;

	// Vanilla
	spoiler: number;
	frontBumper: number;
	rearBumper: number;
	sideSkirt: number;
	exhaust: number;
	frame: number;
	grille: number;
	hood: number;
	fender: number;
	rightFender: number;
	roof: number;
	engine: number;
	brakes: number;
	transmission: number;
	horns: number;
	suspension: number;
	armor: number;
	turbo: number;
	xenon: number;
	frontWheels: number;
	utilShadowSilver: number;
	backWheels: number;
	plateHolders: number;
	trimDesign: number;
	ornaments: number;
	dialDesign: number;
	steeringWheel: number;
	shiftLever: number;
	plaques: number;
	hydraulics: number;
	boost: number;
	windowTint: number;
	livery: number;
	plate: number;
}

/*
	Schemas
*/

const VehicleSchema = new Schema({
	key: {
		type: String,
		required: true,
		unique: true
	},
	modelHash: { type: String, required: true },
	rgscId: { type: String, required: true },
	position: { type: Array, of: Number, default: null },
	heading: { type: Number, default: 0 },
	mods: { type: Object, default: null },
	fuel: { type: Number, default: null },
	impounded: { type: Boolean, default: false },
	inventory: { type: JSON, default: null },
	numberplate: { type: String, default: null }
});

export default model<IVehicle & Document>('Vehicle', VehicleSchema);
