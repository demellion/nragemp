import { IsoString } from 'common/types/date';
import { Document, model, Schema } from 'mongoose';

export interface ISystem {
	lastStartup: IsoString;
	lastPayday: IsoString;
	startupCount: number;
}

const SystemSchema = new Schema({
	lastStartup: { type: String, default: null },
	lastPayday: { type: String, default: null },
	startupCount: { type: Number, default: 0 }
});

export default model<ISystem & Document>('System', SystemSchema);
