import { Document, model, Schema } from 'mongoose';
import { defaultCustomizationData, faceFeatures, headOverlays } from 'common/constants/customizationData';
import { ServerAccount } from 'common/types/account';
import { AdminLevels } from 'common/enums/adminLevels';
import moment from 'moment';
import { getIsoString } from 'common/pipes/date';

// Bans
const AccountBanSchema = new Schema({
	dateFrom: { type: String, required: true },
	dateTo: { type: String, required: true },
	issuerID: { type: String, required: true },
	reason: { type: String, required: true }
});

// Stats
const StatsSchema = new Schema({
	health: { type: Number, required: true },
	armor: { type: Number, required: true },
	hunger: { type: Number, required: true },
	thirst: { type: Number, required: true },
	mood: { type: Number, required: true }
});

const VipSchema = new Schema({
	level: { type: Number, required: true },
	expiryDate: { type: String, required: true } // Date until VIP is active.
});

const AwardsSchema = new Schema({
	type: { type: String, required: true },
	description: { type: String, required: true },
	data: { type: Schema.Types.Mixed, required: true },
	timestamp: { type: String, default: getIsoString(moment()) }
});

const FaceFeaturesSchema = new Schema({
	noseWidth: { type: Number, required: true, default: 0 },
	noseHeight: { type: Number, required: true, default: 0 },
	noseLength: { type: Number, required: true, default: 0 },
	noseBridge: { type: Number, required: true, default: 0 },
	noseTip: { type: Number, required: true, default: 0 },
	noseBridgeShift: { type: Number, required: true, default: 0 },
	browHeight: { type: Number, required: true, default: 0 },
	browWidth: { type: Number, required: true, default: 0 },
	cheekBoneHeight: { type: Number, required: true, default: 0 },
	cheekBoneWidth: { type: Number, required: true, default: 0 },
	cheeksWidth: { type: Number, required: true, default: 0 },
	eyes: { type: Number, required: true, default: 0 },
	lips: { type: Number, required: true, default: 0 },
	jawWidth: { type: Number, required: true, default: 0 },
	jawHeight: { type: Number, required: true, default: 0 },
	chinLength: { type: Number, required: true, default: 0 },
	chinPosition: { type: Number, required: true, default: 0 },
	chinWidth: { type: Number, required: true, default: 0 },
	chinShape: { type: Number, required: true, default: 0 },
	neckWidth: { type: Number, required: true, default: 0 }
});

const HeadOverlaysSchema = new Schema({
	blemishes: { type: Number, required: true, default: 255 },
	facialHair: { type: Number, required: true, default: 255 },
	eyebrows: { type: Number, required: true, default: 255 },
	ageing: { type: Number, required: true, default: 255 },
	makeup: { type: Number, required: true, default: 255 },
	blush: { type: Number, required: true, default: 255 },
	complexion: { type: Number, required: true, default: 255 },
	sunDamage: { type: Number, required: true, default: 255 },
	lipstick: { type: Number, required: true, default: 255 },
	moles: { type: Number, required: true, default: 255 },
	chestHair: { type: Number, required: true, default: 255 },
	bodyBlemishes: { type: Number, required: true, default: 255 },
	addBodyBlemishes: { type: Number, required: true, default: 255 }
});

const CustomizationDataSchema = new Schema({
	gender: { type: Number, required: true, default: 1 },
	father: { type: Number, required: true, default: 0 },
	mother: { type: Number, required: true, default: 0 },
	similar: { type: Number, required: true, default: 0.5 },
	hair: { type: Number, required: true, default: 0 },
	hairPrimaryColor: { type: Number, required: true, default: 0 },
	hairSecondaryColor: { type: Number, required: true, default: 0 },
	facialHairColor: { type: Number, required: true, default: 0 },
	eyeColor: { type: Number, required: true, default: 0 },
	headOverlays: { type: HeadOverlaysSchema, required: true, default: headOverlays },
	faceFeatures: { type: FaceFeaturesSchema, required: true, default: faceFeatures }
});

const CharacterSchema = new Schema({
	name: {
		type: String,
		required: true,
		unique: true
	},
	age: { type: Number, default: 18 },
	level: { type: Number, default: 1 },
	experience: { type: Number, default: 0 },
	cash: { type: Number, default: 1000 },
	bank: { type: Number, default: 0 },
	paytime: { type: Number, default: 0 },
	faction: { type: Number, default: -1 },
	organization: { type: Number, default: -1 },
	stats: { type: StatsSchema, default: { health: 100, armor: 0, hunger: 100, thirst: 100, mood: 100 } },
	inventory: { type: JSON, default: null },
	generic: { type: JSON, default: null },
	modification: { type: JSON, default: null },
	position: { type: Array, of: Number, default: null },
	heading: { type: Number, default: 0 },
	friends: { type: JSON, default: null },
	jailed: { type: Boolean, default: false },
	warned: { type: Boolean, default: false },
	lastCarId: { type: String, default: null },
	cars: { type: Array, of: String, default: [] },
	houseId: { type: String, default: null },
	customizationData: { type: CustomizationDataSchema, default: defaultCustomizationData },
	wantedLevel: { type: Number, default: 0 }
});

// Account body
const AccountSchema = new Schema({
	username: {
		type: String,
		required: true,
		unique: true
	},
	password: {
		type: String,
		required: true
	},
	adminLevel: { type: AdminLevels, default: 0 },
	serial: { type: String, default: null },
	rgscId: { type: String, default: null },
	socialClub: { type: String, default: null },
	vip: { type: VipSchema, default: null },
	subscribers: { type: Number, default: null },
	currency: { type: Number, default: 0 },
	blacklisted: { type: Boolean, default: false }, // Specific account restrictions
	email: { type: String, default: null },
	dateOfBirth: { type: Date, default: null },
	lastIP: { type: String, default: null },
	registeredIP: { type: String, default: null },
	registeredDate: { type: Date, default: new Date() },
	lastConnected: { type: Date, default: new Date() },
	lastDisconnected: { type: Date, default: new Date() },
	playtime: { type: Number, default: 0 }, // Total playtime in minutes
	promo: { type: [String], default: null }, // Promo codes used
	currencyHistory: { type: [String], default: null }, // Array of JSON strings with conversion data
	awards: { type: Array, of: AwardsSchema, default: null }, // Array of Awards
	banInfo: { type: AccountBanSchema, default: null },
	characters: { type: Array, of: CharacterSchema, default: null }
});

export default model<ServerAccount & Document>('Account', AccountSchema);

// TODO
//
//
// AccountSchema.statics.findAccountByName = async function (id: string) {
// 	return this.findById(id).exec();
// };
//
//
