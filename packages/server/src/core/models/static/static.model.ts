// Static body
import { Schema, Document, model } from 'mongoose';

/*
	Interfaces
*/

// Main collection
export interface IStatic {
	alias: string,
	enabled?: boolean,
	reference?: string,
	type?: string
}

/*
	Schemas
*/

const StaticSchema = new Schema({
	alias: {
		type: String,
		required: true,
		unique: true
	},
	enabled: { type: Boolean, default: true },
	reference: { type: String, default: '' },
	type: { type: String, default: '' }
});

export default model<IStatic & Document>('Static', StaticSchema);
