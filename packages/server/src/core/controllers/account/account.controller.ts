import Account from '../../models/account/acount.model';
import { BanAccount } from 'common/types/account/ban';
import { ServerAccount } from 'common/types/account';
import { Character } from 'common/types/character';
import { FilterQuery } from 'mongoose';

interface BanAccountInput {
	socialClub: BanAccount['socialClub'],
	banInfo: BanAccount['banInfo']
}

interface WarnAccountInput {
	socialClub: ServerAccount['socialClub'],
	blacklisted: ServerAccount['blacklisted']
}

export default {
	createAccount: async (account: ServerAccount): Promise<ServerAccount> => {
		return Account.create(account);
	},
	saveAccount: async (schema: Partial<ServerAccount>): Promise<void> => {
		return Account.updateOne({ username: schema.username }, schema);
	},
	findAccount: async (schema: FilterQuery<ServerAccount>, cb?: (...args: any[]) => void): Promise<ServerAccount> => {
		return Account.findOne(schema, cb);
	},
	findAccounts: async (schema: FilterQuery<ServerAccount>, cb?: (...args: any[]) => void): Promise<ServerAccount[]> => {
		return Account.find(schema, cb);
	},
	banAccount: async ({ socialClub, banInfo }: BanAccountInput): Promise<void> => {
		Account.update({ socialClub }, { banInfo });
	},
	warnAccount: async ({ socialClub, blacklisted }: WarnAccountInput): Promise<void> => {
		Account.update({ socialClub }, { blacklisted });
	},
	getCharacters: async (username: string): Promise<Character[]> => {
		const account = await Account.findOne({ username });
		return account.characters;
	},
	createCharacter: async (username: string, character: Character): Promise<void> => {
		const account = await Account.findOne({ username });
		account.characters = account.characters || [];
		account.characters.push(character);
		await account.save();
	},
	findCharacter: async (name: string): Promise<Character> => {
		const account = await Account.findOne({ 'characters.name': name });
		if (!account) return null;
		return account.characters && account.characters.find(x => x.name === name);
	},
	findCharacterAccount: async (name: string, rgscId: string): Promise<Character> => {
		const account = await Account.findOne({ 'characters.name': name, rgscId });
		if (!account) return null;
		return account.characters && account.characters.find(x => x.name === name);
	},
	updateCharacter: async (name: string, schema: Partial<Character>): Promise<void> => {
		const account = await Account.findOne({ 'characters.name': name });
		if (!account) return;
		const character = account.characters.find(x => x.name === name);
		const characterIndex = account.characters.findIndex(x => x.name === name);
		// @ts-ignore TODO: Настроить типы mongoose
		account.characters.set(characterIndex, { ...character, ...schema });
		await account.save();
	}
};
