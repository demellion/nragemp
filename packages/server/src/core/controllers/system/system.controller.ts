import System, { ISystem } from '../../models/system/system.model';
import moment from 'moment';
import { getIsoString } from 'common/pipes/date';

export default {
	load: async (): Promise<ISystem> => {
		return System.findOne({});
	},
	create: async (): Promise<ISystem> => {
		const lastStartup = getIsoString(moment());
		return await System.create({
			lastStartup,
			lastPayday: null,
			startupCount: 1
		});
	},
	update: async (args: Partial<ISystem>): Promise<void> => {
		await System.updateOne({}, args);
	}
};