import Vehicle, { IVehicle } from '../../models/vehicle/vehicle.model';
import accountController from '../account/account.controller';
import { Player } from 'rage-rpc';
import { log } from '../../../index';

export default {
	loadVehicle: async (schema: Partial<IVehicle>, cb?: (...args: any[]) => void): Promise<IVehicle> => {
		return Vehicle.findOne(schema, cb);
	},
	createVehicle: async (key: string, modelHash: string, owner: PlayerMp | Player): Promise<IVehicle> => {
		const rgscId = owner.rgscId;
		log.debug('Updating character info...');
		const carsArray = owner.getVariable('carsArray') || [];
		carsArray.push(key);
		await accountController.updateCharacter(owner.name, { cars: carsArray, lastCarId: key }).catch(log.error);
		owner.setVariable('carsArray', carsArray);
		log.debug('Updated! Creating vehicle in DB...');
		return await Vehicle.create({ key, modelHash, rgscId });
	},
	updateVehicle: async (schema: Partial<IVehicle>, data: Partial<IVehicle>): Promise<void> => {
		return Vehicle.updateOne(schema, data);
	}
};
