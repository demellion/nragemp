import Static, { IStatic } from '../../models/static/static.model';

export default {
	loadStatic: async (alias: string, cb?: (...args: any[]) => void): Promise<IStatic> => {
		return Static.findOne({ alias }, cb);
	},
	createStatic: async (alias: string): Promise<IStatic> => {
		return await Static.create({ alias });
	},
	updateStatic: async (alias: string, ...args: any[]): Promise<void> => {
		return Static.updateOne({ alias }, { ...args });
	}
};
