import { getLogger, Loggers } from '../utils/logger';
import rpc, { CallOptions, Player, ProcedureListener } from 'rage-rpc';
import { InventoryPlayer } from '../classes/inventory';
import moment from 'moment';

const log = getLogger(Loggers.server);

export interface PlayerNR extends PlayerMp {
	_userName: string;
	_email: string;
	_previewVehicle: VehicleMp;
	_noSaveState: boolean;
	_fixedPosition: Vector3Mp;
	_fixedInventory: InventoryPlayer;
	_payTime: number;
	_playTime: number;
	_activePromoCodes: string[];
	_vipMoment: moment.Moment;
	_subscribers: number;
	_characterLoaded: boolean;
}

export interface VehicleNR extends VehicleMp {

}

export interface ObjectNR extends ObjectMp {

}

export interface PedNR extends EntityMp, PedMp {

}

class NRWrap {
	Vector3 = mp.Vector3;
	joaat = mp.joaat;
	Event = mp.Event;

	public rpc = {
		register: (name: string, cb: ProcedureListener): Function => {
			try {
				const r = rpc.register(name, cb);
				log.debug(`NRage.rpc.register: Registered "${name}" event`);
				return r;
			} catch (error) {
				log.error(`NRage.rpc.register: ${error.message}`);
			}
		},
		unregister: (name: string): void => {
			try {
				rpc.unregister(name);
				log.debug(`NRage.rpc.unregister: Unregistered "${name}" event`);
			} catch (error) {
				log.error(`NRage.rpc.unregister: ${error.message}`);
			}
		},
		call: <T = any>(name: string, args?: any, options?: CallOptions): Promise<T> => {
			try {
				const r = rpc.call<T>(name, args, options);
				log.trace(`NRage.rpc.call: Called "${name}" event with args: ${JSON.stringify(args)} and options: ${JSON.stringify(options)}`);
				return r;
			} catch (error) {
				log.error(`NRage.rpc.call: ${error.message}`);
			}
		},
		callServer: <T = any>(name: string, args?: any, options?: CallOptions): Promise<T> => {
			try {
				const r = rpc.callServer<T>(name, args, options);
				log.trace(`NRage.rpc.callServer: Called "${name}" server event with args: ${JSON.stringify(args)} and options: ${JSON.stringify(options)}`);
				return r;
			} catch (error) {
				log.error(`NRage.rpc.callServer: ${error.message}`);
			}
		},
		on: (name: string, cb: ProcedureListener): Function => {
			try {
				const r = rpc.on(name, cb);
				log.debug(`NRage.rpc.on: ${name}`);
				return r;
			} catch (error) {
				log.error(`NRage.rpc.on: ${error.message}`);
			}
		},
		off: (name: string, cb: ProcedureListener): void => {
			try {
				rpc.off(name, cb);
				log.debug(`NRage.rpc.off: ${name}`);
			} catch (error) {
				log.error(`NRage.rpc.off: ${error.message}`);
			}
		},
		trigger: (name: string, args?: any): void => {
			try {
				rpc.trigger(name, args);
				log.trace(`NRage.rpc.trigger: ${name}`);
			} catch (error) {
				log.debug(`NRage.rpc.trigger: ${error.message}`);
			}
		},
		triggerServer: (name: string, args?: any): void => {
			try {
				rpc.triggerServer(name, args);
				log.trace(`NRage.rpc.triggerServer: ${name}`);
			} catch (error) {
				log.debug(`NRage.rpc.triggerServer: ${error.message}`);
			}
		},
		// Server only
		callClient: <T = any>(player: Player, name: string, args?: any, options?: CallOptions): Promise<T> => {
			try {
				const r = rpc.callClient<T>(player, name, args, options);
				log.trace(`NRage.rpc.callClient: Called "${name}" client event with args: ${JSON.stringify(args)} and options: ${JSON.stringify(options)}`);
				return r;
			} catch (error) {
				log.error(`NRage.rpc.callClient: ${error.message}`);
			}
		},
		callBrowsers: <T = any>(player: Player, name: string, args?: any, options?: CallOptions): Promise<T> => {
			try {
				const r = rpc.callBrowsers<T>(player, name, args, options);
				log.trace(`NRage.rpc.callBrowsers: Called "${name}" browser event with args: ${JSON.stringify(args)} and options: ${JSON.stringify(options)}`);
				return r;
			} catch (error) {
				log.error(`NRage.rpc.callBrowsers: ${error.message}`);
			}
		},
		triggerClient: (player: Player, name: string, args?: any): void => {
			try {
				rpc.triggerClient(player, name, args);
				log.trace(`NRage.rpc.triggerClient: ${name}`);
			} catch (error) {
				log.error(`NRage.rpc.triggerClient: ${error.message}`);
			}
		},
		triggerBrowsers: (player: Player, name: string, args?: any): void => {
			try {
				rpc.triggerBrowsers(player, name, args);
				log.trace(`NRage.rpc.triggerBrowsers: ${name}`);
			} catch (error) {
				log.error(`NRage.rpc.triggerBrowsers: ${error.message}`);
			}
		}
	};

	public players = {
		...mp.players,
		length: mp.players.length,
		size: mp.players.size,
		broadcast: (text: string): void => {
			try {
				mp.players.broadcast(text);
				log.debug(`NRage.Players.broadcast: Broadcast: "${text}"`);
			} catch (error) {
				log.error(`NRage.Players.broadcast: ${error.message}`);
			}
		},

		broadcastInRange: (position: Vector3Mp, range: number, text: string): void => {
			try {
				mp.players.broadcastInRange(position, range, text);
				log.debug(`NRage.Players.broadcastInRange: Broadcast: "${text}" in range: ${range}`);
			} catch (error) {
				log.error(`NRage.Players.broadcastInRange: ${error.message}`);
			}
		},

		broadcastInDimension: (position: Vector3Mp, range: number, dimension: number, text: string): void => {
			try {
				mp.players.broadcastInRange(position, range, dimension, text);
				log.debug(`NRage.Players.broadcastInDimension: Broadcast: "${text}" to dimension: ${dimension}`);
			} catch (error) {
				log.error(`NRage.Players.broadcastInDimension: ${error.message}`);
			}
		},

		broadcastInDimensionRange: (position: Vector3Mp, range: number, dimension: number, text: string): void => {
			try {
				mp.players.broadcastInRange(position, range, dimension, text);
				log.debug(`NRage.Players.broadcastInDimensionRange: Broadcast: "${text}" to dimension: ${dimension} in range: ${range}`);
			} catch (error) {
				log.error(`NRage.Players.broadcastInDimensionRange: ${error.message}`);
			}
		},

		call: (eventName: string, ...args: any[]): void => {
			try {
				mp.players.call(eventName, ...args);
				log.trace(`NRage.Players.Call: Called "${eventName}" with args: ${args}`);
			} catch (error) {
				log.error(`NRage.Players.Call: ${error.message}`);
			}
		},

		callArray: (eventName: string, ...args: any[]): void => {
			try {
				mp.players.call(eventName, ...args);
				log.trace(`NRage.Players.CallArray: Called "${eventName}" for ${mp.players.toArray()} with args: ${args}`);
			} catch (error) {
				log.error(`NRage.Players.CallArray: ${error.message}`);
			}
		},

		callInDimension: (dimension: number, eventName: string, ...args: any[]): void => {
			try {
				mp.players.callInDimension(dimension, eventName, ...args);
				log.trace(`NRage.Players.CallInDimension: Called "${eventName}" in dimension ${dimension} with args: ${args}`);
			} catch (error) {
				log.error(`NRage.Players.CallInDimension: ${error.message}`);
			}
		},

		callInRange: (position: Vector3Mp, range: number, eventName: string, ...args: any[]): void => {
			try {
				mp.players.callInRange(position, range, eventName, ...args);
				log.trace(`NRage.Players.CallInRange: Called "${eventName}" at ${position}(${range} meters) with args: ${args}`);
			} catch (error) {
				log.error(`NRage.Players.CallInRange: ${error.message}`);
			}
		}
	};

	public vehicles = {
		...mp.vehicles,
		length: mp.vehicles.length,
		size: mp.vehicles.size,
		new: (model: RageEnums.Hashes.Vehicle | HashOrString, position: Vector3Mp, options?: {
			alpha?: number,
			color?: [Array2d, Array2d] | [RGB, RGB],
			dimension?: number,
			engine?: boolean,
			heading?: number;
			locked?: boolean,
			numberPlate?: string
		}): VehicleMp => {
			try {
				log.debug(`NRage.Vehicles.new: Created vehicle ${model} at ${position} (${options})`);
				return mp.vehicles.new(model, position, options);
			} catch (error) {
				log.error(`NRage.Vehicles.new: ${error.message}`);
			}
		}
	};

	public objects = {
		...mp.objects,
		length: mp.objects.length,
		size: mp.objects.size,
		new: (model: HashOrString, position: Vector3Mp, options?: {
			alpha?: number,
			dimension?: number,
			rotation?: Vector3Mp
		}): ObjectMp => {
			try {
				log.trace(`NRage.Objects.new: Created object ${model} at ${position} (${options})`);
				return mp.objects.new(model, position, options);
			} catch (error) {
				log.error(`NRage.Objects.new: ${error.message}`);
			}
		}
	};

	public pickups = {
		...mp.pickups,
		length: mp.pickups.length,
		size: mp.pickups.size,
		new: (...args: any[]): PickupMp => {
			try {
				log.trace(`NRage.Pickups.new: Created pickup (${args})`);
				return mp.pickups.new(...args);
			} catch (error) {
				log.error(`NRage.Pickups.new: ${error.message}`);
			}
		}
	};

	public blips = {
		...mp.blips,
		length: mp.blips.length,
		size: mp.blips.size,
		new: (sprite: number, position: Vector3Mp, options?: {
			alpha?: number,
			color?: number,
			dimension?: number,
			drawDistance?: number,
			name?: string,
			rotation?: number,
			scale?: number,
			shortRange?: boolean
		}): BlipMp => {
			try {
				log.trace(`NRage.Blips.new: Created blip ${sprite} at ${position} (${options})`);
				return mp.blips.new(sprite, position, options);
			} catch (error) {
				log.error(`NRage.Blips.new: ${error.message}`);
			}
		}
	};

	public peds = {
		...mp.peds,
		length: mp.peds.length,
		size: mp.peds.size,

		new: (modelHash: string | number, position: Vector3Mp, options?: {
			dynamic: boolean,
			frozen: boolean,
			invincible: boolean
		}): PedMp & EntityMp => {
			try {
				const hash = (typeof modelHash === 'string') ? mp.joaat(modelHash) : modelHash;
				log.trace(`NRage.Peds.new: Created ${modelHash} at [${position.x}, ${position.y}, ${position.z}]`);
				return mp.peds.new(hash, position, options) as (PedMp & EntityMp);
			} catch (error) {
				log.error(`NRage.Peds.new: ${error.message}`);
			}
		}
	};

	public colshapes = {
		...mp.colshapes,
		length: mp.colshapes.length,
		size: mp.colshapes.size,

		newCircle: (x: number, y: number, range: number, dimension?: number): ColshapeMp => {
			try {
				log.trace(`NRage.Colshapes.newCircle: Created new colshape at [${x}, ${y}]`);
				return mp.colshapes.newCircle(x, y, range, dimension);
			} catch (error) {
				log.error(`NRage.Colshapes.newCircle: ${error.message}`);
			}
		},
		newCuboid: (x: number, y: number, z: number, width: number, depth: number, height: number): ColshapeMp => {
			try {
				log.trace(`NRage.Colshapes.newCuboid: Created new colshape at [${x}, ${y}, ${z}]`);
				return mp.colshapes.newCuboid(x, y, z, width, depth, height);
			} catch (error) {
				log.error(`NRage.Colshapes.newCuboid: ${error.message}`);
			}
		},
		newRectangle: (x: number, y: number, width: number, height: number): ColshapeMp => {
			try {
				log.trace(`NRage.Colshapes.newRectangle: Created new colshape at [${x}, ${y}]`);
				return mp.colshapes.newRectangle(x, y, width, height);
			} catch (error) {
				log.error(`NRage.Colshapes.newRectangle: ${error.message}`);
			}
		},
		newSphere: (x: number, y: number, z: number, range: number, dimension?: number): ColshapeMp => {
			try {
				log.trace(`NRage.Colshapes.newSphere: Created new colshape at [${x}, ${y}, ${z}]`);
				return mp.colshapes.newSphere(x, y, z, range, dimension);
			} catch (error) {
				log.error(`NRage.Colshapes.newSphere: ${error.message}`);
			}
		},
		newTube: (x: number, y: number, z: number, range: number, height: number): ColshapeMp => {
			try {
				log.trace(`NRage.Colshapes.newTube: Created new colshape at [${x}, ${y}, ${z}]`);
				return mp.colshapes.newTube(x, y, z, range, height);
			} catch (error) {
				log.error(`NRage.Colshapes.newTube: ${error.message}`);
			}
		}
	};

	public markers = {
		...mp.markers,
		length: mp.markers.length,
		size: mp.markers.size,
		new: (type: number, position: Vector3Mp, scale: number, options?: {
			color?: RGBA,
			dimension?: number,
			direction?: Vector3Mp,
			rotation?: Vector3Mp,
			visible?: boolean
		}): MarkerMp => {
			try {
				log.trace(`NRage.Markers.new: Created marker ${type} at ${position}, radius: ${scale}`);
				return mp.markers.new(type, position, scale, options);
			} catch (error) {
				log.error(`NRage.Markers.new: ${error.message}`);
			}
		}
	};

	public checkpoints = {
		...mp.checkpoints,
		length: mp.checkpoints.length,
		size: mp.checkpoints.size,
		new: (type: number, position: Vector3Mp, radius: number, options?: {
			color?: RGBA,
			dimension?: number,
			direction?: Vector3Mp,
			visible?: boolean
		}): CheckpointMp => {
			try {
				log.trace(`NRage.Checkpoints.new: Created checkpoint ${type} at ${position}, radius: ${radius} (${options})`);
				return mp.checkpoints.new(type, position, radius, options);
			} catch (error) {
				log.error(`NRage.Checkpoints.new: ${error.message}`);
			}
		}
	};

	public events = {
		add: (eventName: RageEnums.EventKey | string, cb: (...args: any[]) => void): void => {
			try {
				mp.events.add(eventName, cb);
				log.debug(`NRage.Events.add: Added "${eventName}" event`);
			} catch (error) {
				log.error(`NRage.Events.add: ${error.message}`);
			}
		},

		addArray: (events: ({ [name: string]: (...args: any[]) => void; })): void => {
			try {
				mp.events.add(events);
				log.trace(`NRage.Events.addArray: Added events: ${events}`);
			} catch (error) {
				log.error(`NRage.Events.addArray: ${error.message}`);
			}
		},

		addCommand: (command: string, cb: (player: PlayerMp, fullText: string, ...args: string[]) => void): void => {
			try {
				mp.events.addCommand(command, cb);
				log.debug(`NRage.Events.addCommand: Added /${command} to commands list.`);
			} catch (error) {
				log.error(`NRage.Events.addCommand: ${error.message}`);
			}
		},

		addCommandArray: (commands: { [commandName: string]: (player: PlayerMp, fullText: string, ...args: string[]) => void; }): void => {
			try {
				mp.events.addCommand(commands);
				log.debug(`NRage.Events.addCommandArray: Added ${commands}`);
			} catch (error) {
				log.error(`NRage.Events.addCommandArray: ${error.message}`);
			}
		},

		call: (eventName: string, ...args: any[]): void => {
			try {
				mp.events.call(eventName, ...args);
				log.trace(`NRage.Events.call: Called "${eventName}" event`);
			} catch (error) {
				log.error(`NRage.Events.call: ${error.message}`);
			}
		},

		callLocal: (eventName: string, ...args: any[]): void => {
			try {
				mp.events.callLocal(eventName, ...args);
				log.trace(`NRage.Events.callLocal: Called "${eventName}" event`);
			} catch (error) {
				log.error(`NRage.Events.callLocal: ${error.message}`);
			}
		},

		delayShutdown: (bool: boolean): void => {
			try {
				mp.events.delayShutdown = bool;
				log.trace(`NRage.Events.delayShutdown: Set to ${bool}`);
			} catch (error) {
				log.error(`NRage.Events.delayShutdown: ${error.message}`);
			}
		},

		delayInitialization: (bool: boolean): void => {
			try {
				mp.events.delayInitialization = bool;
				log.trace(`NRage.Events.delayInitialization: Set to ${bool}`);
			} catch (error) {
				log.error(`NRage.Events.delayInitialization: ${error.message}`);
			}
		},

		getAllOf: (eventName: string): void => {
			mp.events.getAllOf(eventName);
		},

		remove: (eventName: string, handler?: (...args: any[]) => void): void => {
			try {
				mp.events.remove(eventName, handler);
				log.warn(`NRage.Events.remove: Removed "${eventName}" event with ID:${handler}`);
			} catch (error) {
				log.error(`NRage.Events.remove: ${error.message}`);
			}
		},

		reset: (): void => {
			try {
				mp.events.reset();
				log.warn('NRage.Events.reset: Events manager was reset, all events are purged!');
			} catch (error) {
				log.error(`NRage.Events.reset: ${error.message}`);
			}
		}
	};

	public config = {
		announce: mp.config.announce,
		bind: mp.config.bind,
		gamemode: mp.config.gamemode,
		encryption: mp.config.encryption,
		maxplayers: mp.config.maxplayers,
		name: mp.config.name,
		'stream-distance': mp.config['stream-distance'],
		port: mp.config.port,
		'disallow-multiple-connections-per-ip': mp.config['disallow-multiple-connections-per-ip'],
		'limit-time-of-connections-per-ip': mp.config['limit-time-of-connections-per-ip'],
		url: mp.config.url,
		language: mp.config.language,
		'sync-rate': mp.config['sync-rate'],
		'resource-scan-thread-limit': mp.config['resource-scan-thread-limit'],
		'max-ping': mp.config['max-ping'],
		'min-fps': mp.config['min-fps'],
		'max-packet-loss': mp.config['max-packet-loss'],
		'allow-cef-debugging': mp.config['allow-cef-debugging'],
		'enable-nodejs': mp.config['enable-nodejs'],
		csharp: mp.config.csharp,
		'enable-http-security': mp.config['enable-http-security'],
		'voice-chat': mp.config['voice-chat'],
		'allow-voice-chat-input': mp.config['allow-voice-chat-input'],
		'voice-chat-sample-rate': mp.config['voice-chat-sample-rate'],
		'fastdl-host': mp.config['fastdl-host']
	};

	public world = {
		weather: mp.world.weather,
		time: {
			hour: mp.world.time.hour,
			minute: mp.world.time.minute,
			second: mp.world.time.second,

			set: (hour: number, minute: number, second: number): void => {
				mp.world.time.set(hour, minute, second);
			}
		},
		trafficLights: {
			locked: mp.world.trafficLights.locked,
			state: mp.world.trafficLights.state
		},

		removeIpl: (name: string): void => {
			log.info(`NRage.World.removeIpl: Disabled transition ${name}`);
			mp.world.requestIpl(name);
		},
		requestIpl: (name: string): void => {
			log.info(`NRage.World.requestIpl: Enabled transition ${name}`);
			mp.world.requestIpl(name);
		},
		setWeatherTransition: (weather: RageEnums.Weather | string, duration?: number): void => {
			log.info(`[DEPRECATED] NRage.World.setWeatherTransition: Weather was changed to ${weather}, transition time: ${duration}`);
			// mp.world.setWeatherTransition(weather, duration);
		}
	};

	// Global
	delayServerShutdown = (bool: boolean): void => {
		try {
			mp.events.delayShutdown = bool;
			log.warn(`NRage.Events.delayShutdown: Set to ${bool}`);
		} catch (error) {
			log.error(`NRage.Events.delayShutdown: ${error.message}`);
		}
	};

	delayServerInitialization = (bool: boolean): void => {
		try {
			mp.events.delayInitialization = bool;
			log.warn(`NRage.Events.delayServerInitialization: Set to ${bool}`);
		} catch (error) {
			log.error(`NRage.Events.delayServerInitialization: ${error.message}`);
		}
	};
}

export const nr = new NRWrap();
