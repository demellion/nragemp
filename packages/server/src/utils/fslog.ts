import path from 'path';
import fs from 'fs';
import moment from 'moment';

export const fsLog = (folder: string, filename: string, text: string, fileType?: string) => {
	const logPath = path.join(process.cwd(), 'logs', folder);
	const timeStampFormatted = moment().format('YYYY-MM-DD HH:MM:SS');
	if (!fs.existsSync(logPath)) fs.mkdirSync(logPath);
	const logName = path.join(logPath, `${filename}.${fileType || 'log'}`);
	fs.appendFile(logName, `[${timeStampFormatted}] ${text}\r\n`, 'utf-8', (err) => {
		if (err.stack) {
			console.log(err.stack);
		}
	});	
};
