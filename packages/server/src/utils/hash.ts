import { createEncryptor } from 'simple-encryptor';

const hashString = 'bf7476e5f1a2188bcf9d4aae177a0772';

export const hash = createEncryptor(hashString);
