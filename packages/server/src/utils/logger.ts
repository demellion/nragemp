import log4js, { Logger } from 'log4js';

export enum LoggerLevels {
	trace = 'trace',
	debug = 'debug',
	info = 'info',
	warn = 'warn',
	error = 'error',
	fatal = 'fatal'
}

export enum Loggers {
	server = 'server',
	client = 'client',
}

export const getLogger = (name: string, level = LoggerLevels.debug): Logger => {
	const logger = log4js.getLogger(name);
	logger.level = level;
	return logger;
};
