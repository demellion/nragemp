import { fsLog } from '../../utils/fslog';
import { currencyAPI } from '../../classes/currency';
import { Player } from 'rage-rpc';
import accountController from '../../core/controllers/account/account.controller';
import { localize } from 'common/index';
import { PlayerNR } from '../../core/wrapper';

currencyAPI.createCurrency('cash', localize.t('GLOBAL.CASH'));
currencyAPI.createCurrency('bank', localize.t('GLOBAL.BANK'));

// Watch currency changes
currencyAPI.on('currencySet', (player: PlayerMp | Player, currencyKey: any, oldAmount: number, newAmount: number, reason: string): void => {
	const text = `${player.name}: '${currencyKey}' was set to ${newAmount}(${reason}) from ${oldAmount}`;
	accountController.updateCharacter(player.name, { [currencyKey]: newAmount });
	fsLog('moneylog', `${player.rgscId}`, text);
});
currencyAPI.on('currencyAdd', (player: PlayerMp | Player, currencyKey: string, oldAmount: number, amount: number, reason: string): void => {
	const total = oldAmount + amount;
	const text = `${player.name}: ${amount} of ${currencyKey} for total: ${total} (${reason})`;
	accountController.updateCharacter(player.name, { [currencyKey]: total });
	fsLog('moneylog', `${player.rgscId}`, text);
});
currencyAPI.on('currencySwap', (player: PlayerMp | Player, source: string, amount: number, to: string, reason: string): void => {
	const text = `${player.name}: ${amount} of ${source} moved to '${to}' (${reason})`;
	const sourceTotal = player.getVariable('wallet')[source] - amount;
	accountController.updateCharacter(player.name, { [source]: sourceTotal, [to]: amount });
	fsLog('moneylog', `${player.rgscId}`, text);
});
currencyAPI.on('pointsAdd', (player: PlayerNR | Player, currencyKey: string, oldAmount: number, newAmount: number, reason: string): void => {
	const total = oldAmount + newAmount;
	const diff = newAmount - oldAmount;
	const text = `${player._userName}: '${diff}' of ${currencyKey} for total: ${total} (${reason})`;
	accountController.saveAccount({ username: player._userName, currency: total });
	fsLog('moneylog', `${player.rgscId}`, text);
	fsLog('', 'currency', text);
});