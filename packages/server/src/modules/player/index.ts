import { NREvents } from 'common/enums/nrEvents';
import { nr } from '../../core/wrapper';
import { locations } from '../../constants/locations';
import { config, getClosestLocation, max, min, setDesiredWantedLevel, setWantedLevel } from '../../core/main';
import { timezone } from '../../index';
import { savePlayer } from '../account/auth';

type Stats = 'hunger' | 'thirst' | 'mood';

nr.rpc.register(NREvents.getServerLang, async () => {
	return nr.config.language;
});

nr.rpc.register(NREvents.clientInit, (_, info): { [k: string]: any } => {
	const { player } = info;
	const locale = nr.config.language;
	const serverName = nr.config.name;
	const serverConfig = config();
	return {
		lang: locale,
		timezone,
		serverName,
		rgscId: player.rgscId,
		serial: player.serial,
		hostname: player.ip === '127.0.0.1' ? `127.0.0.1` : serverConfig.cef_hostname
	};
});

nr.events.add(NREvents.changeStats, (player: PlayerMp, stat: Stats, value: number, cap?: number) => {
	const stats = player.getVariable('stats');
	if (!stats) return;
	stats[stat] = min(max(stats[stat] + value, 0), 100);
	if (cap) {
		if (stats[stat] >= cap) return;
		stats[stat] = min(stats[stat], cap);
	}
	player.setVariable('stats', stats);
});

nr.rpc.register(NREvents.playerRespawn, (args, info) => {
	const { player } = info;
	const { spawncase } = args;
	if (!spawncase) return;
	const lastHeading = player.heading;

	switch (spawncase) {
		case 'safezone': {
			player.spawn(player.position);
			player.heading = lastHeading;
			break;
		}
		case 'knockout': {
			player.spawn(player.position);
			player.heading = lastHeading;
			break;
		}
		case 'death': {
			// @ts-ignore
			const location = getClosestLocation(locations.hospitals, player);
			const position = location.position.add(new mp.Vector3(0, 0, 0.25));
			const heading = location.heading;
			player.spawn(position);
			player.heading = heading;
			player.dimension = 0;
			savePlayer(player);
			break;
		}
	}
});

nr.rpc.register(NREvents.setDesiredWantedLevel, (args, info) => {
	const { wantedLevel } = args;
	const { player } = info;
	return setDesiredWantedLevel(player, wantedLevel);
});
nr.rpc.register(NREvents.setWantedLevel, (args, info) => {
	const { wantedLevel } = args;
	const { player } = info;
	setWantedLevel(player, wantedLevel);
});

nr.rpc.register('toggleCrouch', (_, info) => {
	const { player } = info;
	const state = !!player.getVariable('isCrouched');
	player.setVariable('isCrouched', !state);
});

nr.rpc.register('playAnimation', (args: any, info) => {
	const { animSet, flag, duration, speed } = args;
	if (!animSet) return;
	const { player } = info;
	player.setVariable('animationData', [animSet, flag, duration, speed]);
});
nr.rpc.register('playWalkingStyle', (args: any, info) => {
	const { index } = args;
	if (!index) return;
	const { player } = info;
	player.setVariable('walkingStyle', index);
});
nr.rpc.register('playSpeech', (args: any) => {
	const { entity, speechName, voiceName, speechParam } = args;
	if (!entity) return;
	entity.setVariable('entitySpeech', [speechName, voiceName, speechParam]);
});
