import { nr } from '../../../core/wrapper';
import { objects } from '../../../constants/objects';
import { collectibles } from '../../../constants/collectibles';
import { BlipClass, BlipColors, BlipScales } from '../../../constants/blips';
import { localize } from '../../../../../common';

objects.forEach((thisObject) => {
	const thisProp = nr.objects.new(thisObject.type, thisObject.position, {
		rotation: new mp.Vector3(0, thisObject.rotation, thisObject.heading),
		alpha: 255,
		dimension: thisObject.dimension || 0
	});
	thisProp.setVariable('streamData', {
		action: '#object',
		data: thisObject.data || null
	});
});

collectibles.forEach((thisObject) => {
	const thisProp = nr.objects.new('ex_prop_exec_award_diamond', thisObject.position, {
		alpha: 255,
		dimension: 0
	});
	thisProp.setVariable('streamData', {
		action: '#object',
		data: {
			action: 'obj_collectible',
			actionName: 'ACTIONS.COLLECTIBLE'
		}
	});
	const blip = nr.blips.new(BlipClass.COLLECTIBLE, thisObject.position, {
		name: localize.t('BLIPS.COLLECTIBLE'),
		shortRange: false,
		scale: BlipScales.MEDIUM,
		color: BlipColors.EVENT
	});
	// blip.setVariable('drawDistance', thisObject.range || 30);
	blip.setVariable('drawDistance', 10e7); // TODO: Set line above after debugging
});
