import { getLogger, Loggers } from '../../../utils/logger';
import staticController from '../../../core/controllers/static/static.controller';
import { markers } from '../../../constants/markers';
import { nr } from '../../../core/wrapper';
import { localize } from '../../../../../common';

const log = getLogger(Loggers.server);

const createStaticMarker = async (scale: number, action: string, name: string, dimension: number, thisData: any): Promise<MarkerMp> => {
	const alias = thisData.data.alias;
	const thisMarker = nr.markers.new(1, thisData.position, scale,
		{
			color: [172, 23, 32, 200],
			visible: true,
			dimension
		});
	const streamData = {
		action,
		name: localize.t(name),
		data: thisData.data
	};
	if (alias) {
		const staticData = await staticController.loadStatic(alias);
		if (staticData) {
			thisMarker.setVariable('staticData', staticData);
			// TODO: Связать с актуальным магазином
			streamData.name = localize.t(name) + '~c~' + '\nВладелец: Нубас' + '\nНалог: 20%';
		} else {
			staticController.createStatic(alias).then();
			log.warn(`NRage.createStaticMarker: '${alias}' was missing, created new.`);
		}
	}
	thisMarker.setVariable('streamData', streamData);
	return thisMarker;
};

markers.gasStations.forEach((thisMarker) => {
	createStaticMarker(1, 'gas_station', 'BLIPS.GASSTATION', 0, thisMarker).then();
});

markers.transitions.forEach((thisMarker) => {
	const markerName = thisMarker.data.name ? thisMarker.data.name : '';
	createStaticMarker(1, '#transition', markerName, 0, thisMarker).then();
});
