import { peds } from '../../../constants/peds';
import { getLogger, Loggers } from '../../../utils/logger';
import staticController from '../../../core/controllers/static/static.controller';
import { nr } from '../../../core/wrapper';
import { localize } from '../../../../../common';

const log = getLogger(Loggers.server);
const createStaticPed = async (type: string | number, name: string, dimension: number, thisData: any): Promise<PedMp> => {
	const alias = thisData.data.alias;
	const thisPed = nr.peds.new(type, thisData.position, {
		dynamic: false,
		frozen: true,
		invincible: true
	});

	thisPed.dimension = dimension;
	const streamData = {
		action: '#ped',
		heading: thisData.heading,
		animationSet: thisData.animationSet,
		speechSet: thisData.speechSet,
		ignoreLos: thisData.ignoreLos || false,
		name: localize.t(name),
		data: thisData.data
	};
	if (alias) {
		const staticData = await staticController.loadStatic(alias);
		if (staticData) {
			thisPed.setVariable('staticData', staticData);
			// TODO: Связать с актуальным магазином
			streamData.name = localize.t(name) + '~c~' + '\nВладелец: Нубас' + '\nНалог: 20%';
		} else {
			staticController.createStatic(alias).then();
			log.warn(`NRage.createStaticPed: '${alias}' was missing, created new.`);
		}
	}
	thisPed.setVariable('streamData', streamData);
	return thisPed;
};

// Vehicle shops
peds.vehicleshops.forEach((thisPed) => {
	createStaticPed(thisPed.type, 'BLIPS.VEHICLESHOPELITE', 0, thisPed).then();
});

// Impound
peds.impound.forEach((thisPed) => {
	createStaticPed('ig_trafficwarden', 'BLIPS.VEHICLEIMPOUND', 0, thisPed).then();
});

// Weapons shops
peds.ammunation.forEach((thisPed) => {
	createStaticPed('s_m_y_ammucity_01', 'BLIPS.AMMUNATION', 0, thisPed).then();
});

// 24/7
peds.supermarkets.forEach((thisPed) => {
	createStaticPed('a_m_y_genstreet_01', 'BLIPS.SUPERMARKET', 0, thisPed).then();
});

// Casino peds
peds.casino.forEach((thisPed) => {
	createStaticPed(thisPed.type, thisPed.name, 0, thisPed).then();
});

// Stripclub peds
peds.stripclub.forEach((thisPed) => {
	createStaticPed(thisPed.type, thisPed.name, 0, thisPed).then();
});

// Lore peds
peds.lore.forEach((thisPed) => {
	createStaticPed(thisPed.type, thisPed.name, 0, thisPed).then();
});

// Easter eggs
peds.easteregg.forEach((thisPed) => {
	createStaticPed(thisPed.type, thisPed.name, 0, thisPed).then();
});
