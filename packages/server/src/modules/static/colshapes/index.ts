import { nr } from '../../../core/wrapper';
import { safezones } from '../../../constants/safezones';

// Safezones colshapes
safezones.forEach((thisSafezone) => {
	const thisColshape = nr.colshapes.newRectangle(...thisSafezone.position);
	thisColshape.setVariable('safezoneData', {
		name: thisSafezone.name,
		elevations: thisSafezone.elevations,
		level: thisSafezone.level,
		icon: thisSafezone.icon || null
	});
});