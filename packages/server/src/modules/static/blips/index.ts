import { BlipClass, BlipColors, blips, BlipScales } from '../../../constants/blips';
import { nr } from '../../../core/wrapper';
import { localize } from '../../../../../common';

const createBlip = (hash: number, position: Vector3Mp, name: string, shortRange: boolean, scale: number, color: number): BlipMp => {
	return nr.blips.new(hash, position, {
		name,
		shortRange,
		scale,
		color
	});
};

blips.gasStations.forEach((thisBlip) => {
	createBlip(BlipClass.GAS_STATION, thisBlip.position, localize.t('BLIPS.GASSTATION'), true, BlipScales.MEDIUM, BlipColors.NON_IMPORTANT);
});
blips.ammunations.forEach((thisBlip) => {
	createBlip(BlipClass.AMMUNATION, thisBlip.position, localize.t('BLIPS.AMMUNATION'), true, BlipScales.MEDIUM, BlipColors.NEUTRAL);
});
blips.impounds.forEach((thisBlip) => {
	createBlip(BlipClass.IMPOUND, thisBlip.position, localize.t('BLIPS.VEHICLEIMPOUND'), true, BlipScales.MEDIUM, BlipColors.WARNING);
});
blips.supermarkets.forEach((thisBlip) => {
	createBlip(BlipClass.SUPERMARKET, thisBlip.position, localize.t('BLIPS.SUPERMARKET'), true, BlipScales.MEDIUM, BlipColors.NEUTRAL);
});
// Jobs blips
blips.jobs.forEach((thisBlip) => {
	createBlip(thisBlip.type, thisBlip.position, localize.t(thisBlip.name), true, BlipScales.MEDIUM, BlipColors.JOBS);
});

// Other blips
blips.generic.forEach((thisBlip) => {
	createBlip(thisBlip.type, thisBlip.position, localize.t(thisBlip.name), true, BlipScales.MEDIUM, thisBlip.color);
});
