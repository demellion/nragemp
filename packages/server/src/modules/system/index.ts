import moment from 'moment';
import { nr } from '../../core/wrapper';
import { doPayday, getVehicleSpeed, max } from '../../core/main';
import { vehicleAPI } from '../../classes/vehicles';

const TimeSimulation = () => {
	// Time module
	const time = moment().toArray();
	nr.world.time.set(time[3], time[4], time[5]);
};
const FuelSimulation = () => {
	// Fuel extractor
	mp.vehicles.forEach((thisVehicle) => {
		if (thisVehicle.engine) {
			const vehicleType = thisVehicle.getVariable('class');
			if (!vehicleType) return false;
			let consumptionRate = vehicleAPI.getFuelConsumptionRate(vehicleType);
			if (consumptionRate === 0) return false;
			const speed = getVehicleSpeed(thisVehicle.velocity);
			if (speed < 10) consumptionRate = consumptionRate / 5;
			const fuelCurrent = thisVehicle.getVariable('fuel') || 0;
			const fuelDiff = max(fuelCurrent - consumptionRate, 0);
			thisVehicle.setVariable('fuel', fuelDiff);
			if (fuelDiff === 0) {
				thisVehicle.engine = false;
				thisVehicle.setVariable('engineState', false);
			}
		}
	});
};

const PayDaySimulation = () => {
	mp.players.forEach(doPayday);
};

// Cron
setInterval(TimeSimulation, 60000);
setInterval(PayDaySimulation, 60000);
setInterval(FuelSimulation, 1000);