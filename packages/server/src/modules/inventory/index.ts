/*
	Weapon stats: https://docs.google.com/spreadsheets/d/1foyTBIXxduFatBGEc0Vv1riAfpz_Ljp7yj5S7mL5-ik
*/

import { inventoryAPI } from '../../classes/inventory';

const Sizes: { [k: string]: [number, number] } = {
	single: [1, 1],
	small: [2, 2],
	medium: [3, 3],
	huge: [4, 4],

	// Class-related
	long: [4, 1],
	melee: [4, 2],
	ammunition: [2, 2],
	pistol: [3, 2],
	smg: [3, 2],
	rifle: [4, 3],
	shotgun: [4, 3],
	heavy: [6, 3]
};

/*
	Weaponry
*/

// Melee (Tools)
inventoryAPI.createItem('weapon_bat', 'Baseball Bat', 'weapon', 1, Sizes.melee, true, 2, true);
inventoryAPI.createItem('weapon_flashlight', 'Flashlight', 'weapon', 1, Sizes.melee, true, 0.8, true);
inventoryAPI.createItem('weapon_crowbar', 'Crowbar', 'weapon', 1, Sizes.melee, true, 3, true);
inventoryAPI.createItem('weapon_golfclub', 'Golf Club', 'weapon', 1, Sizes.melee, true, 1, true);
inventoryAPI.createItem('weapon_hammer', 'Hammer', 'weapon', 1, Sizes.melee, true, 1, true);
inventoryAPI.createItem('weapon_hatchet', 'Hatchet', 'weapon', 1, Sizes.melee, true, 2, true);
inventoryAPI.createItem('weapon_wrench', 'Pipe Wrench', 'weapon', 1, Sizes.melee, true, 3, true);
inventoryAPI.createItem('weapon_poolcue', 'Pool Cue', 'weapon', 1, Sizes.long, true, 0.5, true);

// Melee (weapons)
inventoryAPI.createItem('weapon_bottle', 'Broken Bottle', 'weapon', 1, Sizes.small, true, 0.25, true);
inventoryAPI.createItem('weapon_dagger', 'Antique Cavalry Dagger', 'weapon', 1, Sizes.small, true, 0.4, true);
inventoryAPI.createItem('weapon_knuckle', 'Brass Knuckles', 'weapon', 1, Sizes.small, true, 0.8, true);
inventoryAPI.createItem('weapon_knife', 'Knife', 'weapon', 1, Sizes.small, true, 0.5, true);
inventoryAPI.createItem('weapon_machete', 'Machete', 'weapon', 1, Sizes.melee, true, 2, true);
inventoryAPI.createItem('weapon_switchblade', 'Switchblade', 'weapon', 1, Sizes.small, true, 0.5, true);
inventoryAPI.createItem('weapon_nightstick', 'Nightstick', 'weapon', 1, Sizes.long, true, 1, true);
inventoryAPI.createItem('weapon_battleaxe', 'Battle Axe', 'weapon', 1, Sizes.melee, true, 2, true);

// Ammo
inventoryAPI.createItem('ammo_9x19', '9x19', 'ammunition', 300, Sizes.ammunition, true, 0.01, true);
inventoryAPI.createItem('ammo_50cal', '.50', 'ammunition', 50, Sizes.ammunition, true, 0.01, true);
inventoryAPI.createItem('ammo_556', '5.56', 'ammunition', 300, Sizes.ammunition, true, 0.01, true);
inventoryAPI.createItem('ammo_762', '7.62', 'ammunition', 300, Sizes.ammunition, true, 0.01, true);
inventoryAPI.createItem('ammo_12', '12-Gauge', 'ammunition', 120, Sizes.ammunition, true, 0.01, true);
inventoryAPI.createItem('ammo_40mmHE', '40mm HE', 'ammunition', 10, Sizes.ammunition, true, 0.01, true);
inventoryAPI.createItem('ammo_40mmSmoke', '40mm Smoke', 'ammunition', 10, Sizes.ammunition, true, 0.01, true);
inventoryAPI.createItem('ammo_rocket', 'Rocket', 'ammunition', 1, Sizes.medium, true, 1, true);

// Throwable
inventoryAPI.createItem('throw_grenade', 'Grenade', 'throw', 5, Sizes.ammunition, true, 0.6, true);
inventoryAPI.createItem('throw_bzgas', 'BZ Gas', 'throw', 5, Sizes.ammunition, true, 1, true);
inventoryAPI.createItem('throw_molotov', 'Molotov Cocktail', 'throw', 5, Sizes.ammunition, true, 1, true);
inventoryAPI.createItem('throw_stickybomb', 'Sticky Bomb', 'throw', 5, Sizes.ammunition, true, 1, true);
inventoryAPI.createItem('throw_proxmine', 'Proximity Mines', 'throw', 5, Sizes.ammunition, true, 1, true);
inventoryAPI.createItem('throw_snowball', 'Snowballs', 'throw', 100, Sizes.single, true, 1, true);
inventoryAPI.createItem('throw_pipebomb', 'Pipe Bombs', 'throw', 5, Sizes.ammunition, true, 1, true);
inventoryAPI.createItem('throw_ball', 'Baseball', 'throw', 100, Sizes.single, true, 1, true);
inventoryAPI.createItem('throw_smokegrenade', 'Tear Gas', 'throw', 5, Sizes.ammunition, true, 1, true);

// Pistols
inventoryAPI.createItem('weapon_pistol', 'Pistol', 'weapon', 1, Sizes.pistol, true, 0.7, true);
inventoryAPI.createItem('weapon_snspistol', 'SNS Pistol', 'weapon', 1, Sizes.small, true, 0.7, true);
inventoryAPI.createItem('weapon_combatpistol', 'Combat Pistol', 'weapon', 1, Sizes.pistol, true, 0.7, true);
inventoryAPI.createItem('weapon_appistol', 'AP Pistol', 'weapon', 1, Sizes.pistol, true, 0.7, true);
inventoryAPI.createItem('weapon_heavypistol', 'Heavy Pistol', 'weapon', 1, Sizes.pistol, true, 0.7, true);
inventoryAPI.createItem('weapon_ceramicpistol', 'Ceramic Pistol', 'weapon', 1, Sizes.pistol, true, 0.7, true);
inventoryAPI.createItem('weapon_pistol50', 'Pistol .50', 'weapon', 1, Sizes.pistol, true, 0.7, true);

// SMG
inventoryAPI.createItem('weapon_microsmg', 'Micro SMG', 'weapon', 1, Sizes.pistol, true, 0.7, true);
inventoryAPI.createItem('weapon_smg', 'SMG', 'weapon', 1, Sizes.pistol, true, 0.7, true);
inventoryAPI.createItem('weapon_minismg', 'Mini SMG', 'weapon', 1, Sizes.pistol, true, 0.7, true);

// Revolvers
inventoryAPI.createItem('weapon_marksmanpistol', 'Marksman Pistol', 'weapon', 1, Sizes.pistol, true, 1, true);
inventoryAPI.createItem('weapon_revolver', 'Heavy Revolver', 'weapon', 1, Sizes.pistol, true, 1, true);
inventoryAPI.createItem('weapon_doubleaction', 'Double Action Revolver', 'weapon', 1, Sizes.pistol, true, 1, true);
inventoryAPI.createItem('weapon_navyrevolver', 'Navy Revolver', 'weapon', 1, Sizes.pistol, true, 1, true);

// Shotguns
inventoryAPI.createItem('weapon_pumpshotgun', 'Pump Shotgun', 'weapon', 1, Sizes.shotgun, true, 2, true);
inventoryAPI.createItem('weapon_pumpshotgun_mk2', 'Pump Shotgun Mk II', 'weapon', 1, Sizes.shotgun, true, 1.5, true);
inventoryAPI.createItem('weapon_sawnoffshotgun', 'Sawed-Off Shotgun', 'weapon', 1, Sizes.smg, true, 1, true);
inventoryAPI.createItem('weapon_assaultshotgun', 'Assault Shotgun', 'weapon', 1, Sizes.shotgun, true, 2, true);
inventoryAPI.createItem('weapon_bullpupshotgun', 'Bullpup Shotgun', 'weapon', 1, Sizes.shotgun, true, 2, true);
inventoryAPI.createItem('weapon_heavyshotgun', 'Heavy Shotgun', 'weapon', 1, Sizes.shotgun, true, 3, true);
inventoryAPI.createItem('weapon_dbshotgun', 'Double Barrel Shotgun', 'weapon', 1, Sizes.shotgun, true, 1, true);
inventoryAPI.createItem('weapon_autoshotgun', 'Sweeper Shotgun', 'weapon', 1, Sizes.smg, true, 1, true);

// Rifles
inventoryAPI.createItem('weapon_musket', 'Musket', 'weapon', 1, Sizes.rifle, true, 3, true);
inventoryAPI.createItem('weapon_assaultrifle', 'Assault Rifle', 'weapon', 1, Sizes.rifle, true, 2.5, true);
inventoryAPI.createItem('weapon_assaultrifle_mk2', 'Assault Rifle Mk II', 'weapon', 1, Sizes.rifle, true, 1.5, true);
inventoryAPI.createItem('weapon_carbinerifle', 'Carbine Rifle', 'weapon', 1, Sizes.rifle, true, 2.5, true);
inventoryAPI.createItem('weapon_carbinerifle_mk2', 'Carbine Rifle Mk II', 'weapon', 1, Sizes.rifle, true, 1.5, true);
inventoryAPI.createItem('weapon_advancedrifle', 'Advanced Rifle', 'weapon', 1, Sizes.rifle, true, 2.5, true);
inventoryAPI.createItem('weapon_specialcarbine', 'Special Carbine', 'weapon', 1, Sizes.rifle, true, 2, true);
inventoryAPI.createItem('weapon_specialcarbine_mk2', 'Special Carbine Mk II', 'weapon', 1, Sizes.rifle, true, 1.5, true);
inventoryAPI.createItem('weapon_bullpuprifle', 'Bullpup Rifle', 'weapon', 1, Sizes.rifle, true, 2, true);
inventoryAPI.createItem('weapon_bullpuprifle_mk2', 'Bullpup Rifle Mk II', 'weapon', 1, Sizes.rifle, true, 2, true);
inventoryAPI.createItem('weapon_compactrifle', 'Compact Rifle', 'weapon', 1, Sizes.rifle, true, 1.5, true);
inventoryAPI.createItem('weapon_mg', 'MG', 'weapon', 1, Sizes.heavy, true, 5, true);
inventoryAPI.createItem('weapon_combatmg', 'Combat MG', 'weapon', 1, Sizes.heavy, true, 6, true);
inventoryAPI.createItem('weapon_combatmg_mk2', 'Combat MG Mk II', 'weapon', 1, Sizes.heavy, true, 5, true);
inventoryAPI.createItem('weapon_gusenberg', 'Gusenberg Sweeper', 'weapon', 1, Sizes.rifle, true, 3.5, true);
inventoryAPI.createItem('weapon_sniperrifle', 'Sniper Rifle', 'weapon', 1, Sizes.heavy, true, 6, true);
inventoryAPI.createItem('weapon_heavysniper', 'Heavy Sniper', 'weapon', 1, Sizes.heavy, true, 8, true);
inventoryAPI.createItem('weapon_heavysniper_mk2', 'Heavy Sniper Mk II', 'weapon', 1, Sizes.heavy, true, 6, true);
inventoryAPI.createItem('weapon_marksmanrifle', 'Marksman Rifle', 'weapon', 1, Sizes.heavy, true, 5, true);
inventoryAPI.createItem('weapon_marksmanrifle_mk2', 'Marksman Rifle Mk II', 'weapon', 1, Sizes.heavy, true, 4, true);
inventoryAPI.createItem('weapon_rpg', 'RPG', 'weapon', 1, Sizes.heavy, true, 6, true);
inventoryAPI.createItem('weapon_grenadelauncher', 'Grenade Launcher', 'weapon', 1, Sizes.heavy, true, 4.5, true);
inventoryAPI.createItem('weapon_grenadelauncher_smoke', 'Grenade Launcher Smoke', 'weapon', 1, Sizes.heavy, true, 4.5, true);
inventoryAPI.createItem('weapon_minigun', 'Minigun', 'weapon', 1, Sizes.heavy, true, 10, true);
inventoryAPI.createItem('weapon_firework', 'Firework Launcher', 'weapon', 1, Sizes.heavy, true, 1, true);
inventoryAPI.createItem('weapon_railgun', 'Railgun', 'weapon', 1, Sizes.heavy, true, 8, true);
inventoryAPI.createItem('weapon_hominglauncher', 'Homing Launcher', 'weapon', 1, Sizes.heavy, true, 6, true);
inventoryAPI.createItem('weapon_compactlauncher', 'Compact Grenade Launcher', 'weapon', 1, Sizes.pistol, true, 4, true);
inventoryAPI.createItem('weapon_rayminigun', 'Widowmaker', 'weapon', 1, Sizes.heavy, true, 8, true);

// Misc weapons
inventoryAPI.createItem('weapon_stungun', 'Stun Gun', 'weapon', 1, Sizes.pistol, true, 1, true);
inventoryAPI.createItem('weapon_flaregun', 'Flare Gun', 'weapon', 1, Sizes.pistol, true, 1, true);
inventoryAPI.createItem('gadget_parachute', 'Parachute', 'weapon', 1, Sizes.medium, true, 1, true);
inventoryAPI.createItem('weapon_fireextinguisher', 'Fire Extinguisher', 'weapon', 1, Sizes.medium, true, 1, true);
inventoryAPI.createItem('weapon_hazardcan', 'Hazardous Jerry Can', 'weapon', 1, Sizes.medium, true, 1, true);
inventoryAPI.createItem('weapon_petrolcan', 'Jerry Can', 'weapon', 1, Sizes.medium, true, 1, true);
inventoryAPI.createItem('weapon_flare', 'Flare', 'weapon', 1, Sizes.pistol, true, 1, true);

/*
	Visual
*/

/*
	Other
*/

// Food
inventoryAPI.createItem('food_ragesoda', 'NRage Soda', 'generic', 5, Sizes.small, true, 0.33, true);
inventoryAPI.createItem('food_ragesnack', 'NRage Snack', 'generic', 5, Sizes.small, true, 0.25, true);

// Healing/repairing items
inventoryAPI.createItem('item_repairkit', 'Repair Kit', 'generic', 1, Sizes.medium, true, 1, true);
inventoryAPI.createItem('item_stimpack', 'Med Kit', 'generic', 5, Sizes.medium, true, 0.5, true);
inventoryAPI.createItem('item_medkit', 'Med Kit', 'generic', 5, Sizes.medium, true, 0.5, true);
inventoryAPI.createItem('item_bandage', 'Bandage', 'generic', 5, Sizes.medium, true, 0.5, true);
