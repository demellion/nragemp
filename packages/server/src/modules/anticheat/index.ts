import { nr } from '../../core/wrapper';
import { getLogger, Loggers } from '../../utils/logger';
import { kickPlayerServer, banPlayerServer } from '../../core/main';
import { fsLog } from '../../utils/fslog';
import { destroyVehicle } from '../vehicles';
import { NREvents } from 'common/enums/nrEvents';

const log = getLogger(Loggers.server);

// NRage Anticheat core
nr.events.add(NREvents.reportAC, (client: PlayerMp, codeType: number) => {
	const rgscId = client.rgscId;
	const action = (level: number, logmessage: string, reason: string) => {
		const text = `${client.name}(${rgscId}): ${logmessage} (Code ${codeType})`;
		log.warn(text);
		fsLog('', 'anticheat', text);
		switch (level) {
			case 0: {
				nr.rpc.callClient(client, 'ACAction', { codeType, level }).then();
				break;
			}
			case 1: {
				nr.rpc.callClient(client, 'ACAction', { codeType, level }).then(() => {
					kickPlayerServer(client, reason, false);
				});
				break;
			}
			case 2: {
				nr.rpc.callClient(client, 'ACAction', { codeType, level }).then(() => {
					banPlayerServer(client, reason, 365, false).then();
				});
				break;
			}
		}
	};
	const kickReason = `NRage Anticheat: Code ${codeType}`;
	switch (codeType) {
		// Client vehicle spawner
		case 0: {
			action(0, 'entered a locally spawned vehicle', kickReason);
			break;
		}
		// Invisibility
		case 1: {
			action(1, 'invisibility detected', kickReason);
			break;
		}
		// Godmode
		case 2: {
			action(1, 'godmode detected', kickReason);
			break;
		}
		// Excessive armor/health
		case 3: {
			action(1, 'health/armor values over maximum span', kickReason);
			break;
		}
		// Noclips
		case 4: {
			action(1, 'disabled collision', kickReason);
			break;
		}
		// Weapons
		case 5: {
			action(2, 'illegal weapons', kickReason);
			break;
		}
		// Speedhack
		case 6: {
			action(0, 'vehicle speedhack', kickReason);
			break;
		}
		// Wrong model?
		case 7: {
			action(1, 'model was changed', kickReason);
			break;
		}
		// Infinite ammo
		case 8: {
			action(2, 'infinite ammo', kickReason);
		}
	}
});

// Server-side events
// Watch over dodgy vehicles
nr.events.add('playerStartEnterVehicle', (player, vehicle, seat) => {
	const playerID = player.rgscId;
	const isServerIndexed = vehicle.getVariable('class');
	if (isServerIndexed) return;
	const vehicleClass = nr.joaat(vehicle.type);
	log.warn(`NRage Anticheat: ${player.name} (${playerID}) entered illegally spawned ${vehicleClass} (${seat})`);
	setTimeout(() => {
		destroyVehicle(vehicle);
	}, 250);
});