import { log } from '../../index';
import { levelAPI } from '../../classes/leveling';
import { nr } from '../../core/wrapper';
import { NREvents } from 'common/enums/nrEvents';
import { Player } from 'rage-rpc';
const debugMode = false;

if (debugMode) {
	for (let x = 1; x <= 60; x++) {
		log.debug(`${x} needs ${levelAPI.getXPLevel(x)}`);
	}
}

const levelReasons: { [k: string]: number} = {
	payday: 100
};

export const addExperience = (player: PlayerMp | Player, amount: number): void => {
	levelAPI.addXP(player, amount);
};

nr.rpc.register(NREvents.experienceGateway, (args, info) => {
	const { reason } = args;
	const { player } = info;
	const amount = levelReasons[reason];
	if (!amount) return;
	addExperience(player, amount);
});
