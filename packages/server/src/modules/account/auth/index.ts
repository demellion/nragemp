import accountController from '../../../core/controllers/account/account.controller';
import moment from 'moment';
import { Account, ServerAccount } from 'common/types/account';
import { Character } from 'common/types/character';
import { GetCurrentAccountErrorTypes, GetCurrentAccountResult } from 'common/types/api/getCurrentAccount';
import { IsoString } from 'common/types/date';
import { LoginErrorTypes, LoginResult } from 'common/types/api/loginAccount';
import { NREvents } from 'common/enums/nrEvents';
import { OperationFailed, OperationSuccess } from 'common/pipes/operation';
import { Player, ProcedureListenerInfo } from 'rage-rpc';
import { RegisterErrorTypes, RegisterResult } from 'common/types/api/registerAccount';
import { defaultAccount, defaultName } from '../../../constants/account';
import { despawnVehicle, destroyVehicle } from '../../vehicles';
import { fsLog } from '../../../utils/fslog';
import { getIsoString } from 'common/pipes/date';
import { getLogger, Loggers } from '../../../utils/logger';
import { hash } from '../../../utils/hash';
import { localize } from 'common/index';
import { nr, PlayerNR } from '../../../core/wrapper';
import { checkDuplicates, getAccountFromServerAccount } from '../../../core/main';
import { defaultCustomizationData } from 'common/constants/customizationData';

const log = getLogger(Loggers.server);

nr.events.add(NREvents.playerReady, async (player: PlayerMp) => {
	player.dimension = parseInt(player.rgscId);
	const time = moment().toArray();
	await nr.rpc.callClient(player, 'syncCall', { time });
});

nr.rpc.on(NREvents.cefReady, async (_, { player }) => {
	const username = player._userName;
	if (username) return;
	await nr.rpc.callClient(player, NREvents.authCamera, null, { noRet: true });
});

const updatePlayerByAccount = async (player: PlayerNR | Player, account: Account): Promise<void> => {
	if (!account) {
		return null;
	}

	checkDuplicates(player);
	player.setVariable('adminLevel', account.adminLevel);
	player.setVariable('points', account.currency);
	player.setVariable('rgscId', player.rgscId);
	player.setVariable('serial', player.serial);
	player._userName = account.username + '';
	player._playTime = account.playtime || 0;
	player._activePromoCodes = account.promo || [];
	player._subscribers = account.subscribers || 0;
	player._email = account.email || '';

	if (moment(account.vip?.expiryDate) > moment()) {
		player.setVariable('vipLevel', account.vip.level);
		player._vipMoment = moment(account.vip.expiryDate);
	}
};

const getCurrentAccount = async (_: undefined, info: ProcedureListenerInfo): Promise<GetCurrentAccountResult> => {
	try {
		const { player } = info;
		const username = player._userName;

		const serverAccount = await accountController.findAccount({ username });

		if (!serverAccount || username === defaultName) {
			return new OperationFailed(
				GetCurrentAccountErrorTypes.NotFound,
				localize.t('ERRORS.GET_CURRENT_ACCOUNT.NOT_FOUND')
			);
		}

		const account = getAccountFromServerAccount(serverAccount);

		return new OperationSuccess<Account>(account);
	} catch (error) {
		return new OperationFailed();
	}
};

nr.rpc.register(NREvents.getCurrentAccount, getCurrentAccount);

const updateAccountOnLogin = async (account: Account): Promise<void> => {
	await accountController.saveAccount({
		username: account.username,
		lastConnected: getIsoString(moment())
	});
};

const registration = async (
	{ username, password, email }: { username: string, password: string, email: string, dateOfBirth: IsoString }, info: ProcedureListenerInfo
): Promise<RegisterResult> => {
	try {
		const { player } = info;
		const rgscId = player.rgscId;

		if (!username || !password || !email) {
			return new OperationFailed(
				RegisterErrorTypes.WrongParams,
				localize.t('ERRORS.REGISTER.WRONG_PARAMS')
			);
		}

		const existingAccount = await accountController.findAccount({ $or: [{ username }, { email }, { rgscId }] });

		if (existingAccount && existingAccount.username === username) {
			return new OperationFailed(
				RegisterErrorTypes.LoginAlreadyExists,
				localize.t('ERRORS.REGISTER.LOGIN_ALREADY_EXISTS')
			);
		}

		if (existingAccount && existingAccount.email === email) {
			return new OperationFailed(
				RegisterErrorTypes.EmailAlreadyExists,
				localize.t('ERRORS.REGISTER.EMAIL_ALREADY_EXISTS')
			);
		}
		if (existingAccount && existingAccount.rgscId === rgscId) {
			return new OperationFailed(
				RegisterErrorTypes.SocialClubExists,
				localize.t('ERRORS.REGISTER.SOCIALCLUB_ALREADY_EXISTS')
			);
		}

		if (username.length < 3) {
			return new OperationFailed(
				RegisterErrorTypes.TooShortLogin,
				localize.t('ERRORS.REGISTER.TOO_SHORT_LOGIN')
			);
		}

		if (password.length < 6) {
			return new OperationFailed(
				RegisterErrorTypes.TooShortPassword,
				localize.t('ERRORS.REGISTER.TOO_SHORT_PASSWORD')
			);
		}

		const date = getIsoString(moment());

		const serverAccount: ServerAccount = {
			...defaultAccount,
			username,
			password: hash.hmac(password),
			email,
			socialClub: player.socialClub,
			serial: player.serial,
			rgscId: player.rgscId,
			lastIP: player.ip,
			registeredIP: player.ip,
			registeredDate: date,
			lastConnected: date
		};

		await accountController.createAccount(serverAccount);

		const account = getAccountFromServerAccount(serverAccount);

		await updatePlayerByAccount(player, account);

		nr.rpc.triggerClient(player, 'saveLastLogin', username);

		log.debug(`Account ${username} successfully registered`);

		return new OperationSuccess(account);
	} catch (error) {
		log.error(`event: ${NREvents.registration}, error: ${error.message}`);
		return new OperationFailed();
	}
};

nr.rpc.register(NREvents.registration, registration);

const login = async ({ username, password }: { username: string, password: string }, info: ProcedureListenerInfo): Promise<LoginResult> => {
	try {
		const hashedPassword = hash.hmac(password);

		const serverAccount = await accountController.findAccount({ username, password: hashedPassword });

		if (!serverAccount || serverAccount.password !== hashedPassword) {
			return new OperationFailed(
				LoginErrorTypes.WrongUsernameOrPassword,
				localize.t('ERRORS.LOGIN.WRONG_USERNAME_OR_PASSWORD')
			);
		}

		const account = getAccountFromServerAccount(serverAccount);

		const { player } = info;
		const rgscId = player.rgscId;

		if (serverAccount.rgscId !== rgscId) {
			return new OperationFailed(
				LoginErrorTypes.InvalidrgscId,
				localize.t('ERRORS.LOGIN.WRONG_RGSCID')
			);
		}

		await Promise.all([
			updateAccountOnLogin(account),
			updatePlayerByAccount(player, account)
		]);

		nr.rpc.triggerClient(player, 'saveLastLogin', username);

		return new OperationSuccess(account);
	} catch (error) {
		log.error(`event: ${NREvents.login}, error: ${error.message}`);
		return new OperationFailed();
	}
};

nr.rpc.register(NREvents.login, login);

export const savePlayer = (player: PlayerNR | Player): void => {
	const name = player.name;
	if (!name) return;
	if (player._noSaveState) return;
	const position = player.position;
	const heading = player.heading;
	const { cash, bank } = player.getVariable('wallet') || { cash: 0, bank: 0 };
	const { currentLevel, currentXP } = player.getVariable('levelData') || { level: 1, experience: 0 };
	const { hunger, thirst, mood } = player.getVariable('stats') || { hunger: 100, thirst: 100, mood: 100 };
	const inventory = player.getVariable('inventory') || null;
	const wantedLevel = player.getVariable('wantedLevel') || 0;
	const customizationData = player.getVariable('customizationData') || defaultCustomizationData;
	const generic = player.getVariable('generic') || null;
	const health = player.health;
	const armor = player.armour;
	const paytime = player._payTime || 0;

	// Update character
	if (!player._characterLoaded) return;
	accountController.findCharacterAccount(name, player.rgscId).then((character) => {
		if (!character) {
			return;
		}
		log.debug('Saving character: ', name);
		const newCharacter: Character = {
			...character,
			position: [+position.x, +position.y, +position.z],
			stats: { health, armor, hunger, thirst, mood },
			heading,
			level: currentLevel,
			experience: currentXP,
			paytime,
			cash,
			bank,
			inventory,
			customizationData,
			generic,
			wantedLevel
		};
		accountController.updateCharacter(name, newCharacter).catch(
			log.debug
		);
	});
};

export const saveAccount = (player: PlayerNR | Player): void => {
	const name: string = player.name;
	if (!name) return;

	// Update account
	if (!player._characterLoaded) return;
	accountController.findAccount({ 'characters.name': name }).then((account) => {
		log.debug('Saving account:', account.username);
		if (!account) return;
		const date = getIsoString(moment());
		accountController.saveAccount({
			username: account.username,
			lastDisconnected: date,
			playtime: player._playTime,
			promo: player._activePromoCodes,
			subscribers: player._subscribers
		});
	});
};

nr.events.add(NREvents.savePlayer, savePlayer);

const playerQuited = (player: PlayerNR) => {
	const name = player.name;
	if (!name) return;
	savePlayer(player);
	saveAccount(player);

	// Despawn his vehicles
	const ownedVehicle = player.getVariable('ownedVehicle');
	const rentedVehicle = player.getVariable('rentedVehicle');
	if (ownedVehicle) despawnVehicle(ownedVehicle);
	if (rentedVehicle) despawnVehicle(rentedVehicle);
	fsLog('', 'connectlog', `${player.socialClub}(${player.rgscId}) disconnected.`);

	// Cleanup
	const vehiclesToClean = [
		player._previewVehicle
	];
	setTimeout(() => {
		vehiclesToClean.forEach(destroyVehicle);
	}, 1000);
};

nr.events.add(NREvents.playerQuit, playerQuited);
