import { nr } from '../../core/wrapper';
import { Player, ProcedureListenerInfo } from 'rage-rpc';
import { log } from '../../index';
import { ChatLabels } from 'common/enums/chat';
import { ChatMessage } from 'common/types/chat';

export const anything = Math.random();

const getMessageLabel = (str: string): ChatLabels => {
	let type: ChatLabels = null;

	Object.values(ChatLabels).forEach(label => {
		if (str.startsWith(`/${label} `)) {
			type = ChatLabels[label];
		}
	});

	return type;
};

const getMessageFromString = (str: string, player: PlayerMp | Player): ChatMessage => {
	const label = getMessageLabel(str);

	return {
		author: {
			name: player.name,
			id: player.id
		},
		label,
		text: label ? str.replace(`/${label} `, '') : str
	};
};

const sendChatMessage = async (str: string, info: ProcedureListenerInfo): Promise<void> => {
	const player = info.player as PlayerMp;
	const message = getMessageFromString(str, player);
	log.debug(`CHAT: ${JSON.stringify(message)}`);

	let players: PlayerMp[];
	const promises: Promise<void>[] = [];

	switch (message.label) {
		case ChatLabels.f:
		case ChatLabels.r:
		case ChatLabels.fb:
		case ChatLabels.rb: {
			players = mp.players.toArray();
			break;
		}
		default: {
			players = player.streamedPlayers;
			promises.push(nr.rpc.callBrowsers(player, 'chat:push', message));
		}
	}

	players.forEach(player => promises.push(nr.rpc.callBrowsers(player, 'chat:push', message)));
	await Promise.all(promises);
};

nr.rpc.register('chat:send', sendChatMessage);
