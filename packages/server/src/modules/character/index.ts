import { getLogger, Loggers } from '../../utils/logger';
import { nr, PlayerNR } from '../../core/wrapper';
import accountController from '../../core/controllers/account/account.controller';
import { Player, ProcedureListenerInfo } from 'rage-rpc';
import { sendPlayerToLimbo } from '../admin';
import { loadVehicle } from '../vehicles';
import { defaultCharacter } from '../../constants/character';
import { GetCharactersListResult } from 'common/types/api/getCharactersList';
import { OperationFailed, OperationSuccess } from 'common/pipes/operation';
import { CharCreateData } from 'common/types/character/customizationData';
import { localize } from 'common/index';
import { Character } from 'common/types/character';
import { CreateCharacterErrorTypes, CreateCharacterResult } from 'common/types/api/createCharacter';
import { LoadCharacterErrorTypes, LoadCharacterResult } from 'common/types/api/loadCharacter';
import { NREvents } from 'common/enums/nrEvents';
import { levelAPI } from '../../classes/leveling';
import { currencyAPI } from '../../classes/currency';
import { splitCustomizationData } from 'common/helpers/character';
import { defaultCustomizationData } from 'common/constants/customizationData';

const log = getLogger(Loggers.server);

nr.rpc.register('getDefaultCharacter', () => defaultCharacter);

const getCharacters = async (_: undefined, info: ProcedureListenerInfo): Promise<GetCharactersListResult> => {
	try {
		const { player } = info;
		const username = player._userName;

		let characters = await accountController.getCharacters(username) || [];

		// To compatibility with old database character scheme
		characters = characters.map(character => {
			if (character?.customizationData?.faceFeatures === undefined) {
				character = { ...character, customizationData: defaultCustomizationData };
				accountController.updateCharacter(character.name, character);
			}
			if (character.age === undefined) {
				character.age = defaultCharacter.age;
				accountController.updateCharacter(character.name, character);
			}
			return character;
		});

		return new OperationSuccess(characters);
	} catch (error) {
		return new OperationFailed();
	}
};

nr.rpc.register(NREvents.getCharacters, getCharacters);

const createCharacter = async (
	props: { firstname: string, lastname: string, age: number, formData: CharCreateData }, info: ProcedureListenerInfo
): Promise<CreateCharacterResult> => {
	try {
		const { firstname, lastname, age, formData } = props;
		if (!firstname || !lastname || !age) {
			return new OperationFailed(
				CreateCharacterErrorTypes.ParamsIsRequired,
				localize.t('ERRORS.CREATE_CHARACTER.PARAMS_IS_REQUIRED')
			);
		}
		const { player } = info;
		const username = player._userName;

		const name = `${firstname} ${lastname}`;

		const accountPromise = accountController.findAccount({ username });
		const characterPromise = accountController.findCharacter(name);

		const [account, existingCharacter] = await Promise.all([accountPromise, characterPromise]);

		if (!account || (account.characters && account.characters.length >= 3)) {
			return new OperationFailed(
				CreateCharacterErrorTypes.ToManyCharactersError,
				localize.t('ERRORS.CREATE_CHARACTER.TOO_MANY_CHARACTERS')
			);
		}

		if (existingCharacter) {
			return new OperationFailed(
				CreateCharacterErrorTypes.AlreadyExistsError,
				localize.t('ERRORS.CREATE_CHARACTER.ALREADY_EXISTS')
			);
		}

		const character: Character = {
			...defaultCharacter,
			name,
			age,
			customizationData: splitCustomizationData(formData)
		};

		await accountController.createCharacter(username, character);

		log.trace(`Created new character: ${name}`);

		return new OperationSuccess(character);
	} catch (error) {
		return new OperationFailed();
	}
};

nr.rpc.register(NREvents.createCharacter, createCharacter);

const updatePlayerByCharacter = async (player: PlayerNR | Player, character: Character): Promise<void> => {
	const position = character.position;
	player.name = character.name;
	player.position = player.position.add(0);
	player.armour = character.stats.armor || 0;
	player.health = (character.stats.health || 200) + 100; // Don't ask why
	player.setVariable('stats', { hunger: character.stats.hunger, thirst: character.stats.thirst, mood: character.stats.mood });
	player.setVariable('class', player.name);
	player.setVariable('customizationData', character.customizationData);
	player.setVariable('wantedLevel', character.wantedLevel);
	player.setVariable('inventory', character.inventory);
	currencyAPI.setWallet(player, { cash: character.cash, bank: character.bank });
	levelAPI.setLevel(player, character.level);
	levelAPI.setXP(player, character.experience);
	player._payTime = character.paytime;

	// Vehicles management
	player.setVariable('carsArray', character.cars || []);
	await loadVehicle(character.lastCarId, player);

	// Finalize player
	player.dimension = 0;
	player.position = new mp.Vector3(-1707.7742919921875, -1099.919677734375, 13.152441024780273);
	await nr.rpc.callClient(player, NREvents.gameplayCamera);
	if (character.jailed) {
		sendPlayerToLimbo(player);
	} else {
		player.position = new nr.Vector3(...position);
		player.heading = character.heading;
	}
};

const loadCharacter = async (name: string, info: ProcedureListenerInfo): Promise<LoadCharacterResult> => {
	try {
		const { player } = info;
		const character = await accountController.findCharacter(name);

		if (!character) {
			return new OperationFailed(
				LoadCharacterErrorTypes.NotFound,
				localize.t('ERRORS.LOAD_CHARACTER.NOT_FOUND')
			);
		}

		await updatePlayerByCharacter(player, character);
		player._characterLoaded = true;

		log.trace(`Player ${character.name} joined server`);

		return new OperationSuccess(character);
	} catch (error) {
		return new OperationFailed();
	}
};

nr.rpc.register(NREvents.loadCharacter, loadCharacter);
