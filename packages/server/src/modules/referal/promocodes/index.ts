import { PlayerNR } from '../../../core/wrapper';
import { Player } from 'rage-rpc';
import { currencyAPI } from '../../../classes/currency';
import { setVIPLevel } from '../../../core/main';
import { VipLevels } from 'common/enums/vipLevels';
import moment from 'moment';
import { patreonAPI } from '../../../classes/patreon';

export const runPromoCode = async (player: PlayerNR | Player, code: string): Promise<boolean> => {
	// Global promos
	switch (code) {
		case 'STARTERPACK': {
			currencyAPI.addCurrency(player, 'bank', 5000, `Promo-code: ${code}`);
			setVIPLevel(player, VipLevels.premium, moment().add(1, 'days'));
			break;
		}
	}

	// Tier only
	switch (code) {
		case 'SUPPORTER': {
			if (!(await patreonAPI.patronHasTier(player._email, 'Supporter'))) return false;
			currencyAPI.addCurrency(player, 'bank', 50000, `Promo-code: ${code}`);
			setVIPLevel(player, VipLevels.premium, moment().add(30, 'days'));
			break;
		}
		case 'INVESTOR': {
			if (!(await patreonAPI.patronHasTier(player._email, 'Investor'))) return false;
			currencyAPI.addCurrency(player, 'bank', 100000, `Promo-code: ${code}`);
			setVIPLevel(player, VipLevels.premium, moment().add(60, 'days'));
			break;
		}
		case 'BEAST': {
			if (!(await patreonAPI.patronHasTier(player._email, 'Beast'))) return false;
			currencyAPI.addCurrency(player, 'bank', 150000, `Promo-code: ${code}`);
			setVIPLevel(player, VipLevels.premium, moment().add(90, 'days'));
			break;
		}
	}

	return true;
};