import { nr, PlayerNR } from '../../core/wrapper';
import { Player } from 'rage-rpc';
import { referals } from 'common/constants/referals';
import { NREvents } from 'common/enums/nrEvents';
import { saveAccount } from '../account/auth';
import { fsLog } from '../../utils/fslog';
import { runPromoCode } from './promocodes';
import { findPlayerByID, setVIPLevel } from '../../core/main';
import { VipLevels } from 'common/enums/vipLevels';
import moment from 'moment';
import Account from '../../core/models/account/acount.model';
import { currencyAPI } from '../../classes/currency';
import { localize } from 'common/index';
import { log } from '../../index';

const isCodeUsed = (player: PlayerNR | Player, code: string): boolean => {
	const codesUsed: string[] = player._activePromoCodes;
	return codesUsed.includes(code);
};

const isSubscriptionUsed = (player: PlayerNR | Player): boolean => {
	const codesUsed: string[] = player._activePromoCodes;
	codesUsed.forEach((code) => {
		if (referals[code]) return true;
	});
	return false;
};

const subscribePlayer = async (player: PlayerNR | Player, code: string): Promise<void> => {
	const { rgscId } = referals[code];
	await setVIPLevel(player, VipLevels.premium, moment().add(7, 'days'));
	currencyAPI.addCurrency(player, 'cash', 10000, `Subscription (${code})`);
	const referalPlayer = findPlayerByID(rgscId);

	if (referalPlayer) {
		currencyAPI.addPoints(referalPlayer, 50, `+50 - Referal system subscription (${player.socialClub})`);
		player._subscribers += 1;
		await nr.rpc.callClient(player, 'displayNotification', {
			title: localize.t('GENERIC.REFERAL'),
			message: localize.t('GENERIC.REFERALNEW'),
			icon: 'CHAR_SOCIAL_CLUB',
			long: true
		}, { noRet: true });
	} else {
		const referalAccount = await Account.findOne({ rgscId });
		if (!referalAccount) return;
		referalAccount.subscribers += 1;
		referalAccount.currency += 50;
		referalAccount.currencyHistory.push(`+50 - Referal system subscription (${player.socialClub})`);
		await referalAccount.save();
	}
};

nr.rpc.register(NREvents.usePromoCode, async (args, info) => {
	const { player } = info;
	const { promocode } = args;
	if (isCodeUsed(player, promocode)) return false; // It was used already
	if (referals[promocode] && isSubscriptionUsed(player)) return false; // No repetitive subs

	switch (true) {
		case (!!referals[promocode]): {
			subscribePlayer(player, promocode).then(() => {
				log.debug(`${player._userName} subscribed by '${promocode}'`);
			});
			break;
		}
		default: {
			if (!(await runPromoCode(player, promocode))) return false;
			log.debug(`${player._userName} used '${promocode}' promo-code`);
			break;
		}
	}

	fsLog('', 'promos', `${player.socialClub}(${player.rgscId}) used ${promocode} promo-code.`);
	player._activePromoCodes.push(promocode);
	saveAccount(player);
	return true;
});