import { BuyVehicleErrorTypes, BuyVehicleResult } from 'common/types/api/buyVehicle';
import { NREvents } from 'common/enums/nrEvents';
import { OperationFailed, OperationSuccess } from 'common/pipes/operation';
import { localize } from 'common/index';
import { log } from '../../../index';
import { nr, PlayerNR } from '../../../core/wrapper';
import { SetActiveVehicleResult } from 'common/types/api/setActiveVehicle';
import { GetVehicleListResult } from 'common/types/api/getVehicleList';
import { Vehicle } from 'common/types/vehicle';
import { getCurrentVendorFromClient, HEXtoRGB } from '../../../core/main';
import { Shops } from 'common/constants/shopdata';
import { vehicleAPI } from '../../../classes/vehicles';
import { sleep } from 'common/pipes/sleep';
import { Player } from 'rage-rpc';
import { createVehicleAndInsert, destroyVehicle } from '../../vehicles';
import { currencyAPI } from '../../../classes/currency';

interface BuyVehicleProps {
	name: string;
	moneySource: 'cash' | 'bank';
}

const removePreviewVehicle = (player: Player): void => {
	destroyVehicle(player._previewVehicle);
	player._previewVehicle = null;
};

nr.rpc.register(NREvents.getVehicleList, async (alias: string, { player }): Promise<GetVehicleListResult> => {
	try {
		player.dimension = parseInt(player.rgscId);
		player._noSaveState = true;
		player._fixedPosition = player.position;
		const vehicles: Vehicle[] = [];
		const vehicleStatsData = await nr.rpc.callClient(player, NREvents.getVehicleStats, { vehicleArray: Shops[alias].data });
		Shops[alias].data.forEach((thisVehicleType: string, index: number) => {
			const vehicleData = vehicleAPI.get(thisVehicleType);
			const vehicleStats = vehicleStatsData[index];
			vehicles.push(
				{
					name: thisVehicleType,
					class: vehicleData.type,
					price: vehicleData.price,
					fuel: vehicleData.fuel,
					inventory: {
						capacity: vehicleData.inventory,
						slots: vehicleData.slots
					},
					seats: vehicleStats.seats,
					stats: {
						speed: vehicleStats.speed,
						acceleration: vehicleStats.acceleration,
						handling: vehicleStats.handling,
						braking: vehicleStats.braking
					}
				}
			);
		});
		return new OperationSuccess(vehicles);
	} catch (error) {
		return new OperationFailed();
	}
});

const setActiveVehicleInVehShop = async (name: string, player: PlayerNR | Player, colors?: [RGB, RGB]): Promise<SetActiveVehicleResult> => {
	try {
		removePreviewVehicle(player);
		const npc = await getCurrentVendorFromClient(player);
		if (!npc) return new OperationFailed();
		const data = npc.getVariable('streamData')?.data;
		const vehicle = nr.vehicles.new(name, data.previewLocation, { heading: data.previewHeading, dimension: parseInt(player.rgscId) });
		player._previewVehicle = vehicle;
		vehicle.setVariable('class', name);
		vehicle.setVariable('owner', player);
		vehicle.setVariable('fuel', 50);
		if (colors) vehicle.setColorRGB(...colors[0], ...colors[1]);
		await sleep(100);
		return new OperationSuccess();
	} catch (error) {
		log.error(error.message);
		return new OperationFailed();
	}
};

nr.rpc.register(NREvents.setActiveVehicleInVehShop, async (name: string, { player }) => {
	return setActiveVehicleInVehShop(name, player);
});

nr.rpc.register(NREvents.buyVehicleInVehShop, async ({ name, moneySource }: BuyVehicleProps, { player }): Promise<BuyVehicleResult> => {
	try {
		const npc = await getCurrentVendorFromClient(player);
		const data = npc.getVariable('streamData')?.data;
		if (!npc) return new OperationFailed();
		const amount = vehicleAPI.getPrice(name);
		const vehicle = player._previewVehicle;
		if (!currencyAPI.checkWallet(player, moneySource, amount)) return new OperationFailed(BuyVehicleErrorTypes.NotEnoughMoney, localize.t('ERRORS.VEH_SHOP.NOT_ENOUGH_MONEY'));
		player.dimension = 0;
		currencyAPI.addCurrency(player, moneySource, -amount, `${name} from ${data?.alias}`);
		createVehicleAndInsert(player, name, data.location, data.heading, [vehicle.getColorRGB(0), vehicle.getColorRGB(1)]);
		log.debug(`NRage.shops.vehicles: ${player.name}(${player.rgscId}) purchased '${name}' for ${amount} ${moneySource}`);
		return new OperationSuccess();
	} catch (error) {
		return new OperationFailed();
	}
});

nr.rpc.register(NREvents.changeVehicleColorInVehShop, async (args, info) => {
	const { primary, secondary } = args;
	const { player } = info;
	const primaryRGB = HEXtoRGB(primary);
	const secondaryRGB = HEXtoRGB(secondary);
	const vehicle = player._previewVehicle || null;
	if (vehicle) {
		vehicleAPI.setColor(vehicle, primaryRGB, secondaryRGB);
	}
});

nr.rpc.register(NREvents.resetVehicleShop, (_, { player }) => {
	removePreviewVehicle(player);
	player.dimension = 0;
	player._fixedPosition = null;
	player._noSaveState = false;
});

nr.rpc.register(NREvents.doTestDrive, async (_, { player }) => {
	if (!player._previewVehicle) return;
	await nr.rpc.callClient(player, NREvents.closeDialog);
	player._previewVehicle.engine = true;
	player._previewVehicle.setVariable('engineState', true);
	player.putIntoVehicle(player._previewVehicle, RageEnums.VehicleSeat.DRIVER);
	await nr.rpc.callClient(player, NREvents.testDriveInterval, { vehicle: player._previewVehicle }, { noRet: true });
});

nr.rpc.register(NREvents.cancelTestDrive, (_, { player }) => {
	const vehClass = player._previewVehicle?.getVariable('class');
	const colors: [RGB, RGB] = [player._previewVehicle.getColorRGB(0) || [0, 0, 0], player._previewVehicle.getColorRGB(1) || [0, 0, 0]];
	destroyVehicle(player._previewVehicle);
	player.position = player._fixedPosition;
	setActiveVehicleInVehShop(vehClass, player, colors);
	setTimeout(() => {
		nr.rpc.callClient(player, NREvents.createLastDialog, { noRet: true });
	}, 100);
});