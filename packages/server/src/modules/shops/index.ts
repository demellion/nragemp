import rpc from 'rage-rpc';
import { NREvents } from 'common/enums/nrEvents';
import { log } from '../../index';
import { currencyAPI } from '../../classes/currency';

export * from './vehicles';

// Generic purchase
rpc.register(NREvents.purchase, (args, info) => {
	const { player } = info;
	const { source, amount, reason } = args;
	if (!currencyAPI.checkWallet(player, source, amount)) return false;
	log.debug(`NRage.currency.purchase: ${player.name}(${player.rgscId}) did '${reason}' for ${amount} ${source}`);
	currencyAPI.addCurrency(player, source, -amount, reason);
	return true;
});