import { nr } from '../../core/wrapper';
import moment from 'moment';
import accountController from '../../core/controllers/account/account.controller';
import { addMomentTimezone, instanceOfEntity, instanceOfVector } from '../../core/main';
import { bannedIDs, bannedIP } from '../../constants/bans';
import { AdminPermissions } from 'common/enums/permissions';
import { fsLog } from '../../utils/fslog';
import { Player } from 'rage-rpc';
import { log } from '../../index';
import { despawnVehicle, wrapVehicle } from '../vehicles';
import { localize } from 'common/index';
import { getIsoString } from 'common/pipes/date';
import { NREvents } from 'common/enums/nrEvents';

// Client login control
nr.events.add('incomingConnection', (ip, serial, rgscName, rgscId) => {
	if (!ip || !serial || !rgscName || !rgscId) return true;
	fsLog('', 'connectlog', `${rgscName}(${rgscId}) connected from ${ip}`);
	const compareResult = bannedIP.includes(ip) || bannedIDs.includes(rgscName) || bannedIDs.includes(parseInt(rgscId));
	if (compareResult) {
		log.warn(`NRage Server: Connection rejected for ${rgscName} (${rgscId}) from ${ip}`);
		return true;
	}
});

nr.events.add(NREvents.playerReady, (player) => {
	log.debug(`${player.socialClub} (${player.rgscId}) connected.`);
});

// NRay Gun
export const giveNRayGun = (player: PlayerMp): void => {
	if (!checkPermission(player, AdminPermissions.NRAY)) return;
	player.giveWeapon(mp.joaat('weapon_raypistol'), 0);
};
nr.events.add(NREvents.giveNRayGun, giveNRayGun);

// Jail function
export const sendPlayerToLimbo = (player: PlayerMp | Player): void => {
	player.dimension = 1;
	player.position = new mp.Vector3(-589.123046875, 2061.6826171875, 130.83465576171875);
	player.setVariable('jailStamp', addMomentTimezone(4, 'hours'));
};

// Functions
const checkPermission = (client: PlayerMp, permission: AdminPermissions): boolean => {
	if (!client) return false;
	const adminLevel = client.getVariable('adminLevel');
	return (adminLevel >= permission);
};

// Admin functions (remote events)
async function banPlayer(issuer: PlayerMp, violator: PlayerMp, reason: string, time: number): Promise<void> {
	const issuerID = issuer.rgscId;
	if (!checkPermission(issuer, AdminPermissions.BAN)) return;
	const violatorID = violator.rgscId;
	const dateFrom = getIsoString(moment());
	const banDate = getIsoString(moment().add(time, 'days'));
	try {
		await accountController.banAccount({
			socialClub: violatorID,
			banInfo: {
				dateFrom,
				dateTo: banDate,
				issuerID,
				reason
			}
		});
		const text = `NRage.banPlayer: ${violator.name} (${violatorID}) was banned by ${issuer.name}(${issuerID}) for ${time} minutes, reason: ${reason}`;
		const timeFormatted = moment(time, 'd').format('DD');
		violator.kick(localize.t('PLAYER.BAN', { time: banDate.toLocaleString(), text: reason }));
		nr.players.broadcast(localize.t('CHAT.BAN', { name: violator.name, time: timeFormatted, text: reason }));
		log.debug(text);
		fsLog('adminlog', `${issuer.socialClub}_${issuer.rgscId}`, text);
		fsLog('', 'bans', text);
	} catch (error) {
		log.error(`NRage.banPlayer: ${error.message}`);
	}
}

const kickPlayer = (issuer: PlayerMp, violator: PlayerMp, reason: string): void => {
	const issuerID = issuer.rgscId;
	if (!checkPermission(issuer, AdminPermissions.KICK)) return;
	const violatorID = violator.rgscId;
	try {
		violator.kick(localize.t('PLAYER.KICK', { text: reason }));
		const text = `NRage.kickPlayer: ${violator.name} (${violatorID}) was banned by ${issuer.name}(${issuerID}) for: ${reason}`;
		log.debug(text);
		fsLog('adminlog', `${issuer.socialClub}_${issuer.rgscId}`, text);
		nr.players.broadcast(localize.t('CHAT.KICK', { name: violator.name, text: reason }));
	} catch (error) {
		log.error(`NRage.kickPlayer: ${error.message}`);
	}
};

const warnPlayer = async (issuer: PlayerMp, violator: PlayerMp, state: boolean, reason?: string): Promise<void> => {
	const issuerID = issuer.rgscId;
	if (!checkPermission(issuer, AdminPermissions.WARN)) return;
	const violatorID = violator.rgscId;
	try {
		await accountController.warnAccount({
			socialClub: violatorID,
			blacklisted: state
		});
		if (!state) return; // Don't broadcast un-warns

		violator.kick(localize.t('PLAYER.WARN', { text: reason }));
		const text = `NRage.warnPlayer: ${violator.name} (${violatorID}) was warned by ${issuer.name}(${issuerID}) for: ${reason}`;
		log.debug(text);
		fsLog('adminlog', `${issuer.socialClub}_${issuer.rgscId}`, text);
		nr.players.broadcast(localize.t('CHAT.WARN', { name: violator.name, text: reason }));
	} catch (error) {
		log.error(`NRage.warnPlayer: ${error.message}`);
	}
};

function createVehicle(issuer: PlayerMp, type: string, position?: Vector3Mp, options?: {
	alpha?: number,
	color?: [Array2d, Array2d] | [RGB, RGB],
	dimension?: number,
	engine?: boolean,
	heading?: number;
	locked?: boolean,
	numberPlate?: string
}): void {
	if (!checkPermission(issuer, AdminPermissions.CREATE)) return;
	const vehicle = nr.vehicles.new(nr.joaat(type), (position === null ? issuer.position : position), options);
	wrapVehicle(vehicle, type, issuer);
	const text = `NRage.createVehicle: ${issuer.name}(${issuer.rgscId}) created vehicle '${type}' at ${JSON.stringify(position)}`;
	log.debug(text);
	fsLog('adminlog', `${issuer.socialClub}_${issuer.rgscId}`, text);
}

function teleport(issuer: PlayerMp, target: Vector3Mp | EntityMp): void {
	const issuerID = issuer.rgscId;
	if (!checkPermission(issuer, AdminPermissions.TELEPORTSELF)) return;
	try {
		if (instanceOfEntity(target)) {
			issuer.position = target.position;
			log.debug(`NRage.teleport: ${issuer.name} (${issuerID}) teleported to ${target.position.toString()}`);
		}
		if (instanceOfVector(target)) {
			issuer.position = target;
			log.debug(`NRage.teleport: ${issuer.name} (${issuerID}) teleported to ${target.toString()}`);
		}
	} catch (error) {
		log.error(`NRage.teleport: ${error.message}`);
	}
}

function teleportToMe(issuer: PlayerMp, target: PlayerMp): void {
	const issuerID = issuer.rgscId;
	if (!checkPermission(issuer, AdminPermissions.TELEPORT)) return;
	try {
		target.position = issuer.position;
		const text = `NRage.teleportToMe: ${issuer.name} (${issuerID}) teleported ${target.name} (${target.rgscId}) to himself`;
		log.debug(text);
		fsLog('adminlog', `${issuer.socialClub}_${issuer.rgscId}`, text);
	} catch (error) {
		log.error(`NRage.teleportToMe: ${error.message}`);
	}
}

function changeDimension(issuer: PlayerMp, target: EntityMp, dimension: number): void {
	const issuerID = issuer.rgscId;
	if (!checkPermission(issuer, AdminPermissions.DIMENSION)) return;
	try {
		target.dimension = dimension;
		const text = `NRage.changeDimension: ${issuer.name} (${issuerID}) changed dimension of ${target.getVariable('class')} to ${dimension}`;
		log.debug(text);
		fsLog('adminlog', `${issuer.socialClub}_${issuer.rgscId}`, text);
	} catch (error) {
		log.error(`NRage.changeDimension: ${error.message}`);
	}
}

async function jailPlayer(issuer: PlayerMp, target: PlayerMp): Promise<void> {
	const issuerID = issuer.rgscId;
	if (!checkPermission(issuer, AdminPermissions.JAIL)) return;
	try {
		sendPlayerToLimbo(target);
		await accountController.updateCharacter(target.name, { jailed: true });
		const text = `NRage.jailPlayer: ${issuer.name} (${issuerID}) jailed ${target.getVariable('class')}`;
		log.debug(text);
		fsLog('adminlog', `${issuer.socialClub}_${issuer.rgscId}`, text);
	} catch (error) {
		log.error(`NRage.jailPlayer: ${error.message}`);
	}
}

function freezePlayer(issuer: PlayerMp, target: PlayerMp): void {
	const issuerID = issuer.rgscId;
	if (!checkPermission(issuer, AdminPermissions.FREEZE)) return;
	try {
		const result = nr.rpc.callClient(target, 'adminGateway', { eventName: 'freezePlayer' });
		if (result) {
			const text = `NRage.jailPlayer: ${issuer.name} (${issuerID}) frozen ${target.getVariable('class')}`;
			log.debug(text);
			fsLog('adminlog', `${issuer.socialClub}_${issuer.rgscId}`, text);
			return;
		}
		const text = `NRage.jailPlayer: ${issuer.name} (${issuerID}) un-frozen ${target.getVariable('class')}`;
		log.debug(text);
		fsLog('adminlog', `${issuer.socialClub}_${issuer.rgscId}`, text);
	} catch (error) {
		log.error(`NRage.freezePlayer: ${error.message}`);
	}
}

function setEntityPosition(issuer: PlayerMp, entity: EntityMp | VehicleMp, position: Vector3Mp): void {
	if (!checkPermission(issuer, AdminPermissions.NRAY)) return;
	try {
		entity.position = position;
		if ('rotation' in entity) {
			entity.rotation = new mp.Vector3(0, 0, entity.heading);
		}
	} catch (error) {
		log.error(`NRage.setEntityPosition: ${error.message}`);
	}
}

function deleteEntity(issuer: PlayerMp, target: VehicleMp | ObjectMp | PlayerMp): void {
	const issuerID = issuer.rgscId;
	if (!target.getVariable) return;
	if (!checkPermission(issuer, AdminPermissions.NRAY)) return;
	try {
		const what = target.getVariable('class') || `unknown ${target.type}`;
		const text = `NRage.deleteEntity: ${issuer.name} (${issuerID}) deleted ${what} at ${target.position}`;
		log.warn(text);
		fsLog('adminlog', `${issuer.socialClub}_${issuer.rgscId}`, text);
		if ('velocity' in target) {
			despawnVehicle(target);
			return;
		}
		target.destroy();
	} catch (error) {
		log.error(`NRage.deleteEntity: ${error.message}`);
	}
}

function createEntity(issuer: PlayerMp, hash: string, position: Vector3Mp): void {
	if (!checkPermission(issuer, AdminPermissions.NRAY)) return;
	try {
		const object = nr.objects.new(hash, position, {
			rotation: new mp.Vector3(0, 0, 0),
			alpha: 255,
			dimension: issuer.dimension
		});
		if (!object) return;
		const text = `NRage.createEntity: ${issuer.name} (${issuer.rgscId}) created ${hash} at ${JSON.stringify(position)}`;
		log.warn(text);
		fsLog('adminlog', `${issuer.socialClub}_${issuer.rgscId}`, text);
	} catch (error) {
		log.error(`NRage.createEntity: ${error.message}`);
	}
}

// Init admin functions with events
nr.events.add(NREvents.banPlayer, banPlayer);
nr.events.add(NREvents.kickPlayer, kickPlayer);
nr.events.add(NREvents.warnPlayer, warnPlayer);
nr.events.add(NREvents.createVehicle, createVehicle);
nr.events.add(NREvents.teleport, teleport);
nr.events.add(NREvents.teleportToMe, teleportToMe);
nr.events.add(NREvents.changeDimension, changeDimension);
nr.events.add(NREvents.jailPlayer, jailPlayer);
nr.events.add(NREvents.freezePlayer, freezePlayer);
nr.events.add(NREvents.setEntityPosition, setEntityPosition);
nr.events.add(NREvents.deleteEntity, deleteEntity);
nr.events.add(NREvents.createEntity, createEntity);
