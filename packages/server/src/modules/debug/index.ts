/* eslint-disable no-eval, @typescript-eslint/no-unused-vars */
// @ts-nocheck
import { getLogger, Loggers } from '../../utils/logger';
import { NREvents } from '../../../../common/enums/nrEvents';
import moment from 'moment';
import * as API from '../../core/main';
import { nr } from '../../core/wrapper';
import rpc, { ProcedureListenerInfo } from 'rage-rpc';

const log = getLogger(Loggers.server);

const debug = (code: string, info: ProcedureListenerInfo): string => {
	const now = moment().format('HH:mm:ss');
	const { player } = info;
	const vehicle = player.vehicle;
	try {
		switch (true) {
			case (code.includes('V ')): {
				code = code.replace('V ', '');
				API.createAdminVeh(code, player);
				return `[S] ${now}: Vehicle "${code}" created`;
			}
			default: {
				const result = eval(code);
				if (!result) {
					return;
				}
				log.debug(`${player.name} (${player.rgscId}): ${result}`);
				return `[S] ${now}: ${result}`;
			}
		}
	} catch (error) {
		const result = error.message;
		log.debug(result);
		return `[S] ${now}: ${result}`;
	}
};

nr.rpc.register(NREvents.debug, debug);

nr.rpc.register(NREvents.log, (message: string) => {
	log.info(message);
});
