import { nr } from '../../../core/wrapper';
import { getGender, playAnimation } from '../../../core/main';
import { log } from '../../../index';
import { easingsFunctions } from '../../../../../common/constants/easingsFunctions';
import { sleep } from '../../../../../common/pipes/sleep';

const spinnedArray: string[] = [];
let isWheelSpinning = false;
nr.rpc.register('spinWheel', async (args, info) => {
	if (isWheelSpinning) return 'GENERIC.ISSPINNING';
	const wheel: ObjectMp = args.wheel;
	const { player } = info;
	const rgscId = player.rgscId;
	if (spinnedArray.includes(rgscId)) return 'GENERIC.ALREADYSPINNED';
	spinnedArray.push(rgscId);
	player.position = new mp.Vector3(1110.2734375, 228.99310302734375, -49.63580322265625);
	player.heading = 6.36;
	const gender = getGender(player);
	if (!gender) return; // https://youtu.be/AzllNSKItYw
	isWheelSpinning = true;
	playAnimation(player, `anim_casino_a@amb@casino@games@lucky7wheel@${gender}`, 'enter_to_armraisedidle', 1, 1750);
	await sleep(1750);
	playAnimation(player, `anim_casino_a@amb@casino@games@lucky7wheel@${gender}`, 'armraisedidle_to_spinningidle_med', 1, 1750, 2.0);
	let preSpinTime = 5000;
	const totalTime = preSpinTime;
	const spinAngleIncrement = 5;
	const spinFrequency = 10;
	const spinInterval = setInterval(() => {
		const progress = preSpinTime / totalTime;
		const spinIncrement = spinAngleIncrement * (easingsFunctions.outCirc(progress));
		preSpinTime -= spinFrequency;
		wheel.rotation = new mp.Vector3(wheel.rotation.x, (wheel.rotation.y - spinIncrement) % 360, wheel.rotation.z);
		if (preSpinTime <= 0) clearInterval(spinInterval);
	}, spinFrequency);
	await sleep(preSpinTime);

	let postSpinTime = 3000 + Math.random() * 2000;
	const postSpinInterval = setInterval(() => {
		postSpinTime -= spinFrequency;
		wheel.rotation = new mp.Vector3(wheel.rotation.x, (wheel.rotation.y - 0.25) % 360, wheel.rotation.z);
		if (postSpinTime <= 0) clearInterval(postSpinInterval);
	}, spinFrequency);
	await sleep(postSpinTime);
	isWheelSpinning = false;
	log.debug(`NRage.spinWheel: ${player.name}(${player.rgscId}) spinned wheel for ${wheel.rotation.y}`);
	return 'GENERIC.SPINFINISHED';
});
