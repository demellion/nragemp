import { nr } from '../../core/wrapper';
import { NREvents } from 'common/enums/nrEvents';
import { Player } from 'rage-rpc';
import { createHash } from '../../core/main';
import vehicleController from '../../core/controllers/vehicle/vehicle.controller';
import { log } from '../../index';
import { sleep } from 'common/pipes/sleep';
import { vehicleAPI } from '../../classes/vehicles';

// Events
mp.events.add('playerEnterVehicle', (player: PlayerMp, vehicle: VehicleMp, seat) => {
	const neonColor = vehicle.getNeonColor();
	if (vehicle.neonEnabled) vehicle.setNeonColor(neonColor[0], neonColor[1], neonColor[2]); // Server forgets to sync that sometimes.
	log.trace(`NRage.playerEnterVehicle: ${player.name} entered ${vehicle.getVariable('class')} at ${seat} seat`);
});

// Player vehicle actions
const saveEngineState = (args: any) => {
	const { vehicle } = args;
	const state = vehicle.engine;
	vehicle.engine = !state;
	vehicle.setVariable('engineState', !state);
	if (vehicle.engine && vehicle.neonEnabled) {
		log.debug(`${vehicle.neonEnabled}`);
		setTimeout(() => {
			const neonColor = vehicle.getNeonColor();
			vehicle.setNeonColor(neonColor[0], neonColor[1], neonColor[2]); // Server forgets to sync that sometimes.
		}, 100);
	}
};
nr.rpc.register(NREvents.saveEngineState, saveEngineState);
const saveLockState = (args: any) => {
	const { vehicle } = args;
	const state = vehicle.locked;
	vehicle.locked = !state;
	vehicle.setVariable('lockedState', !state);
	const streamedPlayers: PlayerMp[] = vehicle.streamedPlayers;
	streamedPlayers.forEach((thisPlayer) => {
		thisPlayer.call('broadcast.lockSound', [vehicle, !state]);
	});
};
nr.rpc.register(NREvents.saveLockState, saveLockState);
nr.rpc.register('broadcast.shareWaypoint', (args: any) => {
	const { position, vehicle } = args;
	const playersInVehicle: PlayerMp[] = vehicle.getOccupants();
	playersInVehicle.forEach((thisPlayer) => {
		nr.rpc.callClient(thisPlayer, 'broadcast.createSharedWaypoint', position);
	});
});

// Vehicle wrapper
export const wrapVehicle = (vehicle: VehicleMp, type: string, owner: PlayerMp | Player, key?: string): void => {
	vehicle.setVariable('key', key || null);
	vehicle.setVariable('class', type);
	vehicle.setVariable('owner', owner);
	vehicle.setVariable('lockedState', true);
	vehicle.locked = true;
	const variable = key ? 'ownedVehicle' : 'rentedVehicle';
	const fuel = vehicleAPI.getFuel(type) || -1;
	vehicle.setVariable('fuel', fuel);

	// Despawn current vehicle
	const thisVehicle = owner.getVariable(variable);
	if (thisVehicle) despawnVehicle(thisVehicle);
	owner.setVariable(variable, vehicle);
};

export const despawnVehicle = (vehicle: VehicleMp): void => {
	const key = vehicle.getVariable('key');
	const owner = vehicle.getVariable('owner');
	if (!owner) return destroyVehicle(vehicle);
	log.debug(`Despawning vehicle: '${vehicle.getVariable('class')}' (${key || 'rented vehicle'}) was despawned`);
	if (!key) {
		owner.setVariable('rentedVehicle', null);
		destroyVehicle(vehicle);
		return;
	}

	// Save
	vehicleAPI.saveVehicle(vehicle);
	owner.setVariable('ownedVehicle', null);
	destroyVehicle(vehicle);
};
export const destroyVehicle = (vehicle: VehicleMp): void => {
	if (!mp.vehicles.exists(vehicle)) return;
	if (!vehicle?.destroy) return;
	vehicle.destroy();
};

// Create vehicle for player
export const createVehicleAndInsert = async (
	player: PlayerMp | Player,
	vehicleType: string,
	position: Vector3Mp,
	heading: number,
	color: [RGB, RGB]
): Promise<void> => {
	const key = createHash(24);
	await vehicleController.createVehicle(key, vehicleType, player);
	const thisVehicle = nr.vehicles.new(vehicleType, position, {
		numberPlate: '',
		dimension: 0,
		heading
	});
	await sleep(50);
	wrapVehicle(thisVehicle, vehicleType, player, key);

	/*
		Example of mod application:

		vehicleAPI.setNeonColor(thisVehicle, [255, 0, 0]);
		vehicleAPI.setMod(thisVehicle, 11, 3);
		vehicleAPI.setColor(thisVehicle, color[0], color[1]);
		vehicleAPI.setModCustom(thisVehicle, 'tireSmoke', [255, 0, 0]);
		vehicleAPI.setModCustom(thisVehicle, 'gps', true);
		vehicleAPI.setModCustom(thisVehicle, 'remote', true);
	*/
	vehicleAPI.setColor(thisVehicle, color[0], color[1]);
	vehicleAPI.saveMods(thisVehicle);
	await sleep(50);
	player.putIntoVehicle(thisVehicle, RageEnums.VehicleSeat.DRIVER);
};

// Load vehicle
export const loadVehicle = async (key: string, owner: PlayerMp | Player): Promise<VehicleMp> => {
	const data = await vehicleController.loadVehicle({ key });
	if (!data) return null; // No vehicle?
	if (data.impounded) return null; // Vehicle was impounded, cancel.
	if (!data.position) return null; // Vehicle was not properly saved?
	const thisVehicle = nr.vehicles.new(data.modelHash, owner.position, {
		alpha: 0,
		numberPlate: data.numberplate,
		dimension: -1,
		heading: data.heading
	});
	await sleep(50);
	wrapVehicle(thisVehicle, data.modelHash, owner, data.key);
	vehicleAPI.loadMods(thisVehicle, data.mods, data.inventory, data.fuel, data.numberplate);
	// Don't even fucking ask why, you don't want to know.
	thisVehicle.position = new mp.Vector3(...data.position);
	thisVehicle.dimension = 0;
	thisVehicle.alpha = 255;
};
