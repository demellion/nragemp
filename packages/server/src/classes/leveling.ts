import { Player } from 'rage-rpc';
import { nr, PlayerNR } from '../core/wrapper';
import { min } from '../core/main';
import { NREvents } from 'common/enums/nrEvents';
import { LevelingPreset } from 'common/enums/leveling';
import { currencyAPI } from './currency';

class Leveling {
	exponentialCap: number;
	exponentialValue: number;

	constructor() {
		this.exponentialCap = LevelingPreset.CAP;
		this.exponentialValue = LevelingPreset.EXPVALUE;
	}

	getXPLevel(level: number): number {
		const formula = this.exponentialValue + (this.exponentialValue * (min(level, this.exponentialCap) * (Math.log(min(level, this.exponentialCap)))));
		return formula ? Math.round(formula) : this.exponentialValue;
	}

	getXPMultiplier(player: PlayerNR | Player): number {
		const vipLevel = player.getVariable('vipLevel') || 0;
		if (vipLevel === 0) return 1;
		if (vipLevel >= 1) return 2;
		return 1;
	}

	setLevel(player: PlayerMp | Player, level: number): void {
		const data = player.getVariable('levelData') || {};
		data.currentLevel = level;
		player.setVariable('levelData', data);
	}

	setXP(player: PlayerMp | Player, xp: number): void {
		const data = player.getVariable('levelData') || {};
		data.currentXP = xp;
		player.setVariable('levelData', data);
	}

	addXP(player: PlayerMp | Player, amount: number): void {
		const data = player.getVariable('levelData') || { currentLevel: 0, currentXP: 0 };
		let previousXP = data.currentXP;
		amount = amount * this.getXPMultiplier(player);
		let levelup = false;
		let currentLevel = data.currentLevel;
		let currentXP = data.currentXP;
		let currentLimit = this.getXPLevel(currentLevel + 1);
		while (currentXP + amount >= currentLimit) {
			const limitDiff = currentLimit - currentXP;
			levelup = true;
			currentXP = 0; // Reset XP
			previousXP = 0; // Reset XP value not to look backwards
			amount = amount - limitDiff; // Calculate diff
			currentLevel++; // Increase level
			currentLimit = this.getXPLevel(currentLevel + 1); // Update level limit to next level
		}
		currentXP += amount;
		player.setVariable('levelData', { currentLevel, currentXP });
		nr.rpc.callClient(player, NREvents.updateRankBar, {
			limit: 0,
			nextLimit: this.getXPLevel(currentLevel + 1),
			previousXP,
			levelup
		});

		// VIP awards
		if (player.getVariable('vipLevel') > 0) {
			currencyAPI.addCurrency(player, 'bank', 5000, 'VIP Level-up award');
		}
	}
}

export const levelAPI = new Leveling();
