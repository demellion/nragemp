import { log } from '../index';
import { Player } from 'rage-rpc';
import { PlayerNR } from '../core/wrapper';

const EventEmitter = require('events');

interface Currency {
	name: string
}

type CurrencyType = 'cash' | 'bank';

class CurrencyScript extends EventEmitter {
	currencies: { [k: string]: Currency };
	variable: string

	constructor() {
		super();
		this.currencies = {};
		this.variable = 'wallet';
	}

	createCurrency(key: CurrencyType, name: string): void {
		if (this.currencies[key]) return log.fatal(`NRage.libs.currencyAPI: '${key}' currency is already defined`);
		this.currencies[key] = { name };
		this.emit('currencyDefined', key, name);
		log.debug(`NRage.libs.currencyAPI: Created '${key}' currency type`);
	}

	getCurrency(player: PlayerMp | Player, key: CurrencyType): number {
		const wallet = player.getVariable(this.variable);
		return wallet[key] || -1;
	}

	getAllCurrencies() {
		return Object.keys(this.currencies);
	}

	getWallet(player: PlayerMp | Player): object {
		return player.getVariable(this.variable) || {};
	}

	checkWallet(player: PlayerMp | Player, key: string, amount: number): boolean {
		const wallet = player.getVariable('wallet');
		if (!(key in wallet)) {
			log.error(`NRage.currency.purchase: ${player.name}(${player.rgscId}) has no '${key}' currency in wallet`);
			return false;
		}
		return !(amount < 0 || wallet[key] < amount);
	}

	setWallet(player: PlayerMp | Player, newWallet: { [k: string]: number }): void {
		const oldWallet = player.getVariable(this.variable) || {};
		const currencyKeys = this.getAllCurrencies();
		if (currencyKeys.length !== Object.keys(newWallet).length) log.error(`NRage.setWallet: Didn't match a currency set for ${player.name} (${player.rgscId})`);
		player.setVariable(this.variable, newWallet);
		currencyAPI.emit('walletReplaced', player, oldWallet, newWallet);
	}

	setCurrency(player: PlayerMp | Player, key: CurrencyType, amount: number, reason?: string): void {
		const currencyKeys = currencyAPI.getAllCurrencies();
		if (!currencyKeys.includes(key)) return log.error(`NRage.setCurrency: '${key}' is not a valid currency key`);
		const thisWallet = player.getVariable(this.variable) || {};
		const oldAmount = thisWallet[key] || -1;
		thisWallet[key] = amount;
		player.setVariable(this.variable, thisWallet);
		currencyAPI.emit('currencySet', player, key, oldAmount, amount, reason || 'setCurrency');
	}

	addCurrency(player: PlayerMp | Player, key: CurrencyType, amount: number, reason?: string): void {
		const thisWallet = player.getVariable(this.variable) || {};
		const oldAmount = thisWallet[key] || -1;
		thisWallet[key] = thisWallet[key] + amount;
		player.setVariable(this.variable, thisWallet);
		currencyAPI.emit('currencyAdd', player, key, oldAmount, amount, reason || 'addCurrency');
	}

	swapCurrency(player: PlayerMp | Player, source: string, to: string, amount: number, reason?: string): void {
		const thisWallet = player.getVariable(this.variable) || {};
		if (!thisWallet[source] || !thisWallet[to]) return log.error(`NRage.swapCurrency: Couldn't swap '${source}' to '${to}' for ${player.name} (${player.rgscId})`);
		if (thisWallet[source] < amount) return;
		thisWallet[source] = thisWallet[source] - amount;
		thisWallet[to] = thisWallet[to] + amount;
		player.setVariable(this.variable, thisWallet);
		currencyAPI.emit('currencySwap', player, source, amount, to, reason || 'swapCurrency');
	}

	addPoints(player: PlayerNR | Player, amount: number, reason?: string): void {
		const previousAmount = player.getVariable('points') || -1;
		player.setVariable('points', previousAmount + amount);
		currencyAPI.emit('pointsAdd', player, 'NRPoints', previousAmount, amount, reason || 'SERVER');
	}
}

export const currencyAPI = new CurrencyScript();