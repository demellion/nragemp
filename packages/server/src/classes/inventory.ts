import { log } from '../index';
import { Player } from 'rage-rpc';
import { Backpacks } from 'common/constants/inventory';
import { numberRange } from 'common/pipes/range';
import { nr } from '../core/wrapper';
import { NREvents } from 'common/enums/nrEvents';

// Items
type ItemType = 'weapon' | 'ammunition' | 'generic' | 'gear' | 'backpack' | 'throw';
type InventoryType = 'general' | 'backpack';
const maxGridX = 6;
const defaultInventory: InventoryPlayer = { general: [], backpack: [], gear: {} };

interface Item {
	name: string, // Localized title
	type: ItemType, // Type
	stacks: number, // Stacks
	tradable: boolean; // Can be traded
	removable: boolean; // Can be removed without force flag
	size: [number, number]; // X,Y scale
	weight: number; // Weight in kgs
}

interface InventoryItem {
	k: string;
	p: number;
	c: number;
	d: any
}

interface GearItem {
	key: string;
	data?: any;
}

// Inventories
interface Gear {
	// https://wiki.rage.mp/index.php?title=Clothes
	// Clothes
	mask: GearItem;
	torso: GearItem;
	legs: GearItem;
	backpack: GearItem;
	shoes: GearItem;
	accessories: GearItem;
	undershirt: GearItem;
	armor: GearItem;
	decal: GearItem;
	top: GearItem;
	// Props
	hat: GearItem;
	glasses: GearItem;
	ear: GearItem;
	watch: GearItem;
	bracelet: GearItem;
}

export interface InventoryPlayer {
	general: InventoryItem[];
	backpack: InventoryItem[];
	gear: Partial<Gear>;
}

// interface InventoryCargo {
// 	general: InventoryItem[];
// }

class Inventory {
	items: { [k: string]: Item };
	playerGridX: number;
	playerGridY: number;
	playerMaxLoad: number;

	constructor() {
		this.items = {};
		this.playerGridX = maxGridX;
		this.playerGridY = 10;
		this.playerMaxLoad = 10;
	}

	// System operations
	createItem(keyString: string, name: string, type: ItemType, stacks: number, size: [number, number], tradable: boolean, weight: number, removable = true): void {
		const key = `_${keyString}`;
		if (this.items[key]) return log.error(`NRage.inventoryAPI.createItem: '${keyString}' already exists in items.`);
		this.items[key] = {
			name,
			type,
			stacks,
			tradable,
			removable,
			size,
			weight
		};
		log.trace(`NRage.inventoryAPI.createItem: Created '${keyString}' as ${type}.`);
	}

	itemExists(keyString: string): boolean {
		const key = `_${keyString}`;
		return !!this.items[key];
	}

	getItemData(keyString: string): Item {
		/* Returns all data for InventoryItem */
		const key = `_${keyString}`;
		return this.items[key] || null;
	}

	getItemWeight(keyString: string): number {
		/* Returns weight of InventoryItem */
		const key = `_${keyString}`;
		return this.items[key]?.weight || 0;
	}

	getItemDimensions(keyString: string): [number, number] {
		/* Returns [x,y] of InventoryItem */
		const key = `_${keyString}`;
		return this.items[key]?.size || null;
	}

	getInventoryWeight(items: InventoryItem[]): number {
		/* Returns the total weight of items in InventoryItem[] */
		let weight = 0;
		items.forEach((thisItem) => {
			const itemWeight = this.getItemWeight(thisItem.k);
			weight += itemWeight;
		});
		return weight;
	}

	getBackpackMaxLoad(keyString: string): number {
		/* Returns max load weight of a backpack by its key */
		return Backpacks[keyString]?.weight || 0;
	}

	private getBackpackDimensions(keyString: string): [number, number] {
		/* Returns [x, y] size of a backpack by its key */
		return Backpacks[keyString].slots || null;
	}

	// private findItem(keyString: string, where: InventoryType): boolean {
	//
	// }

	private getItemMap(keyString: string, position: number): number[] {
		/* Returns number[] of item occupied positions relative to specified position */
		const [sizeX, sizeY] = this.getItemDimensions(keyString);
		if (!sizeX || !sizeY) {
			log.error(`NRage.inventoryAPI.getItemMap: '${keyString}' has invalid size.`);
			return null;
		}
		const array: number[] = [];
		for (let i = 0; i < sizeY; i++) {
			const iteration = position + (maxGridX * i);
			const range = numberRange(iteration, iteration + sizeX - 1);
			range.forEach(value => array.push(value));
		}
		return array;
	}

	private getInventoryMap(items: InventoryItem[]): number[] {
		/* Returns number[] of occcupied slots in InventoryItem[] */
		const slotsOccupied: number[] = [];
		items.forEach((thisItem) => {
			const thisItemArray = this.getItemMap(thisItem.k, thisItem.p);
			thisItemArray.forEach(value => slotsOccupied.push(value));
		});
		return slotsOccupied;
	}

	canFitAny(inventory: InventoryItem[], dimensions: [number, number], keyString: string): number {
		/* Returns InventoryItem[] position or -1 if there is no space */
		const inventoryMap = this.getInventoryMap(inventory);
		const itemDimensions = this.getItemDimensions(keyString);
		log.trace(`NRage.inventoryAPI.canFitAny: Inventory map: ${inventoryMap}`);
		log.trace(`NRage.inventoryAPI.canFitAny: Item dimensions: ${itemDimensions}`);
		let position = -1;
		for (let i = 0; i <= dimensions[0] * dimensions[1]; i++) {
			if (position !== -1) break;
			const itemMap = this.getItemMap(keyString, i);
			if (position % maxGridX + itemDimensions[0] > maxGridX) {
				log.trace(`NRage.inventoryAPI.canFitAny: Out of bounds (X) for ${keyString} position: ${i}`);
				continue;
			} // Out of bounds (X)
			if (Math.max(...itemMap) > (dimensions[0] * dimensions[1])) {
				log.trace(`NRage.inventoryAPI.canFitAny: Out of bounds (Y) for ${keyString} position: ${i}`);
				continue;
			} // Out of bounds (Y)
			if (inventoryMap.filter(value => itemMap.includes(value)).length > 0) {
				log.trace(`NRage.inventoryAPI.canFitAny: Item crossed occupied slots for ${keyString} position: ${i}`);
				continue;
			} // Crosses occupied slots
			position = i;
		}
		log.trace(`NRage.inventoryAPI.canFitAny: Item fitted in ${position}`);
		return position;
	}

	canFitPosition(entity: PlayerMp | Player, where: InventoryType, position: number, keyString: string, amount: number): boolean {
		/* Returns true if one of player inventories can fit item in specified position. Counts weights and X,Y grid alignments. */
		if (!this.itemExists(keyString)) return false; // No item class
		const itemDimensions = this.getItemDimensions(keyString);
		if (!itemDimensions) return false; // No item size
		if (position % maxGridX + itemDimensions[0] > maxGridX) {
			log.trace(`NRage.inventoryAPI.canFitPosition: Out of bounds (X) for ${keyString} at ${position}(${where})`);
			return false;
		} // Out of bounds (X)
		let inventory: InventoryPlayer = JSON.parse(entity.getVariable('inventory') || 'null');
		if (!inventory) inventory = defaultInventory;
		const itemWeight = this.getItemWeight(keyString) * amount;

		switch (where) {
			case 'general': {
				const inventoryWeight = this.getInventoryWeight(inventory[where]);
				if (inventoryWeight + itemWeight > this.playerMaxLoad) {
					log.trace(`NRage.inventoryAPI.canFitPosition: Weight exceeded (${inventoryWeight} + ${itemWeight}) for ${keyString}(${amount}) max: ${this.playerMaxLoad}`);
					return false;
				} // Weight exceeded
				const itemMap = this.getItemMap(keyString, position);
				if (Math.max(...itemMap) > (this.playerGridX * this.playerGridY)) {
					log.trace(`NRage.inventoryAPI.canFitPosition: Out of bounds (Y) for ${keyString} at ${position}(${where})`);
					return false;
				} // Out of bounds (Y)
				const inventoryMap = this.getInventoryMap(inventory.general);
				if (inventoryMap.filter(value => itemMap.includes(value)).length > 0) {
					log.trace(`NRage.inventoryAPI.canFitPosition: Item crossed occupied slots for ${keyString} at ${position}(${where})`);
					return false;
				} // Crosses occupied slots
				return true;
			}
			case 'backpack': {
				const backpackString = inventory.gear?.backpack?.key;
				if (!backpackString) return false; // No backpack
				const backpackWeight = this.getBackpackMaxLoad(backpackString);
				const inventoryWeight = this.getInventoryWeight(inventory[where]);
				if (inventoryWeight + itemWeight > backpackWeight) {
					log.trace(`NRage.inventoryAPI.canFitPosition: Weight exceeded (${inventoryWeight} + ${itemWeight}) for ${keyString}(${amount}) max: ${backpackWeight}`);
					return false;
				} // Weight exceeded
				const backpackDimensions = this.getBackpackDimensions(backpackString);
				const itemMap = this.getItemMap(keyString, position);
				if (Math.max(...itemMap) > (backpackDimensions[0] * backpackDimensions[1])) {
					log.trace(`NRage.inventoryAPI.canFitPosition: Out of bounds (Y) for ${keyString} at ${position}(${where})`);
					return false;
				} // Out of bounds (Y)
				const inventoryMap = this.getInventoryMap(inventory[where]);
				if (inventoryMap.filter(value => itemMap.includes(value)).length > 0) {
					log.trace(`NRage.inventoryAPI.canFitPosition: Item crossed occupied slots for ${keyString} at ${position}(${where})`);
					return false;
				} // Crosses occupied slots
				return true;
			}
			default: {
				return false;
			}
		}
	}

	addItem(player: PlayerMp | Player, keyString: string, amount = 1, data?: any, position = -1): boolean {
		/*
			Adds item to player general space. If position is not specified will attempt to add anywhere possible.
			Returns true if succeeded
		*/
		log.trace(`Trying to add ${keyString}(${amount}) to ${player.name} at ${position}`);
		if (!this.itemExists(keyString)) return false; // No item class
		let inventory: InventoryPlayer = JSON.parse(player.getVariable('inventory') || 'null');
		if (!inventory) inventory = defaultInventory;
		const amountMax = this.getItemData(keyString).stacks;
		// Add anywhere possible
		if (position === -1) {
			const expectedPosition = this.canFitAny(inventory.general, [this.playerGridX, this.playerGridY], keyString);
			if (expectedPosition === -1) return false;
			position = expectedPosition;
		}
		if (!this.canFitPosition(player, 'general', position, keyString, amount)) return false;
		amount = amount > amountMax ? amountMax : amount;
		inventory.general.push({
			k: keyString,
			p: position,
			d: data,
			c: amount
		});
		player.setVariable('inventory', JSON.stringify(inventory));
	}

	addItemBackpack(player: PlayerMp | Player, keyString: string, amount = 1, data?: any, position = -1): boolean {
		/*
			Adds item to player backpack. If position is not specified will attempt to add anywhere possible.
			Returns true if succeeded
		*/
		if (!this.itemExists(keyString)) return false; // No item class
		let inventory: InventoryPlayer = JSON.parse(player.getVariable('inventory') || 'null');
		if (!inventory) inventory = defaultInventory;
		const amountMax = this.getItemData(keyString).stacks;
		// Add anywhere possible
		if (position === -1) {
			const backpackString = inventory.gear?.backpack?.key;
			const backpackDimensions = this.getBackpackDimensions(backpackString);
			if (!backpackString) return false; // No backpack
			const expectedPosition = this.canFitAny(inventory.backpack, backpackDimensions, keyString);
			if (expectedPosition === -1) return false;
			position = expectedPosition;
		}
		if (!this.canFitPosition(player, 'backpack', position, keyString, amount)) return false;
		amount = amount > amountMax ? amountMax : amount;
		inventory.backpack.push({
			k: keyString,
			p: position,
			d: data,
			c: amount
		});
		player.setVariable('inventory', JSON.stringify(inventory));
	}

	// removeItemPosition(player: PlayerMp | Player, position: number, force?: boolean): boolean {
	// 	const inventory: InventoryPlayer = JSON.parse(player.getVariable('inventory') || 'null');
	// 	if (!inventory) return false;
	// }
	//
	// removeItemBackpackPosition(player: PlayerMp | Player, position: number, force?: boolean): boolean {
	// 	const inventory: InventoryPlayer = JSON.parse(player.getVariable('inventory') || 'null');
	// 	if (!inventory) return false;
	// }

	// Cargo operations
}

export const inventoryAPI = new Inventory();

nr.events.add(NREvents.playerJoin, (player: PlayerMp) => {
	player.setVariable('inventory', JSON.stringify(defaultInventory));
});