import { VehicleColors, VehicleConfig, VehicleMods, VehicleModsCustom, VehiclesPool } from 'common/constants/vehicles';
import vehicleController from '../core/controllers/vehicle/vehicle.controller';
import { RGB } from 'color-name';
import { log } from '../index';
import { IVehicleMods } from '../core/models/vehicle/vehicle.model';

class Vehicles {
	get(model: string): VehicleConfig {
		const modelName = model.toUpperCase();
		return VehiclesPool[modelName] ? VehiclesPool[modelName] : null;
	}

	getAll(): [string, VehicleConfig][] {
		return Object.entries(VehiclesPool);
	}

	getFuel(model: string): number {
		const modelName = model.toUpperCase();
		return VehiclesPool[modelName] ? VehiclesPool[modelName].fuel : null;
	}

	getPrice(model: string): number {
		const modelName = model.toUpperCase();
		return VehiclesPool[modelName] ? VehiclesPool[modelName].price : null;
	}

	getInventorySize(model: string): number {
		const modelName = model.toUpperCase();
		return VehiclesPool[modelName] ? VehiclesPool[modelName].inventory : null;
	}

	getFuelConsumptionRate(model: string): number {
		const modelName = model.toUpperCase();
		const type = VehiclesPool[modelName] ? VehiclesPool[modelName].type : null;
		if (!type) return 0;
		if (['Plane', 'Helicopter'].includes(type)) return 0.5;
		if (['Commercial', 'Truck', 'Offroad', 'Military'].includes(type)) return 0.033;
		if (['Boat', 'Utility', 'Pickup', 'Emergency', 'Van', 'Service', 'Wheel', 'OpenWheel', 'SUV'].includes(type)) return 0.023;
		if (['Coupe', 'Classic', 'Super', 'Sport', 'Motorcycle', 'Sedan', 'Muscle', 'Compact'].includes(type)) return 0.015;
		if (['Train', 'Bicycle', 'Trailer'].includes(type)) return 0;
		return 0;
	}

	setMod(vehicle: VehicleMp, modIndex: number, value: number): void {
		vehicle.setMod(modIndex, value); // More reference on VehicleMods
	}

	getModCustom(vehicle: VehicleMp, name: VehicleModsCustom): any {
		const modsCustom = vehicle.getVariable('modsCustom') || {};
		if (!modsCustom[name]) return null;
		return modsCustom[name];
	}

	setModCustom(vehicle: VehicleMp, name: VehicleModsCustom, value: any): void {
		// More reference at IVehicleMods
		const modsCustom = vehicle.getVariable('modsCustom') || {};
		modsCustom[name] = value;
		vehicle.setVariable('modsCustom', modsCustom);
	}

	setColor(vehicle: VehicleMp, colorPrimary: RGB, colorSecondary: RGB): void {
		vehicle.setColorRGB(...colorPrimary, ...colorSecondary);
	}

	setPearlescentColor(vehicle: VehicleMp, color: VehicleColors): void {
		vehicle.pearlescentColor = color;
	}

	setNeonColor(vehicle: VehicleMp, color: RGB): void {
		vehicle.setNeonColor(...color);
		vehicle.neonEnabled = true;
	}

	saveVehicle(vehicle: VehicleMp): void {
		const key = vehicle.getVariable('key');
		const impounded = false; // TODO: Impound conditions
		const position = vehicle.position;
		const heading = vehicle.heading;
		const numberplate = vehicle.numberPlate;
		const fuel = vehicle.getVariable('fuel') || -1;
		const mods = vehicle.getVariable('mods') || null;
		const inventory = vehicle.getVariable('inventory') || null;
		vehicleController.updateVehicle({ key }, {
			position: [position.x, position.y, position.z],
			heading,
			numberplate,
			fuel,
			mods,
			inventory: inventory ? JSON.stringify(inventory) : null,
			impounded
		});
	}

	loadMods(vehicle: VehicleMp, mods: Partial<IVehicleMods>, inventory: string, fuel: number, numberplate: string): void {
		vehicle.setVariable('fuel', fuel);
		vehicle.setVariable('inventory', inventory);
		vehicle.numberPlate = numberplate;
		log.debug(`NRage.VehiclesConfig.loadMods: Loading mods - ${JSON.stringify(mods)}`);
		if (mods) {
			const {
				neonColor,
				colorPearlescent,
				colorPrimary,
				colorSecondary,
				tireSmoke,
				gps,
				remote,
				...basemods
			} = mods;
			if (neonColor) this.setNeonColor(vehicle, neonColor);
			if (colorPearlescent) this.setPearlescentColor(vehicle, colorPearlescent);
			if (colorPrimary && colorSecondary) this.setColor(vehicle, colorPrimary, colorSecondary);
			if (tireSmoke) this.setModCustom(vehicle, 'tireSmoke', tireSmoke);
			if (gps) this.setModCustom(vehicle, 'gps', gps);
			if (remote) this.setModCustom(vehicle, 'remote', remote);
			const allMods: [string, number][] = Object.entries(basemods);
			allMods.forEach((modArray) => {
				// Vehicle mods values: https://wiki.rage.mp/index.php?title=Vehicle_Mods
				const [key, value] = modArray;
				this.setMod(vehicle, VehicleMods[key], value);
			});
			this.saveMods(vehicle);
		}
	}

	saveMods(vehicle: VehicleMp): void {
		const mods: { [k: string]: any } = {};
		mods.neonColor = vehicle.neonEnabled ? vehicle.getNeonColor() : null;
		mods.colorPrimary = vehicle.getColorRGB(0);
		mods.colorSecondary = vehicle.getColorRGB(1);
		mods.colorPearlescent = vehicle.pearlescentColor;
		mods.tireSmoke = this.getModCustom(vehicle, 'tireSmoke');
		mods.gps = this.getModCustom(vehicle, 'gps');
		mods.remote = this.getModCustom(vehicle, 'remote');
		const modList = Object.entries(VehicleMods);
		modList.forEach((thisMod) => {
			const [key, value] = thisMod;
			if (typeof value !== 'number') return;
			const thisModValue = vehicle.getMod(value);
			if (thisModValue !== -1 && thisModValue !== 255) {
				mods[key] = thisModValue;
			}
		});
		vehicle.setVariable('mods', mods);
	}
}

export const vehicleAPI = new Vehicles();