/* eslint camelcase: 0 */

import { log } from '../index';
import axios from 'axios';
import { IsoString } from 'common/types/date';
import { config } from '../core/main';

interface Patron {
	attributes: {
		currently_entitled_amount_cents: number; // Current monthly paid
		email: string;
		full_name: string;
		is_follower: boolean;
		last_charge_date: IsoString;
		last_charge_status: string; // 'Paid' or else
		lifetime_support_cents: number; // Total cents paid (600 for example is 6EUR)
		patron_status: string; // 'active_patron' or else
	},
	id: string, // Unique GUID
	relationships: {
		currently_entitled_tiers: {
			data: [{ id: string, type: 'tier' }]
		}
	},
	type: 'member'
}

interface Members {
	data: Patron[];
	meta: any;
}

interface Campaign {
	attributes: {
		created_at: IsoString;
		url: string;
		vanity: string;
	},
	id: string,
	type: 'campaign'
}

interface Tier {
	attributes: {
		description: string;
		patron_count: number;
		title: string;
	};
	id: string;
	type: 'tier'
}

export class Patreon {
	private accessToken: string;
	campaignID: string;
	tiers: { [k: string]: string };

	constructor(accessToken: string, campaignID: string) {
		this.accessToken = accessToken;
		this.campaignID = campaignID;
		this.tiers = {};

		// Read and save tiers on init
		this.getTiers().then((tiers) => {
			if (!tiers) return log.fatal('Patreon: Unable to fetch campaign data');
			log.info(`Patreon: Fetching '${campaignID}' campaign data`);
			tiers.forEach((tier) => {
				this.tiers[tier.attributes.title.toLowerCase()] = tier.id;
			});
			log.info(`Patreon: Fetched ${tiers.length} campaign tiers`);
		}).catch(error => {
			log.error(error.message);
		});
	}

	// Static
	static async getAllCampaigns(accessToken: string): Promise<Campaign[]> {
		return await axios.get('https://www.patreon.com/api/oauth2/v2/campaigns?fields%5Bcampaign%5D=vanity,url,created_at', { headers: { authorization: `Bearer ${accessToken}` } })
			.then((res: { data: any }) => {
				return res.data.data;
			}).catch((err: { response: { status: any, statusText: any } }) => {
				log.fatal(
					'NRage.patreon.getAllCampaigns: ',
					err.response.status,
					err.response.statusText
				);
				return err.response.statusText;
			});
	}

	// GET function
	private async getData(request: string): Promise<any> {
		return await axios.get(`https://www.patreon.com/api/oauth2/v2/${request}`, { headers: { authorization: `Bearer ${this.accessToken}` } })
			.then((res: { data: any }) => {
				return res.data;
			}).catch((err: { response: { status: any, statusText: any } }) => {
				log.fatal(
					'NRage.patreon.get: ',
					err.response.status,
					err.response.statusText
				);
				return err.response.statusText;
			});
	}

	// Fetch
	async getCampaignMembers(): Promise<Members> {
		return await this.getData(`campaigns/${this.campaignID}/members?include=currently_entitled_tiers&fields%5Bmember%5D=email,full_name,is_follower,last_charge_date,last_charge_status,lifetime_support_cents,currently_entitled_amount_cents,patron_status`);
	}

	async getTiers(): Promise<Tier[]> {
		const { included } = await this.getData(`campaigns/${this.campaignID}?include=tiers&fields%5Btier%5D=title,description,patron_count`);
		return included;
	}

	// Patron scope
	async getPatrons(): Promise<Patron[]> {
		const { data } = await this.getCampaignMembers();
		return data.filter(patron => {
			const { last_charge_status, patron_status } = patron.attributes;
			return (last_charge_status === 'Paid' && patron_status === 'active_patron');
		});
	}

	async getPatron(email: string): Promise<Patron> {
		const patrons = await this.getPatrons();
		let patron: Patron = null;
		patrons.some((thisPatron) => {
			if (thisPatron.attributes.email === email) {
				patron = thisPatron;
				return true;
			}
		});
		return patron;
	}

	async getPatronTiers(email: string): Promise<string[]> {
		const patron = await this.getPatron(email);
		if (!patron) return null;
		const tiers: string[] = [];
		patron.relationships.currently_entitled_tiers.data.forEach((currentTier) => {
			tiers.push(currentTier.id);
		});
		return tiers;
	}

	async patronHasTier(email: string, tier: string): Promise<boolean> {
		const tierID = this.tiers[tier.toLowerCase()];
		if (!tierID) return false;
		const patronActiveTiers = await this.getPatronTiers(email);
		if (!patronActiveTiers) return false;
		return patronActiveTiers.includes(tierID);
	}
}
const serverConfig = config();
const accessToken = serverConfig.patreon_access;
const campaignID = serverConfig.patreon_campaignid;
export const patreonAPI = new Patreon(accessToken, campaignID);
