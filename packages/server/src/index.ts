import { App } from './app';
import { getLogger, Loggers } from './utils/logger';
import { nr } from './core/wrapper';

export const log = getLogger(Loggers.server);
export const timezone = +3;

// Init application level
(async () => {
	nr.delayServerInitialization(true);
	try {
		log.info('Loading application level');
		await App.start();
		log.info('Server started!');
		nr.delayServerInitialization(false);
	} catch (error) {
		log.fatal(error.message);
	}
})();
