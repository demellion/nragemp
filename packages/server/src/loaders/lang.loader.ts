import { localize } from '../../../common';
import en from '../../../common/lang/en';
import ru from '../../../common/lang/ru';
import { nr } from '../core/wrapper';
import { getLogger, Loggers } from '../utils/logger';

const log = getLogger(Loggers.server);

const lng = nr.config.language;

export const languageLoader = async (): Promise<void> => {
	try {
		await localize.init({
			lng,
			resources: {
				en,
				ru
			}
		});
		log.info('Loaded languages successfully!');
	} catch (error) {
		log.error(`Error while loading languages (Error: ${error.message} / ${error.stack})!`);
	}
};
