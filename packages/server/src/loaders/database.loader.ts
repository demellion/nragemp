import mongoose from 'mongoose';
import { getLogger } from '../utils/logger';
import { config } from '../core/main';

const log = getLogger('server');

export const databaseLoader = async (): Promise<void> => {
	const serverConfig = config();
	const cluster = serverConfig.db_url;
	const user = serverConfig.db_user;
	const pwd = serverConfig.db_pwd;
	const name = serverConfig.db_name;
	const uri = `mongodb+srv://${user}:${pwd}@${cluster}/${name}?retryWrites=true&w=majority`;

	try {
		await mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });
		log.info('Database connected successfully.');
	} catch (error) {
		log.fatal(`Could not connect to database: ${error.message}`);
	}
};
