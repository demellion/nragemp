import { getLogger } from '../utils/logger';
import systemController from '../core/controllers/system/system.controller';
import accountController from '../core/controllers/account/account.controller';
import moment from 'moment';
import { getIsoString } from 'common/pipes/date';

const log = getLogger('server');

export const bootstrapLoader = async (): Promise<void> => {
	try {
		// System actions
		const systemInfo = await systemController.load();
		if (!systemInfo) {
			log.warn('No system data document, creating new.');
			await systemController.create();
			return;
		}
		if (!systemInfo.lastPayday || moment(systemInfo.lastPayday).add(1, 'days') < moment()) {
			log.info('Payday date is due, executing payment sequence');
			const referrals = await accountController.findAccounts({ subscribers: { $gte: 1 } });
			const activeReferrals = referrals.filter((thisReferral) => {
				const lastLogin = moment(thisReferral.lastConnected);
				if (lastLogin.add(3, 'days') < moment()) return false; // Not playing
				return true;
			});
			activeReferrals.forEach((thisReferral) => {
				const subscribers = thisReferral.subscribers;
				const totalAmount = subscribers * 25;
				thisReferral.awards.push({
					type: 'money',
					description: `Referral bonus (${subscribers} subscribers)`,
					data: totalAmount,
					timestamp: getIsoString(moment())
				});
				accountController.saveAccount({ username: thisReferral.username, awards: thisReferral.awards });
				systemInfo.lastPayday = getIsoString(moment());
			});
			log.info(`Fetched referrals count: ${referrals.length}, active: ${activeReferrals.length}`);
		}
		await systemController.update({
			lastStartup: getIsoString(moment()),
			startupCount: ++systemInfo.startupCount,
			lastPayday: systemInfo.lastPayday
		});
		log.info('Server successfully bootstrapped.');
	} catch (error) {
		log.error(`Error while bootstrapping server (Error: ${error.message} / ${error.stack})!`);
	}
};