import { getLogger, Loggers } from '../utils/logger';

const log = getLogger(Loggers.server);

export const moduleLoader = async (): Promise<void> => {
	try {
		require('../modules');
		log.info('All modules loaded successfully.');
	} catch (error) {
		log.error(error.message);
	}
};
