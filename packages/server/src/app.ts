import { databaseLoader } from './loaders/database.loader';
import { languageLoader } from './loaders/lang.loader';
import { bootstrapLoader } from './loaders/bootstrap.loader';
import { moduleLoader } from './loaders/module.loader';

export class App {
	public static async start(): Promise<void> {
		await databaseLoader();
		await languageLoader();
		await bootstrapLoader();
		await moduleLoader();
	}
}