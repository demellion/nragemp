export interface StaticPed {
	position: Vector3Mp,
	heading: number,
	ignoreLos?: boolean,
	animationSet?: [string, string, number],
	speechSet?: [string, string, string],
	name?: string,
	type?: string | number,
	data?: any
}

interface StaticPeds {
	impound: StaticPed[];
	ammunation: StaticPed[];
	supermarkets: StaticPed[];
	casino: StaticPed[];
	stripclub: StaticPed[];
	vehicleshops: StaticPed[];
	lore: StaticPed[];
	easteregg: StaticPed[];
}

export const peds: StaticPeds = {
	impound: [
		{
			position: new mp.Vector3(1442.8714599609375, 3615.402587890625, 34.882301330566406),
			heading: 149,
			animationSet: ['amb@world_human_clipboard@male@idle_a', 'idle_a', 1],
			speechSet: ['SHOP_GREET', 'MP_M_SHOPKEEP_01_LATINO_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				action: 'ped_impound',
				locations: [
					[new mp.Vector3(1423.6033935546875, 3625.686279296875, 34.84834671020508), 195],
					[new mp.Vector3(1419.989501953125, 3624.35888671875, 34.84415817260742), 195],
					[new mp.Vector3(1416.169921875, 3623.328125, 34.85270690917969), 195],
					[new mp.Vector3(1411.968017578125, 3621.814208984375, 34.892364501953125), 195]
				]
			}
		},
		{
			position: new mp.Vector3(-186.8882293701172, 6550.470703125, 11.09785270690918),
			heading: 48,
			animationSet: ['amb@world_human_clipboard@male@idle_a', 'idle_a', 1],
			speechSet: ['SHOP_GREET', 'MP_M_SHOPKEEP_01_LATINO_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				action: 'ped_impound',
				locations: [
					[new mp.Vector3(-193.6244659423828, 6525.72412109375, 11.103171348571777), 41],
					[new mp.Vector3(-198.2590789794922, 6522.7646484375, 11.105118751525879), 41],
					[new mp.Vector3(-203.673095703125, 6519.81201171875, 11.096667289733887), 41],
					[new mp.Vector3(-209.75393676757812, 6516.29833984375, 11.097861289978027), 41]
				]
			}
		},
		{
			position: new mp.Vector3(492.06298828125, -58.49645233154297, 78.11508178710938),
			heading: 49,
			animationSet: ['amb@world_human_clipboard@male@idle_a', 'idle_a', 1],
			speechSet: ['SHOP_GREET', 'MP_M_SHOPKEEP_01_LATINO_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				action: 'ped_impound',
				locations: [
					[new mp.Vector3(481.8325500488281, -40.516143798828125, 77.7183837890625), 240],
					[new mp.Vector3(483.5373840332031, -37.361568450927734, 77.7183837890625), 240],
					[new mp.Vector3(485.45831298828125, -34.19995880126953, 77.7183837890625), 240],
					[new mp.Vector3(487.4634704589844, -30.79712677001953, 77.7183837890625), 240],
					[new mp.Vector3(489.1864929199219, -27.731403350830078, 77.7183837890625), 240],
					[new mp.Vector3(491.04876708984375, -24.405885696411133, 77.7183837890625), 240],
					[new mp.Vector3(475.9625549316406, -63.60926055908203, 77.46096801757812), 149],
					[new mp.Vector3(473.1011047363281, -61.890567779541016, 77.46096801757812), 149],
					[new mp.Vector3(469.7679138183594, -60.02623748779297, 77.46096801757812), 149],
					[new mp.Vector3(462.7628479003906, -71.21466827392578, 77.46096801757812), 342],
					[new mp.Vector3(466.1048889160156, -72.2393569946289, 77.46096801757812), 342],
					[new mp.Vector3(469.55712890625, -73.45366668701172, 77.46096801757812), 342]
				]
			}
		},
		{
			position: new mp.Vector3(347.1014709472656, -1686.2183837890625, 32.53007125854492),
			heading: 275,
			animationSet: ['amb@world_human_clipboard@male@idle_a', 'idle_a', 1],
			speechSet: ['SHOP_GREET', 'MP_M_SHOPKEEP_01_LATINO_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				action: 'ped_impound',
				locations: [
					[new mp.Vector3(399.1622314453125, -1663.7232666015625, 32.52899932861328), 138],
					[new mp.Vector3(396.6038513183594, -1661.59521484375, 32.52899932861328), 138],
					[new mp.Vector3(393.9281311035156, -1659.2818603515625, 32.52899932861328), 138],
					[new mp.Vector3(391.31268310546875, -1657.07763671875, 32.52898025512695), 138],
					[new mp.Vector3(388.52880859375, -1654.6856689453125, 32.52898025512695), 138],
					[new mp.Vector3(385.9432373046875, -1652.4791259765625, 32.52898025512695), 138],
					[new mp.Vector3(382.91131591796875, -1650.25244140625, 32.52898025512695), 138],
					[new mp.Vector3(380.09735107421875, -1647.9212646484375, 32.52898025512695), 138],
					[new mp.Vector3(377.51300048828125, -1645.59912109375, 32.52898025512695), 138]
				]
			}
		},
		{
			position: new mp.Vector3(-1161.1834716796875, -738.340087890625, 19.874698638916016),
			heading: 0,
			animationSet: ['amb@world_human_clipboard@male@idle_a', 'idle_a', 1],
			speechSet: ['SHOP_GREET', 'MP_M_SHOPKEEP_01_LATINO_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				action: 'ped_impound',
				locations: [
					[new mp.Vector3(-1184.2203369140625, -745.6018676757812, 19.923002243041992), 310],
					[new mp.Vector3(-1186.2744140625, -742.8120727539062, 20.102664947509766), 310],
					[new mp.Vector3(-1189.3148193359375, -739.0449829101562, 20.35733413696289), 310],
					[new mp.Vector3(-1191.517333984375, -736.2766723632812, 20.54578971862793), 310],
					[new mp.Vector3(-1201.7884521484375, -729.6323852539062, 21.129566192626953), 310],
					[new mp.Vector3(-1204.1214599609375, -727.1519165039062, 21.30270767211914), 310]
				]
			}
		}
	],
	ammunation: [
		{
			position: new mp.Vector3(23.337509155273438, -1105.239208984375, 29.797040939331055),
			heading: 150,
			animationSet: ['amb@world_human_leaning@male@wall@back@hands_together@idle_a', 'idle_b', 1],
			speechSet: ['SHOP_GREET', 'S_M_M_AMMUCOUNTRY_WHITE_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				alias: 'ammunation_1',
				action: 'ped_ammunation'
			}
		},
		{
			position: new mp.Vector3(809.7131958007812, -2159.716552734375, 29.61901092529297),
			heading: 348,
			animationSet: ['amb@world_human_leaning@male@wall@back@hands_together@idle_a', 'idle_b', 1],
			speechSet: ['SHOP_GREET', 'S_M_M_AMMUCOUNTRY_WHITE_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				alias: 'ammunation_2',
				action: 'ped_ammunation'
			}
		},
		{
			position: new mp.Vector3(-1303.6025390625, -395.2842102050781, 36.69576644897461),
			heading: 67,
			animationSet: ['amb@world_human_leaning@male@wall@back@hands_together@idle_a', 'idle_b', 1],
			speechSet: ['SHOP_GREET', 'S_M_M_AMMUCOUNTRY_WHITE_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				alias: 'ammunation_3',
				action: 'ped_ammunation'
			}
		},
		{
			position: new mp.Vector3(254.42605590820312, -50.8991813659668, 69.94100952148438),
			heading: 61,
			animationSet: ['amb@world_human_leaning@male@wall@back@hands_together@idle_a', 'idle_b', 1],
			speechSet: ['SHOP_GREET', 'S_M_M_AMMUCOUNTRY_WHITE_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				alias: 'ammunation_4',
				action: 'ped_ammunation'
			}
		},
		{
			position: new mp.Vector3(-1119.2398681640625, 2700.4541015625, 18.55416488647461),
			heading: 225,
			animationSet: ['amb@world_human_leaning@male@wall@back@hands_together@idle_a', 'idle_b', 1],
			speechSet: ['SHOP_GREET', 'S_M_M_AMMUCOUNTRY_WHITE_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				alias: 'ammunation_5',
				action: 'ped_ammunation'
			}
		},
		{
			position: new mp.Vector3(1692.043701171875, 3761.748046875, 34.705299377441406),
			heading: 219,
			animationSet: ['amb@world_human_leaning@male@wall@back@hands_together@idle_a', 'idle_b', 1],
			speechSet: ['SHOP_GREET', 'S_M_M_AMMUCOUNTRY_WHITE_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				alias: 'ammunation_6',
				action: 'ped_ammunation'
			}
		},
		{
			position: new mp.Vector3(-331.88226318359375, 6085.6943359375, 31.45478630065918),
			heading: 232,
			animationSet: ['amb@world_human_leaning@male@wall@back@hands_together@idle_a', 'idle_b', 1],
			speechSet: ['SHOP_GREET', 'S_M_M_AMMUCOUNTRY_WHITE_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				alias: 'ammunation_7',
				action: 'ped_ammunation'
			}
		},
		{
			position: new mp.Vector3(2567.648681640625, 291.94244384765625, 108.7348403930664),
			heading: 357,
			animationSet: ['amb@world_human_leaning@male@wall@back@hands_together@idle_a', 'idle_b', 1],
			speechSet: ['SHOP_GREET', 'S_M_M_AMMUCOUNTRY_WHITE_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				alias: 'ammunation_8',
				action: 'ped_ammunation'
			}
		}
	],
	supermarkets: [
		{
			position: new mp.Vector3(-47.42100524902344, -1758.6722412109375, 29.421010971069336),
			heading: 58,
			animationSet: ['anim@amb@casino@mini@drinking@bar@drink@heels@idle_a', 'idle_a_bartender', 1],
			speechSet: ['SHOP_GREET', 'MP_M_SHOPKEEP_01_CHINESE_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				alias: 'supermarket_1',
				action: 'ped_supermarket'
			}
		},
		{
			position: new mp.Vector3(-1820.264892578125, 794.445556640625, 138.08877563476562),
			heading: 147,
			animationSet: ['anim@amb@casino@mini@drinking@bar@drink@heels@idle_a', 'idle_a_bartender', 1],
			speechSet: ['SHOP_GREET', 'MP_M_SHOPKEEP_01_CHINESE_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				alias: 'supermarket_2',
				action: 'ped_supermarket'
			}
		},
		{
			position: new mp.Vector3(2556.849365234375, 380.69532470703125, 108.62296295166016),
			heading: 358,
			animationSet: ['anim@amb@casino@mini@drinking@bar@drink@heels@idle_a', 'idle_a_bartender', 1],
			speechSet: ['SHOP_GREET', 'MP_M_SHOPKEEP_01_CHINESE_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				alias: 'supermarket_3',
				action: 'ped_supermarket'
			}
		},
		{
			position: new mp.Vector3(1697.455078125, 4923.333984375, 42.06368637084961),
			heading: 328,
			animationSet: ['anim@amb@casino@mini@drinking@bar@drink@heels@idle_a', 'idle_a_bartender', 1],
			speechSet: ['SHOP_GREET', 'MP_M_SHOPKEEP_01_CHINESE_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				alias: 'supermarket_4',
				action: 'ped_supermarket'
			}
		},
		{
			position: new mp.Vector3(-706.166015625, -914.6118774414062, 19.215595245361328),
			heading: 85,
			animationSet: ['anim@amb@casino@mini@drinking@bar@drink@heels@idle_a', 'idle_a_bartender', 1],
			speechSet: ['SHOP_GREET', 'MP_M_SHOPKEEP_01_CHINESE_MINI_01', 'SPEECH_PARAMS_FORCE'],
			data: {
				alias: 'supermarket_5',
				action: 'ped_supermarket'
			}
		}
	],
	casino: [
		{
			position: new mp.Vector3(1117.4431396484375, 219.92172241210938, -49.83511199951172),
			heading: 84,
			animationSet: ['amb@prop_human_seat_chair@female@arms_folded@idle_a', 'idle_a', 1],
			name: 'PEDS.CASINOCASH',
			type: 's_f_y_casino_01',
			ignoreLos: true,
			data: {
				action: 'ped_casino_cashier'
			}
		},
		{
			position: new mp.Vector3(1111.8826904296875, 209.74371337890625, -49.440120697021484),
			heading: 357,
			animationSet: ['anim@amb@casino@mini@drinking@bar@drink@heels@base', 'intro_bartender', 1],
			speechSet: ['GENERIC_HI', 'A_M_M_GENERICMALE_01_WHITE_MINI_01', 'SPEECH_PARAMS_FORCE'],
			name: 'PEDS.BARMEN',
			type: 's_m_y_casino_01',
			data: {
				action: 'ped_casino_barmen'
			}
		},
		{
			position: new mp.Vector3(1100.53955078125, 195.73667907714844, -49.44007110595703),
			heading: 312,
			animationSet: null,
			speechSet: ['GENERIC_HI', 'S_F_M_SHOP_HIGH_WHITE_MINI_01', 'SPEECH_PARAMS_FORCE'],
			name: 'BLIPS.CLOTHINGSHOP',
			type: 'U_F_M_CasinoShop_01',
			data: {
				action: 'ped_casino_clothing'
			}
		},
		{
			position: new mp.Vector3(1148.218994140625, 266.20184326171875, -51.84082794189453),
			heading: 131,
			animationSet: ['anim@amb@casino@mini@drinking@bar@drink@heels@idle_a', 'idle_a_bartender', 1],
			name: 'PEDS.ROULETTE',
			type: 's_f_y_casino_01',
			data: {
				action: 'ped_casino_roulette',
				actionName: 'ACTIONS.ROULETTE'
			}
		},
		{
			position: new mp.Vector3(1111.013623046875, 242.75436096191406, -46.18555892944336),
			heading: 358,
			animationSet: ['amb@prop_human_seat_chair@female@legs_crossed@idle_a', 'idle_a', 1],
			speechSet: ['GENERIC_HI', 'AGATHA', 'SPEECH_PARAMS_FORCE'],
			name: 'PEDS.AGATHA',
			type: 'ig_agatha',
			data: {
				action: 'ped_casino_boss'
			}
		}
	],
	stripclub: [
		{
			position: new mp.Vector3(129.5689239501953, -1284.0511474609375, 29.273738861083984),
			heading: 120,
			animationSet: ['anim@amb@casino@mini@drinking@bar@drink@heels@base', 'intro_bartender', 1],
			speechSet: ['GENERIC_HI', 'A_M_M_GENERICMALE_01_WHITE_MINI_01', 'SPEECH_PARAMS_FORCE'],
			name: 'PEDS.BARMEN',
			type: 's_m_y_casino_01',
			data: {
				action: 'ped_strip_barmen'
			}
		},
		{
			position: new mp.Vector3(128.76080322265625, -1298.562744140625, 29.232742309570312),
			heading: 217,
			animationSet: ['missfbi_s4mop', 'lobby_security_guard', 1],
			name: 'PEDS.SECURITY',
			type: 's_m_y_casino_01',
			data: {
				action: 'ped_strip_security'
			}
		}
	],
	vehicleshops: [
		{
			position: new mp.Vector3(-31.881004333496094, -1114.6921875, 26.01614532470703),
			heading: 56,
			animationSet: ['amb@prop_human_seat_chair@male@right_foot_out@idle_a', 'idle_a', 1],
			speechSet: ['GENERIC_HI', 'SIMEON', 'SPEECH_PARAMS_FORCE'],
			type: 'ig_siemonyetarian',
			ignoreLos: true,
			data: {
				alias: 'vehshop_elite',
				location: new mp.Vector3(-30.571020126342773, -1091.0465087890625, 26.422216415405273),
				heading: 341,
				previewLocation: new mp.Vector3(-42.10138931274414, -1100.0434326171875, 26.418140411376953),
				previewHeading: 90
			}
		}
	],
	lore: [
		{
			position: new mp.Vector3(1977.0384521484375, 3820.300537109375, 33.45003890991211),
			heading: 97,
			animationSet: ['missprologueig_6', 'lying_dead_brad', 1],
			speechSet: ['BUMP_DRUNK', 'TREVOR_DRUNK', 'SPEECH_PARAMS_FORCE'],
			name: 'PEDS.TREVOR',
			type: 'player_two',
			data: {
				action: 'ped_trevor'
			}
		},
		{
			position: new mp.Vector3(-9.886710166931152, -1439.2078857421875, 31.101547241210938),
			heading: 61,
			animationSet: ['amb@world_human_yoga@female@base', 'base_a', 1],
			speechSet: ['BUMP', 'DENISE', 'SPEECH_PARAMS_FORCE'],
			name: 'PEDS.DENISE',
			type: -2113195075,
			data: {
				action: 'ped_denise'
			}
		},
		{
			position: new mp.Vector3(726.0044555664062, -1067.298828125, 28.311010360717773),
			heading: 213,
			name: 'PEDS.LAMAR',
			type: 1706635382,
			data: {
				action: null
			}
		},
		{
			position: new mp.Vector3(727.1488647460938, -1068.8768310546875, 28.31101417541504),
			heading: 32,
			animationSet: ['missfbi3_party_d', 'stand_talk_loop_a_male1', 1],
			speechSet: ['GENERIC_OUT_OF_MY_WAY', 'FRANKLIN_NORMAL', 'SPEECH_PARAMS_FORCE'],
			name: 'PEDS.FRANKLIN',
			type: 'player_one',
			data: {
				action: 'ped_franklin'
			}
		}
	],
	easteregg: [
		{
			position: new mp.Vector3(1277.4130859375, -1714.1055908203125, 54.61960906982422),
			heading: 34,
			animationSet: ['creatures@cat@amb@world_cat_sleeping_ground@idle_a', 'idle_a', 1],
			name: 'MrPancakers',
			type: 'a_c_cat_01',
			data: {}
		},
		{
			position: new mp.Vector3(1275.6876220703125, -1710.4476318359375, 54.271507263183594),
			heading: 311,
			animationSet: ['amb@prop_human_seat_computer@male@idle_a', 'idle_b', 1],
			name: 'xday',
			type: 'hc_hacker',
			data: {}
		},
		{
			position: new mp.Vector3(1268.9388427734375, -1710.3126220703125, 54.77145767211914),
			heading: 295,
			animationSet: ['swimming@swim', 'dive_idle', 1],
			name: 'rootcause',
			type: 's_m_m_movspace_01',
			data: {}
		},
		{
			position: new mp.Vector3(1272.20458984375, -1712.0155029296875, 54.771461486816406),
			heading: 281,
			animationSet: ['missprologueig_6', 'lying_dead_brad', 1],
			name: 'ragempdev',
			type: 'u_m_y_zombie_01',
			data: {}
		}
	]
};
