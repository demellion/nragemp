export interface Collectible {
	position: Vector3Mp
	range?: number
}

export const collectibles: Collectible[] = [
	{
		position: new mp.Vector3(-1582.3489990234375, -425.8617248535156, 56.70561981201172) // Some random building
	},
	{
		position: new mp.Vector3(-596.2908935546875, 2089.58447265625, 131.41261291503906) // Mineshaft
	},
	{
		position: new mp.Vector3(-555.457763671875, -622.74169921875, 41.27466583251953) // Some opera hall (dont forget to add markers)
	},
	{
		position: new mp.Vector3(-1191.974853515625, -1399.856689453125, 20.638256072998047) // House nearby west beach
	},
	{
		position: new mp.Vector3(-930.212158203125, -1363.9791259765625, -7.415501117706299) // Underwater nearby west beach
	},
	{
		position: new mp.Vector3(-1066.4443359375, -1998.916015625, 41.7903938293457) // North of airport
	},
	{
		position: new mp.Vector3(-1329.11962890625, -2627.955078125, 18.126733779907227) // Airport
	},
	{
		position: new mp.Vector3(-1174.5731201171875, -3456.840087890625, 37.80508804321289) // Airport Bunker
	},
	{
		position: new mp.Vector3(-352.2902526855469, -2367.608642578125, -7.978431701660156) // Tube under bridge
	},
	{
		position: new mp.Vector3(223.8052520751953, -3158.151123046875, 65.90174102783203) // South port building
	},
	{
		position: new mp.Vector3(1086.083984375, -3251.231689453125, 26.527328491210938) // South dock
	},
	{
		position: new mp.Vector3(990.3448486328125, -2147.714111328125, 29.476076126098633) // Meat factory
	},
	{
		position: new mp.Vector3(270.3385925292969, -1112.384033203125, 88.81459045410156) // Templar hotel
	},
	{
		position: new mp.Vector3(-130.35922241210938, -995.1017456054688, 73.26824951171875) // Construction site
	},
	{
		position: new mp.Vector3(1971.5867919921875, 3819.480712890625, 32.890726470947266), // Trevors house
		range: 10
	},
	{
		position: new mp.Vector3(146.33912658691406, -2202.148681640625, 4.688027858734131) // Torture room
	},
	{
		position: new mp.Vector3(-16.584396362304688, -1430.40185546875, 31.101531982421875), // Franklin House
		range: 10
	},
	{
		position: new mp.Vector3(735.0155029296875, -1064.9468994140625, 22.168169021606445), // Lamars customs
		range: 5
	},
	{
		position: new mp.Vector3(273.3602294921875, -633.1943359375, 54.742610931396484) // Just another building
	},
	{
		position: new mp.Vector3(70.93031311035156, -361.33502197265625, 102.552490234375) // Construction site 2
	},
	{
		position: new mp.Vector3(-599.4522094726562, -112.23580169677734, 57.151885986328125) // Some synagogue
	},
	{
		position: new mp.Vector3(-348.2899169921875, 180.685302734375, 95.6864242553711) // Big hotel
	},
	{
		position: new mp.Vector3(1272.634521484375, -1713.410400390625, 54.471568298339844) // Lesters house
	},
	{
		position: new mp.Vector3(478.26025390625, 67.5358657836914, 111.58511352539062) // Daily globe
	},
	{
		position: new mp.Vector3(26.293344497680664, -637.6531372070312, 16.088062286376953) // Tunnels under construction site
	},
	{
		position: new mp.Vector3(2451.813720703125, 4977.28125, 51.574684143066406) // Farm
	}
];