export interface StaticObject {
	position: Vector3Mp,
	rotation: number,
	heading: number,
	dimension?: number,
	type?: string | number
	data?: any
}

export const objects: StaticObject[] = [
	{
		type: 'vw_prop_vw_luckywheel_02a',
		position: new mp.Vector3(1111.0609375, 229.84027099609375, -49.1358528137207),
		heading: 0,
		rotation: 0,
		data: {
			action: 'obj_luckywheel',
			actionName: 'ACTIONS.LUCKYWHEEL'
		}
	},
	{
		type: 'vw_prop_casino_roulette_01',
		position: new mp.Vector3(1147.598388671875, 265.692138671875, -52.84081268310547),
		heading: -51,
		rotation: 0,
		data: null
	},
	{
		type: 'apa_mp_h_din_chair_12',
		position: new mp.Vector3(1275.6876220703125, -1710.4476318359375, 53.771507263183594),
		heading: 131,
		rotation: 0,
		data: null
	}
];