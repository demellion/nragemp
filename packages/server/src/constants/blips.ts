export interface Blip {
	position: Vector3Mp;
	name?: string;
	color?: number;
	type?: number;
}

interface Blips {
	gasStations: Blip[];
	impounds: Blip[];
	supermarkets: Blip[];
	ammunations: Blip[];
	jobs: Blip[];
	generic: Blip[];
}

export enum BlipClass {
	GAS_STATION = 361,
	SHOOTING_RANGE = 432,
	IMPOUND = 50,
	SUPERMARKET = 52,
	AIR_CARRIER = 16,
	PLAYER_HOUSE = 40,
	TAXI = 198,
	LSPD = 60,
	EMS = 61,
	LIFEINVADER = 77,
	COLLECTOR = 67,
	BARBER = 71,
	CAR_TUNING = 72,
	CLOTHING_SHOP = 73,
	TATTOO = 75,
	FIB = 88,
	GHETTO_SKULL = 84,
	JOB_STRIPCLUB = 121,
	MASKS_SHOP_1 = 102,
	MASKS_SHOP_2 = 362,
	MASKS_SHOP_3 = 442,
	BANK = 108,
	AMMUNATION = 110,
	AMMUNATION_PLUS = 567,
	HUNTING = 141,
	CANNABIS = 140,
	GPS = 162,
	WEAZEL_NEWS = 184,
	PRISON = 188,
	VEHICLE_SHOP = 225,
	VEHICLE_SHOP_ELITE = 523,
	BIKE_CLUB = 226,
	PETS_SHOP = 273,
	BOAT_SHOP = 356,
	REAL_ESTATE = 374,
	HOUSE_SMALL = 475,
	HOUSE_MEDIUM = 492,
	HOUSE_BIG = 492,
	CASINO_1 = 617,
	JOB_CASINOSEC = 679,
	CASINO_2 = 681,
	DIVING = 729,
	MECHANICS = 641,
	CITY_HALL = 419,
	ARMY = 421,
	JOB_TRUCKER = 477,
	JOB_BUS = 513,
	JOB_SYSADMIN = 606,
	PARKING = 524,
	COLLECTIBLE = 66,

}

export enum BlipColors {
	NON_IMPORTANT = 39,
	NEUTRAL = 4,
	INFO = 2,
	WARNING = 64,
	DANGER = 1,
	JOBS = 30,
	EPIC = 27,
	ELITE = 48,
	EVENT = 75
}

export enum BlipScales {
	DEFAULT = 1,
	MEDIUM = 0.8,
	HUGE = 1.2
}

export const blips: Blips = {
	gasStations: [
		{
			position: new mp.Vector3(-70.21582794189453, -1761.7808837890625, 29.533710479736328)
		},
		{
			position: new mp.Vector3(175.9188232421875, -1562.2874755859375, 29.26453399658203)
		},
		{
			position: new mp.Vector3(-321.517578125, -1472.626220703125, 30.548723220825195)
		},
		{
			position: new mp.Vector3(265.5618896484375, -1258.5860595703125, 29.142887115478516)
		},
		{
			position: new mp.Vector3(1208.5703125, -1401.7843017578125, 35.22418975830078)
		},
		{
			position: new mp.Vector3(-525.9422607421875, -1211.055908203125, 18.18483543395996)
		},
		{
			position: new mp.Vector3(-722.7938842773438, -934.4287719726562, 19.03465461730957)
		},
		{
			position: new mp.Vector3(818.01611328125, -1028.608642578125, 26.285476684570312)
		},
		{
			position: new mp.Vector3(1181.610107421875, -330.9309997558594, 69.31663513183594)
		},
		{
			position: new mp.Vector3(620.596923828125, 269.3059997558594, 103.08935546875)
		},
		{
			position: new mp.Vector3(-1437.91845703125, -276.1671142578125, 46.207725524902344)
		},
		{
			position: new mp.Vector3(-2100.296630859375, -314.8264465332031, 13.027825355529785)
		},
		{
			position: new mp.Vector3(2004.818359375, 3774.568603515625, 32.4039306640625)
		},
		{
			position: new mp.Vector3(-1800.2176513671875, 802.7996826171875, 138.65115356445312)
		},
		{
			position: new mp.Vector3(180.09024047851562, 6602.65673828125, 31.868757247924805)
		},
		{
			position: new mp.Vector3(1702.603271484375, 6418.35888671875, 32.63991928100586)
		},
		{
			position: new mp.Vector3(2682.058349609375, 3263.4072265625, 55.240501403808594)
		},
		{
			position: new mp.Vector3(48.14129638671875, 2777.19970703125, 57.884002685546875)
		},
		{
			position: new mp.Vector3(2580.992431640625, 361.85272216796875, 108.46884155273438)
		}
	],
	ammunations: [
		{
			position: new mp.Vector3(811.64404296875, -2153.079833984375, 29.6190185546875)
		},
		{
			position: new mp.Vector3(-1308.5989990234375, -392.0059814453125, 36.69576644897461)
		},
		{
			position: new mp.Vector3(249.0228729248047, -47.27177047729492, 69.94100952148438)
		},
		{
			position: new mp.Vector3(-1116.535400390625, 2694.96142578125, 18.55416488647461)
		},
		{
			position: new mp.Vector3(1695.352294921875, 3756.05810546875, 34.705299377441406)
		},
		{
			position: new mp.Vector3(-329.2236328125, 6080.50537109375, 31.45478630065918)
		},
		{
			position: new mp.Vector3(2569.810302734375, 297.9815368652344, 108.7348403930664)
		}
	],
	impounds: [
		{
			position: new mp.Vector3(1442.8714599609375, 3615.402587890625, 34.882301330566406)
		},
		{
			position: new mp.Vector3(-186.8882293701172, 6550.470703125, 11.09785270690918)
		},
		{
			position: new mp.Vector3(492.06298828125, -58.49645233154297, 78.11508178710938)
		},
		{
			position: new mp.Vector3(347.1014709472656, -1686.2183837890625, 32.53007125854492)
		},
		{
			position: new mp.Vector3(-1161.1834716796875, -738.340087890625, 19.874698638916016)
		}
	],
	supermarkets: [
		{
			position: new mp.Vector3(-47.42100524902344, -1758.6722412109375, 29.421010971069336)
		},
		{
			position: new mp.Vector3(-1820.67236328125, 792.2760009765625, 138.1201934814453)
		},
		{
			position: new mp.Vector3(2556.512451171875, 386.0241394042969, 108.62294006347656)
		},
		{
			position: new mp.Vector3(1698.4708251953125, 4925.35888671875, 42.06368637084961)
		},
		{
			position: new mp.Vector3(-707.80322265625, -914.8709716796875, 19.215595245361328)
		}
	],
	jobs: [
		{
			position: new mp.Vector3(1111.121337890625, 244.85745239257812, -45.831031799316406),
			type: BlipClass.JOB_CASINOSEC,
			name: 'BLIPS.JOBS_CASINOSEC'
		},
		{
			position: new mp.Vector3(-1055.1348876953125, -230.98385620117188, 44.021018981933594),
			type: BlipClass.JOB_SYSADMIN,
			name: 'BLIPS.JOBS_SYSADMIN'
		},
		{
			position: new mp.Vector3(130.3564453125, -1294.7066650390625, 29.269533157348633),
			type: BlipClass.JOB_STRIPCLUB,
			name: 'BLIPS.JOBS_STRIPCLUB'
		}

	],
	generic: [
		{
			position: new mp.Vector3(934.4901123046875, 40.2670783996582, 81.09580993652344),
			type: BlipClass.CASINO_2,
			color: BlipColors.ELITE,
			name: 'BLIPS.CASINO2'
		},
		{
			position: new mp.Vector3(-41.494441986083984, -1099.009521484375, 26.422361373901367),
			type: BlipClass.VEHICLE_SHOP_ELITE,
			color: BlipColors.ELITE,
			name: 'BLIPS.VEHICLESHOPELITE'
		},
		{
			position: new mp.Vector3(20.886266708374023, -1108.8017578125, 29.797029495239258),
			type: BlipClass.AMMUNATION_PLUS,
			color: BlipColors.NEUTRAL,
			name: 'BLIPS.SHOOTINGRANGE'
		},
		{
			position: new mp.Vector3(-1080.4287109375, -251.52304077148438, 37.76325988769531),
			type: BlipClass.LIFEINVADER,
			color: BlipColors.NEUTRAL,
			name: 'BLIPS.LIFEINVADER'
		}
	]
};
