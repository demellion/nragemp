import { Character } from 'common/types/character';
import { defaultName } from './account';
import { defaultCustomizationData } from 'common/constants/customizationData';

export const defaultCharacter: Character = {
	name: defaultName,
	age: 18,
	level: 1,
	experience: 0,
	paytime: 0,
	cash: 1000,
	bank: 0,
	faction: -1,
	organization: -1,
	stats: { health: 100, armor: 0, hunger: 100, thirst: 100, mood: 100 },
	inventory: null,
	generic: null,
	position: [-1039.556640625, -2742.0693359375, 13.90941047668457],
	heading: 0,
	friends: null,
	jailed: false,
	lastCarId: null,
	houseId: null,
	warned: null,
	cars: null,
	customizationData: defaultCustomizationData,
	wantedLevel: 0
};
