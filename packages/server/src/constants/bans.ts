/*
	A list of hard-bans
	Anything matching will reject player connection at socket level.
*/

// IP addresses
export const bannedIP = [
	''
];

// Social club names or ids
export const bannedIDs = [
	'',
	0
];