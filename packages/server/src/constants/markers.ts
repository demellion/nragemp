// new mp.Vector3(-269.5958557128906, 6283.4326171875, 31.483304977416992), 231.70150756835938 // Paletto pet shop
// new mp.Vector3(-2360.750244140625, 3249.46923828125, 92.90377807617188), 333.92388916015625 // Army upper floor elevator

export interface Marker {
	position: Vector3Mp;
	data?: any
}

interface Markers {
	gasStations: Marker[];
	transitions: Marker[];
}

export const markers: Markers = {
	gasStations: [
		{
			position: new mp.Vector3(-72.56893920898438, -1761.1883544921875, 28.442480087280273),
			data: {
				alias: 'gasstation_1'
			}
		},
		{
			position: new mp.Vector3(175.9188232421875, -1562.2874755859375, 28.26453399658203),
			data: {
				alias: 'gasstation_2'
			}
		},
		{
			position: new mp.Vector3(-321.517578125, -1472.626220703125, 29.548723220825195),
			data: {
				alias: 'gasstation_3'
			}
		},
		{
			position: new mp.Vector3(265.5618896484375, -1258.5860595703125, 28.142887115478516),
			data: {
				alias: 'gasstation_4'
			}
		},
		{
			position: new mp.Vector3(1208.5703125, -1401.7843017578125, 34.22418975830078),
			data: {
				alias: 'gasstation_5'
			}
		},
		{
			position: new mp.Vector3(-525.9422607421875, -1211.055908203125, 17.18483543395996),
			data: {
				alias: 'gasstation_6'
			}
		},
		{
			position: new mp.Vector3(-722.7938842773438, -934.4287719726562, 18.03465461730957),
			data: {
				alias: 'gasstation_7'
			}
		},
		{
			position: new mp.Vector3(818.01611328125, -1028.608642578125, 25.285476684570312),
			data: {
				alias: 'gasstation_8'
			}
		},
		{
			position: new mp.Vector3(1181.610107421875, -330.9309997558594, 68.31663513183594),
			data: {
				alias: 'gasstation_9'
			}
		},
		{
			position: new mp.Vector3(620.596923828125, 269.3059997558594, 102.08935546875),
			data: {
				alias: 'gasstation_10'
			}
		},
		{
			position: new mp.Vector3(-1437.91845703125, -276.1671142578125, 45.207725524902344),
			data: {
				alias: 'gasstation_11'
			}
		},
		{
			position: new mp.Vector3(-2100.296630859375, -314.8264465332031, 12.027825355529785),
			data: {
				alias: 'gasstation_12'
			}
		},
		{
			position: new mp.Vector3(2004.818359375, 3774.568603515625, 31.4039306640625),
			data: {
				alias: 'gasstation_13'
			}
		},
		{
			position: new mp.Vector3(-1800.2176513671875, 802.7996826171875, 137.65115356445312),
			data: {
				alias: 'gasstation_14'
			}
		},
		{
			position: new mp.Vector3(180.09024047851562, 6602.65673828125, 30.868757247924805),
			data: {
				alias: 'gasstation_15'
			}
		},
		{
			position: new mp.Vector3(1702.603271484375, 6418.35888671875, 31.63991928100586),
			data: {
				alias: 'gasstation_16'
			}
		},
		{
			position: new mp.Vector3(2682.058349609375, 3263.4072265625, 54.240501403808594),
			data: {
				alias: 'gasstation_17'
			}
		},
		{
			position: new mp.Vector3(48.14129638671875, 2777.19970703125, 56.884002685546875),
			data: {
				alias: 'gasstation_18'
			}
		},
		{
			position: new mp.Vector3(2580.992431640625, 361.85272216796875, 107.46884155273438),
			data: {
				alias: 'gasstation_19'
			}
		}
	],
	transitions: [
		// Casino: Main entrance
		{

			position: new mp.Vector3(935.8480834960938, 46.751075744628906, 80.09580993652344),
			data: {
				destination: new mp.Vector3(1090.0228271484375, 206.0777587890625, -48.99972915649414),
				heading: 355
			}
		},
		{

			position: new mp.Vector3(1090.0228271484375, 206.0777587890625, -49.99972915649414),
			data: {
				destination: new mp.Vector3(935.8480834960938, 46.751075744628906, 81.09580993652344),
				heading: 137
			}
		},
		// Casino management
		{
			position: new mp.Vector3(1123.09814453125, 264.52764892578125, -52.04074478149414),
			data: {
				destination: new mp.Vector3(1111.074951171875, 250.15904235839844, -46.84097671508789),
				heading: 182
			}
		},
		{
			position: new mp.Vector3(1111.074951171875, 250.15904235839844, -46.84097671508789),
			data: {
				destination: new mp.Vector3(1123.09814453125, 264.52764892578125, -52.04074478149414),
				heading: 317
			}
		},
		// Eleveator
		{
			position: new mp.Vector3(1085.5233154296875, 214.5728759765625, -50.20038986206055),
			data: {
				destination: new mp.Vector3(964.8275756835938, 58.66674041748047, 111.552978515625),
				heading: 57
			}
		},
		{
			position: new mp.Vector3(964.8275756835938, 58.66674041748047, 111.552978515625),
			data: {
				destination: new mp.Vector3(1085.5233154296875, 214.5728759765625, -50.20038986206055),
				heading: 291
			}
		}

	]
};