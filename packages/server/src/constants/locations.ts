export interface Location {
	position: Vector3Mp;
	heading: number;
}

interface Locations {
	spawn: Location,
	hospitals: Location[];
}

export const locations: Locations = {
	spawn: {
		// Los Santos Airport
		position: new mp.Vector3(-1039.556640625, -2742.0693359375, 13.90941047668457),
		heading: 330
	},
	hospitals: [
		// Los Santos Airport (spawn-point fallback)
		{
			position: new mp.Vector3(-1039.556640625, -2742.0693359375, 13.90941047668457),
			heading: 330
		},
		// Central Los Santos Medical Center
		{
			position: new mp.Vector3(344.76983642578125, -1395.0693359375, 32.51909255981445),
			heading: 33
		},
		// Pillbox Hill Medical Center
		{
			position: new mp.Vector3(357.3667907714844, -592.9141845703125, 26.787586212158203),
			heading: -110
		},
		// Mount Zonah Medical Center
		{
			position: new mp.Vector3(-401.7393798828125, -364.03704833984375, 30.22505569458008),
			heading: 171

		},
		// Sandy Shores Medical Center
		{
			position: new mp.Vector3(1831.28466796875, 3693.63232421875, 32.224273681640625),
			heading: 30

		},
		// St. Fiacre Hospital
		{
			position: new mp.Vector3(1155.135009765625, -1530.077880859375, 34.84345626831055),
			heading: 335

		},
		// Eclipse Medical Tower
		{
			position: new mp.Vector3(-680.5191040039062, 311.7345275878906, 83.08416748046875),
			heading: 176
		},
		// Paleto Bay
		{
			position: new mp.Vector3(-244.68849182128906, 6326.84521484375, 32.426204681396484),
			heading: 329
		}
	]
};