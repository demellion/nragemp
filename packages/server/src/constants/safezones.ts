import { localize } from 'common/index';

enum SafezoneLevels {
	GREEN,
	YELLOW,
	RED
}

export interface Safezone {
	name: string
	position: [number, number, number, number], // x, y, width, height
	elevations: [number, number] // min z, max z
	level: SafezoneLevels
	icon?: string
}

export const safezones: Safezone[] = [
	// Casino
	{
		name: localize.t('BLIPS.CASINO1'),
		position: [1119, 236, 250, 250],
		elevations: [-60, -40],
		level: SafezoneLevels.GREEN,
		icon: 'CHAR_CASINO'
	},
	// Casino (Upper floor)
	{
		name: localize.t('BLIPS.CASINO1'),
		position: [942, 40, 125, 125],
		elevations: [100, 140],
		level: SafezoneLevels.GREEN,
		icon: 'CHAR_CASINO'
	},
	// Elite vehicle shop (Simeon)
	{
		name: localize.t('BLIPS.VEHICLESHOPELITE'),
		position: [-43, -1099, 50, 50],
		elevations: [20, 40],
		level: SafezoneLevels.GREEN,
		icon: 'WEB_PREMIUMDELUXEMOTORSPORT'
	}
];
