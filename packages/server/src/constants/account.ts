import { ServerAccount } from 'common/types/account';
import { AdminLevels } from 'common/enums/adminLevels';

export const defaultName = 'WeirdNewbee';

export const defaultAccount: ServerAccount = {
	username: null,
	password: null,
	adminLevel: AdminLevels.DEFAULT,
	email: null,
	socialClub: null,
	serial: null,
	rgscId: null,
	vip: null,
	subscribers: 0,
	currency: 0,
	blacklisted: false,
	dateOfBirth: null,
	lastIP: null,
	registeredIP: null,
	registeredDate: null,
	lastConnected: null,
	lastDisconnected: null,
	playtime: 0,
	promo: null,
	currencyHistory: null,
	awards: null,
	banInfo: null,
	characters: null
};
